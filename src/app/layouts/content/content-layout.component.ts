import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
    selector: 'app-content-layout',
    templateUrl: './content-layout.component.html',
    styleUrls: ['./content-layout.component.scss']
})

export class ContentLayoutComponent {
    currentRoute: any;
    constructor(
        private route: ActivatedRoute,
        private router: Router
    ) {
        this.currentRoute = this.route.parent.routeConfig.path;
    }
}