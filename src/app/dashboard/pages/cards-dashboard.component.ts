import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-cards-dashboard',
  template: `
  <div class="row">
    <div class="col-sm-4" *ngFor = "let item of dashboardData">
      <app-card-layout [title]="item.card_title" [count]="item.total" [icon] = "item.icon" [url]="item.url"></app-card-layout>
    </div>
  </div>
  ` ,
  styles: [
    `.card .card-block {
      padding: 1.5rem;
    }
   `,
  ],
})
export class CardsDashboardComponent implements OnInit {

  dashboardData: any;

  constructor(
    private _activateRoute: ActivatedRoute,
  ) {

  }

  ngOnInit() {
    const pageContent = this._activateRoute.snapshot.data.pageData.data;
    this.dashboardData = pageContent.dashboardData;
  }

}
