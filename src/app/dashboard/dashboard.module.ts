import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { ChartistModule } from 'ng-chartist';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MatchHeightModule } from '../shared/directives/match-height.directive';
import { CardsDashboardComponent } from './pages/cards-dashboard.component';
import { CardLayoutComponent } from './components/card-layout.component';
import { SharedModule } from 'app/shared/shared.module';

@NgModule({
    imports: [
        CommonModule,
        DashboardRoutingModule,
        ChartistModule,
        NgbModule,
        MatchHeightModule,

    ],
    exports: [CardsDashboardComponent, CardLayoutComponent],
    declarations: [
        CardsDashboardComponent,
        CardLayoutComponent
    ],
    providers: [],
})
export class DashboardModule { }
