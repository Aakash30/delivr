import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CardsDashboardComponent } from './pages/cards-dashboard.component';
import { RolesGuardService } from 'app/core/guards/roles-guard.service';
import { BusinessAdminResolverService } from 'app/core/resolver/business-admin-resolver.service';
import { AdminDashboardResolverService } from 'app/core/resolver/admin-dashboard-resolver.service';


const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        component: CardsDashboardComponent,
        canActivate: [RolesGuardService],
        data: {
          title: 'cards-dashboard',
          // expectedRole: 'dashboard',
          onlyForUser: 'super_admin',
          // resolve: AdminDashboardResolverService
        },
        resolve: {
          pageData: AdminDashboardResolverService
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DashboardRoutingModule { }
