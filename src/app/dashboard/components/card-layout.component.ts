import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { DashboardFilterRedirectService } from 'app/core/services/dashboard-filter-redirect.service';


@Component({
  selector: 'app-card-layout',
  template: `
  <div class="card" (click)="routeCard(url, title)">
  <div class="d-flex flex-wrapper">
    <div>
      <span class="card-count-title">
        {{count == null ? 0 : count}}
      </span>
      <span class="card-title">
        {{title == null ? '-' : title}}
      </span>
    </div>
    <div>
      <img src="assets/project-images/{{icon}}">
    </div>
  </div>
</div>
  `,
  styles: [
    `.flex-wrapper {
      display: flex;
      justify-content: space-between;
    }    
    .card-title {
      color: #5f5f5f;
      padding-top: 23px;
      display: inline-block;
      font-weight: 500;
    }
    .card-count-title {
      font-size: 2rem;
      color: #3C4791;
      display: block;
      word-break: break-all;
    }
    .card {
      padding: 12px 18px;
      height: 127px;
      min-height: 8em;
    }
    `,
  ],
})
export class CardLayoutComponent implements OnInit {

  @Input() title: string;
  @Input() icon: string;
  @Input() count: any;
  @Input() url: string;

  constructor(private _router: Router, private _dashboardRouteRedirection: DashboardFilterRedirectService) { }

  ngOnInit() {
    this._dashboardRouteRedirection.setFilter("");
  }

  routeCard(url, title) {
    if (url == "admin/vehicles-list" && title == "Total Available Bikes") {
      this._dashboardRouteRedirection.setFilter("AVAILABLE");
    } else if (url == "admin/vehicles-list" && title == "Total Bikes On Lease") {
      this._dashboardRouteRedirection.setFilter("ONLEASE");
    } else if (url == "business/order-management" && title == "Total New Orders") {
      this._dashboardRouteRedirection.setFilter("new");
    } else if (url == "business/order-management" && (title == "Total Delivered Orders" || title == "Total Order Revenue")) {
      this._dashboardRouteRedirection.setFilter("delivered");
    }


    this._router.navigateByUrl(url);
  }

}
