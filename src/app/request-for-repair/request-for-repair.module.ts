import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RequestForRepairRoutingModule } from './request-for-repair-routing.module';
import { RequestForRepairListComponent } from './pages/request-for-repair-list/request-for-repair-list.component';
import { SharedModule } from 'app/shared/shared.module';
import { RequestForRepairDetailsComponent } from './pages/request-for-repair-details/request-for-repair-details.component';

@NgModule({
  imports: [
    CommonModule,
    RequestForRepairRoutingModule,
    SharedModule
  ],
  declarations: [RequestForRepairListComponent, RequestForRepairDetailsComponent]
})
export class RequestForRepairModule { }
