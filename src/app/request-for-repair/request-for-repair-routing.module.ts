import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RequestForRepairListComponent } from './pages/request-for-repair-list/request-for-repair-list.component';
import { RequestForRepairDetailsComponent } from './pages/request-for-repair-details/request-for-repair-details.component';
import { RepairRequestAdminResolverService } from 'app/core/resolver/repair-request-admin-resolver.service';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        component: RequestForRepairListComponent,
        data: {
          sectionTitle: 'Request for repair',
        },
        resolve: {
          pageData: RepairRequestAdminResolverService
        }
      },
      {
        path: 'details/:id',
        component: RequestForRepairDetailsComponent,
        data: {
          sectionTitle: 'Details',
          buttonName: 'Add Vehicle'
        },
        resolve: {
          pageData: RepairRequestAdminResolverService
        }
      },

    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RequestForRepairRoutingModule { }
