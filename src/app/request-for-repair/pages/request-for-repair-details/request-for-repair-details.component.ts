import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UtilityService } from 'app/core/services/utility.service';
import { Subject } from 'rxjs';
import { RepairRequestAdminService } from 'app/core/services/repair-request-admin.service';
import { takeLast } from 'rxjs-compat/operator/takeLast';
import { takeUntil } from 'rxjs/operators';

interface Status {
  name: string;
  code: string;
}

@Component({
  selector: 'app-request-for-repair-details',
  templateUrl: './request-for-repair-details.component.html',
  styleUrls: ['./request-for-repair-details.component.scss']
})
export class RequestForRepairDetailsComponent implements OnInit, OnDestroy {

  requestData: any;
  vehicleData: any;
  driverData: any;
  serviceData: any;
  id: number;
  today = new Date();

  private _unsubscribe = new Subject<Boolean>();

  constructor(
    private _activateRoute: ActivatedRoute,
    private _utility: UtilityService,
    private _requestRepairService: RepairRequestAdminService
  ) {
    this.id = this._activateRoute.snapshot.params['id'];
  }

  addDisplay: Boolean = false;
  checked: boolean;

  showDialog() {
    this.addDisplay = true;
  }

  status: Status[];
  selectedStatus: Status[];

  ngOnInit() {
    const pageContent = this._activateRoute.snapshot.data.pageData.data;
    this.requestData = pageContent.requestDetails;
    this.driverData = pageContent.requestDetails.driver;
    this.vehicleData = pageContent.requestDetails.vehicleData;
    this.serviceData = pageContent.requestDetails.requestedService;

    // Status dropdown list
    this.status = [
      { name: 'Pending', code: ' P' },
      { name: 'Outgoing', code: 'OG' },
      { name: 'Finished', code: 'FIN' },
    ];
  }


  actionRequest() {
    this._utility.loaderStart();
    let data = {
      status: this.requestData.status == 'new' ? 'ongoing' : 'completed',
      id: +(this._utility.base64Decode(this.id))
    }

    this._requestRepairService.actionOnRepairRequest(data).pipe(takeUntil(this._unsubscribe)).subscribe(
      success => {
        this._utility.showSuccess('success', 'Status Changed Successfully!');
        this.requestData.status = data.status;
        this._utility.loaderStop();
      },
      error => {
        this._utility.showError('error', 'failed', error.error.message);
        this._utility.loaderStop();
      }
    )

  }

  ngOnDestroy() {
    this._unsubscribe.next(true);
    this._unsubscribe.complete();
  }

}
