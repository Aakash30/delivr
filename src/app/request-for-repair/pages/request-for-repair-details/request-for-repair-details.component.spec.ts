import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestForRepairDetailsComponent } from './request-for-repair-details.component';

describe('RequestForRepairDetailsComponent', () => {
  let component: RequestForRepairDetailsComponent;
  let fixture: ComponentFixture<RequestForRepairDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequestForRepairDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestForRepairDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
