import { Component, OnInit } from '@angular/core';
import { trigger, transition, style, animate } from '@angular/animations';
import { MenuItem } from 'primeng/api';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { UtilityService } from 'app/core/services/utility.service';
import { ActivatedRoute } from '@angular/router';
import { RepairRequestAdminService } from 'app/core/services/repair-request-admin.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ErrorHandlerService } from 'app/core/services/error-handler.service';

interface Service {
  name: string;
  code: string;
}

interface Status {
  name: string;
  code: string;
}

@Component({
  selector: 'app-request-for-repair-list',
  templateUrl: './request-for-repair-list.component.html',
  styleUrls: ['./request-for-repair-list.component.scss'],
  animations: [
    trigger('openClose', [
      transition(':enter', [
        style({ transform: 'translateX(20%)', opacity: 0 }),
        animate('500ms', style({ transform: 'translateX(0)', opacity: 1 })),
      ]),
      transition(':leave', [
        style({ transform: 'translateX(0)', opacity: 1, display: 'none' }),
        animate('500ms', style({ transform: 'translateX(20%)', opacity: 0 })),
      ]),
    ])
  ],
})
export class RequestForRepairListComponent implements OnInit {

  constructor(
    public _utitlity: UtilityService,
    private _route: ActivatedRoute,
    private _repairRequestService: RepairRequestAdminService,
    private _errorHandling: ErrorHandlerService
  ) { }

  private _unsubscribe = new Subject<Boolean>();
  services: Service[];
  selectedType: Service;

  statusList: Status[];
  selectedStatus: Status[];


  requestList: any[];
  total: number = 0;

  isSubmitted: Boolean = false;
  requestRepairForm: FormGroup;
  // show filter function
  isVisible: boolean = false;
  selectedValue: string = 'Service';
  keyword: string = "";
  offset: number;

  dateValue1;
  dateValue2;

  showFilter() {
    this.isVisible = !this.isVisible;
  }

  brandList: string[];
  modelList: string[];
  modelListFromAPI: string[];
  statusListFromAPI: string[];

  isRequestSubmitted: boolean = false;

  ngOnInit() {
    const pageContent = this._route.snapshot.data.pageData.data;
    this.requestList = pageContent.requestForRepair;
    this.total = pageContent.total;

    this.requestRepairForm = new FormGroup({
      brand: new FormControl(null),
      model: new FormControl(null),
      status: new FormControl(null),
      startDate: new FormControl(null),
      endDate: new FormControl(null),
    });


    let brandsFromApi = pageContent.vehicleBrandList;
    this.brandList = this._utitlity.arrayOfObjectToConvertInDropdownFormat(brandsFromApi, "brand_name", "brand_id");

    this.modelListFromAPI = pageContent.vehicleModelList;
    this.modelList = [];
    this.statusListFromAPI = ['New', 'Ongoing', 'Finished'];
    this.statusList = this._utitlity.arrayOfStringsToArrayOfObjects(this.statusListFromAPI);
  }

  listing() {
    let data = {};

    if (this.keyword.trim() != "") {
      data["keyword"] = this.keyword.trim();
    }

    if (this.offset != 0) {
      data["offset"] = this.offset;
    }

    if (this.requestRepairForm.value.brand != null) {
      data["brand"] = this.requestRepairForm.value.brand;
    }

    if (this.requestRepairForm.value.model != null) {
      data["model"] = this.requestRepairForm.value.model;
    }

    if (this.requestRepairForm.value.status != null) {
      data["status"] = this.requestRepairForm.value.status.toLowerCase();
    }

    if (this.requestRepairForm.value.startDate != null) {
      data['startDate'] = this.requestRepairForm.value.startDate.toLocaleDateString('en-us').toString();
    }

    if (this.requestRepairForm.value.endDate != null) {
      data['endDate'] = this.requestRepairForm.value.endDate.toLocaleDateString('en-us').toString();
    }

    this._repairRequestService.fetchRepairRequest(data).pipe(takeUntil(this._unsubscribe)).subscribe(
      (success: any) => {
        this.requestList = success.data.requestForRepair;
        this.total = success.data.total;
      }, error => {
        this._errorHandling.routeAccordingToError(error);
      });

  }

  /**
   * submits filter form
   */
  onSubmit() {
    this.isSubmitted = true;
    if (this.requestRepairForm.dirty && this.requestRepairForm.touched) {
      this.isSubmitted = true;
      this.offset = 0;
      this.listing();
    }
    else {
      this.isSubmitted = false;
    }
    this.isVisible = false;
  }

  /**
   * clear filter form and reset filter
   */
  clearFilter() {
    this.requestRepairForm.reset();
    this.isSubmitted = false;
    this.isVisible = false;
    this.listing();

  }

  closeFilter() {
    this.isVisible = false;
  }

  onchangeBrand(selectedBrand) {
    this.requestRepairForm.controls.model.setValue(null);
    if (selectedBrand != null) {
      let modelArray = [];
      this.modelListFromAPI.filter((element: any) => {
        if (element.brand_id == selectedBrand) modelArray.push(element);
      });
      this.modelList = this._utitlity.arrayOfObjectToConvertInDropdownFormat(modelArray, "model_name", "model_id");
    } else {
      this.modelList = [];
    }
  }


  /** key searching */
  searchText(event) {
    if (event.charCode == 13) {
      this.keyword = event.target.value;
      this.offset = 0;
      this.listing();
    }

  }

  /** clears category input */
  clearInput(event) {
    this.keyword = event;
    this.listing();
  }

  /**
   * paginate
   */
  paginate(event) {
    if (this.offset != undefined) {
      this.offset = parseInt(event.first);
      this.listing();
    } else {
      this.offset = parseInt(event.first);
    }
  }


  ngOnDestroy(): void {
    this._unsubscribe.next(true);
    this._unsubscribe.complete();
  }

}
