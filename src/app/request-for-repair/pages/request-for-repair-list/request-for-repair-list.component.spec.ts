import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestForRepairListComponent } from './request-for-repair-list.component';

describe('RequestForRepairListComponent', () => {
  let component: RequestForRepairListComponent;
  let fixture: ComponentFixture<RequestForRepairListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequestForRepairListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestForRepairListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
