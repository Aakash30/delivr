import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {
  FormGroup,
  FormControl,
  Validators,
  AbstractControl
} from "@angular/forms";
import { passwordMatchValidator, validateAllFormFields, blankSpaceInputNotValid, noWhitespaceValidator } from "app/shared/utils/custom-validators";
import { AuthService } from 'app/core/services/auth.service';
import { MessageService } from 'primeng/api';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { UtilityService } from 'app/core/services/utility.service';

@Component({
  selector: 'app-activate-account',
  templateUrl: './activate-account.component.html',
  styleUrls: ['./activate-account.component.scss']
})
export class ActivateAccountComponent implements OnInit {

  pageContent: any;
  isSubmitted: boolean = false;
  activateForm: FormGroup;
  token: string = "";
  private _unsubscribe = new Subject<Boolean>();

  constructor(
    private _activateRoute: ActivatedRoute,
    private _authService: AuthService,
    private messageService: MessageService,
    private _utility: UtilityService,
    private _router: Router
  ) {
    this.token = _activateRoute.snapshot.params.verify
  }

  ngOnInit() {
    this.pageContent = this._activateRoute.snapshot.data.pageData;

    this.activateForm = new FormGroup({
      confirmPassword: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required])
    })

  }

  onSubmit() {
    this.isSubmitted = true;
    if (this.activateForm.valid) {
      if (passwordMatchValidator(this.activateForm)) {
        let data = {
          password: this.activateForm.value.password
        }

        this._utility.loaderStart();
        this._authService.activateUserCheck(data, this.token).pipe(takeUntil(this._unsubscribe)).subscribe((success: any) => {
          this.messageService.add({
            severity: "success",
            summary: "Activation Success",
            detail: success.message
          });
          this._router.navigate(['/business/login']);
          this._utility.loaderStop();

        }, error => {
          this.messageService.add({
            severity: "error",
            summary: "Activation Failed",
            detail: error.message
          });
          this._utility.loaderStop();
        });
      } else {
        this.messageService.add({
          severity: "error",
          summary: "Activation Failed",
          detail: "Passwords do not match"
        });
        this._utility.loaderStop();
        validateAllFormFields(this.activateForm);
      }
    } else {
      validateAllFormFields(this.activateForm);
    }
  }

  activateModule() {
    return this.activateForm.controls;
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this._unsubscribe.next(true);
    this._unsubscribe.complete();
  }
}
