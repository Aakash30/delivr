import { Component, OnInit, OnDestroy } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { CustomValidators } from "ng5-validation";
import { Router, ActivatedRoute } from "@angular/router";
import { AuthService } from "app/core/services/auth.service";
import { NgxUiLoaderService } from "ngx-ui-loader";
import { MessageService } from "primeng/api";
import { HttpResponse } from "@angular/common/http";
import { SharedDataService } from "app/core/services/shared.data.service";
import { validateAllFormFields } from "app/shared/utils/custom-validators";
import { Subject } from "rxjs/internal/Subject";
import { takeUntil } from "rxjs/operators";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"]
})
export class LoginComponent implements OnInit, OnDestroy {
  public error;
  userRole: string;
  userType: any;
  isSubmitted: Boolean = false;

  private _unsubscribe = new Subject<Boolean>();

  loginData: any;

  loginForm = new FormGroup({
    email: new FormControl("", [Validators.required, Validators.email]),
    password: new FormControl("", [Validators.required])
  });

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private authService: AuthService,
    private loader: NgxUiLoaderService,
    private messageService: MessageService,
    private sharedDataService: SharedDataService
  ) { }

  ngOnInit() {

    this.userRole = this.route.parent.snapshot.url[0].path;

    if (this.userRole === "admin") {
      this.userType = "super_admin";
    }
    if (this.userRole === "business") {
      this.userType = "vendor";
    }

  }
  check_login() {
    this.error = "";
  }

  onSubmit() {

    this.isSubmitted = true;
    if (this.loginForm.valid) {
      let loginData = this.loginForm.value;
      loginData["userType"] = this.userType;
      // call api
      this.loader.start();
      this.authService.login(loginData)
        .pipe(takeUntil(this._unsubscribe))
        .subscribe(
          (success: any) => {
            localStorage.setItem("token", success.headers.get("Authorization"));
            this.messageService.add({
              severity: "success",
              summary: "Login Success",
              detail: "Welcome to the Delivr"
            });
            this.loader.stop();
            this.router.navigateByUrl(`/${this.userRole}`);
          },
          error => {
            this.messageService.add({
              severity: "error",
              summary: "Login Failed",
              detail: `${error.error.message}`
            });
            this.loader.stop();
            this.router.navigateByUrl(`/${this.userRole}/login`);
          }
        );
      this.isSubmitted = false;
    }
    else {
      validateAllFormFields(this.loginForm);
    }
  }

  ngOnDestroy() {
    this._unsubscribe.next(true);
    this._unsubscribe.complete();
  }
}
