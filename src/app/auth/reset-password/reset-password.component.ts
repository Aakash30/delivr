  import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { AuthService } from "app/core/services/auth.service";
import { MessageService } from "primeng/api";
import { NgxUiLoaderService } from "ngx-ui-loader";
import {
  FormGroup,
  FormControl,
  Validators,
  AbstractControl
} from "@angular/forms";
import { passwordMatchValidator, validateAllFormFields, blankSpaceInputNotValid, noWhitespaceValidator } from "app/shared/utils/custom-validators";
import { Subject } from "rxjs/internal/Subject";

@Component({
  selector: "app-reset-password",
  templateUrl: "./reset-password.component.html",
  styleUrls: ["./reset-password.component.scss"]
})
export class ResetPasswordComponent implements OnInit {
  form: FormGroup;
  resetStatus: boolean;
  verify: string;
  userRole: string;
  userType: any;

  private _unsubscribe = new Subject<Boolean>();

  constructor(
    private auth: AuthService,
    private route: ActivatedRoute,
    private router: Router,
    private messageService: MessageService,
    private loader: NgxUiLoaderService
  ) { }

  ngOnInit() {
    this.form = new FormGroup({
      password: new FormControl("", [Validators.required, noWhitespaceValidator,
        blankSpaceInputNotValid]),
      confirmPassword: new FormControl("", [Validators.required])
    });

    this.resetStatus = false;

    this.verify = this.route.snapshot.paramMap.get("token");

    this.userRole = this.route.parent.snapshot.url[0].path;

    if (this.userRole === "admin") {
      this.userType = "super_admin";
    }
    if (this.userRole === "business") {
      this.userType = "vendor";
    }

  }

  onSubmit() {

    if (passwordMatchValidator(this.form)) {
      this.loader.start();
      this.auth
        .reset({ verify: this.verify, password: this.form.value.password, userType: this.userType })
        .subscribe(
          response => {
            this.resetStatus = true;
            this.messageService.add({
              severity: "success",
              summary: "Reset Success",
              detail: "Your password has been reset"
            });
            this.loader.stop();
          },
          error => {
            this.messageService.add({
              severity: "error",
              summary: "Reset Failed",
              detail: `${error.error.message}`
            });
            this.loader.stop();
          }
        );
    } else {
      this.messageService.add({
        severity: "error",
        summary: "Reset Failed",
        detail: "Passwords do not match"
      });
      validateAllFormFields(this.form);
    }
  }

  resetModule() {
    return this.form.controls;
  }

  ngOnDestroy() {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this._unsubscribe.next(true);
    this._unsubscribe.complete();
  }
}
