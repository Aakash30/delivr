import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CustomValidators } from 'ng5-validation';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from 'app/core/services/auth.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { MessageService } from 'primeng/api';
import { validateAllFormFields, noWhitespaceValidator } from 'app/shared/utils/custom-validators';
import { UtilityService } from 'app/core/services/utility.service';
import { Subject } from 'rxjs/internal/Subject';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-recover-password',
  templateUrl: './recover-password.component.html',
  styleUrls: ['./recover-password.component.scss']
})
export class RecoverPasswordComponent implements OnInit, OnDestroy {
  resetDone: boolean;
  userRole: string;
  userType: any;
  emailId: string;

  private _unsubscribe = new Subject<Boolean>();

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private authService: AuthService,
    private loader: NgxUiLoaderService,
    private messageService: MessageService,
    private _utilityService: UtilityService
  ) { }
  isSubmitted: Boolean = false;
  forgotform = new FormGroup({
    email: new FormControl('', [
      Validators.required,
      noWhitespaceValidator,
      Validators.email
    ])
  });
  ngOnInit() {
    this.resetDone = false;
    this.userRole = this.route.parent.snapshot.url[0].path;

    if (this.userRole === "admin") {
      this.userType = "super_admin";
    }
    if (this.userRole === "business") {
      this.userType = "vendor";
    }
  }

  onSubmit() {

    this.forgotform.value.userType = this.userType;
    this.isSubmitted = true;
    this.emailId = this.forgotform.controls.email.value;
    if (this.forgotform.valid) {
      // call api
      this.loader.start();
      this.authService.forgot(this.forgotform.value)
        .pipe(takeUntil(this._unsubscribe))
        .subscribe(
          (success: any) => {

            this.resetDone = true;
            this.loader.stop();
            this.forgotform.reset();

          },
          error => {
            this._utilityService.showError('error', 'failed', error.error.message);
            this.loader.stop();
          }
        )
    } else {
      validateAllFormFields(this.forgotform);
    }

  }

  ngOnDestroy() {
    this._unsubscribe.next(true);
    this._unsubscribe.complete();
  }
}
