import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReportRoutingModule } from './report-routing.module';
import { SharedModule } from 'app/shared/shared.module';
import { ReportListComponent } from './pages/report-list/report-list.component';

@NgModule({
  imports: [
    CommonModule,
    ReportRoutingModule,
    SharedModule
  ],
  declarations: [ReportListComponent]
})
export class ReportModule { }
