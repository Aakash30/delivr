import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { BaseService } from './base.service';
import { ErrorHandlerService } from './error-handler.service';
import { catchError, timeout } from 'rxjs/operators';
import { UtilityService } from './utility.service';
import { NotificationReadEventService } from './notification-read-event.service';

@Injectable({
  providedIn: 'root'
})
export class RepairRequestBusinessService {
  base_url: string;
  public _notifyQuery = "";

  constructor(
    private http: HttpClient,
    private bs: BaseService,
    private errorHandler: ErrorHandlerService,
    private utility: UtilityService,
    private _notificationEvent: NotificationReadEventService
  ) {
    this.base_url = this.bs.base_url;
  }

  getPage(callfunction, params) {
    if (callfunction == "") {
      return this.fetchRepairRequest();
    } else if (callfunction == "details/:id") {
      return this.repairRequestDetails(params.id);
    }
  }

  /**
   * fetches repair request list in business
   * @param fetchingData defines filter data
   */
  fetchRepairRequest(fetchingData?) {

    let query = "";

    if (fetchingData) {
      var esc = encodeURIComponent;
      query = Object.keys(fetchingData)
        .map(k => esc(k) + '=' + esc(fetchingData[k]))
        .join('&');
    }
    query = query != "" ? "/?" + query : "";

    return this.http.get(this.base_url + "fetchMyDriverRepairRequest" + query).pipe(
      timeout(10000),
      catchError(this.errorHandler.handleError)
    );
  }

  /**
   * fetches details for individual request
   * @param id defines unique request id
   */

  repairRequestDetails(id) {
    let notify = this._notificationEvent.currentNotice.subscribe((res) => {
      if (res) {
        this._notifyQuery = "?readNotice=" + this.utility.base64Encode(res);
        this._notificationEvent.setNotificationStatus(0);
      } else {
      }
    });
    return this.http.get(this.base_url + "fetchMyRepairRequestData/" + id + this._notifyQuery).pipe(
      timeout(10000),
      catchError(this.errorHandler.handleError)
    )
  }
}
