import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseService } from './base.service';
import { ErrorHandlerService } from './error-handler.service';
import { UtilityService } from './utility.service';
import { Router } from '@angular/router';
import { retry, catchError } from 'rxjs/operators';
import { NotificationReadEventService } from './notification-read-event.service';


@Injectable({
  providedIn: 'root'
})
export class NewVehicleService {

  base_url: string;
  userType: string;
  public _notifyQuery = "";


  constructor(
    private http: HttpClient,
    private bs: BaseService,
    private errorHandler: ErrorHandlerService,
    private utility: UtilityService,
    private _router: Router,
    private _notificationEvent: NotificationReadEventService
  ) {
    this.base_url = this.bs.base_url;
  }

  getPage(callfunction, params) {

    if (callfunction == "") {
      return this.getFetchVehicleRequests();

    }
  }

  // fetching new vehicles for admin
  getFetchVehicleRequests(fetchingData?) {
    let query = "";

    let notify = this._notificationEvent.currentNotice.subscribe((res) => {
      if (res) {
        this._notifyQuery = "?readNotice=" + this.utility.base64Encode(res);
        this._notificationEvent.setNotificationStatus(0);
      } else {
      }
    });

    if (fetchingData) {
      var esc = encodeURIComponent;
      query = Object.keys(fetchingData)
        .map(k => esc(k) + '=' + esc(fetchingData[k]))
        .join('&');
    }
    query = (this._notifyQuery != "" ? this._notifyQuery : (query != "" ? "/?" + query : ""));

    notify.unsubscribe();

    return this.http.get(this.base_url + "fetchVehicleRequests" + query).pipe(
      retry(3),
      catchError(this.errorHandler.handleError)
    );
  }

  // perform action on request api on changing the dropdown value
  performActionOnRequest(id, data) {

    return this.http.post(this.base_url + 'performActionOnRequest/' + id, data).pipe(
      retry(3),
      catchError(this.errorHandler.handleError)
    )
  }
}
