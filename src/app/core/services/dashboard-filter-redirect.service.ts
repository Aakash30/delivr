import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class DashboardFilterRedirectService {

  constructor() { }
  private subject = new BehaviorSubject("");

  filterFor = this.subject.asObservable();

  setFilter(val) {
    this.subject.next(val);
  }
}

