import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { retry, catchError, timeout } from "rxjs/operators";
import { ErrorHandlerService } from "./error-handler.service";
import { BaseService } from "./base.service";
import { Observable } from 'rxjs/Observable';
import { JwtHelperService } from "@auth0/angular-jwt";
import { throwError } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class AuthService {
  base_url: string;

  constructor(
    private http: HttpClient,
    private bs: BaseService,
    private errorHandler: ErrorHandlerService,
    private jwtHelperService: JwtHelperService
  ) {
    this.base_url = this.bs.base_url;
  }

  isAuthenticated() {
    const token = localStorage.getItem('token');

    if (!token) {

      // this.logout();
      return false;

    } else {
      // check token validity - is token expired
      if (this.jwtHelperService.isTokenExpired(token)) {
        return false;
      } else {
        return true;
      }
    }
  }

  login(data) {
    return this.http.post(this.base_url + "login", data, { observe: "response" })
      .pipe(
        retry(3),
        catchError(this.errorHandler.handleError)
      );
  }

  forgot(data) {
    return this.http.post(this.base_url + "requestForOTP", data).pipe(
      retry(3),
      catchError(this.errorHandler.handleError)
    );
  }

  verify(data) {
    return this.http.post(this.base_url + "verifyUserLink", data).pipe(
      timeout(10000),
      catchError(this.errorHandler.handleError)
    );
  }

  reset(data) {
    return this.http.post(this.base_url + "verify", data).pipe(
      retry(3),
      catchError(this.errorHandler.handleError)
    );
  }

  logout() {
    return this.http.get(this.base_url + "logout").pipe(
      retry(3),
      catchError(this.errorHandler.handleError)
    );
  }
  updateServerDeviceToken(data) {
    return this.http.post(this.bs.base_url + "updateDeviceToken", data).pipe(
      timeout(10000),
      catchError(this.errorHandler.handleError)
    );
  }

  activateUserCheck(data, param) {
    return this.http.post(this.bs.base_url + "activateUser/" + param, data).pipe(
      timeout(10000),
      catchError((error) => {
        this.errorHandler.routeAccordingToError(error);
        return throwError(error)
      })
    );
  }
}

