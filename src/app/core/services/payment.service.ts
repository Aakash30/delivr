import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { retry, catchError, timeout } from "rxjs/operators";
import { ErrorHandlerService } from "./error-handler.service";
import { BaseService } from "./base.service";
import { Observable } from 'rxjs/Observable';
import { UtilityService } from "./utility.service";
import { throwError } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class PaymentService {
  base_url: string;

  constructor(
    private http: HttpClient,
    private bs: BaseService,
    private errorHandler: ErrorHandlerService,
    private utility: UtilityService
  ) {
    this.base_url = this.bs.base_url;
  }

  getPage() {
    return this.fetchPayments();
  }

  fetchPayments(id?) {
    let query = id != undefined ? "/" + id : "";
    return this.http.get(this.base_url + "payment" + query).pipe(
      timeout(10000),
      catchError((error) => {
        this.errorHandler.routeAccordingToError(error);
        return throwError(error);
      })
    );
  }

  insertPayment(data) {
    return this.http.post(this.base_url + "payment", data).pipe(
      timeout(10000),
      catchError((error) => {
        this.errorHandler.routeAccordingToError(error);
        return throwError(error);
      })
    );
  }

  updatePayment(data) {
    return this.http.put(this.base_url + "payment", data).pipe(
      timeout(10000),
      catchError((error) => {
        this.errorHandler.routeAccordingToError(error);
        return throwError(error);
      })
    );
  }
}

