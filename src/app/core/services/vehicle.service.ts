import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { catchError, timeout } from "rxjs/operators";
import { ErrorHandlerService } from "./error-handler.service";
import { BaseService } from "./base.service";
import { AuthService } from 'app/core/services/auth.service';
import { Observable } from 'rxjs/Observable';
import { UtilityService } from "./utility.service";
import { Router } from "@angular/router";
import { DashboardFilterRedirectService } from "./dashboard-filter-redirect.service";

@Injectable({
  providedIn: "root"
})
export class VehicleService {
  base_url: string;
  userType: string;
  public filterQuery = "";

  constructor(
    private http: HttpClient,
    private bs: BaseService,
    private errorHandler: ErrorHandlerService,
    private utility: UtilityService,
    private _router: Router,
    private _vehicleFetch: DashboardFilterRedirectService
  ) {
    this.base_url = this.bs.base_url;
    // console.log('user type::', this.userType);
  }

  getPage(callfunction, params) {

    if (callfunction == "vehicles-list") {
      return this.getVehicles();

    } else if (callfunction == "vehicles-list/add") {
      return this.fetchprefills();

    } else if (callfunction == "vehicles-list/edit/:id") {
      let id = this.utility.base64Decode(params.id);
      return this.fetchprefills(id);

    } else if (callfunction == "vehicles-list/details/:id") {
      let id = this.utility.base64Decode(params.id);
      return this.getVehicleDetails(id);

    } else if (callfunction == "vehicles-inventory") {
      return this.getInventory();
    }
  }


  /**
   * fetch vehicle list
   * @param fetchingData send data to be filtered on vehicle list
   */
  getVehicles(fetchingData?) {


    let filter = this._vehicleFetch.filterFor.subscribe((res) => {
      if (res) {
        this.filterQuery = "fetchFor=" + res
      } else {
      }
    });

    filter.unsubscribe();
    let query = "";

    if (fetchingData) {
      var esc = encodeURIComponent;
      query = Object.keys(fetchingData)
        .map(k => esc(k) + '=' + esc(fetchingData[k]))
        .join('&');
    } else {
      query = this.filterQuery
    }
    query = query != "" ? "/?" + query : "";

    return this.http.get(this.base_url + "vehicles" + query).pipe(
      timeout(10000),
      catchError(this.errorHandler.handleError)
    );
  }

  // fetching new vehicles for admin
  getFetchVehicleRequests(fetchingData?) {
    let query = "";
    if (fetchingData) {
      var esc = encodeURIComponent;
      query = Object.keys(fetchingData)
        .map(k => esc(k) + '=' + esc(fetchingData[k]))
        .join('&');
    }
    query = query != "" ? "/?" + query : "";

    return this.http.get(this.base_url + "fetchVehicleRequests" + query).pipe(
      timeout(10000),
      catchError(this.errorHandler.handleError)
    );
  }

  // perform action on request api on changing the dropdown value
  performActionOnRequest(id, data) {

    return this.http.post(this.base_url + 'performActionOnRequest/' + id, data).pipe(
      timeout(10000),
      catchError(this.errorHandler.handleError)
    )
  }

  /**
  * fetch vehicle list
  * @param fetchingData send data to be filtered on vehicle list
  */
  getInventory(fetchingData?) {


    let query = "";
    if (fetchingData) {
      var esc = encodeURIComponent;
      query = Object.keys(fetchingData)
        .map(k => esc(k) + '=' + esc(fetchingData[k]))
        .join('&');
    }
    query = query != "" ? "/?" + query : "";

    return this.http.get(this.base_url + "inventoryVehicles" + query).pipe(
      timeout(10000),
      catchError(this.errorHandler.handleError)
    );
  }

  filterVehicles(fetchingData?) {
    let query = "";
    if (fetchingData) {
      var esc = encodeURIComponent;
      query = Object.keys(fetchingData)
        .map(k => esc(k) + '=' + esc(fetchingData[k]))
        .join('&');
    }
    query = query != "" ? "/?" + query : "";

    return this.http.get(this.base_url + "vehicles" + query).pipe(
      timeout(10000),
      catchError(this.errorHandler.handleError)
    );
  }




  getVehicleDetails(id) {
    return this.http.get(this.base_url + "fetchSingleVehicleDetails/" + id).pipe(
      timeout(10000),
      catchError(this.errorHandler.handleError)
    );
  }
  addVehicle(data) {
    return this.http.post(this.base_url + "addUpdateVehicle", data).pipe(
      timeout(10000),
      catchError(this.errorHandler.handleError)
    );
  }
  fetchprefills(id?) {

    let query = id ? "/" + id : "";
    return this.http.get(this.base_url + "fetchDataForAddEditVehicle" + query).pipe(
      timeout(10000),
      catchError(this.errorHandler.handleError)
    );
  }
  changeStatus(id, status) {

    let data = {
      status: (status) ? 'available' : 'unavailable'
    }
    return this.http.post(this.base_url + "markAvailablityForVehicle/" + id, data).pipe(
      catchError(this.errorHandler.handleError)
    );
  }
}

