import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseService } from './base.service';
import { ErrorHandlerService } from './error-handler.service';
import { timeout, catchError } from 'rxjs/operators';
import { UtilityService } from './utility.service';
import { NotificationReadEventService } from './notification-read-event.service';

@Injectable({
  providedIn: 'root'
})
export class VendorVehicleService {
  base_url: string;
  public _notifyQuery = "";

  constructor(private http: HttpClient,
    private bs: BaseService,
    private errorHandler: ErrorHandlerService,
    private utility: UtilityService,
    private _notificationEvent: NotificationReadEventService) {
    this.base_url = this.bs.base_url;
  }

  getPage(callfunction, params) {

    if (callfunction == "vehicle-list") {
      return this.fetchVehicleList();
    } else if (callfunction == "vehicle-list/details/:id") {
      let vehicle_id = this.utility.base64Decode(params.id);
      return this.fetchVehicleData(vehicle_id);
    } else if (callfunction == "vehicle-lease-plan") {

      return this.fetchLeaseBatch();
    } else if (callfunction == "vehicle-lease-plan/details/:id") {
      let batchId = this.utility.base64Decode(params.id);
      return this.fetchLeaseBatchDetails(batchId)
    }

  }

  /** List vehicles */
  fetchVehicleList(fetchingData?) {

    let query = "";
    if (fetchingData) {
      var esc = encodeURIComponent;
      query = Object.keys(fetchingData)
        .map(k => esc(k) + '=' + esc(fetchingData[k]))
        .join('&');
    }
    query = query != "" ? "/?" + query : "";
    return this.http.get(this.base_url + "businessVehicleLists" + query).pipe(
      timeout(10000),
      catchError(this.errorHandler.handleError)
    )
  }

  /** get single vehicle data */

  fetchVehicleData(id) {
    return this.http.get(this.base_url + "businessVehicleDetails/" + id).pipe(
      timeout(10000),
      catchError(this.errorHandler.handleError)
    )
  }

  /**
   * fetches lease data for business
   * @param fetchingData filters to be implement 
   */
  fetchLeaseBatch(fetchingData?) {
    let query = "";
    if (fetchingData) {
      var esc = encodeURIComponent;
      query = Object.keys(fetchingData)
        .map(k => esc(k) + '=' + esc(fetchingData[k]))
        .join('&');
    }
    query = query != "" ? "/?" + query : "";

    return this.http.get(this.base_url + "fetchMyLeasePlan" + query).pipe(
      timeout(10000),
      catchError(this.errorHandler.handleError)
    )
  }

  fetchLeaseBatchDetails(id, fetchingData?) {

    let notify = this._notificationEvent.currentNotice.subscribe((res) => {
      if (res) {
        this._notifyQuery = "?readNotice=" + this.utility.base64Encode(res);
        this._notificationEvent.setNotificationStatus(0);
      } else {
      }
    });

    let query = "";
    if (fetchingData) {
      var esc = encodeURIComponent;
      query = Object.keys(fetchingData)
        .map(k => esc(k) + '=' + esc(fetchingData[k]))
        .join('&');
    }
    query = (this._notifyQuery != "" ? this._notifyQuery : (query != "" ? "/?" + query : ""));

    return this.http.get(this.base_url + "getBatchDetails/" + id + query).pipe(
      timeout(10000),
      catchError(this.errorHandler.handleError)
    )
  }
}
