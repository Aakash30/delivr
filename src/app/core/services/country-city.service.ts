import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { retry, catchError, timeout } from "rxjs/operators";
import { ErrorHandlerService } from "./error-handler.service";
import { BaseService } from "./base.service";
import { Observable } from 'rxjs/Observable';
import { UtilityService } from "./utility.service";

@Injectable({
  providedIn: "root"
})
export class CountryService {
  base_url: string;

  constructor(
    private http: HttpClient,
    private bs: BaseService,
    private errorHandler: ErrorHandlerService,
    private utility: UtilityService
  ) {
    this.base_url = this.bs.base_url;
  }

  getPage(callfunction, params) {

    if (callfunction == "country") {
      return this.fetchCountry();
    } else if (callfunction == "country/details/:id") {
      let country_id = this.utility.base64Decode(params.id);
      return this.fetchCountryDetails(country_id);
    } else if (callfunction == "country/city-details/:id") {
      let country_id = this.utility.base64Decode(params.id);
      return this.fetchCityDetails(country_id);
    }

  }
  fetchCountry(fetchingData?) {

    let query = "";
    if (fetchingData) {
      var esc = encodeURIComponent;
      query = Object.keys(fetchingData)
        .map(k => esc(k) + '=' + esc(fetchingData[k]))
        .join('&');
    }
    query = query != "" ? "/?" + query : "";

    return this.http.get(this.base_url + "countries" + query).pipe(
      timeout(10000),
      catchError(this.errorHandler.handleError)
    );
  }

  fetchCountryDetails(id, fetchingData?) {

    let query = "";
    if (fetchingData) {
      var esc = encodeURIComponent;
      query = Object.keys(fetchingData)
        .map(k => esc(k) + '=' + esc(fetchingData[k]))
        .join('&');
    }
    query = query != "" ? "/?" + query : "";

    return this.http.get(this.base_url + "getCountryDetails/" + id + query).pipe(
      timeout(10000),
      catchError(this.errorHandler.handleError)
    );
  }

  fetchCityDetails(id) {
    return this.http.get(this.base_url + "getCityDetails/" + id).pipe(
      timeout(10000),
      catchError(this.errorHandler.handleError)
    );
  }

  addCountry(data) {
    return this.http.post(this.base_url + "addUpdateCountry", data, { observe: "response" }).pipe(
      timeout(10000),
      catchError(this.errorHandler.handleError)
    );
  }

  updateCountry(data) {
    return this.http.post(this.base_url + "addUpdateCountry", data, { observe: "response" }).pipe(
      timeout(10000),
      catchError(this.errorHandler.handleError)
    );
  }

  changeStatus(id, status) {

    let data = {
      status: (status) ? 'active' : 'inActive'
    }
    return this.http.post(this.base_url + "activeInActiveCountry/" + id, data).pipe(
      catchError(this.errorHandler.handleError)
    );
  }

  addCity(id, city) {
    let data = { "city": city.cityName, "country_id": id.toString() }
    return this.http.post(this.base_url + "addUpdateCity", data, { observe: "response" }).pipe(
      retry(3),
      catchError(this.errorHandler.handleError)
    );
  }

  updateCity(data) {

    return this.http.post(this.base_url + "addUpdateCity", data, { observe: "response" }).pipe(
      retry(3),
      catchError(this.errorHandler.handleError)
    );
  }

  changeCityStatus(id, status) {

    let data = {
      status: (status) ? 'active' : 'inActive'
    }
    return this.http.post(this.base_url + "activeInActiveCity/" + id, data).pipe(
      catchError(this.errorHandler.handleError)
    );
  }

}

