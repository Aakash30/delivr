import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { retry, catchError, timeout } from "rxjs/operators";
import { ErrorHandlerService } from "./error-handler.service";
import { BaseService } from "./base.service";
import { AuthService } from 'app/core/services/auth.service';
import { Observable } from 'rxjs/Observable';
import { UtilityService } from "./utility.service";

@Injectable({
  providedIn: "root"
})
export class DriverManagementService {
  base_url: string;

  constructor(
    private http: HttpClient,
    private bs: BaseService,
    private errorHandler: ErrorHandlerService,
    private utility: UtilityService
  ) {
    this.base_url = this.bs.base_url;
  }


  getPage(callfunction, params) {

    if (callfunction == 'driver-list') {
      return this.getDrivers();
    } else if (callfunction == 'driver-list/add') {
      return this.fetchDataForAddEditForm();
    } else if (callfunction == 'driver-list/edit/:id') {
      let id = this.utility.base64Decode(params.id)
      return this.fetchDataForAddEditForm(id);
    } else if (callfunction == 'driver-list/details/:id') {
      let id = this.utility.base64Decode(params.id)
      return this.getDriverDetails(id);
    } else if (callfunction == "driver-feedback") {
      return this.fetchDriverAverageRating();
    } else if (callfunction == "driver-feedback/details/:id") {
      let id = this.utility.base64Decode(params.id)
      return this.fetchDriverReviews(id);
    }

  }

  /**
   * fetches driver list
   * @param fetchingData data array to filter driver
   */
  getDrivers(fetchingData?) {
    let query = "";
    if (fetchingData) {
      var esc = encodeURIComponent;
      query = Object.keys(fetchingData)
        .map(k => esc(k) + '=' + esc(fetchingData[k]))
        .join('&');
    }
    query = query != "" ? "/?" + query : "";

    return this.http.get(this.base_url + "driverList" + query).pipe(
      timeout(10000),
      catchError(this.errorHandler.handleError)
    );
  }

  /**
   * get driver details
   * @param id defines unique driver id
   */
  getDriverDetails(id) {
    return this.http.get(this.base_url + "fetchSingleDriver/" + id).pipe(
      timeout(10000),
      catchError(this.errorHandler.handleError)
    );
  }

  /**
   * used to add / update driver details in the system
   * @param data driver details filledup in form
   */
  addDriver(data) {
    return this.http.post(this.base_url + 'addUpdateDriver', data).pipe(
      timeout(10000),
      catchError(this.errorHandler.handleError)
    );
  }

  /**
   * fetches driver form related data. If id is passed then it gets data of driver as well.
   * @param id defines unique driver id
   */
  fetchDataForAddEditForm(id?) {

    let fetchString = id != undefined ? "/" + id : "";

    return this.http.get(this.base_url + "addEditDriverFormData" + fetchString).pipe(
      timeout(10000),
      catchError(this.errorHandler.handleError)
    );
  }

  /**
   * 
   * @param keyword fetches unallocated vehicle list based on keyword
   */
  fetchAvailableVehicle(keyword) {

    return this.http.get(this.base_url + "fetchAvailableVehicleForBusiness?keyword=" + keyword).pipe(
      timeout(10000),
      catchError(this.errorHandler.handleError)
    );
  }

  /**
   * 
   * @param id defines unique driver id
   * @param status defines status active / block
   */
  activeOrBlockDriver(id, status) {

    let data = {
      user_id: id,
      user_status: (status) ? 'active' : 'deactive'
    }
    return this.http.post(this.base_url + "activeBlockDriver", data).pipe(
      timeout(10000),
      catchError(this.errorHandler.handleError)
    );
  }

  fetchDriverAverageRating(fetchingData?) {

    let query = "";
    if (fetchingData) {
      var esc = encodeURIComponent;
      query = Object.keys(fetchingData)
        .map(k => esc(k) + '=' + esc(fetchingData[k]))
        .join('&');
    }
    query = query != "" ? "/?" + query : "";

    return this.http.get(this.base_url + "fetchDriverFeedback" + query).pipe(
      timeout(10000),
      catchError(this.errorHandler.handleError)
    )
  }

  fetchDriverReviews(id, fetchingData?) {

    let query = "";
    if (fetchingData) {
      var esc = encodeURIComponent;
      query = Object.keys(fetchingData)
        .map(k => esc(k) + '=' + esc(fetchingData[k]))
        .join('&');
    }
    query = query != "" ? "/?" + query : "";

    return this.http.get(this.base_url + "getFeedbackDetails/" + id + query).pipe(
      timeout(10000),
      catchError(this.errorHandler.handleError)
    )
  }

  resendOTP(data) {
    return this.http.post(this.base_url + "resendOTPOrLink", data).pipe(
      timeout(10000),
      catchError(this.errorHandler.handleError)
    )
  }

}

