import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { retry, catchError, timeout } from "rxjs/operators";
import { ErrorHandlerService } from "./error-handler.service";
import { BaseService } from "./base.service";
import { UtilityService } from './utility.service';
import { NotificationReadEventService } from './notification-read-event.service';
import { DashboardFilterRedirectService } from './dashboard-filter-redirect.service';

@Injectable({
  providedIn: 'root'
})
export class OrderManagementService {

  base_url: string;
  public _notifyQuery = "";
  public filterQuery = "";

  constructor(
    private http: HttpClient,
    private bs: BaseService,
    private errorHandler: ErrorHandlerService,
    private utility: UtilityService,
    private _notificationEvent: NotificationReadEventService,
    private _vehicleFetch: DashboardFilterRedirectService
  ) {
    this.base_url = this.bs.base_url;


  }

  getPage(callfunction, params) {

    if (callfunction == '') {
      return this.getOrders();
    } else if (callfunction == 'add') {
      return this.fetchDataForOrderForm();
    } else if (callfunction == 'edit/:id') {

      let decodeID = this.utility.base64Decode(params.id);
      return this.fetchDataForOrderForm(decodeID);

    } else if (callfunction == 'details/:id') {
      return this.fetchOrderDetails(params.id);
    } else if (callfunction == 'assign-driver/:id') {

      let decodeID = this.utility.base64Decode(params.id);

      return this.fetchDriversForOrder(decodeID)
    }

  }

  /**
   * List to fetch data for order list
   * @param fetchingData (optional) stores the filter array
   */
  getOrders(fetchingData?) {
    let query = "";

    let filter = this._vehicleFetch.filterFor.subscribe((res) => {
      if (res) {
        this.filterQuery = "status=" + res
      } else {
      }
    });
    if (fetchingData) {
      var esc = encodeURIComponent;
      query = Object.keys(fetchingData)
        .map(k => esc(k) + '=' + esc(fetchingData[k]))
        .join('&');
    } else {
      query = this.filterQuery
    }
    query = query != "" ? "/?" + query : "";

    return this.http.get(this.base_url + "orderList" + query).pipe(
      retry(3),
      catchError(this.errorHandler.handleError)
    );
  }

  /**
   * fetches order form data
   * @param id (optional) id stores order id in case of editing order
   */
  fetchDataForOrderForm(id?) {
    let fetchString = id != undefined ? "/" + id : "";

    return this.http.get(this.base_url + "fetchOrderFormData" + fetchString).pipe(
      timeout(10000),
      catchError(this.errorHandler.handleError)
    );
  }

  /**
   * function to add / update orders
   * @param data posted form data
   */
  addUpdateOrder(data) {
    return this.http.post(this.base_url + 'addUpdateOrderData', data).pipe(
      timeout(10000),
      catchError(this.errorHandler.handleError)
    );
  }

  /**
   * function to fetch order details
   * @param id defines order id encoded, we need to decode it first.
   */
  fetchOrderDetails(id) {

    let notify = this._notificationEvent.currentNotice.subscribe((res) => {
      if (res) {
        this._notifyQuery = "?readNotice=" + this.utility.base64Encode(res);
        this._notificationEvent.setNotificationStatus(0);
      } else {
      }
    });

    notify.unsubscribe();

    return this.http.get(this.base_url + "fetchSingleOrder/" + id + this._notifyQuery).pipe(
      timeout(10000),
      catchError(this.errorHandler.handleError)
    );

  }

  /**
   * fetches driver available for orders
   * @param id defines order id encoded, we need to decode it first.
   */
  fetchDriversForOrder(id) {
    return this.http.get(this.base_url + "fetchDriversToAssignOrder?order_id=" + id).pipe(
      timeout(10000),
      catchError(this.errorHandler.handleError)
    );
  }

  assignOrderToDriver(data) {
    return this.http.post(this.base_url + "assignOrderToDriver", data).pipe(
      timeout(10000),
      catchError(this.errorHandler.handleError)
    )
  }
}
