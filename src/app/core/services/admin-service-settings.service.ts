import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseService } from './base.service';
import { ErrorHandlerService } from './error-handler.service';
import { timeout, catchError } from 'rxjs/operators';
import { UtilityService } from './utility.service';

@Injectable({
  providedIn: 'root'
})
export class AdminServiceSettingsService {
  base_url: string;
  constructor(private http: HttpClient,
    private bs: BaseService,
    private errorHandler: ErrorHandlerService,
    private utility: UtilityService) {
    this.base_url = this.bs.base_url;
  }

  getPage(callfunction, params) {
    if (callfunction == "service-category") {
      return this.fetchServiceCategory();
    } else if (callfunction == "service-list/add-service") {
      return this.fetchServiceFormData();
    } else if (callfunction == "service-list") {
      return this.fetchServices();
    } else if (callfunction == 'service-list/edit-service/:id') {
      let id = this.utility.base64Decode(params.id);
      return this.fetchServiceFormData(id)
    }
  }

  /**
   * fetches service category
   * @param fetchingData filter for service category
   */
  fetchServiceCategory(fetchingData?) {

    let query = "";
    if (fetchingData) {
      var esc = encodeURIComponent;
      query = Object.keys(fetchingData)
        .map(k => esc(k) + '=' + esc(fetchingData[k]))
        .join('&');
    }
    query = query != "" ? "/?" + query : "";

    return this.http.get(this.base_url + "fetchServiceCategoryList" + query).pipe(
      timeout(10000),
      catchError(this.errorHandler.handleError)
    );
  }
  /**
   * add / update service category
   * @param data data to b saved in the system for service category
   */
  addUpdateServiceCategory(data) {
    return this.http.post(this.base_url + "addUpdateServiceCategory", data).pipe(
      timeout(10000),
      catchError(this.errorHandler.handleError)
    );
  }

  updateServiceCategoryStatus(data) {
    return this.http.post(this.base_url + "updateServiceCategoryStatus", data).pipe(
      timeout(10000),
      catchError(this.errorHandler.handleError)
    )
  }

  fetchServices(fetchingData?) {

    let query = "";
    if (fetchingData) {
      var esc = encodeURIComponent;
      query = Object.keys(fetchingData)
        .map(k => esc(k) + '=' + esc(fetchingData[k]))
        .join('&');
    }
    query = query != "" ? "/?" + query : "";

    return this.http.get(this.base_url + "fetchServices" + query).pipe(
      timeout(10000),
      catchError(this.errorHandler.handleError)
    )
  }
  fetchServiceFormData(id?) {

    let query = id ? "/" + id : "";
    return this.http.get(this.base_url + "fetchDataForServiceForm" + query).pipe(
      timeout(10000),
      catchError(this.errorHandler.handleError)
    )
  }

  addUpdateServiceData(data) {
    return this.http.post(this.base_url + "addUpdateServices", data).pipe(
      timeout(10000),
      catchError(this.errorHandler.handleError)
    )
  }

}
