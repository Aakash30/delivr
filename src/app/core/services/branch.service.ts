import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError, timeout } from 'rxjs/operators';
import { ErrorHandlerService } from './error-handler.service';
import { BaseService } from './base.service';
import { AuthService } from 'app/core/services/auth.service';
import { Observable } from 'rxjs/Observable';
import { UtilityService } from './utility.service';

@Injectable({
  providedIn: 'root'
})
export class BranchService {
  base_url: string;

  constructor(
    private http: HttpClient,
    private bs: BaseService,
    private errorHandler: ErrorHandlerService,
    private utility: UtilityService
  ) {
    this.base_url = this.bs.base_url;
  }

  getPage(callfunction, params) {
    if (callfunction == "") {
      return this.getBranches();
    } else if (callfunction == "add") {
      return this.fetchBranchFormData();
    } else if (callfunction == "details/:id") {
      let id = this.utility.base64Decode(params.id);
      return this.getBranchDetails(id);
    } else if (callfunction == "edit/:id") {
      let id = this.utility.base64Decode(params.id);
      return this.fetchBranchFormData(id);
    } else if (callfunction == "assign-bike/:id") {
      return this.fetchvehiclesForAssignBike(params.id);
    }
  }

  /**
   * get branch list
   */
  getBranches(fetchingData?) {
    let query = "";
    if (fetchingData) {
      var esc = encodeURIComponent;
      query = Object.keys(fetchingData)
        .map(k => esc(k) + '=' + esc(fetchingData[k]))
        .join('&');
    }
    query = query != "" ? "/?" + query : "";
    return this.http.get(this.base_url + 'fetchMyBranches' + query).pipe(
      timeout(10000),
      catchError(this.errorHandler.handleError)
    );
  }
  /**
   * fetches data for add / edit branch
   * @param id defines id of branch in case of edit form
   */
  fetchBranchFormData(id?) {

    let query = "";
    query = id ? "/" + id : "";

    return this.http.get(this.base_url + 'addEditBusinessBranchFormData' + query).pipe(
      timeout(10000),
      catchError(this.errorHandler.handleError)
    );
  }
  /**
   * 
   * @param id 
   */
  getBranchDetails(id) {
    return this.http.get(this.base_url + 'fetchSingleBusinessBranch/' + id).pipe(
      timeout(10000),
      catchError(this.errorHandler.handleError)
    );
  }
  addUpdateBranch(data) {
    return this.http.post(this.base_url + 'addUpdateBusinessArea', data).pipe(
      timeout(10000),
      catchError(this.errorHandler.handleError)
    );
  }

  changeStatus(data) {
    return this.http.post(this.base_url + 'updateBranchStatus', data).pipe(
      timeout(10000),
      catchError(this.errorHandler.handleError)
    )
  }

  fetchvehiclesForAssignBike(id, data?) {
    let query = "";
    if (data) {
      var esc = encodeURIComponent;
      query = Object.keys(data)
        .map(k => esc(k) + '=' + esc(data[k]))
        .join('&');
    }
    query = query != "" ? "/?" + query : "";
    return this.http.get(this.base_url + "fetchBranchAssignBikeList/" + id + query).pipe(
      timeout(10000),
      catchError(this.errorHandler.handleError)
    );
  }

  assignBikesToBranch(data) {
    return this.http.post(this.base_url + "assignVehiclesToMyBranches", data).pipe(
      timeout(10000),
      catchError(this.errorHandler.handleError)
    )
  }
}

