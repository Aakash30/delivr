import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { retry, catchError, timeout } from "rxjs/operators";
import { ErrorHandlerService } from "./error-handler.service";
import { BaseService } from "./base.service";
import { Observable } from 'rxjs/Observable';
import { UtilityService } from "./utility.service";
import { throwError } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class BusinessService {
  base_url: string;

  constructor(
    private http: HttpClient,
    private bs: BaseService,
    private errorHandler: ErrorHandlerService,
    private utility: UtilityService
  ) {
    this.base_url = this.bs.base_url;
  }

  getPage(callfunction, params) {
    if (callfunction == "business-type") {
      return this.getbusinesstypesList();
    } else if (callfunction == "business-type-details/:id") {
      let businessTypeId = this.utility.base64Decode(params.id)
      return this.getbusinesstype(businessTypeId)
    } else if (callfunction == "business-list") { // business list
      return this.getbusinessList();
    } else if (callfunction == "business-list/details/:id") { // business details

      let businessId = this.utility.base64Decode(params.id)
      return this.getbusinessDetails(businessId);
    } else if (callfunction == "business-list/add") {
      return this.fetchprefills();
    } else if (callfunction == "business-list/edit/:id") {
      let businessId = this.utility.base64Decode(params.id)
      return this.fetchprefills(businessId);
    } else if (callfunction == "business-list/assign-bike/:id") {
      return this.fetchvehicles(params.id);
    } else if (callfunction == 'business-list/renew-bike/:id/:batch') {
      return this.fetchbatchvehicles(params.id, params.batch);
    }
  }


  fetchprefills(id?) {

    let query = id != undefined ? "/" + id : "";
    return this.http.get(this.base_url + "fetchDataForAddEditBusiness" + query).pipe(
      timeout(10000),
      catchError(this.errorHandler.handleError)
    );
  }

  fetchvehicles(id, data?) {
    let query = "";
    if (data) {
      var esc = encodeURIComponent;
      query = Object.keys(data)
        .map(k => esc(k) + '=' + esc(data[k]))
        .join('&');
    }
    query = query != "" ? "/?" + query : "";
    return this.http.get(this.base_url + "fetchAvailableVehiclesForAdmin/" + id + query).pipe(
      timeout(10000),
      catchError(this.errorHandler.handleError)
    );
  }

  fetchbatchvehicles(id, batch, data?) {
    let query = "";
    if (data) {
      var esc = encodeURIComponent;
      query = Object.keys(data)
        .map(k => esc(k) + '=' + esc(data[k]))
        .join('&');
    }
    query = query != "" ? "/?" + query : "";
    return this.http.get(this.base_url + "fetchBatchDataForRenewal/" + id + '/' + batch + query).pipe(
      timeout(10000),
      catchError(this.errorHandler.handleError)
    );
  }

  getbusinesstypesList(data?) {
    let query = "";
    if (data) {
      var esc = encodeURIComponent;
      query = Object.keys(data)
        .map(k => esc(k) + '=' + esc(data[k]))
        .join('&');
    }
    query = query != "" ? "/?" + query : "";

    return this.http.get(this.base_url + "businessTypes" + query).pipe(
      timeout(10000),
      catchError(this.errorHandler.handleError)
    );
  }

  getbusinesstype(id) {
    return this.http.get(this.base_url + "fetchSingleBusinessType/" + id).pipe(
      timeout(10000),
      catchError(this.errorHandler.handleError)
    );
  }

  getbusinessDetails(id) {
    return this.http.get(this.base_url + "fetchSingleBusinessUser/" + id).pipe(
      timeout(10000),
      catchError(this.errorHandler.handleError)
    );
  }

  getbusinessList(data?) {
    let query = "";
    if (data) {
      var esc = encodeURIComponent;
      query = Object.keys(data)
        .map(k => esc(k) + '=' + esc(data[k]))
        .join('&');
    }
    query = query != "" ? "/?" + query : "";

    return this.http.get(this.base_url + "businessList" + query).pipe(
      timeout(10000),
      catchError(this.errorHandler.handleError)
    );
  }

  addbusinesstype(data) {
    return this.http.post(this.base_url + "addUpdateBusinessTypes", data, { observe: "response" }).pipe(
      timeout(10000),
      catchError(this.errorHandler.handleError)
    );
  }

  updatebusinesstype(id, data) {
    return this.http.post(this.base_url + "addUpdateBusinessTypes/" + id, data).pipe(
      timeout(10000),
      catchError(this.errorHandler.handleError)
    );
  }


  addbusiness(data) {
    return this.http.post(this.base_url + "addUpdateBusinessUser", data).pipe(
      timeout(10000),
      catchError(error => {
        return throwError(error);
      })
    );
  }

  assignbike(id, data) {

    return this.http.post(this.base_url + "assignVehicleToVendor/" + id, data, { observe: "response" }).pipe(
      timeout(10000),
      catchError(this.errorHandler.handleError)
    );
  }

  renewBatch(id, data) {

    return this.http.post(this.base_url + "renewBatch/" + id, data, { observe: "response" }).pipe(
      timeout(10000),
      catchError(error => {
        this.errorHandler.routeAccordingToError(error);
        return throwError(error);
      })
    );
  }
  changeStatus(id, status) {

    let data = {
      status: (status) ? 'active' : 'deactive'
    }
    return this.http.post(this.base_url + "blockOrActivateBusiness/" + id, data).pipe(
      timeout(10000),
      catchError(this.errorHandler.handleError)
    );
  }

  changeBusinessTypeStatus(id, status) {

    let data = {
      status: (status) ? 'active' : 'block'
    }
    return this.http.post(this.base_url + "updateBusinnessTypeStatus/" + id, data).pipe(
      timeout(10000),
      catchError(this.errorHandler.handleError)
    );
  }

  resendLink(data) {
    return this.http.post(this.base_url + "resendOTPOrLink", data).pipe(
      timeout(10000),
      catchError(this.errorHandler.handleError)
    )
  }
}

