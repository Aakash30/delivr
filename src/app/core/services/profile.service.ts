import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { BaseService } from './base.service';
import { ErrorHandlerService } from './error-handler.service';
import { UtilityService } from './utility.service';
import { retry, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class ProfileService {

  base_url: string;
  id: any;

  constructor(
    private http: HttpClient,
    private bs: BaseService,
    private errorHandler: ErrorHandlerService,
    private utility: UtilityService
  ) {
    this.base_url = this.bs.base_url;
  }

  getPage(callfunction, params) {
    // console.log('call function::', callfunction);
    // if (callfunction == "list") {
    //   return this.getVehicles(this.id);
    // } else if (callfunction == "add") {
    //   // return this.fetchprefills();
    // }
    return this.fetchProfile();

  }

  /**
   * fetch vehicle list
   * @param fetchingData send data to be filtered on vehicle list
   */

  fetchProfile() {

    // let query = "";
    // if (fetchingData) {
    //   var esc = encodeURIComponent;
    //   query = Object.keys(fetchingData)
    //     .map(k => esc(k) + '=' + esc(fetchingData[k]))
    //     .join('&');
    // }
    // query = query != "" ? "/?" + query : "";

    return this.http.get(this.base_url + "fetchUserProfile").pipe(
      retry(3),
      catchError(this.errorHandler.handleError)
    );
  }

  // update user profile
  updateUserProfile(data?, file?) {
    if (file) {
      let formData: FormData = new FormData();
      formData.append('profile', file);
      return this.http.post(this.base_url + 'updateUserProfile', formData).pipe(
        catchError(this.errorHandler.handleError)
      );
    } else {
      return this.http.post(this.base_url + 'updateUserProfile', data).pipe(
        catchError(this.errorHandler.handleError)
      )
    }
  }

  // update business profile
  updateBusinessProfile(data?, file?) {
    if (file) {
      let formData: FormData = new FormData();
      formData.append('business_logo', file);
      return this.http.post(this.base_url + 'updateBusinessLogo', formData).pipe(
        catchError(this.errorHandler.handleError)
      );
    } else {
      return this.http.post(this.base_url + 'updateBusinessLogo', data).pipe(
        catchError(this.errorHandler.handleError)
      )
    }
  }


}
