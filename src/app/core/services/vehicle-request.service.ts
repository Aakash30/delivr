import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError, timeout } from 'rxjs/operators';
import { ErrorHandlerService } from './error-handler.service';
import { BaseService } from './base.service';
import { UtilityService } from './utility.service';
import { NotificationReadEventService } from './notification-read-event.service';

@Injectable({
  providedIn: 'root'
})
export class VehicleRequestService {
  base_url: string;
  public _notifyQuery = "";

  constructor(private http: HttpClient,
    private bs: BaseService,
    private errorHandler: ErrorHandlerService,
    private utility: UtilityService,
    private _notificationEvent: NotificationReadEventService
  ) {
    this.base_url = this.bs.base_url;
  }

  getPage(callfunction, params) {
    if (callfunction == "") {
      return this.getMyRequestsList();
    }
  }

  getMyRequestsList(fetchingData?) {

    let query = "";
    let notify = this._notificationEvent.currentNotice.subscribe((res) => {
      if (res) {
        this._notifyQuery = "?readNotice=" + this.utility.base64Encode(res);
        this._notificationEvent.setNotificationStatus(0);
      } else {
      }
    });

    if (fetchingData) {
      var esc = encodeURIComponent;
      query = Object.keys(fetchingData)
        .map(k => esc(k) + '=' + esc(fetchingData[k]))
        .join('&');
    }
    query = (this._notifyQuery != "" ? this._notifyQuery : (query != "" ? "/?" + query : ""));

    notify.unsubscribe();
    return this.http.get(this.base_url + "fetchMyRequestedVehicles" + query).pipe(
      timeout(10000),
      catchError(this.errorHandler.handleError)
    );
  }

  raiseNewRequest(data) {
    return this.http.post(this.base_url + "askForNewVehicles", data).pipe(
      timeout(10000),
      catchError(this.errorHandler.handleError)
    )
  }
}
