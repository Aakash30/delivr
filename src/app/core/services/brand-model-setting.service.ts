import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseService } from './base.service';
import { ErrorHandlerService } from './error-handler.service';
import { UtilityService } from './utility.service';
import { timeout, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class BrandModelSettingService {

  base_url = "";
  constructor(private http: HttpClient,
    private bs: BaseService,
    private errorHandler: ErrorHandlerService,
    private utility: UtilityService) {
    this.base_url = this.bs.base_url;
  }

  getPage(callfunction, params) {
    if (callfunction == "brand") {
      return this.fetchBrandList();
    } else if (callfunction == "brand/brand-details/:id") {
      return this.getBrandModelDetails(params.id);
    }
  }

  fetchBrandList(fetchingData?) {
    let query = "";
    if (fetchingData) {
      var esc = encodeURIComponent;
      query = Object.keys(fetchingData)
        .map(k => esc(k) + '=' + esc(fetchingData[k]))
        .join('&');
    }
    query = query != "" ? "/?" + query : "";

    return this.http.get(this.base_url + "fetchBrandList" + query).pipe(
      timeout(10000),
      catchError(this.errorHandler.handleError)
    );
  }

  addUpdateBrandData(data) {
    return this.http.post(this.base_url + "addUpdateBrands", data).pipe(
      timeout(10000),
      catchError(this.errorHandler.handleError)
    );
  }

  getBrandModelDetails(id, fetchingData?) {

    let query = "";
    if (fetchingData) {
      var esc = encodeURIComponent;
      query = Object.keys(fetchingData)
        .map(k => esc(k) + '=' + esc(fetchingData[k]))
        .join('&');
    }
    query = query != "" ? "/?" + query : "";

    return this.http.get(this.base_url + "fetchBrandDetails/" + id + "" + query).pipe(
      timeout(10000),
      catchError(this.errorHandler.handleError)
    );
  }

  addUpdateModelData(data) {
    return this.http.post(this.base_url + "addUpdateModel", data).pipe(
      timeout(10000),
      catchError(this.errorHandler.handleError)
    );
  }
}
