import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseService } from './base.service';
import { ErrorHandlerService } from './error-handler.service';
import { UtilityService } from './utility.service';
import { timeout, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class BusinessAdminServiceService {
  base_url = "";
  constructor(private http: HttpClient,
    private bs: BaseService,
    private errorHandler: ErrorHandlerService,
    private utility: UtilityService) {
    this.base_url = this.bs.base_url;
  }

  getPage(callfunction, params) {
    if (callfunction == "") {
      return this.fetchBusinessAdmin();
    } else if (callfunction == "add") {
      return this.fetchDataForAddEditBusinessAdmin();
    } else if (callfunction == "edit/:id") {
      return this.fetchDataForAddEditBusinessAdmin(params.id);
    } else if (callfunction == "details/:id") {
      return this.showUserDetails(params.id);
    }
  }

  /**
   * fetches business admin
   * @param fetchingData filter data array 
   */
  fetchBusinessAdmin(fetchingData?) {

    let query = "";
    if (fetchingData) {
      var esc = encodeURIComponent;
      query = Object.keys(fetchingData)
        .map(k => esc(k) + '=' + esc(fetchingData[k]))
        .join('&');
    }
    query = query != "" ? "/?" + query : "";

    return this.http.get(this.base_url + "fetchMyUsers" + query).pipe(
      timeout(10000),
      catchError(this.errorHandler.handleError)
    )
  }

  /**
   * 
   * @param id defines unique id for user to fetch data for form component
   */
  fetchDataForAddEditBusinessAdmin(id?) {

    let queryParam = id ? "/" + id : "";
    return this.http.get(this.base_url + "fetchAddEditBusinessAdminForm" + queryParam).pipe(
      timeout(10000),
      catchError(this.errorHandler.handleError)
    )
  }

  /**
   * Submits user data to save / update user data in database
   * @param data - defines related form data
   */
  submitUserData(data) {
    return this.http.post(this.base_url + "addBusinessUsers", data).pipe(
      timeout(10000),
      catchError(this.errorHandler.handleError)
    )
  }

  showUserDetails(id) {
    return this.http.get(this.base_url + "fetchMyBusinessUser/" + id).pipe(
      timeout(10000),
      catchError(this.errorHandler.handleError)
    );
  }

  changeUserStatus(data) {
    return this.http.post(this.base_url + "updateMyUsersStatus/", data).pipe(
      timeout(10000),
      catchError(this.errorHandler.handleError)
    );
  }

  resendOTP(data) {
    return this.http.post(this.base_url + "resendOTPOrLink", data).pipe(
      timeout(10000),
      catchError(this.errorHandler.handleError)
    );
  }
}
