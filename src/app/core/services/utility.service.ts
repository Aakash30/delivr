"use strict";
import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MessageService } from 'primeng/api';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { ToastrService } from 'ngx-toastr';
import jwt_decode from 'jwt-decode';
import * as moment from 'moment';


declare var window: any;

@Injectable()
export class UtilityService {
  constructor(
    private messageService: MessageService,
    private loader: NgxUiLoaderService,
    private toastrService: ToastrService

  ) { }

  scrollToTop() {
    window.scroll(0, 0);
  }

  convertDateToNumberFormat(d) {
    const formattedTime =
      (d.getHours() < 10 ? '0' + d.getHours() : d.getHours()) +
      '' +
      (d.getMinutes() < 10 ? '0' + d.getMinutes() : d.getMinutes());
    return +formattedTime;
  }

  timeToObject(startTime, endTime) {
    return {
      open: this.convertDateToNumberFormat(startTime),
      close: this.convertDateToNumberFormat(endTime)
    };
  }

  convertNumberToDateFormat(time_in_number) {
    const finalTime = new Date();
    const hourAndMinute = time_in_number.toString().match(/.{1,2}/g);
    finalTime.setHours(+hourAndMinute[0]);
    finalTime.setMinutes(+hourAndMinute[1]);
    return finalTime;
  }

  arrayOfStringsToArrayOfObjects(arr: any[]) {
    const newArray = [];
    arr.forEach(element => {
      newArray.push({
        label: element,
        value: element
      });
    });
    return newArray;
  }

  arrayOfObjectToConvertInDropdownFormat(arr: any[], labelKey, valueKey, statusKey?, statusNotEqual?) {
    const newArray = [];
    arr.forEach(element => {
      if (statusKey) {
        newArray.push({
          label: element[labelKey],
          value: element[valueKey],
          disabled: element[statusKey] == statusNotEqual
        });
      } else {
        newArray.push({
          label: element[labelKey],
          value: element[valueKey],
        });
      }

    });
    return newArray;
  }
  countryJSONtoDropdown(arr: any[], labelKey) {
    const newArray = [];
    arr.forEach(element => {
      newArray.push({
        label: element[labelKey],
        value: element
      });
    });
    return newArray;
  }
  arrayOfObjectToArrayOfStrings(obj: []) {
    const newArray = [];
    obj.forEach(element => {
      newArray.push(element['value']);
    });
    return newArray;
  }

  stringToNumber(str: string) {
    return +str;
  }

  validateEmail(controls) {
    // Create a regular expression
    // tslint:disable-next-line:max-line-length
    const regExp = new RegExp(
      /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    );
    // Test email against regular expression
    if (regExp.test(controls.value)) {
      return null; // Return as valid email
    } else {
      return { validateEmail: true }; // Return as invalid email
    }
  }

  markFormGroupTouched(formGroup: FormGroup) {
    (<any>Object).values(formGroup.controls).forEach(control => {
      control.markAsTouched();

      if (control.controls) {
        this.markFormGroupTouched(control);
      }
    });
  }

  showError(severity: string, summary: string, error: string) {
    this.messageService.add({
      severity: `${severity}`,
      summary: `${summary}`,
      detail: `${error}`,
      life: 10000
    });
    this.scrollToTop();
  }

  showSuccess(severity: string, summary: string) {
    this.messageService.add({
      severity: `${severity}`,
      summary: `${summary}`,
      life: 10000
    });
    this.scrollToTop();
  }

  success(message) {
    this.toastrService.success(message);
  }

  error(error) {
    this.toastrService.error(error);
  }

  base64Encode(stringText) {
    return window.btoa(stringText);
  }
  base64Decode(stringText) {
    return window.atob(stringText);
  }

  scrollToError() {

    setTimeout(() => {
      document.getElementsByClassName('error').item(0).scrollIntoView(
        { behavior: 'smooth', block: 'center' }
      );
    }, 400);


  }

  onInputValidator(event, is_decimal = false) {

    let pattern;
    if (is_decimal) {
      pattern = /^[0-9]+(\.[0-9]*){0,2}$/g;
    } else {
      pattern = /^[0-9]+$/;
    }

    let current: string = event.target.value;

    let next: string = current.concat(event.key);
    if (event.keyCode != 8 && event.keyCode != 9 && event.keyCode != 13) {
      if (!pattern.test(next)) {

        return false
      }
    }

  }

  allowCharacters(event, type) {
    let pattern;
    switch (type) {
      case 'alphaNumeric':
        pattern = /^[a-zA-Z0-9]/;
        break;
      case 'alphaNumericWithoutSpace':
        pattern = /^\S[a-zA-Z0-9]*$/;
        break;
      case 'customPattern':
        pattern = new RegExp(event.target.pattern);
        break;
      default:
        pattern = "";
    }
    let next: string;
    if (event.clipboardData) {

      let clipboardData = event.clipboardData || window.clipboardData;
      let pastedText = clipboardData.getData('text');
      next = clipboardData.getData('text');
    } else {
      let current: string = event.target.value;
      next = current.concat(event.key);
    }


    if (event.keyCode != 8 && event.keyCode != 9 && event.keyCode != 13 && event.keyCode != 16 && event.keyCode != 20) {
      if (!pattern.test(next)) {
        return false
      }
    }
  }
  paste(event) {
  }

  capitalizeValue(formController) {
    formController.setValue(formController.value.toUpperCase());
  }
  removeSpecialCharacters(event) {
    //  debugger;
    let pattern;

    pattern = /[!@#$%-_+=^&*(),.?":{}|<>]/g;


    let current: string = event.target.value;

    let next: string = current.concat(event.key);

    if (event.keyCode != 8 && event.keyCode != 9 && event.keyCode != 13) {
      if (pattern.test(next)) {

        return false
      }
    }

  }
  removeInputSpace(formController) {
    formController.setValue(formController.value.trim());
  }

  removeError(formController, errorString) {
    let errorArray = {};
    errorArray[errorString] = null;

    if (formController.hasError(errorString)) {

      formController.setErrors(errorArray);
    }
  }

  loaderStart() {
    this.loader.start();
  }

  loaderStop() {
    this.loader.stop();
  }

  // showSuccess(message) {
  //     this.toastr.success(message);
  // }

  // errorMessage(message) {
  //     this.toastr.error(message);
  // }


  dateDifference(startDate, endDate) {

    // let diffTime = (Date.parse(endDate) - Date.parse(startDate))
    // const diffDays = (diffTime / (1000 * 60 * 60 * 24));
    let end = moment(new Date(endDate), "YYYY-MM-DD");
    let start = moment(new Date(startDate), "YYYY-MM-DD");
    var years = end.diff(start, 'year');
    start.add(years, 'years');

    var months = end.diff(start, 'months');
    start.add(months, 'months');

    var days = end.diff(start, 'days');


    let stringText = "";
    if (Math.round(years) != 0) {
      stringText += Math.round(years) + (Math.round(years) > 1 ? " Years " : " Year ");
    }

    if (Math.round(months) != 0) {
      stringText += Math.round(months) + (Math.round(months) > 1 ? " Months " : " Month ");
    }

    if (Math.round(days) != 0) {
      stringText += Math.round(days) + (Math.round(days) > 1 ? " Days " : " Day ");
    }
    return stringText;
  }

  checkUserRoleWithRoute(roleToCheck, checkForUser) {

    const token = localStorage.getItem("token").replace("Bearer ", "");

    var decoded = jwt_decode(token);

    var user_roles = JSON.parse(decoded.roles);

    var user_type = decoded.type;

    if (checkForUser.indexOf(user_type) !== -1 && user_roles.indexOf(roleToCheck) !== -1) {

      return true;
    } else {
      return false
    }
  }
}
