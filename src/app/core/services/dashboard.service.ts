import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseService } from './base.service';
import { ErrorHandlerService } from './error-handler.service';
import { UtilityService } from './utility.service';
import { timeout, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {
  base_url: string;
  constructor(private http: HttpClient,
    private bs: BaseService,
    private errorHandler: ErrorHandlerService,
    private utility: UtilityService, ) {
    this.base_url = this.bs.base_url;
  }

  fetchAdminDashboard() {
    return this.http.get(this.base_url + "fetchAdminDashboard").pipe(
      timeout(10000),
      catchError((error) => {
        this.errorHandler.routeAccordingToError(error);
        return throwError(error)
      })
    );
  }

  fetchBusinessDashboard() {
    return this.http.get(this.base_url + "fetchMyDashboard").pipe(
      timeout(10000),
      catchError((error) => {
        this.errorHandler.routeAccordingToError(error);
        return throwError(error)
      })
    );
  }
}
