import { Injectable } from '@angular/core';
import { Subject, BehaviorSubject, Observable } from 'rxjs';
import { ErrorHandlerService } from './error-handler.service';
import { Router } from '@angular/router';
import { retry, catchError } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { MessageService } from 'primeng/api';
import { AuthService } from './auth.service';
import { BaseService } from './base.service';

@Injectable({
    providedIn: 'root'
})
export class SharedDataService {
    // store for user data
    private loginData = new BehaviorSubject(null);
    login_data = this.loginData.asObservable();

    public baseUrl: string;
    answerFlag: boolean = false;

    constructor(
        private http: HttpClient,
        private errorHandler: ErrorHandlerService,
        private router: Router,
        private loader: NgxUiLoaderService,
        private messageService: MessageService,
        private authService: AuthService,
        private baseService: BaseService,
    ) {
        this.baseUrl = this.baseService.base_url;
    }


    setLoginData(data: any) {
        this.loginData.next(data);
    }

    // getLoginData(data) {
    //     this.loader.start();
    //     this.authService.login(data).subscribe(
    //         (success: any) => {
    //             this.setLoginData(success.data);
    //             console.log("login data::", success);
    //             this.loader.stop();
    //         },
    //         error => {
    //             this.messageService.add({
    //                 severity: 'error',
    //                 summary: 'Get Profile Data Failed',
    //                 detail: `${error}`
    //             });
    //             this.loader.stop();
    //             // this.handleErrorCode('Unauthorized');
    //         }
    //     );
    // }
}
