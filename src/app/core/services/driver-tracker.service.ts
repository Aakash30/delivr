import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseService } from './base.service';
import { ErrorHandlerService } from './error-handler.service';
import { timeout, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DriverTrackerService {

  base_url: string;

  constructor(
    private http: HttpClient,
    private bs: BaseService,
    private errorHandler: ErrorHandlerService
  ) {
    this.base_url = this.bs.base_url;
  }

  getPage(callfunction, params) {

    if (callfunction == '') {
      return this.getDriverTracker();
    }

  }

  getDriverTracker() {
    return this.http.get(this.base_url + "driverTrackingList").pipe(
      timeout(10000),
      catchError(this.errorHandler.handleError)
    )
  }
}
