import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { retry, catchError, timeout } from "rxjs/operators";
import { ErrorHandlerService } from "./error-handler.service";
import { BaseService } from "./base.service";
import { UtilityService } from './utility.service';
import jwt_decode from 'jwt-decode';
import { throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CouponManagementService {

  base_url: string;
  userType: string;


  couponData = {
    code: ''
  }

  constructor(
    private http: HttpClient,
    private bs: BaseService,
    private errorHandler: ErrorHandlerService,
    private utility: UtilityService,
  ) {
    this.base_url = this.bs.base_url;

    const token = localStorage.getItem("token").replace("Bearer ", "");
    var decoded = jwt_decode(token);
    this.userType = decoded.type;
  }

  getPage(callfunction, params) {

    if (callfunction == 'coupon') {
      return this.getCoupons();
    } else if (callfunction == 'coupon/add') {
      return this.fetchDataForAddEditCoupons();
    } else if (callfunction == 'coupon/edit/:id') {

      return this.fetchDataForAddEditCoupons(params.id);

    } else if (callfunction == 'coupon/details/:id') {

      return this.fetchDataForAddEditCoupons(params.id);
    }

  }


  //fetch coupon list for this business
  getCoupons(data?) {
    let query = "";

    if (data) {
      var esc = encodeURIComponent;
      query = Object.keys(data)
        .map(k => esc(k) + '=' + esc(data[k]))
        .join('&');
    }
    query = query != "" ? "/?" + query : "";

    let fetchUrl = (this.userType == 'super_admin') ? this.base_url + 'fetchCouponsForAdmin' + query : this.base_url + "fetchCouponsForBusiness" + query;

    return this.http.get(fetchUrl).pipe(
      catchError((error) => {
        this.errorHandler.routeAccordingToError(error);
        return throwError(error)
      })
    );
  }

  /**
   * fetches order form data
   * @param id (optional) id stores order id in case of editing order
   */

  fetchDataForAddEditCoupons(id?) {
    let fetchString = id != undefined ? "/" + id : "";

    return this.http.get(this.base_url + "fetchDataForAddEditCoupons" + fetchString).pipe(
      timeout(10000),
      catchError((error) => {
        this.errorHandler.routeAccordingToError(error);
        return throwError(error)
      })
    );
  }

  // add and details coupon data
  addUpdateCoupon(data, file) {

    const formData = new FormData();

    Object.keys(data).forEach(elementKey => {
      formData.append(elementKey, data[elementKey]);
    });


    return this.http.post(this.base_url + 'addUpdateCoupon', formData).pipe(
      timeout(10000),
      catchError(this.errorHandler.handleError)
    );
  }


  /**
   * function to fetch order details
   * @param id defines order id encoded, we need to decode it first.
   */
  fetchOrderDetails(id) {
    let order_id = this.utility.base64Decode(id);
    return this.http.get(this.base_url + "fetchSingleOrder/" + order_id).pipe(
      timeout(10000),
      catchError((error) => {
        this.errorHandler.routeAccordingToError(error);
        return throwError(error)
      })
    );
  }

  // generate code
  generateCode() {
    return this.http.get(this.base_url + 'generateCode').pipe(
      timeout(10000),
      catchError(this.errorHandler.handleError)
    )
  }

  /**
   * fetches driver available for orders
   * @param id defines order id encoded, we need to decode it first.
   */
  fetchDriversForOrder(id) {
    return this.http.get(this.base_url + "fetchDriversToAssignOrder?order_id=" + id).pipe(
      timeout(10000),
      catchError((error) => {
        this.errorHandler.routeAccordingToError(error);
        return throwError(error)
      })
    );
  }

  assignOrderToDriver(data) {
    return this.http.post(this.base_url + "assignOrderToDriver", data).pipe(
      timeout(10000),
      catchError(this.errorHandler.handleError)
    )
  }
}
