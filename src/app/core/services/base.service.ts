import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";

@Injectable()
export class BaseService {
  // on local
  // public base_url: string = 'http://127.0.0.1:3000/api/v1/';

  //staging
  public base_url: string = "http://13.234.120.28/api/v1/";
  public ws_url: string = "http://13.234.120.28:3000/";
  // //production
  //public base_url: string = 'https://delivr.biz/api/v1/';
  //public ws_url: string = "https://delivr.biz/socketUrl/";

  //LAN IP
  // public base_url: string = "http://192.168.1.146:3000/api/v1/";
  // public ws_url: string = "http://192.168.1.146:3000/";

  constructor(private http: HttpClient) {}
}
