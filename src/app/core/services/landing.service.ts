import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { retry, catchError } from "rxjs/operators";
import { ErrorHandlerService } from "./error-handler.service";
import { BaseService } from "./base.service";
import { Observable } from 'rxjs/Observable';
import { UtilityService } from "./utility.service";

@Injectable({
  providedIn: 'root'
})
export class LandingService {

  base_url: string;

  constructor(
    private http: HttpClient,
    private bs: BaseService,
    private errorHandler: ErrorHandlerService,
    private utility: UtilityService
  ) {
    this.base_url = this.bs.base_url;
  }

  // contact form api call
  addContactRequest(data) {
    return this.http.post(this.base_url + "addContactRequest", data, { observe: "response" }).pipe(
      retry(3),
      catchError(this.errorHandler.handleError)
    );
  }
}
