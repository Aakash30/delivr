import { Injectable } from '@angular/core';
import { MessageService } from 'primeng/api';
import { HttpErrorResponse } from '@angular/common/http';
import { throwError, TimeoutError } from 'rxjs';
import { Router } from '@angular/router';
import { THROW_IF_NOT_FOUND } from '@angular/core/src/di/injector';
import jwt_decode from "jwt-decode"
import { WebsocketService } from './websocket.service';
@Injectable({
  providedIn: 'root'
})
export class ErrorHandlerService {

  role: string;
  userType: string = "";
  constructor(
    private messageService: MessageService,
    private router: Router,
    private webSocketService: WebsocketService
  ) {
    if (localStorage.getItem("token")) {
      const token = localStorage.getItem("token").replace("Bearer ", "");

      let decoded = jwt_decode(token);

      // var userRole = JSON.parse(decoded.roles);
      var user_type = decoded.type;

      if (user_type === "super_admin") {
        this.userType = "admin";
      } else if (user_type === "vendor") {
        this.userType = "business";
      }
    }

  }

  handleError(error: HttpErrorResponse) {
    let errorMessage: any;
    // let errorArray = ((error || {}).error || {}).message;
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error.message}`
      );
      errorMessage = error.error.message || error.error.message;
    }
    return throwError(error);
  }

  routeAccordingToError(error) {

    const errorMess = ((error || {}).error || {}).message;
    const errorCode = ((error || {}).error || {}).statusCode;
    if (error instanceof TimeoutError) {
      this.messageService.add({
        severity: 'error',
        summary: `${errorMess}`
      });
    } else {
      this.messageService.add({
        severity: 'error',
        summary: `${errorMess}`
      });

      // if verification link is not valid
      if (errorCode === 404) {
        this.router.navigate(['**']);
      } else if (
        (errorMess ===
          'Either invalid link or link is expired or already used' || errorMess === "Either link has been expired or your email is already verified. Please check with your business.") ||
        (errorCode === 401 && errorMess === 'You have been logged in with other device.')
      ) {
        //this.webSocketService.disconnect();
        setTimeout(() => {
          this.redirectToLogin();
        }, 2000);
      } else {
        console.log(errorMess)
      }
    }
    // generic error message toast

  }

  redirectToLogin() {

    if (localStorage.getItem('token')) {
      localStorage.removeItem('token');
      this.router.navigateByUrl(`/${this.userType != 'admin' ? 'business' : 'admin'}/login`);
    } else {
      this.router.navigateByUrl("/");
    }
  }
}
