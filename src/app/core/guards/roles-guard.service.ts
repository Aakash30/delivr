import { Injectable } from '@angular/core';
import {
  Router,
  CanActivate,
  ActivatedRouteSnapshot
} from '@angular/router';

import jwt_decode from 'jwt-decode';
import { AuthService } from '../services/auth.service';

@Injectable({
  providedIn: 'root'
})

export class RolesGuardService implements CanActivate {

  constructor(
    public router: Router,
    private _auth: AuthService
  ) {

  }

  canActivate(route: ActivatedRouteSnapshot, state: import("@angular/router").RouterStateSnapshot): boolean {


    // debugger;
    const expectedRoles = route.data.expectedRole;
    const onlyForUser = route.data.onlyForUser;
    const token = localStorage.getItem("token").replace("Bearer ", "");

    var decoded = jwt_decode(token);

    var user_roles = JSON.parse(decoded.roles);

    var user_type = decoded.type;

    let valid = false;
    expectedRoles.forEach(role => {
      if (onlyForUser.indexOf(user_type) !== -1 && user_roles.indexOf(role) !== -1) {
        valid = true;

      }
    });
    if (valid) {
      return true;
    } else {
      this.router.navigate(['']); //  route here for navigation
    }



  }


}

