import { Injectable } from '@angular/core';
import {
  Router,
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  ActivatedRoute
} from '@angular/router';
import jwt_decode from 'jwt-decode';
import { ROUTES } from 'app/shared/sidebar/sidebar-routes.config';

@Injectable({
  providedIn: 'root'
})
export class RouteGuardService implements CanActivate {
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | import("rxjs").Observable<boolean> | Promise<boolean> {
    if (localStorage.getItem('token')) {

      const token = localStorage.getItem("token").replace("Bearer ", "");

      var decoded = jwt_decode(token);

      var user_roles = JSON.parse(decoded.roles);

      var user_type = decoded.type;
      // debugger;
      if (
        route.parent.url[0].path == "admin" ||
        route.parent.url[0].path == "business"
      ) {
        // debugger;
        let selectedRoute = [];
        ROUTES.filter(routeMenu => {
          if (routeMenu.submenu.length > 0) {
            let subRole = routeMenu.submenu.filter(submenu => {
              if (user_roles.indexOf(submenu.role) !== -1 && submenu.user_type === route.parent.url[0].path)
                selectedRoute.push(submenu);
              return;
            });
            //return selectedRoute.push(subRole);
          } else {
            if (
              user_roles.indexOf(routeMenu.role) !== -1 &&
              routeMenu.user_type === route.parent.url[0].path
            )
              selectedRoute.push(routeMenu);
          }
          return;
        });
        this.router.navigateByUrl(selectedRoute[0]["path"]);

      } else {
        return true;
      }
    } else {
      this.router.navigateByUrl(route.parent.url[0].path + "/login");
      return false;
    }
  }

  constructor(public router: Router, public activatedRoute: ActivatedRoute) {

  }
}
