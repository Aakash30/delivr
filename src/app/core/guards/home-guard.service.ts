import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import jwt_decode from "jwt-decode";

@Injectable({
  providedIn: "root"
})
export class HomeGuardService implements CanActivate {
  canActivate(
    route: ActivatedRouteSnapshot,
    state: import("@angular/router").RouterStateSnapshot
  ): boolean | import("rxjs").Observable<boolean> | Promise<boolean> {
    // debugger;
    if (!localStorage.getItem("token")) {
      return true;
    }
    else {
      const token = localStorage.getItem("token").replace("Bearer ", "");

      let decoded = jwt_decode(token);

      // var userRole = JSON.parse(decoded.roles);
      var user_type = decoded.type;

      if (user_type === "super_admin") {
        var userRole = "admin";
      } else if (user_type === "vendor") {
        var userRole = "business";
      }
      this.router.navigateByUrl(`/${userRole}`);
    }
  }
  constructor(public router: Router) { }
}
