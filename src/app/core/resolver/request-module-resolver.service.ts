import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { map, catchError } from 'rxjs/operators';
import { ErrorHandlerService } from 'app/core/services/error-handler.service';
import { throwError } from 'rxjs';
import { RequestModuleService } from '../services/request-module.service';

@Injectable({
    providedIn: 'root'
})

export class RequestModuleResolverService implements Resolve<any>{
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {

        return this.requestModule.getPage(route.routeConfig.path, route.params).pipe(
            map(data => {
                return data;
            }),
            catchError(error => {
                this.errorHandler.routeAccordingToError(error);
                return throwError(error);
            })
        );
    }

    constructor(private requestModule: RequestModuleService, private errorHandler: ErrorHandlerService) { }
}
