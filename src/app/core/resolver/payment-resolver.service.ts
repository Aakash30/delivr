import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { map, catchError } from 'rxjs/operators';
import { ErrorHandlerService } from 'app/core/services/error-handler.service';
import { throwError } from 'rxjs';
import { PaymentService } from 'app/core/services/payment.service';

@Injectable({
  providedIn: 'root'
})
export class PaymentResolverService implements Resolve<any> {
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.pay.getPage().pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        //this.errorHandler.routeAccordingToError(error);
        return throwError(error);
      })
    );
  }

  constructor(private pay: PaymentService, private errorHandler: ErrorHandlerService) { }
}
