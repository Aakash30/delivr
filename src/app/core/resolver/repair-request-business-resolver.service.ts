import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { ErrorHandlerService } from '../services/error-handler.service';
import { map, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { RepairRequestBusinessService } from '../services/repair-request-business.service';

@Injectable({
  providedIn: 'root'
})
export class RepairRequestBusinessResolverService implements Resolve<any>{
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this._repairRequestService.getPage(route.routeConfig.path, route.params).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        this._errorHandler.routeAccordingToError(error);
        return throwError(error);
      })
    );
  }

  constructor(private _repairRequestService: RepairRequestBusinessService, private _errorHandler: ErrorHandlerService) { }
}
