import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { map, catchError } from 'rxjs/operators';
import { ErrorHandlerService } from 'app/core/services/error-handler.service';
import { throwError } from 'rxjs';
import { VendorVehicleService } from 'app/core/services/vendor-vehicle.service';

@Injectable({
  providedIn: 'root'
})
export class VendorVehicleResolverService implements Resolve<any>{
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.vehicleService.getPage(route.routeConfig.path, route.params).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        this.errorHandler.routeAccordingToError(error);
        return throwError(error);
      })
    );
  }

  constructor(private vehicleService: VendorVehicleService, private errorHandler: ErrorHandlerService) { }
}
