import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { map, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { AuthService } from '../services/auth.service';
import { ErrorHandlerService } from '../services/error-handler.service';

@Injectable({
  providedIn: 'root'
})
export class ResetResolverService implements Resolve<any>{
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {

    let dataToSend = { verify: route.params.token, userType: route.data.requestFor }
    return this.auth.verify(dataToSend).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        this.errorHandler.routeAccordingToError(error);
        return throwError(error);
      })
    );
  }

  constructor(private auth: AuthService, private errorHandler: ErrorHandlerService) { }
}
