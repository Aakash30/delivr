import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { map, catchError } from 'rxjs/operators';
import { ErrorHandlerService } from 'app/core/services/error-handler.service';
import { throwError } from 'rxjs';
import { CountryService } from 'app/core/services/country-city.service';

@Injectable({
  providedIn: 'root'
})
export class CountryResolverService implements Resolve<any> {
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.countryService.getPage(route.routeConfig.path, route.params).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        this.errorHandler.routeAccordingToError(error);
        return throwError(error);
      })
    );
  }

  constructor(private countryService: CountryService, private errorHandler: ErrorHandlerService) { }
}
