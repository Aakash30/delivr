import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { RepairRequestAdminService } from '../services/repair-request-admin.service';
import { ErrorHandlerService } from '../services/error-handler.service';
import { map, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RepairRequestAdminResolverService implements Resolve<any>{
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this._repairRequestService.getPage(route.routeConfig.path, route.params).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        this._errorHandler.routeAccordingToError(error);
        return throwError(error);
      })
    );
  }

  constructor(private _repairRequestService: RepairRequestAdminService, private _errorHandler: ErrorHandlerService) { }
}
