import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { map, catchError } from 'rxjs/operators';
import { ErrorHandlerService } from 'app/core/services/error-handler.service';
import { throwError } from 'rxjs';
import { BrandModelSettingService } from '../services/brand-model-setting.service';

@Injectable({
  providedIn: 'root'
})
export class BrandModelSettingResolverService implements Resolve<any>{
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.brandModelService.getPage(route.routeConfig.path, route.params).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        this.errorHandling.routeAccordingToError(error);
        return throwError(error);
      })
    );
  }

  constructor(private brandModelService: BrandModelSettingService, private errorHandling: ErrorHandlerService) { }
}
