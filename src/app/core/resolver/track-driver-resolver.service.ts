import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { map, catchError } from 'rxjs/operators';
import { ErrorHandlerService } from 'app/core/services/error-handler.service';
import { throwError } from 'rxjs';
import { DriverTrackerService } from 'app/core/services/driver-tracker.service';

@Injectable({
  providedIn: 'root'
})
export class TrackDriverResolverService implements Resolve<any> {

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {

    return this.driverTrackerServices.getPage(route.routeConfig.path, route.params).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        this.errorHandler.routeAccordingToError(error);
        return throwError(error);
      })
    );
  }
  constructor(private driverTrackerServices: DriverTrackerService, private errorHandler: ErrorHandlerService) { }
}
