import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { map, catchError } from 'rxjs/operators';
import { ErrorHandlerService } from 'app/core/services/error-handler.service';
import { throwError } from 'rxjs';
import { VehicleService } from '../services/vehicle.service';
import { NewVehicleService } from '../services/new-vehicle.service';

@Injectable({
    providedIn: 'root'
})

export class NewVehicleResolverService implements Resolve<any>{
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        return this.newVehicleService.getPage(route.routeConfig.path, route.params).pipe(
            map(data => {
                return data;
            }),
            catchError(error => {
                this.errorHandler.routeAccordingToError(error);
                return throwError(error);
            })
        );
    }

    constructor(private newVehicleService: NewVehicleService, private errorHandler: ErrorHandlerService) { }
}
