import { Injectable } from "@angular/core";
import {
  Resolve,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from "@angular/router";
import { map, catchError } from "rxjs/operators";
import { ErrorHandlerService } from "app/core/services/error-handler.service";
import { throwError } from "rxjs";
import { CouponManagementService } from "app/core/services/coupon-management.service";


@Injectable({
  providedIn: "root"
})
export class CouponResolverService implements Resolve<any> {
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {

    //coupon list service
    return this.couponService.getPage(route.routeConfig.path, route.params).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        // this.errorHandler.routeAccordingToError(error);
        return throwError(error);
      })
    );
  }

  constructor(
    private couponService: CouponManagementService,
    private errorHandler: ErrorHandlerService
  ) { }
}
