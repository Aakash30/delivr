import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { map, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { VehicleRequestService } from '../services/vehicle-request.service';
import { ErrorHandlerService } from '../services/error-handler.service';

@Injectable({
  providedIn: 'root'
})
export class VehicleRequestResolverService implements Resolve<any>{
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.vehicleService.getPage(route.routeConfig.path, route.params).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        this.errorHandler.routeAccordingToError(error);
        return throwError(error);
      })
    );
  }

  constructor(private vehicleService: VehicleRequestService, private errorHandler: ErrorHandlerService) { }
}
