import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { map, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { AdminServiceSettingsService } from '../services/admin-service-settings.service';
import { ErrorHandlerService } from '../services/error-handler.service';

@Injectable({
  providedIn: 'root'
})
export class AdminServiceSettingResolverService implements Resolve<any>{

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.seriveSetting.getPage(route.routeConfig.path, route.params).pipe(
      map(data => {
        return data;
      }),
      catchError(error => {
        this.errorHandler.routeAccordingToError(error);
        return throwError(error);
      })
    );
  }
  constructor(private seriveSetting: AdminServiceSettingsService, private errorHandler: ErrorHandlerService) { }
}
