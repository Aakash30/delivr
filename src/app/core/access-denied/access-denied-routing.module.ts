import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AccessDeniedComponent } from './pages/access-denied/access-denied.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        redirectTo: 'access-denied',
        pathMatch: 'full',
        data: {
          sectionTitle: 'Access Denied',
        }
      },
      {
        path: 'dashboard',
        component: AccessDeniedComponent,
        data: {
          sectionTitle: 'Access Denied',
        },
      },

    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccessDeniedRoutingModule { }
