import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AccessDeniedRoutingModule } from './access-denied-routing.module';
import { AccessDeniedComponent } from './pages/access-denied/access-denied.component';
import { SharedModule } from 'app/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    AccessDeniedRoutingModule
  ],
  declarations: [AccessDeniedComponent]
})
export class AccessDeniedModule { }
