import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from 'app/auth/login/login.component';

export const LANDING_ROUTES: Routes = [
    {
        path: '',
        loadChildren: './landing/landing.module#LandingModule'
    }
];