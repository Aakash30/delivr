import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from 'app/auth/login/login.component';

export const CONTENT_ROUTES: Routes = [
    {
        path: 'admin',
        loadChildren: './auth/auth.module#AuthModule'
    }
];
