import { Routes, RouterModule } from '@angular/router';
import { BusinessComponent } from 'app/landing/pages/business/business.component';
import { FullLayoutComponent } from 'app/layouts/full/full-layout.component';
import { ContentLayoutComponent } from 'app/layouts/content/content-layout.component';
import { LoginComponent } from 'app/auth/login/login.component';
import { RecoverPasswordComponent } from 'app/auth/recover-password/recover-password.component';
import { ResetPasswordComponent } from 'app/auth/reset-password/reset-password.component';
import { RolesGuardService as RoleGuard } from '../guards/roles-guard.service';
import { ResetResolverService } from '../resolver/reset-resolver.service';
import { AuthGuardService as AuthGuard } from '../guards/auth-guard.service';
import { RouteGuardService } from '../guards/route-guard.service';
import { AccessDeniedComponent } from '../access-denied/pages/access-denied/access-denied.component';
import { ActivateUserCheckResolverService } from '../resolver/activate-user-check-resolver.service';
import { ActivateAccountComponent } from 'app/auth/activate-account/activate-account.component';
import { HomeGuardService } from '../guards/home-guard.service';
//Route for content layout with sidebar, navbar and footer.

export const Full_ROUTES: Routes = [
  {
    path: "admin",
    children: [
      {
        path: "",
        component: ContentLayoutComponent,
        canActivate: [RouteGuardService]
      },
      {
        path: "",
        component: FullLayoutComponent,
        loadChildren: "./vehicles/vehicles.module#VehiclesModule",
        canActivate: [AuthGuard, RoleGuard],
        data: {
          onlyForUser: ["super_admin"],
          expectedRole: ["vehicle-management", "vehicle-inventory"]
        }
      },
      {
        path: "login",
        component: LoginComponent
      },
      {
        path: "recover-password",
        component: RecoverPasswordComponent
      },
      {
        path: "reset-password/:token",
        component: ResetPasswordComponent,
        resolve: {
          pageData: ResetResolverService
        },
        data: {
          requestFor: "super_admin"
        }
      },
      {
        path: "dashboard",
        component: FullLayoutComponent,
        loadChildren: "./dashboard/dashboard.module#DashboardModule",
        canActivate: [AuthGuard, RoleGuard],
        data: {
          onlyForUser: ["super_admin"],
          expectedRole: ["dashboard"]
        }
      },
      // {
      //   path: 'country',
      //   component: FullLayoutComponent,
      //   loadChildren: './country/country.module#CountryModule',
      //   canActivate: [AuthGuard, RoleGuard],
      //   data: {
      //     onlyForUser: ['super_admin'],
      //     expectedRole: ['location-setting']
      //   }
      // },
      {
        path: "",
        component: FullLayoutComponent,
        loadChildren: "./business/business.module#BusinessModule",
        canActivate: [AuthGuard, RoleGuard],
        data: {
          onlyForUser: ["super_admin"],
          expectedRole: ["business-management", "business-category"]
        }
      },
      {
        path: "maintenance",
        component: FullLayoutComponent,
        loadChildren:
          "./maintenance/maintenance.module#MaintenanceModule",
        canActivate: [AuthGuard, RoleGuard],
        data: {
          onlyForUser: ["super_admin"],
          expectedRole: ["maintenance"]
        }
      },
      {
        path: "payment",
        component: FullLayoutComponent,
        loadChildren: "./payment/payment.module#PaymentModule",
        canActivate: [AuthGuard, RoleGuard],
        data: {
          onlyForUser: ["super_admin"],
          expectedRole: ["payments"]
        }
      },
      {
        path: "",
        component: FullLayoutComponent,
        loadChildren: "./coupon/coupon.module#CouponModule",
        canActivate: [AuthGuard, RoleGuard],
        data: {
          onlyForUser: ["super_admin"],
          expectedRole: ["view-coupons"]
        }
      },
      {
        path: "request-for-repair",
        component: FullLayoutComponent,
        loadChildren:
          "./request-for-repair/request-for-repair.module#RequestForRepairModule",
        canActivate: [AuthGuard, RoleGuard],
        data: {
          onlyForUser: ["super_admin"],
          expectedRole: ["repair-request-management"]
        }
      },
      {
        path: "request-for-module",
        component: FullLayoutComponent,
        loadChildren:
          "./request-for-module/request-for-module.module#RequestForModuleModule",
        canActivate: [AuthGuard, RoleGuard],
        data: {
          onlyForUser: ["super_admin"],
          expectedRole: ["module-request-management"]
        }
      },
      {
        path: "request-for-new-vehicle",
        component: FullLayoutComponent,
        loadChildren:
          "./request-for-new-vehicle/request-for-new-vehicle.module#RequestForNewVehicleModule",
        canActivate: [AuthGuard, RoleGuard],
        data: {
          onlyForUser: ["super_admin"],
          expectedRole: ["vehicle-request-management"]
        }
      },
      {
        path: "report",
        component: FullLayoutComponent,
        loadChildren: "./report/report.module#ReportModule",
        canActivate: [AuthGuard, RoleGuard],
        data: {
          onlyForUser: ["super_admin"],
          expectedRole: ["reports-management"]
        }
      },
      {
        path: "profile",
        component: FullLayoutComponent,
        loadChildren: "./profile/profile.module#ProfileModule",
        canActivate: [AuthGuard],
        data: {
          onlyForUser: ["super_admin"]
        }
      },
      {
        path: "notification",
        component: FullLayoutComponent,
        loadChildren:
          "./notification/notification.module#NotificationModule",
        canActivate: [AuthGuard],
        data: {
          onlyForUser: ["super_admin"]
        }
      },
      {
        path: "",
        component: FullLayoutComponent,
        loadChildren:
          "./system-settings/system-settings.module#SystemSettingsModule",
        canActivate: [AuthGuard, RoleGuard],
        data: {
          onlyForUser: ["super_admin"],
          expectedRole: [
            "location-setting",
            "service-setting",
            "service-category-setting",
            "brand-model-setting"
          ]
        }
      }
    ]
  },
  {
    path: "business",
    children: [
      {
        path: "",
        component: ContentLayoutComponent,
        canActivate: [RouteGuardService]
      },
      {
        path: "login",
        component: LoginComponent,
        canActivate: [HomeGuardService]
      },
      {
        path: "recover-password",
        component: RecoverPasswordComponent,
      },
      {
        path: "reset-password/:token",
        component: ResetPasswordComponent,
        resolve: {
          pageData: ResetResolverService
        },
        data: {
          requestFor: "business"
        }
      },
      {
        path: "dashboard",
        component: FullLayoutComponent,
        loadChildren:
          "./business-user/vendor-dashboard/vendor-dashboard.module#VendorDashboardModule",
        canActivate: [AuthGuard, RoleGuard],
        data: {
          onlyForUser: ["vendor", "sub_ordinate"],
          expectedRole: ["dashboard"]
        }
      },
      {
        path: "order-management",
        component: FullLayoutComponent,
        loadChildren:
          "./business-user/vendor-order-management/vendor-order-management.module#VendorOrderManagementModule",
        canActivate: [AuthGuard, RoleGuard],
        data: {
          onlyForUser: ["vendor", "sub_ordinate"],
          expectedRole: ["order-management"]
        }
      },
      {
        path: "",
        component: FullLayoutComponent,
        loadChildren:
          "./business-user/vendor-driver-management/vendor-driver-management.module#VendorDriverManagementModule",
        canActivate: [AuthGuard, RoleGuard],
        data: {
          onlyForUser: ["vendor", "sub_ordinate"],
          expectedRole: ["driver-management", "driver-feedback"]
        }
      },
      {
        path: "",
        component: FullLayoutComponent,
        loadChildren:
          "./business-user/vendor-vehicle-management/vendor-vehicle-management.module#VendorVehicleManagementModule",
        canActivate: [AuthGuard, RoleGuard],
        data: {
          onlyForUser: ["vendor", "sub_ordinate"],
          expectedRole: ["vehicle-details", "vehicle-lease-plan"]
        }
      },
      {
        path: "branch",
        component: FullLayoutComponent,
        loadChildren:
          "./business-user/vendor-branch/vendor-branch.module#VendorBranchModule",
        canActivate: [AuthGuard, RoleGuard],
        data: {
          onlyForUser: ["vendor", "sub_ordinate"],
          expectedRole: ["branch-management"]
        }
      },
      {
        path: "track-driver",
        component: FullLayoutComponent,
        loadChildren:
          "./business-user/vendor-track-driver/vendor-track-driver.module#VendorTrackDriverModule",
        canActivate: [AuthGuard, RoleGuard],
        data: {
          onlyForUser: ["vendor", "sub_ordinate"],
          expectedRole: ["track-your-driver"]
        }
      },
      {
        path: "maintenance",
        component: FullLayoutComponent,
        loadChildren:
          "./business-user/vendor-maintenance/vendor-maintenance.module#VendorMaintenanceModule",
        canActivate: [AuthGuard, RoleGuard],
        data: {
          onlyForUser: ["vendor", "sub_ordinate"],
          expectedRole: ["maintenance"]
        }
      },
      {
        path: "request-for-vehicle",
        component: FullLayoutComponent,
        loadChildren:
          "./business-user/vendor-request-for-vehicle/vendor-request-for-vehicle.module#VendorRequestForVehicleModule",
        canActivate: [AuthGuard, RoleGuard],
        data: {
          onlyForUser: ["vendor", "sub_ordinate"],
          expectedRole: ["raise-vehicle-request"]
        }
      },
      {
        path: "tickets",
        component: FullLayoutComponent,
        loadChildren:
          "./vendor-tickets/vendor-tickets.module#VendorTicketsModule",
        canActivate: [AuthGuard, RoleGuard],
        data: {
          onlyForUser: ["vendor", "sub_ordinate"],
          expectedRole: ["ticket-management"]
        }
      },
      {
        path: "accounts",
        component: FullLayoutComponent,
        loadChildren:
          "./business-user/vendor-accounts/vendor-accounts.module#VendorAccountsModule",
        canActivate: [AuthGuard, RoleGuard],
        data: {
          onlyForUser: ["vendor", "sub_ordinate"],
          expectedRole: ["accounts-management"]
        }
      },
      {
        path: "admin-list",
        component: FullLayoutComponent,
        loadChildren:
          "./business-user/vendor-admin/vendor-admin.module#VendorAdminModule",
        canActivate: [AuthGuard, RoleGuard],
        data: {
          onlyForUser: ["vendor", "sub_ordinate"],
          expectedRole: ["user-management"]
        }
      },
      {
        path: "",
        component: FullLayoutComponent,
        loadChildren:
          "./business-user/coupon/coupon.module#CouponModule",
        canActivate: [AuthGuard, RoleGuard],
        data: {
          onlyForUser: ["vendor", "sub_ordinate"],
          expectedRole: ["coupon-management"]
        }
      },

      {
        path: 'profile',
        component: FullLayoutComponent,
        loadChildren: './profile/profile.module#ProfileModule',
        canActivate: [AuthGuard],
        data: {
          onlyForUser: ['vendor', 'sub_ordinate'],
        }
      },
      {
        path: 'notification',
        component: FullLayoutComponent,
        loadChildren: './notification/notification.module#NotificationModule',
        canActivate: [AuthGuard],
        data: {
          onlyForUser: ['vendor', 'sub_ordinate'],
        }
      },
      {
        path: "request-for-repair",
        component: FullLayoutComponent,
        loadChildren:
          "./business-user/request-for-repair/request-for-repair.module#RequestForRepairModule",
        canActivate: [AuthGuard, RoleGuard],
        data: {
          onlyForUser: ["vendor", "sub_ordinate"],
          expectedRole: ["request-for-repair"]
        }
      },
      {
        path: 'activate/:verify',
        component: ActivateAccountComponent,
        resolve: {
          pageData: ActivateUserCheckResolverService
        }
      },
    ]
  },
  // {
  //   path: 'landing',
  //   children: [
  //     {
  //       path: '',
  //       redirectTo: 'landing',
  //       pathMatch: 'full',
  //     },
  //     {
  //       path: 'landing',
  //       loadChildren: './landing/landing.module#LandingModule'
  //     },
  //   ]
  // }

];
