import { Component, OnInit } from '@angular/core';
import { Location } from "@angular/common";
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators, AbstractControl, FormBuilder, FormArray } from '@angular/forms';
import { OrderManagementService } from 'app/core/services/order-management.service';

@Component({
  selector: 'app-profile-form-page',
  templateUrl: './profile-form-page.component.html',
  styleUrls: ['./profile-form-page.component.scss']
})
export class ProfileFormPageComponent implements OnInit {

  profileForm: FormGroup;
  sectionTitle: string;
  buttonName: string;
  isSubmitted: boolean = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private orderService: OrderManagementService,
    private _location: Location,
  ) {
    this.sectionTitle = route.snapshot.data['sectionTitle'];
    this.buttonName = route.snapshot.data['buttonName'];
  }
  countryCode: String;
  ngOnInit() {

    this.profileForm = new FormGroup({
      firstName: new FormControl('', [
        Validators.required
      ]),
      lastName: new FormControl('', [
        Validators.required
      ]),
      contact: new FormControl('', [Validators.required]),
      alternate_number: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required]),
      agreement: new FormControl(''),
      license: new FormControl(''),
      bank_statement: new FormControl(''),
      emirates_id: new FormControl(''),
      passport: new FormControl(''),
      other_document: new FormControl('')
    });
  }

  // profile form page submission
  onSubmit() {
    this.isSubmitted = true;
    if (this.profileForm.valid) {
    }
  }

  resetForm() {
    this.profileForm.reset();
    this._location.back();
  }
  uploadImage(event, key) {

  }
}
