import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { VehicleService } from 'app/core/services/vehicle.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { MessageService, ConfirmationService } from 'primeng/api';
import { FileUploadService } from 'app/core/services/file-upload.service';
import { UtilityService } from 'app/core/services/utility.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ProfileService } from 'app/core/services/profile.service';
import jwt_decode from "jwt-decode"
import { ErrorHandlerService } from 'app/core/services/error-handler.service';


@Component({
  selector: 'app-profile-page',
  templateUrl: './profile-page.component.html',
  styleUrls: ['./profile-page.component.scss']
})
export class ProfilePageComponent implements OnInit {

  businessData: any;
  profileContent: any;
  profileForm: FormGroup;
  userType: string;
  businessBranchList: any[];

  imageUrl: any;
  editTypeForm: any;
  imageDisplayFlag: boolean = false;
  editTextFlag: boolean = false;
  isProfileSubmitted: boolean = false;

  constructor(
    private route: ActivatedRoute,
    public utility: UtilityService,
    private profileService: ProfileService,
    private _errorHandling: ErrorHandlerService

  ) { }

  ngOnInit() {

    this.businessBranchList = [];
    this.profileContent = this.route.snapshot.data.pageData.data;
    console.log('profile content::', this.profileContent);

    if (this.profileContent.Business !== null) {
      this.businessData = this.profileContent.Business;
      this.businessBranchList = this.businessData.businessBranch;
    }

    this.profileForm = new FormGroup({
      first_name: new FormControl('', [
        Validators.required
      ]),
      last_name: new FormControl('', [
        Validators.required
      ]),
      mobile_number: new FormControl('', [Validators.required, Validators.minLength(9)]),
      alternate_number: new FormControl('', [Validators.required]),
      email_id: new FormControl('', [Validators.required]),
      agreement: new FormControl(''),
      license: new FormControl(''),
      bank_statement: new FormControl(''),
      emirates_id: new FormControl(''),
      passport: new FormControl(''),
      other_document: new FormControl('')
    });

    if (localStorage.getItem('token')) {
      const token = localStorage.getItem("token").replace("Bearer ", "");
      let decoded = jwt_decode(token);

      var user_type = decoded.type;
      if (user_type === "super_admin") {
        this.userType = "admin";
      } else if (user_type === "vendor") {
        this.userType = "business";
      }
    }
  }

  openImageDialog(type, imageObject, key) {
    this.imageDisplayFlag = true;

    switch (type) {
      case type = 'agreement':
        this.imageUrl = imageObject;
        break;

      case type = 'license':
        this.imageUrl = imageObject;
        break;
    }
  }


  patchFormValue(type) {

    // this.editTextFlag = true;
    this.editTypeForm = type;
    if (this.editTypeForm == type) {
      this.editTextFlag = true;
    }
    this.profileForm.controls.first_name.patchValue(this.profileContent.first_name);
    this.profileForm.controls.last_name.patchValue(this.profileContent.last_name);

    if (this.profileContent.Business != null) {
      this.profileForm.controls.mobile_number.patchValue(this.businessData.mobile_number);
      this.profileForm.controls.alternate_number.patchValue(this.businessData.alternate_number);
    } else if (this.profileContent.Business == null) {
      this.profileForm.controls.mobile_number.patchValue(this.profileContent.mobile_number);
    }

  }


  closeFormField(type) {

    if (this.editTypeForm == type) {
      this.editTextFlag = false;
      this.editTypeForm = '';
    }
  }

  onProfileSubmit() {
    let data = {};
    this.isProfileSubmitted = true;
    switch (this.editTypeForm) {
      case this.editTypeForm = 'first_name':
        data = {
          first_name: this.profileForm.controls.first_name.value
        }
        break;

      case this.editTypeForm = 'last_name':
        data = {
          last_name: this.profileForm.controls.last_name.value
        }
        break;

      case this.editTypeForm = 'mobile_number':
        data = {
          mobile_number: this.profileForm.controls.mobile_number.value
        }
        break;

      case this.editTypeForm = 'alternate_number':
        data = {
          alternate_number: this.profileForm.controls.alternate_number.value
        }
        break;
    }
    this.utility.loaderStart();
    this.profileService.updateUserProfile(data).subscribe(
      (success: any) => {

        // if (this.editTypeForm == 'first_name') {
        //   this.profileContent.first_name = success.data.first_name;
        // }
        // if (this.editTypeForm == 'last_name') {
        //   this.profileContent.last_name = success.data.last_name;
        // }
        // if (this.editTypeForm == 'mobile_number') {
        //   this.businessData.mobile_number = success.data.mobile_number;
        // }
        // if (this.editTypeForm == 'alternate_number') {
        //   this.businessData.alternate_number = success.data.alternate_number;
        // }
        switch (this.editTypeForm) {

          case 'first_name':
            this.profileContent.first_name = success.data.first_name;
            break;

          case 'last_name':
            this.profileContent.last_name = success.data.last_name;
            break;

          case 'mobile_number':
            if (this.profileContent.Business !== null) {
              this.businessData.mobile_number = success.data.mobile_number;
            } else {
              this.profileContent.mobile_number = success.data.mobile_number;
            }
            break;

          case 'alternate_number':
            if (this.profileContent.Business !== null) {
              this.businessData.alternate_number = success.data.alternate_number;
            } else {
              this.profileContent.alternate_number = success.data.alternate_number;
            }
            break;

        }

        this.editTypeForm = '';
        this.utility.loaderStop();
      },
      error => {
        this._errorHandling.routeAccordingToError(error);
        this.utility.loaderStop();
      }
    )
  }

  fileUploadImage(event) {
    // let file = {
    //   profile: event.target.files[0].name
    // }
    let file = event.target.files[0];
    this.utility.loaderStart();
    this.profileService.updateUserProfile({}, file).subscribe(
      (success: any) => {
        this.profileContent.profile_image = success.data.profile_image;
        this.utility.loaderStop();
      },
      error => {
        this._errorHandling.routeAccordingToError(error);
        this.utility.loaderStop();
      }
    )
  }

  fileUploadBusinessImage(event) {
    let file = event.target.files[0];
    this.utility.loaderStart();
    this.profileService.updateBusinessProfile({}, file).subscribe(
      (success: any) => {
        this.businessData.business_logo = success.data.business_logo;
        this.utility.loaderStop();
      },
      error => {
        this._errorHandling.routeAccordingToError(error);
        this.utility.loaderStop();
      }
    )
  }

}
