import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProfilePageComponent } from './pages/profile-page/profile-page.component';
import { ProfileFormPageComponent } from './pages/profile-form-page/profile-form-page.component';
import { ProfileResolverService } from 'app/core/resolver/profile-resolver.service';
import { AccessDeniedComponent } from './pages/access-denied/access-denied.component';

const routes: Routes = [
  {
    path: '',
    component: ProfilePageComponent,
    data: {
      sectionTitle: 'Details',
    },
    resolve: {
      pageData: ProfileResolverService
    }
  },
  {
    path: 'details',
    component: ProfileFormPageComponent,
    data: {
      sectionTitle: 'Profile',
      buttonName: 'Update'
    }

  },
  {
    path: 'access-denied',
    component: AccessDeniedComponent,
    data: {
      sectionTitle: 'Profile',
      buttonName: 'Update'
    }

  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProfileRoutingModule { }
