import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfileRoutingModule } from './profile-routing.module';
import { ProfilePageComponent } from './pages/profile-page/profile-page.component';
import { ProfileFormPageComponent } from './pages/profile-form-page/profile-form-page.component';
import { SharedModule } from 'app/shared/shared.module';
import { AccessDeniedComponent } from './pages/access-denied/access-denied.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    ProfileRoutingModule
  ],
  declarations: [
    ProfilePageComponent,
    ProfileFormPageComponent,
    AccessDeniedComponent
  ]
})
export class ProfileModule { }
