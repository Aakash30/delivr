import { Component, OnInit } from '@angular/core';
import { trigger, transition, style, animate } from '@angular/animations';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { CouponManagementService } from 'app/core/services/coupon-management.service';
import { takeUntil, map } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { UtilityService } from 'app/core/services/utility.service';
import { ErrorHandlerService } from 'app/core/services/error-handler.service';

interface Country {
  name: string;
  code: string;
}

interface City {
  name: string;
  code: string;
}

interface User {
  name: string;
  code: string;
}

@Component({
  selector: 'app-coupon-list',
  templateUrl: './coupon-list.component.html',
  styleUrls: ['./coupon-list.component.scss'],
  animations: [
    trigger('openClose', [
      transition(':enter', [
        style({ transform: 'translateX(20%)', opacity: 0 }),
        animate('500ms', style({ transform: 'translateX(0)', opacity: 1 })),
      ]),
      transition(':leave', [
        style({ transform: 'translateX(0)', opacity: 1, display: 'none' }),
        animate('500ms', style({ transform: 'translateX(20%)', opacity: 0 })),
      ]),
    ])
  ],
})
export class CouponListComponent implements OnInit {


  couponData: any = [];
  offset: number = 0;
  code: string = "";
  total: number = 0;
  today = new Date();
  expiredDate: Date;
  business;

  private _unsubscribe = new Subject<Boolean>();

  constructor(
    private _route: ActivatedRoute,
    private couponManagementService: CouponManagementService,
    private _utility: UtilityService,
    private errorHandling: ErrorHandlerService
  ) { }

  isSubmitted: Boolean = false;
  couponListForm: FormGroup;
  // show filter function
  isVisible: boolean = false;

  countryListFromAPI: string[];
  cityListFromAPI: string[];
  userListFromAPI: string[];
  businessListFromAPI: any[];

  rangeValues: number[] = [0, 100];

  showFilter() {
    this.isVisible = !this.isVisible;
  }


  ngOnInit() {

    const pageContent = this._route.snapshot.data.pageData.data;

    this.couponData = pageContent.couponList;
    this.total = pageContent.total;

    this.businessListFromAPI = pageContent.business;

    this.business = this._utility.arrayOfObjectToConvertInDropdownFormat(this.businessListFromAPI, "business_name", "business_id");

    this.couponListForm = new FormGroup({
      business: new FormControl(null),
      discount_range: new FormControl([0, 100]),
      expiring_on: new FormControl(null)
    });

  }

  /**
   * paginator
   * @param event pagination event stores page and offset details
   */
  paginate(event) {
    if (this.offset != undefined) {
      this.offset = parseInt(event.first);
      this.searchListing();
    } else {
      this.offset = parseInt(event.first);
    }
  }

  /**
   * list down coupon
   */
  searchListing() {
    this._utility.loaderStart();
    let data: any = {};

    /**prepare filter data */
    if (this.offset != 0) {
      data.offset = this.offset;
    }

    if (this.code.trim() != "") {
      data.code = this.code.trim();
    }
    if (this.couponListForm.value.business != null) {
      data.business = this.couponListForm.value.business;
    }

    if (this.couponListForm.value.expiring_on != null) {
      data.expired_on = this.couponListForm.value.expiring_on.toString();
    }

    this.couponManagementService.getCoupons(data).pipe(map((success: any) => {
      if (success) {
        return success.data;
      }
    }), takeUntil(this._unsubscribe)).subscribe(
      (success: any) => {
        this.couponData = success.couponList;
        this.total = success.total;
        this._utility.loaderStop();
      },
      error => {
        this._utility.loaderStop();

        this.errorHandling.routeAccordingToError(error);
      }
    )

  }

  /**keyword search */
  searchText(event) {

    this.code = event.target.value;
    if (event.charCode == 13) {
      // on click of enter
      this.searchListing();
    }
  }

  /**clear keyword search */
  clearInput(event) {
    this.code = event;
    this.searchListing();

  }

  /**submits the filter */
  onSubmit() {
    this.isSubmitted = true;
    if (this.couponListForm.dirty && this.couponListForm.touched) {
      this.searchListing();
    }
    this.isSubmitted = false;
    this.isVisible = false;
  }


  /**
     * clear filter
     */
  clearFilter() {
    this.couponListForm.reset();
    this.couponListForm.controls.business.setValue(null);
    this.isSubmitted = false;
    this.isVisible = false;
  }

  closeFilter() {
    this.isVisible = false;
  }
  ngOnDestroy() {
    this._unsubscribe.next(true);
    this._unsubscribe.complete();
  }

}
