import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CouponRoutingModule } from './coupon-routing.module';
import { CouponListComponent } from './pages/coupon-list/coupon-list.component';
import { SharedModule } from 'app/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    CouponRoutingModule,
    SharedModule
  ],
  declarations: [CouponListComponent]
})
export class CouponModule { }
