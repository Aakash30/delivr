import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CouponListComponent } from './pages/coupon-list/coupon-list.component';
import { CouponResolverService } from 'app/core/resolver/coupon-resolver.service';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'coupon',
        component: CouponListComponent,
        data: {
          sectionTitle: 'Coupon',
        },
        resolve: {
          pageData: CouponResolverService
        }
      }

    ]
  }


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CouponRoutingModule { }
