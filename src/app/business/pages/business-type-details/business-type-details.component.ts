import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { validateAllFormFields, noWhitespaceValidator, blankSpaceInputNotValid } from '../../../shared/utils/custom-validators';
import { BusinessService } from 'app/core/services/business.service';
import { MessageService } from 'primeng/api';
import { ActivatedRoute, Router } from '@angular/router';
import { UtilityService } from 'app/core/services/utility.service';
import { ErrorHandlerService } from 'app/core/services/error-handler.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-business-type-details',
  templateUrl: './business-type-details.component.html',
  styleUrls: ['./business-type-details.component.scss']
})
export class BusinessTypeDetailsComponent implements OnInit {
  // addDisplay: Boolean = false;
  checked: boolean;
  businessType: any;
  businessTypeId: any;
  editDisplay: Boolean = false;
  isSubmitted: Boolean = false;
  vendorTypeDetailsForm: FormGroup;
  businessList: any[];

  editable: {
    businessType: string;
  };

  private _unsubscribe = new Subject<Boolean>();
  // addCountry() {
  //   this.addDisplay = true;
  // }
  editVendor(businessType) {

    this.vendorTypeDetailsForm.controls.vendorType.setValue(businessType);
    this.editable = {
      businessType: businessType
    }
    this.editDisplay = true;
  }

  // show filter function
  isVisible: boolean = false;

  showFilter() {
    this.isVisible = !this.isVisible;

  }

  closeFilter() {
    this.isVisible = false;
  }

  constructor(
    private route: ActivatedRoute,
    private businessService: BusinessService,
    private messageService: MessageService,
    private _router: Router,
    public utiltiy: UtilityService,
    private errorHandling: ErrorHandlerService
  ) {
    this.businessTypeId = utiltiy.base64Decode(this.route.snapshot.paramMap.get('id'));
  }

  ngOnInit() {
    this.editable = { businessType: '' };
    this.vendorTypeDetailsForm = new FormGroup({
      vendorType: new FormControl('', [Validators.required, noWhitespaceValidator, blankSpaceInputNotValid]),
    });
    const pageContent = this.route.snapshot.data.pageData.data;

    this.businessType = pageContent.businessTypeData;
    this.businessList = pageContent.businessData;
  }


  onSubmit() {
    this.isSubmitted = true;
    if (this.vendorTypeDetailsForm.valid) {

      let data = { "businessType": this.vendorTypeDetailsForm.value.vendorType }
      this.businessService.updatebusinesstype(this.businessTypeId, data).pipe(takeUntil(this._unsubscribe)).subscribe(
        (success: any) => {
          this.businessType = success.data[0];
          this.messageService.add({ severity: 'success', summary: "Success", detail: success.message });
        },
        error => {
          this.errorHandling.routeAccordingToError(error);
        }
      );

      this.vendorTypeDetailsForm.reset();
      this.isSubmitted = false;
      this.editDisplay = false;
    }
    else {
      validateAllFormFields(this.vendorTypeDetailsForm);
    }
  }


  resetForm() {

    //this.vendorTypeDetailsForm.reset();
    this.isSubmitted = false;
    this.editDisplay = false;
  }

  get vendorDetailForm() {
    return this.vendorTypeDetailsForm.controls;
  }

  // back to previous page
  previousPage() {
    this._router.navigateByUrl('/admin/business-type');
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this._unsubscribe.next(true);
    this._unsubscribe.complete();
  }
}
