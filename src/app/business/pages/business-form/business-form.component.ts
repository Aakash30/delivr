import { Component, OnInit, NgZone, ElementRef, ViewChild, OnDestroy } from '@angular/core';
import { trigger, transition, style, animate } from "@angular/animations";
import { ActivatedRoute, Router } from "@angular/router";
import {
  FormGroup,
  FormControl,
  Validators,
  AbstractControl,
  FormBuilder,
  FormArray
} from "@angular/forms";
import { UtilityService } from 'app/core/services/utility.service';
import { MapsAPILoader, MouseEvent } from '@agm/core';
import {
  validateAllFormFields,
  noWhitespaceValidator,
  blankSpaceInputNotValid
} from "../../../shared/utils/custom-validators";

import { BusinessService } from "app/core/services/business.service";
import { FileUploadService } from "app/core/services/file-upload.service";
import { NgxUiLoaderService } from "ngx-ui-loader";
import { MessageService } from "primeng/api";
import { Location } from "@angular/common";
import { Subject } from 'rxjs';
import { takeUntil, map } from 'rxjs/operators';
import { ErrorHandlerService } from 'app/core/services/error-handler.service';

declare var google: any;

interface Vendor {
  name: string;
  code: string;
}

interface City {
  name: string;
  code: string;
}

interface Country {
  name: string;
  code: string;
}

interface Business {
  name: string;
  code: string;
}

@Component({
  selector: "app-business-form",
  templateUrl: "./business-form.component.html",
  styleUrls: ["./business-form.component.scss"]
})
export class BusinessFormComponent implements OnInit, OnDestroy {

  vendors: Vendor[];
  selectedVendor: Vendor;
  business: Business[];
  selectedBusiness: Business;
  cities: City[] = [];
  countries: Country[];
  selectedCity: City;
  selectedCountry: Country;
  sectionTitle: String;
  buttonName: String;
  countryCode: string;
  id: number;
  country: any;
  vendorListForm: FormGroup;
  isSubmitted: Boolean = false;
  imageDisplay: boolean = false;
  imageUrl: any;
  selectedFile: File;

  latitude: number = 25.197525;
  longitude: number = 55.274288;
  zoom: number = 12;
  paceChange;

  private _unsubscribe = new Subject<boolean>();

  private geoCoder;

  @ViewChild('address')
  searchElementRef: ElementRef;

  formBuilder: FormBuilder = new FormBuilder();

  businessUsers;
  businessBranch;
  imageObject: any = {
    agreement: "",
    license: [] = [],
    bank_statement: "",
    emirates_id: [],
    passport_document: [],
    credit_application_form: "",
    other_document: "",
    business_logo: ""
  };

  vendorListFromAPI: string[];
  cityListFromAPI: [{ country_id: number }];
  countryListFromAPI: string[];
  businessListFromAPI: string[];
  position;

  // isVisible: boolean = true;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private _location: Location,
    public utility: UtilityService,
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone,
    private businessService: BusinessService,
    private loader: NgxUiLoaderService,
    private messageService: MessageService,
    private uploader: FileUploadService,
    private errorHandling: ErrorHandlerService
  ) {
    this.sectionTitle = route.snapshot.data["sectionTitle"];
    this.buttonName = route.snapshot.data["buttonName"];

    if (route.snapshot.paramMap.get('id')) {
      this.id = utility.base64Decode(route.snapshot.paramMap.get('id'));

    }

  }

  initUserForm() {
    // initialize user
    return this.formBuilder.group({
      user_id: new FormControl(null),
      first_name: new FormControl("", [
        Validators.required,
        Validators.pattern(/^[a-zA-Z ]*$/),
        noWhitespaceValidator,
        blankSpaceInputNotValid
      ]),
      last_name: new FormControl("", [
        Validators.required,
        Validators.pattern(/^[a-zA-Z ]*$/),
        noWhitespaceValidator,
        blankSpaceInputNotValid
      ]),
      designation: new FormControl("", [
        Validators.required,
        Validators.pattern(/^[a-z A-Z 0-9]*$/),
        noWhitespaceValidator,
        blankSpaceInputNotValid
      ]),
      emirates_id: new FormControl(''),
      passport_document: new FormControl('')

    });
  }

  initAddressForm() {
    return this.formBuilder.group({
      branch_area_id: new FormControl(null),
      country: new FormControl("", [
        Validators.required
      ]),
      city: new FormControl("", [
        Validators.required
      ]),
      latitude: new FormControl(''),
      longitude: new FormControl(''),
      address_branch_name: new FormControl("Main Branch", [
        Validators.required,
        Validators.pattern(/^[a-z A-Z 0-9]*$/),
        noWhitespaceValidator,
        blankSpaceInputNotValid
      ]),
      address_line_2: new FormControl("", [
        Validators.required,
        noWhitespaceValidator,
        blankSpaceInputNotValid
      ]),
      address_line_1: new FormControl("", [
        Validators.required,
        blankSpaceInputNotValid
      ]),
      postal_Code: new FormControl("", [
        Validators.required,
        Validators.pattern(/^[0-9]\d{5}$/)
      ]),
    });
  }

  ngOnInit() {

    this.vendorListForm = this.formBuilder.group({
      business_name: new FormControl("", [
        Validators.required,
        Validators.pattern(/^[a-z A-Z 0-9]*$/),
        noWhitespaceValidator,
        blankSpaceInputNotValid
      ]),
      business_type: new FormControl("", [
        Validators.required
      ]),
      email_id: new FormControl("", [
        Validators.required,
        noWhitespaceValidator,
        blankSpaceInputNotValid,
        Validators.pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/),
        // Validators.email
      ]),
      mobile_number: new FormControl("", [
        Validators.required,
        // Validators.pattern(/^[0-9]\d{0,10}$/),
        // Validators.minLength(10)
      ]),
      alternate_number: new FormControl("", [
        // Validators.pattern(/^[0-9]\d{0,10}$/),
        Validators.minLength(10)
      ]),
      business_logo: new FormControl(''),
      agreement: new FormControl(''),
      license: new FormControl(''),
      bank_statement: new FormControl(''),
      credit_application_form: new FormControl(''),
      other_document: new FormControl(''),
      businessBranch: this.initAddressForm(),
      businessUsers: this.initUserForm()

    });
    this.countryCode = '+00';

    //fetch the selectable form data
    const pageContent = this.route.snapshot.data.pageData.data;

    this.countryListFromAPI = pageContent.countryList;
    this.countries = this.utility.arrayOfObjectToConvertInDropdownFormat(this.countryListFromAPI, "country", "country_id", "status", "inActive");

    this.cityListFromAPI = pageContent.cityList;

    // this.cities = this.utility.arrayOfObjectToConvertInDropdownFormat(this.cityListFromAPI, "city", "city_id");
    this.vendors = this.utility.arrayOfObjectToConvertInDropdownFormat(pageContent.businessType, "business_type", "id", "status", "deactive");


    if (pageContent.business) {
      setTimeout(() => {
        let businessData = pageContent.business;
        this.countryCode = businessData.mobile_number.split('-')[0];
        businessData.mobile_number = businessData.mobile_number.split('-')[1];

        businessData.alternate_number = businessData.alternate_number.split('-', 2)[1];

        // if (businessData.businessUsers[0].emirates_id && businessData.businessUsers[0].emirates_id != "") {
        //   businessData.businessUsers[0].emirates_id = JSON.parse(businessData.businessUsers[0].emirates_id);
        // }
        // if (businessData.businessUsers[0].passport_document && businessData.businessUsers[0].passport_document != "") {
        //   businessData.businessUsers[0].passport_document = JSON.parse(businessData.businessUsers[0].passport_document);
        // }

        businessData.business_type = businessData.business_type;

        this.vendorListForm.patchValue(businessData);
        this.vendorListForm.controls.businessUsers.patchValue(businessData.businessUsers[0])
        this.filtercities(businessData.businessBranch[0].country);
        this.latitude = businessData.businessBranch[0].latitude;
        this.longitude = businessData.businessBranch[0].longitude;
        this.vendorListForm.controls.businessBranch.patchValue(businessData.businessBranch[0]);
        this.imageObject.agreement = businessData.agreement;
        this.imageObject.license = businessData.license;
        this.imageObject.bank_statement = businessData.bank_statement;
        this.imageObject.credit_application_form = businessData.credit_application_form;
        this.imageObject.other_document = businessData.other_document;
        this.imageObject.emirates_id = businessData.businessUsers[0].emirates_id;
        this.imageObject.passport_document = businessData.businessUsers[0].passport_document;
        this.imageObject.business_logo = businessData.business_logo;
      }, 200);
    } else {
      this.reloadMap();
    }
  }


  filtercities(country) {
    if (country != null && country != "") {
      let selectedCountry: any = this.countryListFromAPI.filter((countryData: any) => {
        return countryData.country_id == country;
      });
      this.countryCode = selectedCountry[0].country_code;

      let alphaCode = selectedCountry[0].alpha_code;

      this.reloadMap(alphaCode)
      let na = []
      let selectedCity = this.cityListFromAPI.filter((cityData: any) => {
        return cityData.country_id == country;

      });

      this.cities = this.utility.arrayOfObjectToConvertInDropdownFormat(selectedCity, "city", "city_id", "status", "inActive");
    } else {
      this.cities = [];
    }
  }


  uploadImage(event, key) {

    let formController;
    if (key == "passport_document" || key == "emirates_id") {
      formController = this.vendorListForm.controls.businessUsers["controls"][key];
    } else {
      formController = this.vendorListForm.controls[key];
    }

    let fileUploadType = "single";

    if (event.target.attributes.multiple != undefined) {
      fileUploadType = "multiple";
    }

    /** check the max files uploaded */
    if (event.target.attributes.limit != undefined) {

      let limit = event.target.attributes.limit.value;
      if (Array.from(event.target.files).length > limit) {
        //formController.setErrors({ "limit": true })
        this.messageService.add({
          severity: "error",
          summary: "limit exceeds",
          detail: "You can upload only " + limit + " files."
        });
        return false;
      } else {
        formController.setErrors(null)
      }
    }

    var fileType = event.target.attributes.accept.value == ".pdf" ? /application\/pdf/ : /image\/png|image\/jpeg|image\/jpg/;

    let maxsize = event.target.attributes.maxsize.value;

    let errorType = false;
    let errorSize = false;
    let fileArray = event.target.files;

    /** check for type of file upload and max file size */
    Array.from(fileArray).forEach(element => {

      if (!element["type"].match(fileType)) {

        errorType = true;
      }
      if (maxsize < element["size"]) { //allow 2 mb
        errorSize = true;

      }
    });

    /** set errors accordingly */
    if (errorType) {
      //formController.setErrors({ "pattern": true });
      this.messageService.add({
        severity: "error",
        summary: "Invalid Type",
        detail: "Only " + event.target.attributes.accept.value + " type is allowed"
      });
      return false;

    } else {
      formController.setErrors(null);
    }

    if (errorSize) {
      this.messageService.add({
        severity: "error",
        summary: "size exceeds",
        detail: "You can upload only " + (maxsize / 1000000) + " MB size files."
      });
      //  formController.setErrors({ "size": true });
      return false;

    } else {
      formController.setErrors(null);
    }

    /** Final upload of the file */
    this.uploader.fileUploadImages(Array.from(event.target.files)).pipe(
      map((success: any) => {
        if (success) {
          return success;
        }
      }),
      takeUntil(this._unsubscribe)
    )
      .subscribe((success: any) => {

        if (fileUploadType != "single") {
          this.imageObject[key] = [];
        }
        success.image.forEach(element => {

          if (fileUploadType == "single") {
            this.imageObject[key] = element.url;
          } else {
            this.imageObject[key].push(element.url)

          }

        });
        formController.setValue(this.imageObject[key]);
        formController.markAsDirty();
        formController.markAsTouched();
      });

  }

  onSubmit() {
    this.isSubmitted = true;
    let data: any = Object.assign({}, this.vendorListForm.value);

    // if (data.businessUsers.emirates_id && data.businessUsers.emirates_id != "") {
    //   data.businessUsers.emirates_id = JSON.stringify(data.businessUsers.emirates_id);
    // }
    // if (data.businessUsers.passport_document && data.businessUsers.passport_document != "") {
    //   data.businessUsers.passport_document = JSON.stringify(data.businessUsers.passport_document);
    // }

    data.mobile_number = this.countryCode + "-" + data.mobile_number;

    if (data.alternate_number && data.alternate_number.trim() != "") {
      data.alternate_number = this.countryCode + "-" + data.alternate_number;

    }

    if (this.id) {
      data.business_id = this.id;
    }

    data.businessBranch.latitude = this.latitude;
    data.businessBranch.longitude = this.longitude;

    if (this.vendorListForm.valid) {
      this.utility.loaderStart();

      if (this.vendorListForm.dirty && this.vendorListForm.touched) {

        this.businessService.addbusiness(data).pipe(
          map((success: any) => {
            if (success) {
              return success;
            }
          }),
          takeUntil(this._unsubscribe)
        ).subscribe(
          (success: any) => {

            this.messageService.add({
              severity: "success",
              summary: success.message,
              detail: ""
            });
            this.router.navigateByUrl("/admin/business-list/details/" + this.utility.base64Encode(success.data.business_id))
            this.utility.loaderStop();
          },
          error => {

            if (error.error.message == "Business with this email already exists!") {
              this.vendorListForm.controls.email_id.setErrors({ duplicate: true })
            } else if (error.error.message == "Business with this name already exists!") {
              this.vendorListForm.controls.business_name.setErrors({ duplicate: true })
            }

            this.errorHandling.routeAccordingToError(error);
            this.utility.loaderStop();
          }
        );
        // this.vendorListForm.reset();
        this.isSubmitted = false;
      } else {
        this.loader.stop();
        this.router.navigateByUrl("/admin/business-list/details/" + this.utility.base64Encode(this.id))
      }

    } else {
      validateAllFormFields(this.vendorListForm);
      this.utility.scrollToError();
    }
  }

  // changeCountry(country?) {

  //   let countryId = country ? country : this.vendorListForm.controls.businessBranch["controls"].country.value;
  //   this.vendorListForm.controls.address_2.setValue("");

  //   if (countryId != null && countryId != "") {
  //     let selectedCountry: any = this.countryListFromAPI.filter((countryData: any) => {
  //       return countryData.country_id == countryId;
  //     });

  //     let selectedCity = this.cityListFromAPI.filter((cityData: any) => {
  //       return cityData.country_id == countryId;

  //     });

  //     this.cities = this.utility.arrayOfObjectToConvertInDropdownFormat(selectedCity, "city", "city_id");
  //     let alphaCode = selectedCountry[0].alpha_code;
  //     this.reloadMap(alphaCode);

  //   } else {
  //     this.reloadMap();
  //   }

  // }

  reloadMap(alphaCode?) {
    if (alphaCode) {

      this.mapsAPILoader.load().then(() => {

        this.geoCoder = new google.maps.Geocoder;
        const autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
          types: ['address'],
          componentRestrictions: { country: alphaCode }
        });
        this.paceChange = autocomplete.addListener('place_changed', () => {
          this.ngZone.run(() => {
            //get the place result
            let place: google.maps.places.PlaceResult = autocomplete.getPlace();

            //verify result
            if (place.geometry === undefined || place.geometry === null) {
              return;
            }

            //set latitude, longitude and zoom
            this.latitude = place.geometry.location.lat();
            this.longitude = place.geometry.location.lng();
            this.vendorListForm.controls.businessBranch["controls"].address_line_2.patchValue(place.formatted_address)
            this.zoom = 12;
          });
        });
      });
    } else {

      this.mapsAPILoader.load().then(() => {

        this.geoCoder = new google.maps.Geocoder;
        const autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
          types: ["street_address", "address"]
        });
        this.paceChange = autocomplete.addListener('place_changed', () => {
          this.ngZone.run(() => {
            //get the place result
            let place: google.maps.places.PlaceResult = autocomplete.getPlace();

            //verify result
            if (place.geometry === undefined || place.geometry === null) {
              return;
            }
            //set latitude, longitude and zoom
            this.latitude = place.geometry.location.lat();
            this.longitude = place.geometry.location.lng();
            this.zoom = 12;
          });
        });
      });

    }
  }

  markerDragEnd($event: MouseEvent) {
    this.latitude = $event.coords.lat;
    this.longitude = $event.coords.lng;
    this.getAddress(this.latitude, this.longitude);
  }


  getAddress(latitude, longitude) {
    this.geoCoder.geocode({ 'location': { lat: latitude, lng: longitude } }, (results, status) => {

      if (status === 'OK') {
        if (results[0]) {
          this.zoom = 12;
          this.vendorListForm.controls.businessBranch["controls"].address_line_2.patchValue(results[0].formatted_address)
          this.vendorListForm.controls.businessBranch["controls"].address_line_2.markAsDirty();
          this.vendorListForm.controls.businessBranch["controls"].address_line_2.markAsTouched();
        } else {
          window.alert('No results found');
        }
      } else {
        window.alert('Geocoder failed due to: ' + status);
      }
    });
  }

  resetForm() {
    this.router.navigate(['/admin/business-list']);
    this.vendorListForm.reset();
    this.isSubmitted = false;
  }

  get vendorList() {
    return this.vendorListForm.controls;
  }

  get businessUserControl() {
    return this.vendorListForm.controls.businessUsers["controls"];
  }

  get businessAddressControl() {
    return this.vendorListForm.controls.businessBranch["controls"];
  }


  openImageDialog(key) {
    this.imageDisplay = true;

    if (key == 'license') {
      this.imageUrl = this.vendorListForm.controls.license.value;
    } else if (key == 'emirates_id') {
      this.imageUrl = String(this.imageObject.emirates_id);
    } else if (key == 'passport_document') {
      this.imageUrl = String(this.imageObject.passport_document);
    }
  }

  ngOnDestroy() {
    this._unsubscribe.next(true);
    this._unsubscribe.complete();
  }
}
