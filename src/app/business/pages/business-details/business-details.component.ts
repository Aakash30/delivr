import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, UrlTree, UrlSegmentGroup, PRIMARY_OUTLET, UrlSegment } from '@angular/router';
import { BusinessService } from 'app/core/services/business.service';
import { MessageService } from 'primeng/api';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { UtilityService } from 'app/core/services/utility.service';


@Component({
  selector: 'app-business-details',
  templateUrl: './business-details.component.html',

  styleUrls: ['./business-details.component.scss']
})
export class BusinessDetailsComponent implements OnInit {

  sectionTitle: String;
  buttonName: String;
  id: number;
  url: String;
  businessDetails: any;
  selectedValues: string[] = [];
  selectedBikes: string[] = [];
  bikes: any[];
  batch: any[];
  cols: any[];
  bikesUI: any[];
  today: any;
  public currentUrl: string;
  imageUrl: any;
  // isVisible: boolean = true;
  imageDisplay: boolean = false;
  businessData: any;

  addDisplay: Boolean = false;
  checked: boolean;
  assignBikeDialog() {
    this.addDisplay = true;
  }
  marked: boolean = false;
  toggleVisibility(e) {
    this.marked = e.target.checked;

  }
  constructor(private route: ActivatedRoute, private router: Router,
    private businessService: BusinessService, private messageService: MessageService, private loader: NgxUiLoaderService, public utility: UtilityService) {
    this.sectionTitle = route.snapshot.data['sectionTitle'];
    this.buttonName = route.snapshot.data['buttonName']
    this.id = utility.base64Decode(route.snapshot.paramMap.get('id'));
    this.currentUrl = router.url;
  }

  ngOnInit() {
    this.today = new Date();
    const pageContent = this.route.snapshot.data.pageData.data;

    this.businessDetails = pageContent.business;
    console.log('page content of business:', this.businessDetails);
    this.bikes = pageContent.vehicleList;
    this.batch = pageContent.batch;
    if (pageContent.business) {
      // if (this.businessData.businessUsers[0].emirates_id && this.businessData.businessUsers[0].emirates_id != "") {
      //   this.businessData.businessUsers[0].emirates_id = JSON.parse(this.businessData.businessUsers[0].emirates_id);
      // }
      // if (this.businessData.businessUsers[0].passport_document && this.businessData.businessUsers[0].passport_document != "") {
      //   this.businessData.businessUsers[0].passport_document = JSON.parse(this.businessData.businessUsers[0].passport_document);
      // }
    }

  }

  encode(id) {
    return this.utility.base64Encode(id);
  }
  parse(vehicles) {
    return JSON.stringify(vehicles);
  }
  assignBikePage() {
    this.router.navigateByUrl(encodeURI('/admin/business-list/assign-bike/' + this.utility.base64Encode(this.id)));
  }

  previousPage() {
    this.router.navigate(['/admin/business-list']);
  }

  getDate(date) {
    //onsole.log(new Date(date).getDate())
    return new Date(date);
  }

  renewBatch(batch_id) {
    this.router.navigate(['/admin/business-list/renew-bike', this.utility.base64Encode(this.id), this.utility.base64Encode(batch_id)]);
    //this.router.navigateByUrl(('/admin/business-list/renew-bike/' + this.utility.base64Encode(this.id) + '/' + this.utility.base64Encode(batch_id)));
  }

  openImageDialog(imgObject, type, key) {
    this.imageDisplay = true;

    switch (type) {
      case type = 'license':
        this.imageUrl = imgObject;
        break;
    }
  }

  closeDialog() {
    this.imageDisplay = false;
  }
}
