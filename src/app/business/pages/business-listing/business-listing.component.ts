import { Component, OnInit, OnDestroy } from '@angular/core';
import { trigger, transition, style, animate } from '@angular/animations';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { MenuItem, MessageService } from 'primeng/api';
import { BusinessService } from 'app/core/services/business.service';
import { ActivatedRoute } from '@angular/router';
import { UtilityService } from 'app/core/services/utility.service';
import { Subject } from 'rxjs';
import { takeUntil, map } from 'rxjs/operators';
import { ErrorHandlerService } from 'app/core/services/error-handler.service';

interface Type {
  name: string;
  code: string;
}

interface Location {
  name: string;
  code: string;
}

@Component({
  selector: 'app-business-listing',
  templateUrl: './business-listing.component.html',
  styleUrls: ['./business-listing.component.scss'],
  animations: [
    trigger('openClose', [
      transition(':enter', [
        style({ transform: 'translateX(20%)', opacity: 0 }),
        animate('500ms', style({ transform: 'translateX(0)', opacity: 1 })),
      ]),
      transition(':leave', [
        style({ transform: 'translateX(0)', opacity: 1, display: 'none' }),
        animate('500ms', style({ transform: 'translateX(20%)', opacity: 0 })),
      ]),
    ])
  ],
})
export class BusinessListingComponent implements OnInit, OnDestroy {

  activeTab: string;
  items: MenuItem[];
  activeItem: MenuItem;
  vendorList: any[];
  activeList: any[];
  rejectedList: any[];
  sectionTitle: String;
  types: Type[];
  selectedType: Type;
  location: Location[];
  countries: any[];
  cities: any = [];
  cityListFromAPI: any;
  selectedLocation: Location;
  isSubmitted: Boolean = false;
  vendorListForm: FormGroup;
  selectedValue: string = 'Active';
  keyword: string = "";
  offset: number;
  total: number = 0;
  status: string = "";
  possibleValue = ['active', 'deactive'];

  // show filter function
  isVisible: boolean = false;

  showFilter() {
    this.isVisible = !this.isVisible;
  }

  private _unsubscribe = new Subject<boolean>();

  constructor(private businessService: BusinessService, private messageService: MessageService, private route: ActivatedRoute, public utility: UtilityService, private errorHandling: ErrorHandlerService) { }

  getItem(event, index) {
    event.preventDefault();
    if (index === 0) {
      this.activeTab = 'all';
      this.status = '';
    } else if (index === 1) {
      this.activeTab = 'active';
      this.status = 'active';
    } else if (index === 2) {
      this.activeTab = 'rejected';
      this.status = 'deactive';
    }
    this.getAllBusinessList();
  }

  ngOnInit() {

    this.vendorListForm = new FormGroup({
      businessType: new FormControl(null),
      country: new FormControl(null),
      city: new FormControl(null)
    });

    this.activeTab = 'all';

    this.items = [
      {
        label: 'All Business'
      },
      {
        label: 'Active'
      },
      {
        label: 'Rejected'
      },
    ];

    const pageContent = this.route.snapshot.data.pageData.data;
    this.vendorList = pageContent.business;

    this.types = this.utility.arrayOfObjectToConvertInDropdownFormat(pageContent.businessType, "business_type", "id");
    this.countries = this.utility.arrayOfObjectToConvertInDropdownFormat(pageContent.country, "country", "country_id");
    this.cityListFromAPI = pageContent.city;
    this.total = pageContent.count;

    this.activeItem = this.items[0];


  }


  /** list down business list with and without filters */
  getAllBusinessList() {
    /** start loade**/
    this.utility.loaderStart();
    /** data to be filtered */
    let data: any = {};
    if (this.keyword.trim() != "") {
      data["keyword"] = this.keyword;
    }

    if (this.offset != 0) {
      data["offset"] = this.offset;
    }

    if (this.status != "") {
      data["status"] = this.status;
    }

    if (this.vendorListForm.controls["businessType"].value != "" && this.vendorListForm.controls["businessType"].value != null) {
      data.businessType = this.vendorListForm.controls["businessType"].value;
    }

    if (this.vendorListForm.controls["city"].value != "" && this.vendorListForm.controls["city"].value != null) {
      data.city = this.vendorListForm.controls["city"].value;
    }

    if (this.vendorListForm.controls["country"].value != "" && this.vendorListForm.controls["country"].value != null) {
      data.country = this.vendorListForm.controls["country"].value;
    }

    this.businessService.getbusinessList(data).subscribe(
      (success: any) => {
        this.vendorList = success.data.business;
        this.total = success.data.count;
        this.utility.loaderStop();
      },
      error => {
        this.utility.loaderStop();
        this.errorHandling.routeAccordingToError(error)
      }
    );
  }

  /** triggers for searching keyword */
  searchText(event) {
    this.keyword = event.target.value;
    if (event.charCode == 13) {
      // on click of enter
      this.getAllBusinessList();
    }
  }

  /** filters cities */
  filtercities(country) {
    let na = []
    this.cityListFromAPI.forEach((city) => {
      if (city.country_id == country) na.push(city);
      else return;
    })
    if (country != null && country != "") {
      this.cities = this.utility.arrayOfObjectToConvertInDropdownFormat(na, "city", "city_id");
    } else {
      this.cities = [];
    }
    this.vendorListForm.controls["city"].setValue(null)
  }

  /** submit form filters */
  onSubmit() {
    let data: any = {};
    if (this.vendorListForm.dirty && this.vendorListForm.touched) {
      this.getAllBusinessList();
      this.isSubmitted = true;
    } else {
      this.isSubmitted = false;
    }
    this.closeFilter()

  }

  closeFilter() {
    //this.vendorListForm.reset();
    this.isVisible = false;
  }

  changeStatus(status, id) {

    /** starts loader */
    this.utility.loaderStart();
    this.businessService.changeStatus(id, status).pipe(
      map((success: any) => {
        if (success) {
          return success;
        }
      }),
      takeUntil(this._unsubscribe)
    ).subscribe((success: any) => {
      this.messageService.add({
        severity: 'success',
        summary: 'status changed',
        detail: success.message
      });
      /** stops loader */
      this.utility.loaderStop();
    }, error => {
      /** stops loader */
      this.errorHandling.routeAccordingToError(error);
      this.utility.loaderStop();
    });
  }

  clearFilter() {

    if (this.vendorListForm.dirty && this.vendorListForm.touched && this.isSubmitted) {
      this.vendorListForm.reset();
      this.getAllBusinessList();
    } else {
      this.vendorListForm.reset();
    }

    this.isSubmitted = false;
    this.isVisible = false;
  }

  clearInput(event) {
    this.keyword = event;
    this.getAllBusinessList();
  }

  paginate(event) {
    if (this.offset != undefined) {
      this.offset = parseInt(event.first);
      this.getAllBusinessList();
    } else {
      this.offset = parseInt(event.first);
    }
  }

  resendLink(business) {
    this.utility.loaderStart();
    let data = { "user_id": business.businessUsers[0].user_id, 'user_type': "vendor", "email_id": business.email_id, "name": business.businessUsers[0].first_name };
    this.businessService.resendLink(data).pipe(takeUntil(this._unsubscribe)).subscribe((success: any) => {
      this.messageService.add({
        severity: 'success',
        summary: "Success",
        detail: success.message
      });
      this.utility.loaderStop();
    }, error => {
      this.errorHandling.routeAccordingToError(error);
      this.utility.loaderStop();
    })
  }
  ngOnDestroy() {
    this._unsubscribe.next(true);
    this._unsubscribe.complete();
  }

}
