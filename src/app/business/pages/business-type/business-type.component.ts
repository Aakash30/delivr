import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { validateAllFormFields, noWhitespaceValidator, blankSpaceInputNotValid } from '../../../shared/utils/custom-validators';
import { BusinessService } from 'app/core/services/business.service';
import { MessageService } from 'primeng/api';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { ActivatedRoute } from '@angular/router';
import { UtilityService } from 'app/core/services/utility.service';
import { ErrorHandlerService } from 'app/core/services/error-handler.service';
import { Subject } from 'rxjs';
import { takeUntil, map } from 'rxjs/operators';

@Component({
  selector: 'app-business-type',
  templateUrl: './business-type.component.html',
  styleUrls: ['./business-type.component.scss']
})
export class BusinessTypeComponent implements OnInit, OnDestroy {
  addDisplay: Boolean = false;
  // editDisplay: Boolean = false;
  vendors: any[];
  possibleValue = ['active', 'block'];
  checked: boolean;
  isSubmitted: Boolean = false;
  vendorTypeForm: FormGroup;
  keyword: string = "";
  offset: number = 0;
  total: number = 0;
  addVendor() {
    this.addDisplay = true;
  }

  private _unsubscribe = new Subject<boolean>();

  constructor(private businessService: BusinessService,
    private messageService: MessageService,
    private loader: NgxUiLoaderService,
    private route: ActivatedRoute,
    public utility: UtilityService,
    private errorHandling: ErrorHandlerService) { }

  ngOnInit() {

    const pageContent = this.route.snapshot.data.pageData.data;
    this.vendors = pageContent.businessTypeListData;
    this.total = pageContent.count;

    this.vendorTypeForm = new FormGroup({
      businessType: new FormControl('', [Validators.required, Validators.pattern(/^[a-zA-Z0-9 .-]*$/)])
    });

  }

  getBusinessTypeList() {
    let data = {};

    if (this.keyword.trim() != "") {

      data["keyword"] = this.keyword;
    }

    if (this.offset != 0) {
      data["offset"] = this.offset;
    }

    this.businessService.getbusinesstypesList(data)
      .pipe(
        map((success: any) => {
          if (success) {
            return success.data.businessTypeListData;
          }
        }),
        takeUntil(this._unsubscribe)
      )
      .subscribe(
        (success: any) => {

          this.vendors = success;
          this.loader.stop()
        },
        error => {
          this.errorHandling.routeAccordingToError(error);
          this.loader.stop()
        }
      );
  }

  changeStatus(status, id) {
    this.businessService.changeBusinessTypeStatus(id, status)
      .pipe(
        map((success: any) => {
          if (success) {
            return success;
          }
        }),
        takeUntil(this._unsubscribe)
      )
      .subscribe((success: any) => {
        this.messageService.add({
          severity: 'success',
          summary: 'Status updated',
          detail: success.message
        })
      }, error => {
        this.errorHandling.routeAccordingToError(error);
      });
  }

  onSubmit() {
    this.isSubmitted = true;
    if (this.vendorTypeForm.valid) {

      this.businessService.addbusinesstype(this.vendorTypeForm.value)
        .pipe(
          map((success: any) => {
            if (success) {
              return success;
            }
          }),
          takeUntil(this._unsubscribe)
        )
        .subscribe(
          (success: any) => {
            this.messageService.add({ severity: 'success', summary: "Success", detail: success.message });
            this.vendorTypeForm.reset();
            this.isSubmitted = false;
            this.addDisplay = false;
            this.getBusinessTypeList();
          },
          error => {
            this.errorHandling.routeAccordingToError(error);
          }
        );

    }
    else {
      validateAllFormFields(this.vendorTypeForm);
    }
  }

  resetForm() {
    this.vendorTypeForm.reset();
    this.isSubmitted = false;
    this.addDisplay = false;
  }

  get vendorForm() {
    return this.vendorTypeForm.controls;
  }

  paginate(event) {
    this.offset = parseInt(event.first);
    this.getBusinessTypeList();

  }
  searchText(event) {
    this.keyword = event.target.value;
    if (event.charCode == 13) {
      // on click of enter
      this.getBusinessTypeList();
    }

  }


  clearInput(event) {
    this.keyword = event;
    this.getBusinessTypeList();
  }

  ngOnDestroy() {
    this._unsubscribe.next(true);
    this._unsubscribe.complete();
  }

}
