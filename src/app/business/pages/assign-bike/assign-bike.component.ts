import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BusinessService } from "app/core/services/business.service";
import { NgxUiLoaderService } from "ngx-ui-loader";
import { MessageService } from "primeng/api";


@Component({
  selector: 'app-assign-bike',
  templateUrl: './assign-bike.component.html',
  styleUrls: ['./assign-bike.component.scss']
})
export class AssignBikeComponent implements OnInit {

  bikeList: any[];
  sectionTitle: string;
  buttonName: string;
  id: number;
  currentUrl: any;

  selectedValue;

  constructor(private route: ActivatedRoute, private router: Router, private businessService: BusinessService,
    private loader: NgxUiLoaderService,
    private messageService: MessageService, ) {
    this.sectionTitle = route.snapshot.data['sectionTitle'];
    this.buttonName = route.snapshot.data['buttonName']
    this.id = +(route.snapshot.paramMap.get('id'));
    this.currentUrl = router.url;

  }

  ngOnInit() {

    this.bikeList = this.route.snapshot.data.pageData.data.vehicleList[0]
    // this.businessService.fetchvehicles(this.id).subscribe(
    //   (success: any) => {
    //     // console.log("this is the real success: " + JSON.stringify(success));

    //     this.messageService.add({
    //       severity: "success",
    //       summary: "fetched vehicles",
    //       detail: ""
    //     });

    //     this.bikeList = success.data.vehicleList[0];
    //     this.loader.stop();
    //   },
    //   error => {
    //     // console.log('vehicle addition error ' + error);
    //     this.messageService.add({
    //       severity: "error",
    //       summary: "vehicle fetching failed",
    //       detail: `${error}`
    //     });
    //     this.loader.stop();
    //   }
    // );

    // this.bikeList = [
    //   {
    //     id: 1,
    //     regNum: 'F 3434',
    //     brand: 'Hero',
    //     model: 'CD Delux',
    //   },
    //   {
    //     id: 2,
    //     regNum: 'F 3434',
    //     brand: 'Hero',
    //     model: 'CD Delux',
    //   },
    //   {
    //     id: 3,
    //     regNum: 'F 3434',
    //     brand: 'Hero',
    //     model: 'CD Delux',
    //   }
    // ];
  }


  checkboxSelection(bikeId) {
  }

}
