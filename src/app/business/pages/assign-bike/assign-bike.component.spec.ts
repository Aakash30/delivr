import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssignBikeComponent } from './assign-bike.component';

describe('AssignBikeComponent', () => {
  let component: AssignBikeComponent;
  let fixture: ComponentFixture<AssignBikeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssignBikeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignBikeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
