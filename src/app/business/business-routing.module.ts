import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BusinessTypeComponent } from './pages/business-type/business-type.component';
import { BusinessFormComponent } from './pages/business-form/business-form.component';
import { BusinessTypeDetailsComponent } from './pages/business-type-details/business-type-details.component';
import { BusinessListingComponent } from './pages/business-listing/business-listing.component';
import { BusinessDetailsComponent } from './pages/business-details/business-details.component';
import { AssignBikeComponent } from 'app/shared/components/assign-bike/assign-bike.component';
import { BusinessManagementResolverService } from 'app/core/resolver/business-management-resolver.service';
import { RolesGuardService } from 'app/core/guards/roles-guard.service';
import { AuthGuardService } from 'app/core/guards/auth-guard.service';
import { BusinessComponent } from 'app/landing/pages/business/business.component';
// import { AssignBikeComponent } from './pages/assign-bike/assign-bike.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'business-type',
        component: BusinessTypeComponent,
        data: {
          sectionTitle: 'Business Type',
          expectedRole: ['business-category'],
          onlyForUser: ['super_admin']
        },
        canActivate: [AuthGuardService, RolesGuardService],
        resolve: {
          pageData: BusinessManagementResolverService
        }
      },
      {
        path: 'business-list',
        component: BusinessListingComponent,
        data: {
          expectedRole: ['business-management'],
          onlyForUser: ['super_admin']
        },
        canActivate: [AuthGuardService, RolesGuardService],
        resolve: {
          pageData: BusinessManagementResolverService
        }
      },
      {
        path: 'business-type-details/:id',
        component: BusinessTypeDetailsComponent,
        data: {
          expectedRole: ['business-category'],
          onlyForUser: ['super_admin']
        },
        canActivate: [AuthGuardService, RolesGuardService],
        resolve: {
          pageData: BusinessManagementResolverService
        }
      },

      {
        path: 'business-list/details/:id',
        component: BusinessDetailsComponent,
        data: {
          sectionTitle: 'Details',
          buttonName: 'Save',
          expectedRole: ['business-management'],
          onlyForUser: ['super_admin']
        },
        canActivate: [AuthGuardService, RolesGuardService],
        resolve: {
          pageData: BusinessManagementResolverService
        }
      },
      {
        path: 'business-list/assign-bike/:id',
        component: AssignBikeComponent,
        data: {
          expectedRole: ['business-management'],
          onlyForUser: ['super_admin'],
          buttonName: 'Assign',
          sectionTitle: 'Assign Bikes'
        },
        canActivate: [AuthGuardService, RolesGuardService],
        resolve: {
          pageData: BusinessManagementResolverService
        }
      },
      {
        path: 'business-list/edit/:id',
        component: BusinessFormComponent,
        data: {
          expectedRole: ['business-management'],
          onlyForUser: ['super_admin'],
          buttonName: 'Update'
        },
        canActivate: [AuthGuardService, RolesGuardService],
        resolve: {
          pageData: BusinessManagementResolverService
        }
      },
      {
        path: 'business-list/add',
        component: BusinessFormComponent,
        data: {
          expectedRole: ['business-management'],
          onlyForUser: ['super_admin'],
          buttonName: 'Create'
        },
        canActivate: [AuthGuardService, RolesGuardService],
        resolve: {
          pageData: BusinessManagementResolverService
        }
      },
      {
        path: 'business-list/renew-bike/:id/:batch',
        component: AssignBikeComponent,
        data: {
          expectedRole: ['business-management'],
          onlyForUser: ['super_admin'],
          buttonName: 'Assign',
          sectionTitle: 'Assign Bikes'
        },
        canActivate: [AuthGuardService, RolesGuardService],
        resolve: {
          pageData: BusinessManagementResolverService
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VendorRoutingModule { }
