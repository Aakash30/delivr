import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VendorRoutingModule } from './business-routing.module';
import { BusinessTypeComponent } from './pages/business-type/business-type.component';
import { SharedModule } from 'app/shared/shared.module';
import { BusinessFormComponent } from './pages/business-form/business-form.component';
import { BusinessTypeDetailsComponent } from './pages/business-type-details/business-type-details.component';
import { BusinessListingComponent } from './pages/business-listing/business-listing.component';
import { BusinessDetailsComponent } from './pages/business-details/business-details.component';
import { BusinessService } from 'app/core/services/business.service';
import { AgmCoreModule, GoogleMapsAPIWrapper } from '@agm/core';
import { AssignBikeComponent } from './pages/assign-bike/assign-bike.component';
import { LandingModule } from 'app/landing/landing.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDHapbY7BfjFKhd2WJ4BHv6YDfWuPYhoL8',
      libraries: ['places']
    }),
    VendorRoutingModule
  ],
  declarations: [BusinessTypeComponent, BusinessFormComponent, BusinessTypeDetailsComponent,
    BusinessListingComponent, BusinessDetailsComponent, AssignBikeComponent],
  providers: [BusinessService, GoogleMapsAPIWrapper]
})
export class BusinessModule { }
