import { Component, OnInit } from '@angular/core';
import { UtilityService } from 'app/core/services/utility.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-vehicles-layout',
  templateUrl: './vehicles-layout.component.html',
  styleUrls: ['./vehicles-layout.component.scss']
})
export class VehiclesLayoutComponent implements OnInit {

  constructor(private router: Router, private utilityService: UtilityService) { }

  ngOnInit() {
    let decidedUrl = "";
    // if (this.router.url == "/admin/vehicles") {
    //   if (this.utilityService.checkUserRoleWithRoute('vehicle-management', ['super_admin'])) {
    //     decidedUrl = "/list";
    //   } else if (this.utilityService.checkUserRoleWithRoute('vehicle-inventory', ['super_admin'])) {
    //     decidedUrl = "/inventory";
    //   }
    //   this.router.navigateByUrl(this.router.url + "" + decidedUrl);
    // }
  }

}
