import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VehiclesLayoutComponent } from './vehicles-layout.component';

describe('VehiclesLayoutComponent', () => {
  let component: VehiclesLayoutComponent;
  let fixture: ComponentFixture<VehiclesLayoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VehiclesLayoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VehiclesLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
