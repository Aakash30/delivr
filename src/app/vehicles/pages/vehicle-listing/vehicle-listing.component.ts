//TODO: http 500 when searching for a model
//put a check for price range
import { Component, OnInit, OnDestroy } from "@angular/core";
import { MenuItem, MessageService } from "primeng/api";
import {
  trigger,
  style,
  transition,
  animate
} from "@angular/animations";
import { VehicleService } from "app/core/services/vehicle.service";
import { ActivatedRoute, Router } from "@angular/router";
import {
  FormGroup,
  FormControl,
} from "@angular/forms";
import { UtilityService } from "app/core/services/utility.service";
import { ErrorHandlerService } from "app/core/services/error-handler.service";
import { Subject } from "rxjs/internal/Subject";
import { takeUntil } from "rxjs/operators";
import { DashboardFilterRedirectService } from "app/core/services/dashboard-filter-redirect.service";

interface Brand {
  name: string;
  code: string;
}

interface Country {
  name: string;
  code: string;
}

interface City {
  name: string;
  code: string;
}


@Component({
  selector: "app-vehicle-listing",
  templateUrl: "./vehicle-listing.component.html",
  styleUrls: ["./vehicle-listing.component.scss"],
  animations: [
    trigger("openClose", [
      transition(":enter", [
        style({ transform: "translateX(20%)", opacity: 0 }),
        animate("500ms", style({ transform: "translateX(0)", opacity: 1 }))
      ]),
      transition(":leave", [
        style({ transform: "translateX(0)", opacity: 1, display: "none" }),
        animate("500ms", style({ transform: "translateX(20%)", opacity: 0 }))
      ])
    ])
  ]
})
export class VehicleListingComponent implements OnInit, OnDestroy {

  public data: any = [];
  public activeTab: string;
  items: MenuItem[];
  activeItem: MenuItem;
  vehicleList: any[];
  leaseList: any[];
  availableList: any[];
  brands: Brand[];
  models: any[] = [];
  selectedBrand: Brand;
  countries: Country[];
  selectedCountry: Country;
  cities: City[] = [];
  posibleValue: string[] = ['available', 'unavailable'];

  private _unsubscribe = new Subject<Boolean>();

  selectedCity: City;
  sectionTitle: String;
  isSubmitted: Boolean = false;
  usageStatus =
    [
      {
        label: 'Used',
        value: 'used'
      },
      {
        label: 'New',
        value: 'new'
      }
    ]

  keyword: String = "";
  offset: number;
  total: number = 0;

  getItem(event, index) {
    event.preventDefault();
    if (index === 0) {
      this.activeTab = "all";
    } else if (index === 1) {
      this.activeTab = "lease";
    } else if (index === 2) {
      this.activeTab = "available";
    } else if (index === 3) {
      this.activeTab = "unavailable";
    }
    this.offset = 0;
    this.listing();
  }

  brandListFromAPI: string[];
  modelListFromAPI: any[];
  countryListFromAPI: string[];
  cityListFromAPI: [{ country_id: number }];

  // show filter function
  isVisible: boolean = false;

  showFilter() {
    this.isVisible = !this.isVisible;
  }

  closeFilter() {
    this.isVisible = false;
  }

  selectedValue: string = "Active";
  vehicleFilterForm: FormGroup;

  constructor(
    private route: ActivatedRoute,
    private vehicleService: VehicleService,
    private messageService: MessageService,
    private router: Router,
    public utility: UtilityService,
    private errorHandling: ErrorHandlerService,
    private _vehicleFetch: DashboardFilterRedirectService
  ) {
    this.sectionTitle = route.snapshot.data["sectionTitle"];
  }

  ngOnInit() {

    this.activeTab = "all";

    this.items = [
      {
        label: "All Vehicles"
      },
      {
        label: "On Lease"
      },
      {
        label: "Available"
      },
      {
        label: "Unavailable"
      }
    ];

    this.activeItem = this.items[0];
    this.vehicleList = [];
    let filter = this._vehicleFetch.filterFor.subscribe((res) => {
      if (res) {
        switch (res) {
          case "AVAILABLE":
            this.activeTab = "available";
            this.activeItem = this.items[2];
            break;
          case "ONLEASE":
            this.activeTab = "lease";
            this.activeItem = this.items[1];
            break;
          default:
            this.activeTab = "all";
            this.activeItem = this.items[0];
        }
      } else {
      }
    });

    filter.unsubscribe();
    this._vehicleFetch.setFilter("");
    const pageData = this.route.snapshot.data.pageData.data;

    this.countries = this.utility.arrayOfObjectToConvertInDropdownFormat(pageData.country, "country", "country_id");
    this.cityListFromAPI = pageData.city;
    this.brandListFromAPI = pageData.vehicleBrand;
    this.modelListFromAPI = pageData.vehicleModel;
    this.brands = this.utility.arrayOfObjectToConvertInDropdownFormat(pageData.vehicleBrand, "brand_name", "brand_id");
    // this.models = this.utility.arrayOfObjectToConvertInDropdownFormat(pageData.vehicleModel, "model_name", "model_id");

    this.vehicleList = pageData.vehicle;
    this.total = pageData.count;



    this.vehicleFilterForm = new FormGroup({
      brand: new FormControl(null),
      usage: new FormControl(null),
      serviceType: new FormControl(null),
      model: new FormControl(null),
      country: new FormControl(null),
      city: new FormControl(null),
      status: new FormControl(null),
      priceFrom: new FormControl(null),
      priceTo: new FormControl(null)
    });

  }

  listing() {


    let data: any = {};

    if (this.activeTab == "lease") {
      data["fetchFor"] = "ONLEASE";
    } else if (this.activeTab == "available") {
      data["fetchFor"] = "AVAILABLE";
    } else if (this.activeTab == "unavailable") {
      data["fetchFor"] = "UNAVAILABLE";
    }

    if (this.keyword.trim() != "") {
      data["keyword"] = this.keyword;
    }

    if (this.offset != 0) {
      data["offset"] = this.offset;
    }

    if (this.vehicleFilterForm.controls["brand"].value != "" && this.vehicleFilterForm.controls["brand"].value != null) {
      data.brand = this.vehicleFilterForm.controls["brand"].value;
    }

    if (this.vehicleFilterForm.controls["model"].value != "" && this.vehicleFilterForm.controls["model"].value != null) {
      data.model = this.vehicleFilterForm.controls["model"].value;
    }

    if (this.vehicleFilterForm.controls["country"].value != "" && this.vehicleFilterForm.controls["country"].value != null) {
      data.country = this.vehicleFilterForm.controls["country"].value;
    }

    if (this.vehicleFilterForm.controls["city"].value != "" && this.vehicleFilterForm.controls["city"].value != null) {
      data.city = this.vehicleFilterForm.controls["city"].value;
    }

    if (this.vehicleFilterForm.controls["usage"].value != "" && this.vehicleFilterForm.controls["usage"].value != null) {
      data.usageStatus = this.vehicleFilterForm.controls["usage"].value;
    }

    if (this.vehicleFilterForm.controls["priceFrom"].value != "" && this.vehicleFilterForm.controls["priceFrom"].value != null) {
      if (this.vehicleFilterForm.controls["priceFrom"].value > this.vehicleFilterForm.controls["priceTo"].value)
        data.priceFrom = this.vehicleFilterForm.controls["priceFrom"].value;
    }

    if (this.vehicleFilterForm.controls["priceTo"].value != "" && this.vehicleFilterForm.controls["priceTo"].value != null) {
      data.priceTo = this.vehicleFilterForm.controls["priceTo"].value;
    }

    this.vehicleService.getVehicles(data)
      .pipe(takeUntil(this._unsubscribe))
      .subscribe(
        (success: any) => {
          this.vehicleList = success.data.vehicle;
          this.total = success.data.count;

        },
        error => {
          this.errorHandling.routeAccordingToError(error);
        }
      );

  }

  /** filter models on the basis of brand */
  filterModelOnBrandChange() {
    if (this.vehicleFilterForm.value.brand != null && this.vehicleFilterForm.value.brand != "") {
      let filteredModel = this.modelListFromAPI.filter(element => {
        return element.brand_id == this.vehicleFilterForm.value.brand
      });
      this.models = this.utility.arrayOfObjectToConvertInDropdownFormat(filteredModel, "model_name", "model_id");

    } else {
      this.models = [];
    }
    this.vehicleFilterForm.controls["model"].setValue(null);
  }

  clearFilter() {

    let status = this.vehicleFilterForm.touched && this.vehicleFilterForm.dirty && this.isSubmitted;
    this.vehicleFilterForm.reset();
    this.isSubmitted = false;
    this.isVisible = false;
    if (status) {
      this.listing();
    }
  }


  filtercities(country) {
    if (country != null) {
      let na = []
      this.cityListFromAPI.forEach((city) => {
        if (city.country_id == country) na.push(city);
        else return;
      })
      this.cities = this.utility.arrayOfObjectToConvertInDropdownFormat(na, "city", "city_id");
    } else {
      this.cities = [];
    }
    this.vehicleFilterForm.controls["city"].setValue(null);

  }

  changeStatus(status, id) {
    this.vehicleService.changeStatus(id, status)
      .pipe(takeUntil(this._unsubscribe))
      .subscribe((success: any) => {
        this.messageService.add({
          severity: 'success',
          summary: 'status changed',
          detail: ''
        });
        setTimeout(() => {
          this.listing();
        }, 200);
      }, error => {
        this.errorHandling.routeAccordingToError(error);
      });
  }


  onSubmit() {
    if (this.vehicleFilterForm.dirty && this.vehicleFilterForm.touched) {
      this.offset = 0;
      this.listing();
      this.isSubmitted = true;
    } else {
      this.isSubmitted = false;
    }
    this.isVisible = false;
  }

  searchText(event) {
    this.keyword = event.target.value;
    if (event.charCode == 13) {
      // on click of enter
      this.offset = 0;
      this.listing();
    }

  }

  clearInput(event) {
    this.keyword = event;
    this.listing();
  }

  paginate(event) {
    if (this.offset != undefined) {
      this.offset = parseInt(event.first);
      this.listing();
    } else {
      this.offset = parseInt(event.first);
    }

  }

  ngOnDestroy() {
    this._unsubscribe.next(true);
    this._unsubscribe.complete();
  }


}
