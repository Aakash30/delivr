import { Component, OnInit } from "@angular/core";
import { Location } from "@angular/common";
import { ActivatedRoute, Router } from "@angular/router";
import { UtilityService } from "app/core/services/utility.service";

@Component({
  selector: "app-vehicle-details",
  templateUrl: "./vehicle-details.component.html",
  styleUrls: ["./vehicle-details.component.scss"]
})
export class VehicleDetailsComponent implements OnInit {
  sectionTitle: String;
  buttonName: String;
  id: number;
  public currentUrl: String;
  imageDisplayFlag: boolean = false;
  imageUrl: any;
  vehicle: any;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private utility: UtilityService,
    private _location: Location
  ) {
    this.sectionTitle = route.snapshot.data["sectionTitle"];
    this.buttonName = route.snapshot.data["buttonName"];
    this.id = utility.base64Decode(route.snapshot.paramMap.get("id"));
    this.currentUrl = router.url;
  }

  ngOnInit() {
    this.vehicle = this.route.snapshot.data.pageData.data;

  }

  openImageDialog(type, imageObject, key) {
    this.imageDisplayFlag = true;

    switch (type) {
      case type = 'registration_card':
        this.imageUrl = imageObject;
        break;

      case type = 'insurance':
        this.imageUrl = imageObject;
        break;

      case type = 'vehicle_image':
        imageObject.forEach((item, index) => {
          if (key === index) {
            this.imageUrl = item.vehicle_image;
          }
        });
        break;
    }
  }

  previousPage() {
    this.router.navigate(['/admin/vehicles-list']);
  }

}
