import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { VehicleService } from 'app/core/services/vehicle.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { ErrorHandlerService } from 'app/core/services/error-handler.service';
import {
  trigger,
  style,
  transition,
  animate
} from "@angular/animations";
import { FormGroup, FormControl } from '@angular/forms';
import { UtilityService } from 'app/core/services/utility.service';
import { Subject } from 'rxjs/internal/Subject';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-inventory',
  templateUrl: './inventory.component.html',
  animations: [
    trigger("openClose", [
      transition(":enter", [
        style({ transform: "translateX(20%)", opacity: 0 }),
        animate("500ms", style({ transform: "translateX(0)", opacity: 1 }))
      ]),
      transition(":leave", [
        style({ transform: "translateX(0)", opacity: 1, display: "none" }),
        animate("500ms", style({ transform: "translateX(20%)", opacity: 0 }))
      ])
    ])
  ],
  styleUrls: ['./inventory.component.scss']
})
export class InventoryComponent implements OnInit, OnDestroy {

  inventory: any[];
  keyword: String = "";
  offset: Number;
  total: Number = 0;
  brands: any[];
  models: any[] = [];
  modelListFromAPI: any[];
  brandListFromAPI: any[];
  selectedBrand: any;
  countries: any[];
  selectedCountry: any;
  cityListFromAPI: any;
  cities: any[] = [];
  isSubmitted: Boolean = false;
  // show filter function
  isVisible: boolean = false;

  private _unsubscribe = new Subject<Boolean>();

  usageStatus =
    [
      {
        label: 'Used',
        value: 'used'
      },
      {
        label: 'New',
        value: 'new'
      }
    ]

  showFilter() {
    this.isVisible = !this.isVisible;
  }

  closeFilter() {
    this.isVisible = false;
  }

  vehicleFilterForm: FormGroup;

  constructor(private route: ActivatedRoute,
    private vehicleService: VehicleService,
    private router: Router,
    private loader: NgxUiLoaderService,
    public utility: UtilityService,
    private errorHandling: ErrorHandlerService) { }

  ngOnInit() {

    const pageContent = this.route.snapshot.data.pageData.data;

    this.inventory = pageContent.inventoryData;
    this.total = pageContent.count;

    this.countries = this.utility.arrayOfObjectToConvertInDropdownFormat(pageContent.country, "country", "country_id");
    this.cityListFromAPI = pageContent.city;
    this.modelListFromAPI = pageContent.vehicleModel;
    this.brandListFromAPI = pageContent.vehicleBrand;

    this.brands = this.utility.arrayOfObjectToConvertInDropdownFormat(pageContent.vehicleBrand, "brand_name", "brand_id");
    // this.models = this.utility.arrayOfObjectToConvertInDropdownFormat(pageContent.vehicleModel, "model_name", "model_id");

    // console.log(this.brands);

    this.vehicleFilterForm = new FormGroup({
      brand: new FormControl(null),
      usage: new FormControl(null),
      serviceType: new FormControl(null),
      model: new FormControl(null),
      country: new FormControl(null),
      city: new FormControl(null),
      status: new FormControl(null),
      priceFrom: new FormControl(null),
      priceTo: new FormControl(null)
    });
  }

  listing() {


    let data: any = {};

    if (this.keyword.trim() != "") {
      data["keyword"] = this.keyword;
    }

    if (this.offset != 0) {
      data["offset"] = this.offset;
    }

    if (this.vehicleFilterForm.controls["brand"].value != "" && this.vehicleFilterForm.controls["brand"].value != null) {
      data.brand = this.vehicleFilterForm.controls["brand"].value;
    }

    if (this.vehicleFilterForm.controls["model"].value != "" && this.vehicleFilterForm.controls["model"].value != null) {
      data.model = this.vehicleFilterForm.controls["model"].value;
    }

    if (this.vehicleFilterForm.controls["country"].value != "" && this.vehicleFilterForm.controls["country"].value != null) {
      data.country = this.vehicleFilterForm.controls["country"].value;
    }

    if (this.vehicleFilterForm.controls["city"].value != "" && this.vehicleFilterForm.controls["city"].value != null) {
      data.city = this.vehicleFilterForm.controls["city"].value;
    }

    if (this.vehicleFilterForm.controls["usage"].value != "" && this.vehicleFilterForm.controls["usage"].value != null) {
      data.usageStatus = this.vehicleFilterForm.controls["usage"].value;
    }

    if (this.vehicleFilterForm.controls["priceFrom"].value != "" && this.vehicleFilterForm.controls["priceFrom"].value != null) {
      if (this.vehicleFilterForm.controls["priceFrom"].value > this.vehicleFilterForm.controls["priceTo"].value)
        data.priceFrom = this.vehicleFilterForm.controls["priceFrom"].value;
    }

    if (this.vehicleFilterForm.controls["priceTo"].value != "" && this.vehicleFilterForm.controls["priceTo"].value != null) {
      data.priceTo = this.vehicleFilterForm.controls["priceTo"].value;
    }

    this.vehicleService.getInventory(data)
      .pipe(takeUntil(this._unsubscribe))
      .subscribe(
        (response: any) => {
          this.inventory = response.data.inventoryData;
          this.total = response.data.count;
        },
        error => {
          this.errorHandling.routeAccordingToError(error);
        }
      );
  }

  /** filter models on the basis of brand */
  filterModelOnBrandChange() {
    if (this.vehicleFilterForm.value.brand != null && this.vehicleFilterForm.value.brand != "") {
      let filteredModel = this.modelListFromAPI.filter(element => {
        return element.brand_id == this.vehicleFilterForm.value.brand
      });
      this.models = this.utility.arrayOfObjectToConvertInDropdownFormat(filteredModel, "model_name", "model_id");

    } else {
      this.models = [];
    }
    this.vehicleFilterForm.controls["model"].setValue(null);
  }

  searchText(event) {
    this.keyword = event.target.value;
    if (event.charCode == 13) {
      this.offset = 0;
      // on click of enter
      this.listing();
    }

  }
  clearInput(event) {
    this.keyword = event;
    this.listing();
  }

  filtercities(country) {
    let na = []
    this.cityListFromAPI.forEach((city) => {
      if (city.country_id == country) na.push(city);
      else return;
    })

    if (country != null && country != "") {
      this.cities = this.utility.arrayOfObjectToConvertInDropdownFormat(na, "city", "city_id");
    } else {
      this.cities = [];
    }

    this.vehicleFilterForm.controls["city"].setValue(null)
  }

  onSubmit() {

    this.isVisible = false;
    this.offset = 0;
    this.listing();
  }

  paginate(event) {
    if (this.offset != null) {
      this.offset = parseInt(event.first);
      this.listing();
    }


  }
  clearFilter() {

    let status = this.vehicleFilterForm.touched && this.vehicleFilterForm.dirty;

    this.vehicleFilterForm.reset();
    this.isSubmitted = false;
    this.isVisible = false;
    if (status) {
      this.listing();
    }
  }

  ngOnDestroy() {
    this._unsubscribe.next(true);
    this._unsubscribe.complete();
  }
}
