import { Component, OnInit, OnDestroy } from "@angular/core";
import { Location } from "@angular/common";
import { ActivatedRoute, Router } from "@angular/router";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import {
  validateAllFormFields,
  noWhitespaceValidator,
  priceCompareValidor,
  blankSpaceInputNotValid,
} from "../../../shared/utils/custom-validators";
import { VehicleService } from "app/core/services/vehicle.service";
import { FileUploadService } from "app/core/services/file-upload.service";
import { NgxUiLoaderService } from "ngx-ui-loader";
import { MessageService, ConfirmationService } from "primeng/api";
import { UtilityService } from "app/core/services/utility.service";
import { ErrorHandlerService } from "app/core/services/error-handler.service";
import { Subject } from "rxjs/internal/Subject";
import { takeUntil } from "rxjs/operators";

interface Country {
  name: string;
  code: string;
}

interface City {
  name: string;
  code: string;
}

interface Mechanism {
  name: string;
  code: string;
}

interface Tyre {
  name: string;
  code: string;
}

interface Wheel {
  name: string;
  code: string;
}

interface Stroke {
  name: string;
  code: string;
}

interface Front {
  name: string;
  code: string;
}

@Component({
  selector: "app-vehicle-form",
  templateUrl: "./vehicle-form.component.html",
  styleUrls: ["./vehicle-form.component.scss"],
})
export class VehicleFormComponent implements OnInit, OnDestroy {
  countries: Country[];
  selectedCountry: Country;
  cities: City[];
  selectedCity: City;
  mechanisms: Mechanism[];
  selectedMechanism: Mechanism;
  tyres: Tyre[];
  selectedTyre: Tyre;
  wheels: Wheel[];
  selectedWheel: Wheel;
  strokes: Stroke[];
  selectedStroke: Stroke;
  fronts: Front[];
  selectedFront: Front;
  public isSubmitted: Boolean = false;
  sectionTitle: string;
  buttonName: string;

  id: number;
  imageDisplay: boolean = false;
  imageUrl: any;

  private _unsubscribe = new Subject<Boolean>();

  vehicleForm: FormGroup;
  imageUrls: any = [];
  imageObject: any = [];
  selectedFile: File;

  url: any;
  usage = [
    {
      label: "Used",
      value: "used",
    },
    {
      label: "New",
      value: "new",
    },
  ];

  countryListFromAPI: any[];
  cityListFromAPI: [{ country_id: number }];
  mechanismListFromAPI: string[];
  tyreListFromAPI: string[];
  wheelListFromAPI: string[];
  strokeListFromAPI: string[];
  frontListFromAPI: string[];
  vehicleData: any;
  modelListFromAPI;
  brands;
  models;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private location: Location,
    private vehicleService: VehicleService,
    private loader: NgxUiLoaderService,
    private messageService: MessageService,
    private uploader: FileUploadService,
    private confirmationService: ConfirmationService,
    public utility: UtilityService,
    private errorHandler: ErrorHandlerService
  ) {
    this.sectionTitle = route.snapshot.data["sectionTitle"];
    this.buttonName = route.snapshot.data["buttonName"];
  }

  ngOnInit() {
    this.url = this.route.snapshot.url[0].path;
    this.vehicleForm = new FormGroup(
      {
        country: new FormControl("", [Validators.required]),
        city: new FormControl("", [Validators.required]),
        brand: new FormControl("", [Validators.required]),
        model: new FormControl("", [Validators.required]),
        color: new FormControl("", [
          Validators.required,
          noWhitespaceValidator,
          blankSpaceInputNotValid,
        ]),
        engine_capacity: new FormControl("", [
          Validators.required,
          noWhitespaceValidator,
          Validators.pattern(/^\d+(\.\d{1,2})?$/),
          blankSpaceInputNotValid,
        ]),
        engine_number: new FormControl("", [Validators.required]),
        starting_mechanism: new FormControl("", [
          Validators.required,
          noWhitespaceValidator,
          blankSpaceInputNotValid,
        ]),
        tyre_type: new FormControl("", [
          Validators.required,
          noWhitespaceValidator,
          blankSpaceInputNotValid,
        ]),
        wheel_type: new FormControl("", [
          Validators.required,
          noWhitespaceValidator,
          blankSpaceInputNotValid,
        ]),
        usage_status: new FormControl(null, [Validators.required]),
        price: new FormControl("", [
          Validators.required,
          Validators.pattern(/^\d+(\.\d{1,2})?$/),
          noWhitespaceValidator,
          blankSpaceInputNotValid,
        ]),
        lease_price: new FormControl("", [
          Validators.required,
          Validators.pattern(/^\d+(\.\d{1,2})?$/),
          noWhitespaceValidator,
          blankSpaceInputNotValid,
        ]),
        vehicle_number: new FormControl("", [
          Validators.required,
          noWhitespaceValidator,
          blankSpaceInputNotValid,
        ]),
        mileage: new FormControl("", [
          Validators.required,
          Validators.pattern(/^\d+(\.\d{1,2})?$/),
          noWhitespaceValidator,
          blankSpaceInputNotValid,
        ]),
        registration_card: new FormControl(""),
        insurance: new FormControl(""),
        vehicle_images: new FormControl(""),
        currency: new FormControl(""),
        chasing_number: new FormControl("", [Validators.required]),
      },
      { validators: priceCompareValidor }
    );

    const pageContent = this.route.snapshot.data.pageData.data;

    // this.countries = this.utility.arrayOfObjectToConvertInDropdownFormat(pageContent.countryList, "country", "country_id");
    this.countries = pageContent.countryList;

    this.countryListFromAPI = pageContent.countryList;

    this.cityListFromAPI = pageContent.cityList;

    this.cities = [];

    this.mechanisms = this.utility.arrayOfStringsToArrayOfObjects(
      pageContent.starting_mechanism
    );

    this.modelListFromAPI = pageContent.vehicleModel;
    this.brands = this.utility.arrayOfObjectToConvertInDropdownFormat(
      pageContent.vehicleBrand,
      "brand_name",
      "brand_id"
    );
    this.models = [];

    this.vehicleData = pageContent.vehicleList;

    if (this.vehicleData) {
      this.id = this.vehicleData.vehicle_id;
      //delete this.vehicleData.currency;

      let selectedCountry = this.countryListFromAPI.filter((element: any) => {
        return element.country_id == this.vehicleData.country;
      });
      this.filterModel(this.vehicleData.brand);
      this.filtercities(selectedCountry[0]);

      this.imageObject = this.vehicleData.vehicle_images;

      delete this.vehicleData.vehicle_id;
      this.vehicleData.country = selectedCountry[0];

      this.vehicleForm.patchValue(this.vehicleData);
    }
  }

  disableOptionClick(isDisabledOption) {
    if (isDisabledOption) {
      event.stopPropagation();
    }
  }

  /** Saves vehicle details */
  onSubmit() {
    this.isSubmitted = true;

    if (this.vehicleForm.valid) {
      this.loader.start();
      if (this.vehicleData) {
      }
      this.vehicleForm.value.vehicle_id = this.id;

      let data = Object.assign({}, this.vehicleForm.value);

      data.country = this.vehicleForm.controls.country.value.country_id;

      this.vehicleService
        .addVehicle(data)
        .pipe(takeUntil(this._unsubscribe))
        .subscribe(
          (success: any) => {
            this.messageService.add({
              severity: "success",
              summary: "Action Success",
              detail: success.message,
            });
            this.loader.stop();
            this.router.navigateByUrl("/admin/vehicles-list");
          },
          (error) => {
            this.loader.stop();
            this.errorHandler.routeAccordingToError(error);
          }
        );
      this.isSubmitted = false;
    } else {
      this.loader.stop();
      validateAllFormFields(this.vehicleForm);
      this.loader.stop();
      this.utility.scrollToError();
    }
  }

  /** filter cities on the basis of country */
  filtercities(country) {
    this.vehicleForm.controls.city.setValue("");
    if (country != null) {
      let na = [];
      this.cityListFromAPI.forEach((city) => {
        if (city.country_id == country.country_id) na.push(city);
        else return;
      });
      this.cities = this.utility.arrayOfObjectToConvertInDropdownFormat(
        na,
        "city",
        "city_id"
      );

      this.vehicleForm.controls.currency.setValue(country.currency);
    } else {
      this.vehicleForm.controls.currency.setValue("");
      this.cities = [];
    }
  }

  /** filter cities on the basis of country */
  filterModel(brand) {
    this.vehicleForm.controls.model.setValue("");
    if (brand != null) {
      let newModel = [];
      this.modelListFromAPI.forEach((model) => {
        if (model.brand_id == brand) {
          newModel.push(model);
        }
      });
      this.models = this.utility.arrayOfObjectToConvertInDropdownFormat(
        newModel,
        "model_name",
        "model_id"
      );
    } else {
      this.models = [];
    }
  }
  /** upload images  */
  uploadImage(event, key) {
    //application/pdf
    var imageType = /image.*/;
    this.vehicleForm.controls[key].setErrors(null);
    this.isSubmitted = false;
    this.selectedFile = <File>event.target.files[0];

    if (!event.target.files[0].type.match(imageType)) {
      //let control = this.vehicleForm.controls[key].setErrors({ "pattern": true })
      this.messageService.add({
        severity: "error",
        summary: "Invalid Type",
        detail: "Only image type is allowed.",
      });
      return false;
    } else {
      this.vehicleForm.controls[key].setErrors(null);
    }

    if (2000000 < event.target.files[0].size) {
      //allow 2 mb
      // this.vehicleForm.controls[key].setErrors({ "maxLength": true });
      this.messageService.add({
        severity: "error",
        summary: "size exceeds",
        detail: "You can upload only 2 MB size files.",
      });
      return false;
    } else {
      this.vehicleForm.controls[key].setErrors(null);
    }

    this.uploader
      .fileUploadImage(this.selectedFile)
      .pipe(takeUntil(this._unsubscribe))
      .subscribe((success: any) => {
        this.vehicleForm.controls[key].setValue(success.image.url);

        this.vehicleForm.controls[key].markAsDirty();
        this.vehicleForm.controls[key].markAsTouched();

        this.messageService.add({
          severity: "success",
          summary: "File(s) attached",
          detail: "",
        });
      });
  }

  /** add vehicle images */
  add_vehicle_images(event) {
    var imageType = /image.*/;
    let fileArray = event.target.files;

    let errorType = false;
    let errorSize = false;
    this.vehicleForm.controls["vehicle_images"].setErrors(null);
    this.isSubmitted = false;
    Array.from(fileArray).forEach((element) => {
      if (!element["type"].match(imageType)) {
        errorType = true;
      }
      if (2000000 < element["size"]) {
        //allow 2 mb
        errorSize = true;
      }
    });

    if (errorType) {
      // let control = this.vehicleForm.controls["vehicle_images"].setErrors({ "pattern": true })
      this.messageService.add({
        severity: "error",
        summary: "Invalid Type",
        detail: "Only image type is allowed.",
      });
      return false;
    } else {
      this.vehicleForm.controls["vehicle_images"].setErrors(null);
    }

    if (errorSize) {
      this.messageService.add({
        severity: "error",
        summary: "size exceeds",
        detail: "You can upload only 2 MB size files.",
      });
      return false;
    } else {
      this.vehicleForm.controls["vehicle_images"].setErrors(null);
    }

    this.uploader
      .fileUploadImages(Array.from(event.target.files))
      .pipe(takeUntil(this._unsubscribe))
      .subscribe((success: any) => {
        success.image.forEach((element) => {
          this.imageObject.push({ vehicle_image: element.url });
          this.imageUrls.push(element.url);
        });

        this.vehicleForm.controls["vehicle_images"].setValue(this.imageObject);
        this.vehicleForm.controls["vehicle_images"].markAsDirty();
        this.vehicleForm.controls["vehicle_images"].markAsTouched();

        this.messageService.add({
          severity: "success",
          summary: "Files added",
          detail: "",
        });
      });
  }

  onBasicUpload(event) {
    this.selectedFile = <File>event.target.files[0];

    this.uploader
      .fileUploadImage(this.selectedFile)
      .subscribe((success: any) => {
        this.messageService.add({
          severity: "success",
          summary: "File added",
          detail: "",
        });
      });
  }

  resetForm() {
    if (this.vehicleForm.dirty && this.vehicleForm.touched) {
      this.confirmationService.confirm({
        message: "You have some unsaved changes.",
        header: "Confirmation",
        accept: () => {
          this.onSubmit();
        },
        reject: () => {
          this.location.back();
        },
        rejectLabel: "Discard Changes",
        acceptLabel: "Save & Close",
      });
    } else {
      this.location.back();
      this.vehicleForm.reset();
      this.isSubmitted = false;
    }

    // this.display = false;
  }

  get vehForm() {
    return this.vehicleForm.controls;
  }

  delete(index, image, type) {
    console.log(index, image, type);
    this.confirmationService.confirm({
      message: "Do you want to remove Image?",
      header: "Confirmation",
      reject: () => {},
      rejectLabel: "No",
      accept: () => {
        if (type === "vehicle-image") {
          // using splice method
          this.imageUrls.splice(index, 1);
          this.imageObject.splice(index, 1);

          if (this.imageUrls.length != 0) {
            this.vehicleForm.controls["vehicle_images"].setValue(
              this.imageObject
            );
          } else {
            this.vehicleForm.controls["vehicle_images"].setValue("");
          }
          this.vehicleForm.controls["vehicle_images"].markAsDirty();
          this.vehicleForm.controls["vehicle_images"].markAsTouched();
        }
        if (type === "registration") {
          this.vehicleForm.controls["registration_card"].setValue("");
          this.vehicleForm.controls.registration_card.reset();
        }
        if (type === "insurance") {
          this.vehicleForm.controls["insurance"].setValue("");
          this.vehicleForm.controls.insurance.reset();
        }
      },
      acceptLabel: "Yes",
    });
  }

  // image dialog wrapper
  openImageDialog(key) {
    this.imageDisplay = true;

    if (key == "registration_card") {
      this.imageUrl = this.vehicleForm.controls.registration_card.value;
    } else if (key == "insurance") {
      this.imageUrl = this.vehicleForm.controls.insurance.value;
    } else if (key.vehicle_image != "") {
      this.imageUrl = key.vehicle_image;
    }
  }

  ngOnDestroy() {
    this._unsubscribe.next(true);
    this._unsubscribe.complete();
  }
}
