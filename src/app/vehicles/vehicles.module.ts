import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VehiclesRoutingModule } from './vehicles-routing.module';
import { SharedModule } from 'app/shared/shared.module';
import { VehicleListingComponent } from './pages/vehicle-listing/vehicle-listing.component';
import { VehicleFormComponent } from './pages/vehicle-form/vehicle-form.component';
import { InventoryComponent } from './pages/inventory/inventory.component';
import { VehicleDetailsComponent } from './pages/vehicle-details/vehicle-details.component';
import { VehiclesLayoutComponent } from './vehicles-layout/vehicles-layout.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    VehiclesRoutingModule,
  ],
  declarations: [VehicleListingComponent, VehicleFormComponent, InventoryComponent, VehicleDetailsComponent, VehiclesLayoutComponent]
})
export class VehiclesModule { }
