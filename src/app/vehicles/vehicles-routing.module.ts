import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VehicleListingComponent } from './pages/vehicle-listing/vehicle-listing.component';
import { VehicleFormComponent } from './pages/vehicle-form/vehicle-form.component';
import { InventoryComponent } from './pages/inventory/inventory.component';
import { VehicleDetailsComponent } from './pages/vehicle-details/vehicle-details.component';
import { VehicleManagementResolverService } from 'app/core/resolver/vehicle-management-resolver.service';

import { AuthGuardService as AuthGuard } from './../core/guards/auth-guard.service';
import { RolesGuardService as RoleGuard } from './../core/guards/roles-guard.service';
import { VehiclesLayoutComponent } from './vehicles-layout/vehicles-layout.component';

const routes: Routes = [
  {
    path: '',
    data: {
      breadcrumb: 'Vehicle list'
    },
    children: [
      {
        path: 'vehicles-list',
        component: VehicleListingComponent,
        canActivate: [AuthGuard, RoleGuard],
        data: {
          breadcrumb: 'Vehicle List',
          sectionTitle: 'Vehicle List',
          onlyForUser: ['super_admin'],
          expectedRole: ['vehicle-management']
        },
        resolve: {
          pageData: VehicleManagementResolverService
        }
      },
      {
        path: 'vehicles-list/add',
        component: VehicleFormComponent,
        canActivate: [AuthGuard, RoleGuard],
        data: {
          breadcrumb: 'Add Vehicle',
          sectionTitle: 'Add Vehicle',
          buttonName: 'Add',
          onlyForUser: ['super_admin'],
          expectedRole: ['vehicle-management']
        }, resolve: {
          pageData: VehicleManagementResolverService
        }
      },
      {
        path: 'vehicles-list/details/:id',
        component: VehicleDetailsComponent,
        canActivate: [AuthGuard, RoleGuard],
        data: {
          breadcrumb: 'Add Vehicle',
          sectionTitle: 'Details',
          buttonName: 'Add Vehicle',
          onlyForUser: ['super_admin'],
          expectedRole: ['vehicle-management']
        }, resolve: {
          pageData: VehicleManagementResolverService
        }
      },
      {
        path: 'vehicles-list/edit/:id',
        component: VehicleFormComponent,
        canActivate: [AuthGuard, RoleGuard],
        data: {
          sectionTitle: 'Edit Vehicle',
          breadcrumb: 'Add Vehicle',
          buttonName: 'Save',
          onlyForUser: ['super_admin'],
          expectedRole: ['vehicle-management']
        }, resolve: {
          pageData: VehicleManagementResolverService
        }
      },
      {
        path: 'vehicles-inventory',
        component: InventoryComponent,
        canActivate: [AuthGuard, RoleGuard],
        data: {
          sectionTitle: 'Inventory',
          onlyForUser: ['super_admin'],
          expectedRole: ['vehicle-inventory']
        },
        resolve: {
          pageData: VehicleManagementResolverService
        }
      },

    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VehiclesRoutingModule { }
