import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VendorTicketsRoutingModule } from './vendor-tickets-routing.module';
import { TicketsListComponent } from './pages/tickets-list/tickets-list.component';
import { SharedModule } from 'app/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    VendorTicketsRoutingModule,
    SharedModule
  ],
  declarations: [TicketsListComponent]
})
export class VendorTicketsModule { }
