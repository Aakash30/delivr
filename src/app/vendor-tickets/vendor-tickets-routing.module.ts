import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TicketsListComponent } from './pages/tickets-list/tickets-list.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        component: TicketsListComponent,
        data: {
          sectionTitle: 'Tickets'
        }
      },
      {
        path: 'tickets-list',
        component: TicketsListComponent,
        data: {
          sectionTitle: 'Tickets',
          buttonName: 'Create'
        }
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VendorTicketsRoutingModule { }
