import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { trigger, transition, style, animate } from '@angular/animations';
import { MenuItem } from 'primeng/api';

interface Brand {
  name: string;
  code: string;
}

interface Country {
  name: string;
  code: string;
}

interface City {
  name: string;
  code: string;
}

interface Status {
  name: string;
  code: string;
}

interface StatusList {
  name: string;
  code: string;
}


@Component({
  selector: 'app-maintenance-list',
  templateUrl: './maintenance-list.component.html',
  styleUrls: ['./maintenance-list.component.scss'],
  animations: [
    trigger('openClose', [
      transition(':enter', [
        style({ transform: 'translateX(20%)', opacity: 0 }),
        animate('500ms', style({ transform: 'translateX(0)', opacity: 1 })),
      ]),
      transition(':leave', [
        style({ transform: 'translateX(0)', opacity: 1, display: 'none' }),
        animate('500ms', style({ transform: 'translateX(20%)', opacity: 0 })),
      ]),
    ])
  ],
})
export class MaintenanceListComponent implements OnInit {

  activeTab: string;
  items: MenuItem[];
  status: Status[];
  selectedStatus: Status;
  statusList: StatusList[];
  selectedStatusList: StatusList;
  activeItem: MenuItem;
  currentList: any[];
  serviceList: any[];
  pastList: any[];
  sectionTitle: String;
  brands: Brand[];
  selectedBrand: Brand;
  countries: Country[];
  selectedCountry: Country;
  cities: City[];
  selectedCity: City;
  addDisplay: Boolean = false;

  previousVal: any;
  currentVal: any;

  selectedValue: string = 'Service';
  // editDisplay: Boolean = false;
  checked: boolean;
  showDescription() {
    this.addDisplay = true;
  }
  // editCountry() {
  //   this.editDisplay = true;
  // }
  constructor() { }
  getItem(event, index) {
    event.preventDefault();
    if (index === 0) {
      this.activeTab = 'current';
    } else if (index === 1) {
      this.activeTab = 'service';
    } else if (index === 2) {
      this.activeTab = 'past';
    }
  }

  isSubmitted: Boolean = false;
  maintenanceForm: FormGroup;
  maintenanceListForm: FormGroup;

  brandListFromAPI: string[];
  countryListFromAPI: string[];
  cityListFromAPI: string[];
  statusListFromAPI: string[];

  // show filter function
  isVisible: boolean = false;

  showFilter() {
    this.isVisible = !this.isVisible;
  }

  ngOnInit() {
    this.selectedStatusList = null;
    this.activeTab = 'current';

    this.items = [
      {
        label: 'Current'
      },
      {
        label: 'Service History'
      },
      {
        label: 'Past History'
      },
    ];
    this.currentList = [
      {
        id: 1,
        brand: 'bajaj',
        model: 'pulsar TS',
        registration: 'MP 09 SD 2847',
        lastService: 'Apr 12, 2019',
        distance: '1879',
        location: 'Hatta',
        status: 'Pending',
      },
      {
        id: 2,
        brand: 'bajaj',
        model: 'pulsar TS',
        registration: 'MP 09 SD 2847',
        lastService: 'Apr 12, 2019',
        distance: '1879',
        location: 'Hatta',
        status: 'Pending',
      },
      {
        id: 3,
        brand: 'bajaj',
        model: 'pulsar TS',
        registration: 'MP 09 SD 2847',
        lastService: 'Apr 12, 2019',
        distance: '1879',
        location: 'Hatta',
        status: 'Outgoing',
      }
    ];
    this.serviceList = [
      {
        id: 1,
        brand: 'bajaj',
        model: 'pulsar TS',
        registration: 'MP 09 SD 2847',
        lastService: 'Apr 12, 2019',
        distance: '1879',
        location: 'Hatta',
        status: 'Pending',
      },
      {
        id: 2,
        brand: 'bajaj',
        model: 'pulsar TS',
        registration: 'MP 09 SD 2847',
        lastService: 'Apr 12, 2019',
        distance: '1879',
        location: 'Hatta',
        status: 'Finished',
      },
      {
        id: 3,
        brand: 'bajaj',
        model: 'pulsar TS',
        registration: 'MP 09 SD 2847',
        lastService: 'Apr 12, 2019',
        distance: '1879',
        location: 'Hatta',
        status: 'Finished',
      }
    ];
    this.pastList = [
      {
        id: 1,
        brand: 'bajaj',
        model: 'pulsar TS',
        registration: 'MP 09 SD 2847',
        lastService: 'Apr 12, 2019',
        distance: '1879',
        location: 'Hatta',
        status: 'Outgoing',
      },
      {
        id: 2,
        brand: 'bajaj',
        model: 'pulsar TS',
        registration: 'MP 09 SD 2847',
        lastService: 'Apr 12, 2019',
        distance: '1879',
        location: 'Hatta',
        status: 'Finished',
      },
      {
        id: 3,
        brand: 'bajaj',
        model: 'pulsar TS',
        registration: 'MP 09 SD 2847',
        lastService: 'Apr 12, 2019',
        distance: '1879',
        location: 'Hatta',
        status: 'Finished',
      }
    ];
    this.activeItem = this.items[0];

    this.maintenanceForm = new FormGroup({
      brand: new FormControl(''),
      model: new FormControl(''),
      serviceRangeOne: new FormControl('', [Validators.pattern(/^[0-9]\d{0,3}$/)]),
      serviceRangeTwo: new FormControl('', [Validators.pattern(/^[0-9]\d{0,3}$/)]),
      distanceRangeOne: new FormControl('', [Validators.pattern(/^[0-9]\d{0,3}$/)]),
      distanceRangeTwo: new FormControl('', [Validators.pattern(/^[0-9]\d{0,3}$/)]),
      country: new FormControl(''),
      city: new FormControl(''),
      status: new FormControl(''),
      statusList: new FormControl(''),

    });

    this.maintenanceListForm = new FormGroup({

    });

    this.brandListFromAPI = ['Audi', 'BMW', 'Fiat'];
    this.brands = this.arrayOfStringsToArrayOfObjects(this.brandListFromAPI);

    this.countryListFromAPI = ['Audi', 'BMW', 'Fiat'];
    this.countries = this.arrayOfStringsToArrayOfObjects(this.brandListFromAPI);

    this.cityListFromAPI = ['Audi', 'BMW', 'Fiat'];
    this.cities = this.arrayOfStringsToArrayOfObjects(this.cityListFromAPI);

    this.statusListFromAPI = ['Pending', 'Outgoing', 'Finished'];
    this.status = this.arrayOfStringsToArrayOfObjects(this.statusListFromAPI);

    this.statusListFromAPI = ['Pending', 'Outgoing', 'Finished'];
    this.statusList = this.arrayOfStringsToArrayOfObjects(this.statusListFromAPI);
    // brand dropdown list
    // this.brands = [
    //   { name: 'New York', code: 'NY' },
    //   { name: 'Rome', code: 'RM' },
    //   { name: 'London', code: 'LDN' },
    //   { name: 'Istanbul', code: 'IST' },
    //   { name: 'Paris', code: 'PRS' }
    // ];
    // Country dropdown list
    // this.countries = [
    //   { name: 'New York', code: 'NY' },
    //   { name: 'Rome', code: 'RM' },
    //   { name: 'London', code: 'LDN' },
    //   { name: 'Istanbul', code: 'IST' },
    //   { name: 'Paris', code: 'PRS' }
    // ];
    // City dropdown list
    // this.cities = [
    //   { name: 'New York', code: 'NY' },
    //   { name: 'Rome', code: 'RM' },
    //   { name: 'London', code: 'LDN' },
    //   { name: 'Istanbul', code: 'IST' },
    //   { name: 'Paris', code: 'PRS' }
    // ];
    // Status dropdown list
    // this.status = [
    //   { name: 'Pending', code: ' P' },
    //   { name: 'Outgoing', code: 'OG' },
    //   { name: 'Finished', code: 'FIN' },
    // ];
  }

  arrayOfStringsToArrayOfObjects(arr: string[]) {
    const newArray = [];
    arr.forEach(element => {
      newArray.push({
        label: element,
        value: element
      });
    });
    return newArray;
  }

  arrayOfObjectToArrayOfStrings(obj: []) {
    const newArray = [];
    obj.forEach(element => {
      newArray.push(element['value']);
    });
    return newArray;
  }

  onSubmit() {
    this.isSubmitted = true;
    if (this.maintenanceForm.valid) {
      console.log(this.maintenanceForm.value);
      this.maintenanceForm.reset();
      this.isSubmitted = false;
    }
    else {
      console.log('Fill all required fields');
    }
  }

  closeFilter() {
    this.maintenanceForm.reset();
    this.isSubmitted = false;
    this.isVisible = false;
  }



  resetForm() {

    this.maintenanceForm.reset();
    this.isSubmitted = false;
    // this.display = false;
  }

  onSelectType(event, id) {
    // console.log("event target value::", event);
    if (event) {
      this.previousVal = this.currentVal;
      this.currentVal = event;
    }
    this.currentVal = this.selectedStatusList;
    this.selectedStatusList = null;
    console.log("previousVal:::", this.previousVal);
    console.log("current val:::", this.currentVal);
  }

}
