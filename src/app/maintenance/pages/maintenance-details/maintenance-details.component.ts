import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { FormGroup, FormControl, Validators, AbstractControl, FormArray } from '@angular/forms';
import { validateAllFormFields, noWhitespaceValidator } from '../../../shared/utils/custom-validators';

interface Status {
  name: string;
  code: string;
}


@Component({
  selector: 'app-maintenance-details',
  templateUrl: './maintenance-details.component.html',
  styleUrls: ['./maintenance-details.component.scss'],

})
export class MaintenanceDetailsComponent implements OnInit {


  addDisplay: Boolean = false;
  checked: boolean;
  showDialog() {
    this.addDisplay = true;
  }
  status: Status[];
  selectedStatus: Status[];
  selectedValue: string = 'Service';

  isSubmitted: Boolean = false;
  maintenanceDetailsForm: FormGroup;

  constructor() { }


  ngOnInit() {

    this.maintenanceDetailsForm = new FormGroup({
      brand: new FormControl(''),
      service: new FormControl(''),
      partRepair: new FormControl(''),
      description: new FormControl('', [Validators.required, Validators.maxLength(200), noWhitespaceValidator]),
    });

    // Status dropdown list
    this.status = [
      { name: 'Pending', code: ' P' },
      { name: 'Outgoing', code: 'OG' },
      { name: 'Finished', code: 'FIN' },
    ];
  }

  onSubmit() {
    this.isSubmitted = true;
    if (this.maintenanceDetailsForm.valid) {
      console.log(this.maintenanceDetailsForm.value);
      this.maintenanceDetailsForm.reset();
      this.isSubmitted = false;
      this.addDisplay = false;
    }
    else {
      validateAllFormFields(this.maintenanceDetailsForm);
      console.log('Fill all required fields');
    }
  }
  resetForm() {

    this.maintenanceDetailsForm.reset();
    this.isSubmitted = false;
    this.addDisplay = false;
    // this.display = false;
  }

  // form convient getter
  get meForm() {
    return this.maintenanceDetailsForm.controls;
  }
}


