import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MaintenanceRoutingModule } from './maintenance-routing.module';
import { MaintenanceDetailsComponent } from './pages/maintenance-details/maintenance-details.component';
import { SharedModule } from 'app/shared/shared.module';
import { MaintenanceListComponent } from './pages/maintenance-list/maintenance-list.component';

@NgModule({
  imports: [
    CommonModule,
    MaintenanceRoutingModule,
    SharedModule
  ],
  declarations: [MaintenanceDetailsComponent, MaintenanceListComponent]
})
export class MaintenanceModule { }
