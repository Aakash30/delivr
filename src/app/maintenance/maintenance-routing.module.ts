import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MaintenanceDetailsComponent } from './pages/maintenance-details/maintenance-details.component';
import { MaintenanceListComponent } from './pages/maintenance-list/maintenance-list.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        redirectTo: 'list',
        pathMatch: 'full',
        data: {
          sectionTitle: 'Maintenance',
        }
      },
      {
        path: 'list',
        component: MaintenanceListComponent,
        data: {
          sectionTitle: 'Maintenance',
        }
      },
      {
        path: 'details/:id',
        component: MaintenanceDetailsComponent,
        data: {
          sectionTitle: 'Details',
          buttonName: 'Add Vehicle'
        }
      },

    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MaintenanceRoutingModule { }
