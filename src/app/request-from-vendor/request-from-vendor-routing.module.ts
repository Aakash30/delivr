import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RequestFromVendorListComponent } from './pages/request-from-vendor-list/request-from-vendor-list.component';
import { RequestFromVendorDetailsComponent } from './pages/request-from-vendor-details/request-from-vendor-details.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        redirectTo: 'list',
        pathMatch: 'full',
        data: {
          sectionTitle: 'Request from vendor',
        }
      },
      {
        path: 'list',
        component: RequestFromVendorListComponent,
        data: {
          sectionTitle: 'Request from Vendor',
        }
      },
      {
        path: 'details/:id',
        component: RequestFromVendorDetailsComponent,
        data: {
          sectionTitle: 'Details',
          buttonName: 'Add Vehicle'
        }
      },

    ]
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RequestFromVendorRoutingModule { }
