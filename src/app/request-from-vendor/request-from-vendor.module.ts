import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RequestFromVendorRoutingModule } from './request-from-vendor-routing.module';
import { RequestFromVendorListComponent } from './pages/request-from-vendor-list/request-from-vendor-list.component';
import { SharedModule } from 'app/shared/shared.module';
import { RequestFromVendorDetailsComponent } from './pages/request-from-vendor-details/request-from-vendor-details.component';

@NgModule({
  imports: [
    CommonModule,
    RequestFromVendorRoutingModule,
    SharedModule
  ],
  declarations: [RequestFromVendorListComponent, RequestFromVendorDetailsComponent]
})
export class RequestFromVendorModule { }
