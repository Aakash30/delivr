import { Component, OnInit } from '@angular/core';
import { trigger, transition, style, animate } from '@angular/animations';
import { MenuItem } from 'primeng/api';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { UtilityService } from 'app/core/services/utility.service';

interface Country {
  name: string;
  code: string;
}

interface City {
  name: string;
  code: string;
}

interface Status {
  name: string;
  code: string;
}

interface Priority {
  name: string;
  code: string;
}


@Component({
  selector: 'app-request-from-vendor-list',
  templateUrl: './request-from-vendor-list.component.html',
  styleUrls: ['./request-from-vendor-list.component.scss'],
  animations: [
    trigger('openClose', [
      transition(':enter', [
        style({ transform: 'translateX(20%)', opacity: 0 }),
        animate('500ms', style({ transform: 'translateX(0)', opacity: 1 })),
      ]),
      transition(':leave', [
        style({ transform: 'translateX(0)', opacity: 1, display: 'none' }),
        animate('500ms', style({ transform: 'translateX(20%)', opacity: 0 })),
      ]),
    ])
  ],
})
export class RequestFromVendorListComponent implements OnInit {

  constructor(
    private _utility: UtilityService
  ) { }

  countries: Country[];
  selectedCountry: Country;
  cities: City[];
  selectedCity: City;
  status: Status[];
  selectedStatus: Status[];
  priorities: Priority[];
  selectedPriority: Priority;
  activeTab: string;
  items: MenuItem[];
  activeItem: MenuItem;
  requestList: any[];
  pendingList: any[];
  resolvedList: any[];

  getItem(event, index) {
    event.preventDefault();
    if (index === 0) {
      this.activeTab = 'request';
    } else if (index === 1) {
      this.activeTab = 'pending';
    } else if (index === 2) {
      this.activeTab = 'resolved';
    }
  }

  isSubmitted: Boolean = false;

  requestVendorForm: FormGroup;
  priorityListFromAPI: string[];
  countryListFromAPI: string[];
  cityListFromAPI: string[];
  statusListFromAPI: string[];

  // show filter function
  isVisible: boolean = false;
  selectedValue: string = 'Service';

  showFilter() {
    this.isVisible = !this.isVisible;

  }

  ngOnInit() {
    this.activeTab = 'request';

    this.items = [
      {
        label: 'All Request'
      },
      {
        label: 'Pending'
      },
      {
        label: 'Resolved'
      },
    ];
    this.requestList = [
      {
        id: 1,
        title: 'Engine fail',
        priority: 'Medium',
        category: 'lease',
        vendorName: 'Kelly Pratt',
        location: 'Indore, India',
        status: 'Pending',
      },
      {
        id: 2,
        title: 'Engine fail',
        priority: 'High',
        category: 'lease',
        vendorName: 'Kelly Pratt',
        location: 'Indore, India',
        status: 'Pending',
      },
      {
        id: 3,
        title: 'Engine fail',
        priority: 'Medium',
        category: 'lease',
        vendorName: 'Kelly Pratt',
        location: 'Indore, India',
        status: 'Pending',
      }
    ];
    this.pendingList = [
      {
        id: 1,
        title: 'Engine fail',
        priority: 'Medium',
        category: 'lease',
        vendorName: 'Kelly Pratt',
        location: 'Indore, India',
        status: 'Pending',
      },
      {
        id: 2,
        title: 'Engine fail',
        priority: 'Low',
        category: 'lease',
        vendorName: 'Kelly Pratt',
        location: 'Indore, India',
        status: 'Pending',
      },
      {
        id: 3,
        title: 'Engine fail',
        priority: 'Medium',
        category: 'lease',
        vendorName: 'Kelly Pratt',
        location: 'Indore, India',
        status: 'Pending',
      }
    ];
    this.resolvedList = [
      {
        id: 1,
        title: 'Engine fail',
        priority: 'High',
        category: 'lease',
        vendorName: 'Kelly Pratt',
        location: 'Indore, India',
        status: 'Pending',
      },
      {
        id: 2,
        title: 'Engine fail',
        priority: 'Medium',
        category: 'lease',
        vendorName: 'Kelly Pratt',
        location: 'Indore, India',
        status: 'Pending',
      },
      {
        id: 3,
        title: 'Engine fail',
        priority: 'High',
        category: 'lease',
        vendorName: 'Kelly Pratt',
        location: 'Indore, India',
        status: 'Pending',
      }
    ];
    this.activeItem = this.items[0];

    this.requestVendorForm = new FormGroup({
      priority: new FormControl('High'),
      category: new FormControl(''),
      country: new FormControl(''),
      city: new FormControl(''),
      status: new FormControl('Pending'),
    });

    this.priorityListFromAPI = ['High', 'Medium', 'Low'];
    this.priorities = this._utility.arrayOfStringsToArrayOfObjects(this.priorityListFromAPI);

    this.countryListFromAPI = ['New York', 'London', 'Paris'];
    this.countries = this._utility.arrayOfStringsToArrayOfObjects(this.countryListFromAPI);

    this.cityListFromAPI = ['New York', 'London', 'Paris'];
    this.cities = this._utility.arrayOfStringsToArrayOfObjects(this.cityListFromAPI);

    this.statusListFromAPI = ['Pending', 'Outgoing', 'Finished'];
    this.status = this._utility.arrayOfStringsToArrayOfObjects(this.statusListFromAPI);
  }



  onSubmit() {
    this.isSubmitted = true;
    if (this.requestVendorForm.valid) {
      this.requestVendorForm.reset();
      this.isSubmitted = false;
    }
    else {
    }
  }

  closeFilter() {
    this.requestVendorForm.reset();
    this.isSubmitted = false;
    this.isVisible = false;
  }

}
