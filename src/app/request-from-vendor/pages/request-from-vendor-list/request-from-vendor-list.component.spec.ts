import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestFromVendorListComponent } from './request-from-vendor-list.component';

describe('RequestFromVendorListComponent', () => {
  let component: RequestFromVendorListComponent;
  let fixture: ComponentFixture<RequestFromVendorListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequestFromVendorListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestFromVendorListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
