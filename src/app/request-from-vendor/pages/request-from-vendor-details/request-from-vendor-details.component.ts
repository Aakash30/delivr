import { Component, OnInit } from '@angular/core';

interface Status {
  name: string;
  code: string;
}

@Component({
  selector: 'app-request-from-vendor-details',
  templateUrl: './request-from-vendor-details.component.html',
  styleUrls: ['./request-from-vendor-details.component.scss']
})
export class RequestFromVendorDetailsComponent implements OnInit {

  addDisplay: Boolean = false;
  checked: boolean;
  showDialog() {
    this.addDisplay = true;
  }
  status: Status[];
  selectedStatus: Status;
  constructor() { }

  ngOnInit() {
    // Status dropdown list
    this.status = [
      { name: 'Pending', code: ' P' },
      { name: 'Outgoing', code: 'OG' },
      { name: 'Finished', code: 'FIN' },
    ];
  }

}
