import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestFromVendorDetailsComponent } from './request-from-vendor-details.component';

describe('RequestFromVendorDetailsComponent', () => {
  let component: RequestFromVendorDetailsComponent;
  let fixture: ComponentFixture<RequestFromVendorDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequestFromVendorDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestFromVendorDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
