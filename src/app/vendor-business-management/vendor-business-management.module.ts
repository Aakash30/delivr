import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VendorBusinessManagementRoutingModule } from './vendor-business-management-routing.module';
import { BusinessManagementListingComponent } from './pages/business-management-listing/business-management-listing.component';
import { BusinessManagementFormComponent } from './pages/business-management-form/business-management-form.component';
import { BusinessManagementDetailsComponent } from './pages/business-management-details/business-management-details.component';
import { SharedModule } from 'app/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    VendorBusinessManagementRoutingModule
  ],
  declarations: [BusinessManagementListingComponent, BusinessManagementFormComponent, BusinessManagementDetailsComponent]
})
export class VendorBusinessManagementModule { }
