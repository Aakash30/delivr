import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-business-management-details',
  templateUrl: './business-management-details.component.html',
  styleUrls: ['./business-management-details.component.scss']
})
export class BusinessManagementDetailsComponent implements OnInit {

  sectionTitle: any;
  buttonName: any;
  id: number;
  currentUrl: string;
  constructor(private route: ActivatedRoute, private router: Router) {
    this.sectionTitle = route.snapshot.data['sectionTitle'];
    this.buttonName = route.snapshot.data['buttonName']
    this.id = +(route.snapshot.paramMap.get('id'));
    this.currentUrl = router.url;

  }

  ngOnInit() {
  }

}
