import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BusinessManagementDetailsComponent } from './business-management-details.component';

describe('BusinessManagementDetailsComponent', () => {
  let component: BusinessManagementDetailsComponent;
  let fixture: ComponentFixture<BusinessManagementDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BusinessManagementDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinessManagementDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
