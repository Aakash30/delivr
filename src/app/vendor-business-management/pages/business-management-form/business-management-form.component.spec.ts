import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BusinessManagementFormComponent } from './business-management-form.component';

describe('BusinessManagementFormComponent', () => {
  let component: BusinessManagementFormComponent;
  let fixture: ComponentFixture<BusinessManagementFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BusinessManagementFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinessManagementFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
