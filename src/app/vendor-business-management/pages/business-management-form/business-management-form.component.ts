import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { validateAllFormFields, noWhitespaceValidator, blankSpaceInputNotValid } from '../../../shared/utils/custom-validators';
import { UtilityService } from 'app/core/services/utility.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';

interface Branch {
  name: string;
  code: string;
}

@Component({
  selector: 'app-business-management-form',
  templateUrl: './business-management-form.component.html',
  styleUrls: ['./business-management-form.component.scss']
})
export class BusinessManagementFormComponent implements OnInit {

  sectionTitle: any;
  buttonName: any;

  branches: Branch[];
  selectedBranch: Branch;
  isSubmitted: boolean = false;

  selectedValue: string[] = ['MC Donald'];
  checked: boolean = false;

  businessListForm: FormGroup;
  branchListFromAPI: string[];

  constructor(
    private utilityService: UtilityService,
    private route: ActivatedRoute,
    private location: Location,
    private router: Router
  ) {

    this.sectionTitle = route.snapshot.data['sectionTitle'];
    this.buttonName = route.snapshot.data['buttonName'];
  }

  ngOnInit() {
    this.businessListForm = new FormGroup({
      firstName: new FormControl('', [Validators.required, Validators.pattern(/^[a-zA-Z]*$/), noWhitespaceValidator, blankSpaceInputNotValid]),
      lastName: new FormControl('', [Validators.required, Validators.pattern(/^[a-zA-Z]*$/), noWhitespaceValidator, blankSpaceInputNotValid]),
      email: new FormControl('', [Validators.required, noWhitespaceValidator, blankSpaceInputNotValid]),
      mobile: new FormControl('', [Validators.required, Validators.pattern(/^[0-9]\d{0,9}$/)]),
      branch: new FormControl('', [Validators.required, noWhitespaceValidator, blankSpaceInputNotValid])
    });

    // branch list dropdown
    this.branchListFromAPI = ['Audi', 'BMW', 'Fiat'];
    this.branches = this.utilityService.arrayOfStringsToArrayOfObjects(this.branchListFromAPI);
  }

  get businessForm() {
    return this.businessListForm.controls;
  }

  onSubmit() {
    this.isSubmitted = true;
    if (this.businessListForm.valid) {
      this.businessListForm.reset();
      this.isSubmitted = false;
    }
    else {
      validateAllFormFields(this.businessListForm);
    }
  }

  // reset form
  resetForm() {
    this.location.back();
    this.businessListForm.reset();
    this.isSubmitted = false;
  }

}
