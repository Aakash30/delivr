import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-business-management-listing',
  templateUrl: './business-management-listing.component.html',
  styleUrls: ['./business-management-listing.component.scss']
})
export class BusinessManagementListingComponent implements OnInit {

  businessList: any[];
  constructor() { }

  ngOnInit() {

    this.businessList = [
      {
        id: 1,
        businessId: '101',
        name: 'Donalds',
        branchName: 'Mc Donald',
        address: 'Vijay Nagar',
        vehicles: '101',
        status: false
      },
      {
        id: 2,
        businessId: '102',
        name: 'Donalds',
        branchName: 'Mc Donald',
        address: 'Vijay Nagar',
        vehicles: '101',
        status: true
      },
      {
        id: 3,
        businessId: '103',
        name: 'Donalds',
        branchName: 'Mc Donald',
        address: 'Vijay Nagar',
        vehicles: '101',
        status: true
      }
    ];
  }

}
