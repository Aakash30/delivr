import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BusinessManagementListingComponent } from './business-management-listing.component';

describe('BusinessManagementListingComponent', () => {
  let component: BusinessManagementListingComponent;
  let fixture: ComponentFixture<BusinessManagementListingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BusinessManagementListingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinessManagementListingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
