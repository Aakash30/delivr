import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BusinessListingComponent } from 'app/business/pages/business-listing/business-listing.component';
import { BusinessManagementFormComponent } from './pages/business-management-form/business-management-form.component';
import { BusinessManagementDetailsComponent } from './pages/business-management-details/business-management-details.component';
import { BusinessManagementListingComponent } from './pages/business-management-listing/business-management-listing.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        component: BusinessManagementListingComponent,
        data: {
          sectionTitle: 'Business User List',
        }
      },
      {
        path: 'add',
        component: BusinessManagementFormComponent,
        data: {
          sectionTitle: 'Add Business User',
          buttonName: 'Add'
        }
      },
      {
        path: 'details/:id',
        component: BusinessManagementDetailsComponent,
        data: {
          sectionTitle: 'Details',
          buttonName: 'Add Business User'
        }
      },
      {
        path: 'edit/:id',
        component: BusinessManagementFormComponent,
        data: {
          sectionTitle: 'Edit Business User',
          buttonName: 'Save'
        }
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VendorBusinessManagementRoutingModule { }
