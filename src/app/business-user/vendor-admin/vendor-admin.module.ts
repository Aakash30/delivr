import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VendorAdminRoutingModule } from './vendor-admin-routing.module';
import { AdminListComponent } from './pages/admin-list/admin-list.component';
import { AdminFormComponent } from './pages/admin-list/admin-form/admin-form.component';
import { SharedModule } from 'app/shared/shared.module';
import { AdminDetailsComponent } from './pages/admin-list/admin-details/admin-details.component';

@NgModule({
  imports: [
    CommonModule,
    VendorAdminRoutingModule,
    SharedModule
  ],
  declarations: [AdminListComponent, AdminFormComponent, AdminDetailsComponent]
})
export class VendorAdminModule { }
