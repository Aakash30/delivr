import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-admin-details',
  templateUrl: './admin-details.component.html',
  styleUrls: ['./admin-details.component.scss']
})
export class AdminDetailsComponent implements OnInit {

  userData;
  rolesList: [];
  id;
  constructor(
    private _location: Location,
    private _route: ActivatedRoute,
    private _router: Router
  ) {
    this.id = _route.snapshot.params.id;
  }

  ngOnInit() {
    const pageContent = this._route.snapshot.data.pageData.data;
    this.userData = pageContent.fetchSingleUser;

    let userRole = JSON.parse(pageContent.fetchSingleUser.user_role);
    this.rolesList = pageContent.rolesList;
    let userRoleData = [];

    this.rolesList.forEach((element) => {
      if (userRole.indexOf(element["roles"]) !== -1) {
        userRoleData.push(element["role_label"]);
      }
    });

    this.userData.user_role = userRoleData;
    let title = pageContent.user_title;
    let selectedDesignation = title.filter((element) => {
      return element.key == this.userData.designation
    })[0].label;
    this.userData.designation = selectedDesignation;
  }

  previousPage() {
    // this._location.back();
    this._router.navigate(['/business/admin-list']);
  }

}
