import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UtilityService } from 'app/core/services/utility.service';
import { ErrorHandlerService } from 'app/core/services/error-handler.service';
import { BusinessAdminServiceService } from 'app/core/services/business-admin-service.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { MessageService } from 'primeng/api';
import { trigger, transition, style, animate } from '@angular/animations';
import { FormGroup, FormControl } from '@angular/forms';

interface Status {
  name: string;
  code: string;
}

@Component({
  selector: 'app-admin-list',
  templateUrl: './admin-list.component.html',
  styleUrls: ['./admin-list.component.scss'],
  animations: [
    trigger("openClose", [
      transition(":enter", [
        style({ transform: "translateX(20%)", opacity: 0 }),
        animate("450ms", style({ transform: "translateX(0)", opacity: 1 }))
      ]),
      transition(":leave", [
        style({ transform: "translateX(0)", opacity: 1, display: "none" }),
        animate("400ms", style({ transform: "translateX(20%)", opacity: 0 }))
      ])
    ])
  ]
})
export class AdminListComponent implements OnInit {

  adminList: any[];
  total: number = 0;
  offset: number = 0;
  keyword: string = "";
  possibleValue = ['active', 'deactive'];

  branches: any[];
  adminFilterForm: FormGroup;
  isVisible: boolean = false;
  isSubmitted: boolean = false;

  status: Status[];

  statusList: string[];

  private _unsubscribe = new Subject<Boolean>();

  constructor(private _activatedRoute: ActivatedRoute, public utility: UtilityService, private _errorHandling: ErrorHandlerService, private _businessAdminService: BusinessAdminServiceService,
    private _messageService: MessageService) { }

  ngOnInit() {

    const pageContent = this._activatedRoute.snapshot.data.pageData.data;
    this.adminList = pageContent.userList;

    if (pageContent.myBusinessBranches) {
      this.branches = this.utility.arrayOfObjectToConvertInDropdownFormat(pageContent.myBusinessBranches, "address_branch_name", "branch_area_id");
    }

    this.total = pageContent.count;

    this.adminFilterForm = new FormGroup({
      business_branch_id: new FormControl(null),
      user_status: new FormControl(null),

    });

    this.status = [
      { name: 'Pending', code: 'pending' },
      { name: 'Active', code: 'active' },
      { name: 'Deactive', code: 'deactive' },
    ];

  }

  onSubmit() {
    this.isSubmitted = true;
    this.isVisible = false;
    this.listing();

  }

  showFilter() {
    this.isVisible = !this.isVisible;
  }

  clearFilter() {
    this.adminFilterForm.reset();
    this.isSubmitted = false;
    this.isVisible = false;
    this.listing();
  }

  /**
   * list down admin user. Specially used for filters and pagination
   */
  listing() {

    let data: any = {};



    if (this.offset != 0) {
      data["offset"] = this.offset;
    }

    if (this.keyword.trim() != "") {
      data["keyword"] = this.keyword;
    }

    if (this.adminFilterForm.controls["business_branch_id"].value != null && this.adminFilterForm.controls["business_branch_id"].value != "") {
      data.business_branch_id = this.adminFilterForm.controls["business_branch_id"].value;
    }

    if (this.adminFilterForm.controls["user_status"].value != null && this.adminFilterForm.controls["user_status"].value != "") {
      data.user_status = this.adminFilterForm.controls["user_status"].value.code;
    }

    this._businessAdminService.fetchBusinessAdmin(data).pipe(takeUntil(this._unsubscribe)).subscribe((success: any) => {
      this.adminList = success.data.userList;
      this.total = success.data.count;

    }, error => {
      this._errorHandling.routeAccordingToError(error);
    });

  }


  /** key searching */
  searchText(event) {
    if (event.charCode == 13) {

      this.keyword = event.target.value;

      this.listing();
    }

  }
  /** clears category input */
  clearInput(event) {
    this.keyword = event;
    this.listing();
  }
  /**pagination */
  paginate(event) {
    this.offset = event.first;
    this.listing();
  }

  changeStatus(event, user_id) {
    let user_status = event ? "active" : "deactive";
    let data = { "user_status": user_status, user_id: this.utility.base64Encode(user_id) };

    this._businessAdminService.changeUserStatus(data).pipe(takeUntil(this._unsubscribe)).subscribe((success: any) => {
      this._messageService.add({
        severity: "success",
        summary: "Action Success",
        detail: success.message
      });
      this.listing();
    }, error => {
      this._errorHandling.routeAccordingToError(error);
    })
  }

  resendOTP(user) {
    let data = { user_id: user.user_id, user_type: "sub_ordinate", email_id: user.email_id, name: user.first_name };
    this._businessAdminService.resendOTP(data).pipe(takeUntil(this._unsubscribe)).subscribe((success: any) => {
      this._messageService.add({
        severity: 'success',
        summary: success.message,
        detail: ''
      });
    }, error => {
      this._errorHandling.routeAccordingToError(error);
    })
  }

  ngOnDestroy(): void {
    this._unsubscribe.next(true);
    this._unsubscribe.complete();

  }

}
