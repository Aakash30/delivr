import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder, FormArray } from '@angular/forms';
import { Location } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { UtilityService } from 'app/core/services/utility.service';
import { validateAllFormFields } from 'app/shared/utils/custom-validators';
import { BusinessAdminServiceService } from 'app/core/services/business-admin-service.service';
import { Subject } from 'rxjs';
import { takeUntil, single } from 'rxjs/operators';
import { MessageService } from 'primeng/api';

interface Title {
  name: string;
  code: string;
}

@Component({
  selector: 'app-admin-form',
  templateUrl: './admin-form.component.html',
  styleUrls: ['./admin-form.component.scss']
})
export class AdminFormComponent implements OnInit {

  isSubmitted: boolean = false;
  sectionTitle: string;
  buttonName: string;
  selectedValue: boolean[] = [];
  selectedCheckValue: boolean = false;

  adminForm: FormGroup;
  branches;
  titles: Title[];
  rolesList: [];
  controlList: FormControl[] = [];
  formBuilder: FormBuilder = new FormBuilder();
  private _unsubscribe = new Subject<Boolean>();
  country_code: string = "+";
  branchesFromApi: any[] = [];

  constructor(
    private _router: Router,
    private _activateRoute: ActivatedRoute,
    private _location: Location,
    private _utilityService: UtilityService,
    private _businessAdminService: BusinessAdminServiceService,
    private _messageService: MessageService
  ) {
    this.buttonName = this._activateRoute.snapshot.data['buttonName'];
  }

  initUserRole() {
    let roleArrayForm = new FormArray([]);
    this.rolesList.forEach((element, i) => {
      const control = new FormControl(""); // if first item set to true, else false
      roleArrayForm.push(control);
    });
    return roleArrayForm;
  }
  ngOnInit() {

    const pageContent = this._activateRoute.snapshot.data.pageData.data;

    this.branchesFromApi = pageContent.branchAreaLis


    this.branches = this._utilityService.arrayOfObjectToConvertInDropdownFormat(pageContent.branchAreaList, "address_branch_name", "branch_area_id", "status", "deactive");

    this.titles = this._utilityService.arrayOfObjectToConvertInDropdownFormat(pageContent.user_title, "label", "key");
    this.rolesList = pageContent.rolesList;

    this.adminForm = this.formBuilder.group({
      user_id: new FormControl(null),
      first_name: new FormControl('', [Validators.required]),
      last_name: new FormControl('', [Validators.required]),
      email_id: new FormControl('', [Validators.required]),
      mobile_number: new FormControl('', [Validators.required]),
      business_branch_id: new FormControl(null, [Validators.required]),
      designation: new FormControl(null, [Validators.required]),
      user_role: this.initUserRole(),
      check_all: new FormControl(''),
    });

    if (pageContent.fetchSingleUser) {
      this.selectedCheckValue = true;
      let singleUser = pageContent.fetchSingleUser;

      const mobileNumber = singleUser.mobile_number.split("-");
      this.country_code = mobileNumber[0];

      singleUser.mobile_number = mobileNumber[1];
      let userRoleArray = JSON.parse(singleUser.user_role);
      singleUser.user_role = [];
      this.rolesList.forEach((element, i) => {
        if (userRoleArray.indexOf(element["roles"]) !== -1) {
          singleUser.user_role[i] = element["roles"];
        } else {
          singleUser.user_role[i] = "";
          this.selectedCheckValue = false;
        }
      });

      this.adminForm.patchValue(singleUser);
    }
    // an array of titles

  }

  onSubmit() {

    this.isSubmitted = true;
    let data = Object.assign({}, this.adminForm.value);

    let user_role = [];
    this.adminForm.value.user_role.forEach(element => {
      if (element != "") {
        user_role.push(element);
      }
    });
    if (user_role.length == 0) {
      this.adminForm.controls.check_all.setErrors({ "required": true });
    } else {
      this.adminForm.controls.check_all.setErrors(null);
    }
    if (this.adminForm.valid) {

      if (this.adminForm.dirty && this.adminForm.touched) {
        //let data = this.adminForm.value;

        data.user_role = JSON.stringify(user_role);
        delete data.check_all;
        //data.business_branch_id = this.adminForm.value.business_branch_id["branch_area_id"];
        data.mobile_number = this.country_code + "-" + this.adminForm.value.mobile_number;

        this._businessAdminService.submitUserData(data).pipe(takeUntil(this._unsubscribe)).subscribe((success: any) => {
          this._messageService.add({
            severity: "success",
            summary: "Action Success",
            detail: success.message
          });
          this._router.navigateByUrl("/business/admin-list/details/" + this._utilityService.base64Encode(success.data.user_id))

        }, error => {
          this._messageService.add({
            severity: "error",
            summary: this._activateRoute.snapshot.params.id ? "Updation failed" : 'Addition failed',
            detail: `${error.message}`
          });
        });

      } else {
        this._messageService.add({
          severity: "success",
          summary: "Action Success",
          detail: "User has been updated successfully."
        });
        this._router.navigateByUrl("/business/admin-list/details/" + this._activateRoute.snapshot.params.id)
      }
    } else {
      validateAllFormFields(this.adminForm);
      this._utilityService.scrollToError();
    }
  }

  resetForm() {
    // this.adminForm.reset();
    this._location.back();
  }

  onBranchChange(branch_module) {
    if (branch_module != null) {
      let filterBranch = this.branchesFromApi.filter((element) => {
        return element.branch_area_id == branch_module;
      });
      this.country_code = filterBranch[0].country_code;
    } else {
      this.country_code = "+";
    }

  }

  checkAll(event) {

    this.adminForm.controls.user_role.markAsDirty();
    this.adminForm.controls.user_role.markAsTouched();
    let i = 0;
    let tempRole = this.rolesList;
    if (!this.selectedCheckValue) {
      this.adminForm.controls.user_role["controls"].forEach(control => {
        control.setValue("");
        this.selectedValue[i] = false;
        i++;
      });
    } else {
      this.adminForm.controls.user_role["controls"].forEach(control => {
        control.setValue(tempRole[i]["roles"]);
        // this.selectedValue[i] = true;
        i++;
      });
    }



  }

  selectCheckList(event, ind) {

    const selectedOrderIds = this.adminForm.value.user_role
      .map((v, i) => v ? this.rolesList[i]["roles"] : "");

    this.adminForm.controls.user_role.setValue(selectedOrderIds);
    this.adminForm.controls.user_role.markAsDirty();
    this.adminForm.controls.user_role.markAsTouched();

    if (this.adminForm.controls.user_role.value.indexOf("") == -1) {
      this.selectedCheckValue = true;
    } else {
      this.selectedCheckValue = false;
      //this.adminForm.controls.check_all.setValue()
    }
  }

  get adminModule() {
    return this.adminForm.controls;
  }

  get getUserRoleControl() {
    return this.adminForm.controls.user_role["controls"];
  }

  ngOnDestroy(): void {
    this._unsubscribe.next(true);
    this._unsubscribe.complete();

  }
}
