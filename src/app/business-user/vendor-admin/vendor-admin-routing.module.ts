import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminListComponent } from './pages/admin-list/admin-list.component';
import { AdminFormComponent } from './pages/admin-list/admin-form/admin-form.component';
import { AdminDetailsComponent } from './pages/admin-list/admin-details/admin-details.component';
import { BusinessAdminResolverService } from 'app/core/resolver/business-admin-resolver.service';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        component: AdminListComponent,
        data: {
          sectionTitle: 'Admin',
          buttonName: 'Create'
        },
        resolve: {
          pageData: BusinessAdminResolverService
        }
      },
      {
        path: 'add',
        component: AdminFormComponent,
        data: {
          sectionTitle: 'Create Admin',
          buttonName: 'Create'
        },
        resolve: {
          pageData: BusinessAdminResolverService
        }
      },
      {
        path: 'details/:id',
        component: AdminDetailsComponent,
        data: {
          sectionTitle: 'Admin',
          buttonName: 'Create'
        },
        resolve: {
          pageData: BusinessAdminResolverService
        }
      },
      {
        path: 'edit/:id',
        component: AdminFormComponent,
        data: {
          sectionTitle: 'Admin',
          buttonName: 'Update'
        },
        resolve: {
          pageData: BusinessAdminResolverService
        }
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VendorAdminRoutingModule { }
