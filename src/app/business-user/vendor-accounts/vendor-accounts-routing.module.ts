import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AccountsListComponent } from './pages/accounts-list/accounts-list.component';
import { AccountsResolverService } from 'app/core/resolver/accounts-resolver.service';
import { AccountsDetailsComponent } from './pages/accounts-list/accounts-details/accounts-details.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        component: AccountsListComponent,
        data: {
          sectionTitle: 'Accounts'
        },
        resolve: {
          pageData: AccountsResolverService
        }
      },
      {
        path: 'details/:id',
        component: AccountsDetailsComponent,
        data: {
          sectionTitle: 'Accounts Details'
        },
        resolve: {
          pageData: AccountsResolverService
        }
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VendorAccountsRoutingModule { }
