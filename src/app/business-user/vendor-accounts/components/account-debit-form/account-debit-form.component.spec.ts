import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountDebitFormComponent } from './account-debit-form.component';

describe('AccountDebitFormComponent', () => {
  let component: AccountDebitFormComponent;
  let fixture: ComponentFixture<AccountDebitFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountDebitFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountDebitFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
