import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { validateAllFormFields } from 'app/shared/utils/custom-validators';
import { UtilityService } from 'app/core/services/utility.service';

@Component({
  selector: 'app-account-debit-form',
  templateUrl: './account-debit-form.component.html',
  styleUrls: ['./account-debit-form.component.scss']
})
export class AccountDebitFormComponent implements OnInit {

  @Input() accountDebit: any;
  isSubmitted: boolean = false;
  accountDebitForm: FormGroup;

  constructor(public _utility: UtilityService) { }

  ngOnInit() {

    this.accountDebitForm = new FormGroup({
      driver_id: new FormControl(this.accountDebit.driver_id),
      account_type: new FormControl(this.accountDebit.cost),
      debit_type: new FormControl('')
    });
  }

  onSubmit() {
    this.isSubmitted = true;
    if (this.accountDebitForm.valid) {
    } else {
      validateAllFormFields(this.accountDebitForm);

    }
  }

  resetForm() {
    this.accountDebitForm.reset();
  }

  get accountModule() {
    return this.accountDebitForm.controls;
  }

}
