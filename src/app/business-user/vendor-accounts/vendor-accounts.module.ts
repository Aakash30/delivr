import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VendorAccountsRoutingModule } from './vendor-accounts-routing.module';
import { AccountsListComponent } from './pages/accounts-list/accounts-list.component';
import { SharedModule } from 'app/shared/shared.module';
import { AccountsDetailsComponent } from './pages/accounts-list/accounts-details/accounts-details.component';
import { AccountDebitFormComponent } from './components/account-debit-form/account-debit-form.component';

@NgModule({
  imports: [
    CommonModule,
    VendorAccountsRoutingModule,
    SharedModule
  ],
  exports: [
    AccountDebitFormComponent
  ],
  declarations: [AccountsListComponent, AccountsDetailsComponent, AccountDebitFormComponent]
})
export class VendorAccountsModule { }
