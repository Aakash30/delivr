import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { validateAllFormFields } from 'app/shared/utils/custom-validators';
import { UtilityService } from 'app/core/services/utility.service';
import { ActivatedRoute } from '@angular/router';
import { AccountsService } from 'app/core/services/accounts.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { SubjectSubscriber } from 'rxjs/internal/Subject';
import { ErrorHandlerService } from '../../../../core/services/error-handler.service';

@Component({
  selector: 'app-accounts-list',
  templateUrl: './accounts-list.component.html',
  styleUrls: ['./accounts-list.component.scss']
})
export class AccountsListComponent implements OnInit {

  accountList: any = [];
  accountCreditDisplay: boolean = false;
  fuelDialogDisplay: boolean = false;
  salikDialogDisplay: boolean = false;
  isSubmitted: boolean = false;
  isSalikSubmitted: boolean = false;
  isFuelSubmitted: boolean = false;
  fuelData: any = [];
  driverName: string;
  dialogName: string;
  formTypeDebit = "";
  offset: number = 0;
  total: number = 0;

  keyword: string = "";

  accountDebitArray: any = [];

  accountCreditForm: FormGroup;
  salikForm: FormGroup;
  accountDebitForm: FormGroup;
  business_id: number;
  business_branch_id: number;

  private _unsubscribe = new Subject<Boolean>();

  constructor(
    public _utility: UtilityService,
    private _router: ActivatedRoute,
    private _accountService: AccountsService,
    private _errorHandling: ErrorHandlerService
  ) { }

  ngOnInit() {

    const pageContent = this._router.snapshot.data.pageData.data;
    this.total = pageContent.totalCount;

    this.accountList = pageContent.accountDetails;

    this.accountCreditForm = new FormGroup({
      amount: new FormControl('', [Validators.required]),
      driver_id: new FormControl(null),
      account_type: new FormControl('')
    });

    this.salikForm = new FormGroup({
      amount: new FormControl('', [Validators.required]),
      driver_id: new FormControl(null),
      account_type: new FormControl(''),
      debit_type: new FormControl('')
    });

    this.accountDebitForm = new FormGroup({
      driver_id: new FormControl(''),
      amount: new FormControl(''),
      account_type: new FormControl(''),
      debit_type: new FormControl('')
    });
  }

  listing() {
    let data = {};

    if (this.offset != 0) {
      data["offset"] = this.offset;
    }

    if (this.keyword.trim() != "") {
      data["keyword"] = this.keyword;
    }
    this._accountService.fetchAccountsData(data).pipe(takeUntil(this._unsubscribe)).subscribe(
      (success: any) => {
        this.accountList = success.data.accountDetails;
      }
    )
  }
  /**
   *
   * @param event get pagination event
   */
  paginate(event) {
    this.offset = event.first;
    this.listing();
  }

  /** key searching */
  searchText(event) {
    if (event.charCode == 13) {
      this.keyword = event.target.value;
      this.listing();
    }
  }

  /** clears category input */
  clearInput(event) {
    this.keyword = event;
    this.listing();
  }


  onSubmit() {
    this.isSubmitted = true;
    if (this.accountCreditForm.valid) {

      let data = this.accountCreditForm.value;

      data.business_id = this.business_id;
      data.business_branch = this.business_branch_id;

      this._accountService.updateAccountCreditDebit(data).pipe(takeUntil(this._unsubscribe)).subscribe(
        (success: any) => {
          this.accountCreditDisplay = false;
          this.listing();

        }, error => {
          this._utility.showError('error', '', error);
        });
    }
    else {
      validateAllFormFields(this.accountCreditForm);
    }
  }

  onDebitSubmit(item, currentFuelValue, type) {
    this.isFuelSubmitted = true;
    if (this.accountDebitForm.valid) {

      let data = {
        driver_id: item.driver_id,
        account_type: 'debit',
        amount: currentFuelValue,
        debit_type: type,
        id: item.id,
        business_id: this.business_id,
        business_branch: this.business_branch_id
      }
      this._accountService.updateAccountCreditDebit(data).pipe(takeUntil(this._unsubscribe)).subscribe(
        (success: any) => {
          this.fuelDialogDisplay = false;
          this.listing();

        }, error => {
          this._errorHandling.routeAccordingToError(error);
        });
    } else {
      validateAllFormFields(this.accountDebitForm);
    }
  }

  salikFormSubmit() {
    this.isSalikSubmitted = true;
    if (this.salikForm.valid) {
      let data = this.salikForm.value;
      data.business_id = this.business_id;
      data.business_branch = this.business_branch_id;

      this._accountService.updateAccountCreditDebit(data).pipe(takeUntil(this._unsubscribe)).subscribe(
        (success: any) => {
          this.salikDialogDisplay = false;
          this.listing();
        }, error => {
          this._errorHandling.routeAccordingToError(error);
        });
    } else {
      validateAllFormFields(this.salikForm);
    }
  }

  resetSalikForm() {
    this.salikForm.reset();
    this.salikDialogDisplay = false;
  }

  openAccountDialog(item, type) {
    this.driverName = item.first_name + " " + item.last_name;
    this.business_id = item.business_id;
    this.business_branch_id = item.business_branch_id;

    switch (type) {
      case type = 'credit':
        this.accountCreditForm.controls.driver_id.setValue(item.user_id);
        this.accountCreditForm.controls.account_type.setValue("credit");
        this.accountCreditDisplay = true;
        break;

      case type = 'fuel':
        this.dialogName = 'Fuel';
        this.fuelData = item.fuel_accounts;
        this.fuelDialogDisplay = true;
        this.dialogName = 'Fuel';
        this.formTypeDebit = type;
        break;

      case type = 'fine':
        this.fuelData = item.fuel_accounts;
        this.fuelData = item.police_fine;
        this.fuelDialogDisplay = true;
        this.dialogName = 'Police Fine';
        this.formTypeDebit = type;
        break;

      default:
        let data = {
          driver_id: item.user_id,
          account_type: 'debit',
          debit_type: 'salik'
        }
        this.salikForm.patchValue(data);
        this.salikDialogDisplay = true;
        this.dialogName = 'Salik';
        break;
    }
  }

  resetForm() {
    this.accountCreditForm.reset();
    this.accountCreditDisplay = false;
  }

  get creditForm() {
    return this.accountCreditForm.controls;
  }

  get salikModule() {
    return this.salikForm.controls;
  }

}
