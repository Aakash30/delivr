import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { UtilityService } from 'app/core/services/utility.service';
import { AccountsService } from 'app/core/services/accounts.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-accounts-details',
  templateUrl: './accounts-details.component.html',
  styleUrls: ['./accounts-details.component.scss']
})
export class AccountsDetailsComponent implements OnInit {

  accountDetailsForm: FormGroup;
  accountDetailsData: any = [];
  buttonName: string;
  sectionTitle: string;
  driver: String = "";
  balance;
  currency;
  id;

  dateValue2: Date;
  dateValue1: Date;
  private _unsubscribe = new Subject<Boolean>();
  constructor(
    private _activateRoute: ActivatedRoute,
    private _router: Router,
    public utility: UtilityService,
    private _accountService: AccountsService
  ) {
    this.sectionTitle = _activateRoute.snapshot.data['sectionTitle'];
    this.buttonName = _activateRoute.snapshot.data['buttonName'];
    this.id = _activateRoute.snapshot.params.id;
  }

  ngOnInit() {

    const pageContent = this._activateRoute.snapshot.data.pageData.data;

    this.accountDetailsForm = new FormGroup({
      start_date: new FormControl(null),
      end_date: new FormControl(null)
    });

    this.driver = pageContent.accountData.driver;
    this.balance = pageContent.accountData.balance;

    this.currency = pageContent.accountData.countries.currency;

    this.accountDetailsData = pageContent.accountData.accountsUser;
  }

  previousPage() {
    this._router.navigate(['/business/accounts']);
  }

  /**
   * submits the filterdd
   */
  onSubmit() {

    let data = {};
    if (this.accountDetailsForm.value.start_date != null) {
      data["start_date"] = this.accountDetailsForm.value.start_date.toString();
    }

    if (this.accountDetailsForm.value.end_date != null) {
      data["end_date"] = this.accountDetailsForm.value.end_date.toString();
    }

    this._accountService.fetchDriverAccounts(this.id, data).pipe(takeUntil(this._unsubscribe)).subscribe((success: any) => {
      this.accountDetailsData = success.data.accountData.accountsUser;
    });
  }

  /**
   * call on date change
   */
  changeDatePicker() {
    this.onSubmit();
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this._unsubscribe.next(true);
    this._unsubscribe.complete();
  }
}
