import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VendorBranchRoutingModule } from './vendor-branch-routing.module';
import { BranchListingComponent } from './pages/branch-listing/branch-listing.component';
import { BranchFormComponent } from './pages/branch-form/branch-form.component';
import { BranchDetailsComponent } from './pages/branch-details/branch-details.component';
import { SharedModule } from 'app/shared/shared.module';
import { AgmCoreModule, GoogleMapsAPIWrapper } from '@agm/core';
import { BusinessModule } from 'app/business/business.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDHapbY7BfjFKhd2WJ4BHv6YDfWuPYhoL8',//'AIzaSyD5g_QO7b1pBfgl9OtX6vchzZwX1Vmxo2Y',
      libraries: ['places']
    }),
    VendorBranchRoutingModule,
    BusinessModule,

  ],
  declarations: [BranchListingComponent, BranchFormComponent, BranchDetailsComponent],
  providers: [
    GoogleMapsAPIWrapper
  ]
})
export class VendorBranchModule { }
