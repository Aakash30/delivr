import { Component, OnInit } from '@angular/core';
import { MenuItem, MessageService } from 'primeng/api';
import { BranchService } from 'app/core/services/branch.service';
import { ActivatedRoute, Router } from '@angular/router';
import { UtilityService } from 'app/core/services/utility.service';
import { ErrorHandlerService } from 'app/core/services/error-handler.service';
import { map, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-branch-listing',
  templateUrl: './branch-listing.component.html',
  styleUrls: ['./branch-listing.component.scss'],
})
export class BranchListingComponent implements OnInit {

  activeTab: string;
  items: MenuItem[];
  activeItem: MenuItem;
  branchList: any[];

  keyword: String = "";
  total: number = 0;

  possibleValue = ['active', 'deactive'];

  constructor(
    private route: ActivatedRoute,
    private branchService: BranchService,
    private messageService: MessageService,
    private router: Router,
    public utiltiy: UtilityService,
    private errorHandling: ErrorHandlerService
  ) { }

  private _unsubscribe = new Subject<boolean>();


  ngOnInit() {
    // this.listing();
    const pageContent = this.route.snapshot.data.pageData.data;
    this.branchList = pageContent.businessareas;
    this.total = pageContent.total;
  }

  listing() {

    let data: any = {};
    if (this.keyword.trim() != "") {
      data.keyword = this.keyword;
    }
    this.branchService.getBranches(data).subscribe(
      (success: any) => {
        this.branchList = success.data.businessareas;
      },
      error => {
        this.errorHandling.routeAccordingToError(error)
      }
    );
  }

  searchText(event) {
    if (event.charCode == 13) {
      this.keyword = event.target.value;
      // on click of enter
      this.listing();
    }
  }

  clearInput(event) {
    this.keyword = event;
    this.listing();
  }

  changeStatus(status, id) {

    let data = { branch_area_id: id, status: status ? 'active' : 'deactive' };
    this.branchService.changeStatus(data).pipe(
      map((success: any) => {
        if (success) {
          return success;
        }
      }),
      takeUntil(this._unsubscribe)
    ).subscribe((success: any) => {
      this.messageService.add({
        severity: 'success',
        summary: 'status changed',
        detail: success.message
      })
    }, error => {
      this.messageService.add({
        severity: 'error',
        summary: 'action failed',
        detail: error.message
      })
    });
  }
}
