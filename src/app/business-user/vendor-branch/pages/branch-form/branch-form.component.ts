import { Component, OnInit, ElementRef, ViewChild, NgZone, } from '@angular/core';
import { MapsAPILoader, MouseEvent } from '@agm/core';
import { FormGroup, FormControl, Validators, AbstractControl, FormBuilder } from '@angular/forms';

import { UtilityService } from 'app/core/services/utility.service';
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { BranchService } from 'app/core/services/branch.service';
import { MessageService, ConfirmationService } from "primeng/api";
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { noWhitespaceValidator, blankSpaceInputNotValid, validateAllFormFields } from 'app/shared/utils/custom-validators';
import { ErrorHandlerService } from 'app/core/services/error-handler.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

declare var google: any;

interface Marker {
  lat: number;
  lng: number;
  label?: string;
  draggable: boolean;
}
interface City {
  name: string;
  code: string;
}

@Component({
  selector: 'app-branch-form',
  templateUrl: './branch-form.component.html',
  styleUrls: ['./branch-form.component.scss']
})

export class BranchFormComponent implements OnInit {

  cities: City[] = [];
  selectedCity: City;
  branchListForm: FormGroup;
  isSubmitted: boolean = false;
  sectionTitle: any;
  buttonName: any;
  partnerList: any[] = [];
  id: any;
  currentUrl: any;

  formBuilder: FormBuilder = new FormBuilder();

  countryListFromAPI: string[];
  country: any = [];
  cityListFromAPI: string[];
  @ViewChild('address') searchElementRef: ElementRef;


  latitude: number = 25.197525;
  longitude: number = 55.274288;
  zoom: number = 12;
  paceChange;

  private geoCoder;
  private _unsubscribe = new Subject<Boolean>();

  constructor(
    public utility: UtilityService,
    private location: Location,
    private route: ActivatedRoute,
    private router: Router,
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone,
    private branchService: BranchService,
    private messageService: MessageService,
    private loader: NgxUiLoaderService,
    private errorHandling: ErrorHandlerService,
    private confirmationService: ConfirmationService
  ) {
    this.sectionTitle = route.snapshot.data['sectionTitle'];
    this.buttonName = route.snapshot.data['buttonName'];
    this.id = utility.base64Decode(route.snapshot.paramMap.get('id'));
    this.currentUrl = router.url;
  }
  public isPartnerOld = false;

  ngOnInit() {
    const pageContent = this.route.snapshot.data.pageData.data;
    this.countryListFromAPI = pageContent.countryList;
    this.country = this.utility.arrayOfObjectToConvertInDropdownFormat(this.countryListFromAPI, "country", "country_id", "status", "inActive")
    this.cityListFromAPI = pageContent.cityList;
    let partnerData = pageContent.partnerList;
    this.partnerList = this.utility.arrayOfObjectToConvertInDropdownFormat(partnerData, "partner", "partner_id");


    this.branchListForm = this.formBuilder.group({
      partner_id: new FormControl(null),
      partner: new FormControl(''),
      branch: this.initBranchForm()
    })

    if (pageContent.businessBranch) {
      this.changeCountry(pageContent.businessBranch.country);
      this.latitude = parseFloat(pageContent.businessBranch.latitude);
      this.longitude = parseFloat(pageContent.businessBranch.longitude);
      let formInput = {};
      formInput['partner_id'] = pageContent.businessBranch.partner_id;
      if (formInput['partner_id'] != null) {
        this.choosePartner(formInput['partner_id']);
        let selectedPartner: any = this.partnerList.filter((partner: any) => {
          return partner.value == formInput['partner_id'];
        });
        formInput['partner'] = selectedPartner[0].label;
      }

      formInput['branch'] = pageContent.businessBranch;
      this.branchListForm.patchValue(formInput);
    }
    this.reloadMap();
  }

  initBranchForm(): any {
    return this.formBuilder.group({
      branch_area_id: new FormControl(null),
      address_branch_name: new FormControl('', [Validators.required]),
      country: new FormControl('', [Validators.required]),
      city: new FormControl('', [Validators.required]),
      address_line_1: new FormControl('', [Validators.required, noWhitespaceValidator, blankSpaceInputNotValid]),
      address_line_2: new FormControl('', [Validators.required, noWhitespaceValidator, blankSpaceInputNotValid]),
      postal_Code: new FormControl('', [
        Validators.required,
        // Validators.pattern(/^[0-9]\d{5}$/)
      ]),
      latitude: new FormControl(""),
      longitude: new FormControl(""),
    })
  }

  choosePartner(partner?) {
    let partner_id = partner ? partner : this.branchListForm.controls.partner_id.value;

    if (partner_id != null) {
      this.isPartnerOld = true;
      let selectedPartner: any = this.partnerList.filter((partner: any) => {
        return partner.value == partner_id;
      });
      this.branchListForm.controls['partner'].setValue(selectedPartner[0].label)
      this.branchListForm.controls['partner'].disable();
    } else {
      this.branchListForm.controls['partner'].setValue('');
      this.branchListForm.controls['partner'].enable();
      this.isPartnerOld = false;
    }
  }

  changeCountry(country?) {
    let countryId = country ? country : this.branchListForm.controls.branch["controls"].country.value;
    this.branchListForm.controls.branch["controls"].address_line_2.setValue("");

    if (countryId != null && countryId != "") {
      let selectedCountry: any = this.countryListFromAPI.filter((countryData: any) => {
        return countryData.country_id == countryId;
      });
      let selectedCity = this.cityListFromAPI.filter((cityData: any) => {
        return cityData.country_id == countryId;

      });

      this.cities = this.utility.arrayOfObjectToConvertInDropdownFormat(selectedCity, "city", "city_id", "status", "inActive")
      let alphaCode = selectedCountry[0].alpha_code;
      this.reloadMap(alphaCode);

    } else {
      this.reloadMap();
    }

  }

  /**
   *
   * @param alphaCode defines the restriction - happens when a country is selected
   */

  reloadMap(alphaCode?) {
    if (alphaCode) {

      this.mapsAPILoader.load().then(() => {
        this.geoCoder = new google.maps.Geocoder;
        const autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
          types: ['address'],
          componentRestrictions: { country: alphaCode }
        });
        this.paceChange = autocomplete.addListener('place_changed', () => {
          this.ngZone.run(() => {
            //get the place result
            let place: google.maps.places.PlaceResult = autocomplete.getPlace();

            //verify result
            if (place.geometry === undefined || place.geometry === null) {
              return;
            }

            //set latitude, longitude and zoom
            this.latitude = place.geometry.location.lat();
            this.longitude = place.geometry.location.lng();
            this.branchListForm.controls.branch["controls"].address_line_2.patchValue(place.formatted_address)
            this.zoom = 12;
          });
        });
      });

    } else {
      this.mapsAPILoader.load().then(() => {

        this.geoCoder = new google.maps.Geocoder;
        const autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
          types: ["address"]
        });
        this.paceChange = autocomplete.addListener('place_changed', () => {
          this.ngZone.run(() => {
            //get the place result
            let place: google.maps.places.PlaceResult = autocomplete.getPlace();

            //verify result
            if (place.geometry === undefined || place.geometry === null) {
              return;
            }


            //set latitude, longitude and zoom
            this.latitude = place.geometry.location.lat();
            this.longitude = place.geometry.location.lng();
            this.zoom = 12;
          });
        });
      });

    }
  }
  markerDragEnd($event: MouseEvent) {
    this.latitude = $event.coords.lat;
    this.longitude = $event.coords.lng;
    this.getAddress(this.latitude, this.longitude);
  }

  getAddress(latitude, longitude) {
    this.geoCoder.geocode({ 'location': { lat: latitude, lng: longitude } }, (results, status) => {
      if (status === 'OK') {
        if (results[0]) {
          this.zoom = 12;

          this.branchListForm.controls.branch["controls"].address_line_2.patchValue(results[0].formatted_address)

        } else {
          window.alert('No results found');
        }
      } else {
        window.alert('Geocoder failed due to: ' + status);
      }
    });
  }

  /**
   * submit form to save or update branch details.
   */
  onSubmit() {
    this.isSubmitted = true;
    if (this.branchListForm.valid) {

      if (this.branchListForm.dirty && this.branchListForm.touched) {

        let data = this.branchListForm.value;

        data.branch.latitude = this.latitude;
        data.branch.longitude = this.longitude;

        this.branchService.addUpdateBranch(data).pipe(takeUntil(this._unsubscribe)).subscribe(
          (success: any) => {

            this.messageService.add({
              severity: 'success',
              summary: success.message,
              detail: ''
            });
            this.router.navigateByUrl('/business/branch');
            this.loader.stop();
          },
          error => {
            this.errorHandling.routeAccordingToError(error);
            this.loader.stop();
          }
        );
      } else {
        this.messageService.add({
          severity: 'success',
          summary: 'Branch has been updated successfully.',
          detail: ''
        });
        this.router.navigateByUrl('/business/branch');
      }

    } else {
      validateAllFormFields(this.branchListForm);
      this.utility.scrollToError();
    }
  }

  // reset form
  resetForm() {
    if (this.branchListForm.dirty && this.branchListForm.touched) {

      this.confirmationService.confirm({
        message: 'You have some unsaved changes.',
        header: 'Confirmation',
        reject: () => {
          this.router.navigateByUrl('/business/branch');
        },
        rejectLabel: "Discard Changes",
        accept: () => {
          this.onSubmit();
        },
        acceptLabel: "Save & Close"
      });
    } else {
      this.router.navigateByUrl('/business/branch');
    }
    this.branchListForm.reset();
    this.isSubmitted = false;
  }

  get businessPartnerControl() {
    return this.branchListForm.controls;
  }

  get branchForm() {
    return this.branchListForm.controls.branch["controls"];
  }

  ngOnDestroy() {
    this._unsubscribe.next(true);
    this._unsubscribe.complete();
  }
}
