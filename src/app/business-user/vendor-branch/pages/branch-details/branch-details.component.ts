import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UtilityService } from 'app/core/services/utility.service';

@Component({
  selector: 'app-branch-details',
  templateUrl: './branch-details.component.html',
  styleUrls: ['./branch-details.component.scss']
})
export class BranchDetailsComponent implements OnInit {

  sectionTitle: any;
  buttonName: any;
  id: number;
  currentUrl: string;
  branchDetails;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    public utility: UtilityService
  ) {
    this.sectionTitle = route.snapshot.data['sectionTitle'];
    this.buttonName = route.snapshot.data['buttonName']
    this.id = utility.base64Decode(route.snapshot.paramMap.get('id'));
    this.currentUrl = router.url;
  }

  ngOnInit() {
    const pageContent = this.route.snapshot.data.pageData.data;
    this.branchDetails = pageContent.businessBranch;
  }

  assignNewBike() {
    this.router.navigate(['/business/branch/assign-bike/' + this.utility.base64Encode(this.id)]);
  }

  previousPage() {
    this.router.navigate(['/business/branch']);
  }

}
