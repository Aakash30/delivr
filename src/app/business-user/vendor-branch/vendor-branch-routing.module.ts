import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BranchListingComponent } from './pages/branch-listing/branch-listing.component';
import { BranchFormComponent } from './pages/branch-form/branch-form.component';
import { BranchDetailsComponent } from './pages/branch-details/branch-details.component';
import { BranchResolverService } from 'app/core/resolver/branch-resolver.service';
// import { AssignBikeComponent } from 'app/business/pages/assign-bike/assign-bike.component';
import { AssignBikeComponent } from 'app/shared/components/assign-bike/assign-bike.component';
import { BusinessManagementResolverService } from 'app/core/resolver/business-management-resolver.service';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        component: BranchListingComponent,
        data: {
          breadcrumb: 'Branch List',
          sectionTitle: 'Branch List',
        },
        resolve: {
          pageData: BranchResolverService
        }
      },
      {
        path: 'add',
        component: BranchFormComponent,
        data: {
          breadcrumb: 'Add Branch',
          sectionTitle: 'Add Branch',
          buttonName: 'Add'
        },
        resolve: {
          pageData: BranchResolverService
        }
      },
      {
        path: 'details/:id',
        component: BranchDetailsComponent,
        data: {
          breadcrumb: 'Add Branch',
          sectionTitle: 'Branch Details',
          buttonName: 'Add Branch'
        },
        resolve: {
          pageData: BranchResolverService
        }
      },
      {
        path: 'edit/:id',
        component: BranchFormComponent,
        data: {
          sectionTitle: 'Edit Branch',
          breadcrumb: 'Add Branch',
          buttonName: 'Save'
        },
        resolve: {
          pageData: BranchResolverService
        }
      },
      {
        path: 'assign-bike/:id',
        component: AssignBikeComponent,
        data: {
          sectionTitle: 'Assign Bike',
          buttonName: 'Assign'
        },
        resolve: {
          pageData: BranchResolverService
        }
      }

    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VendorBranchRoutingModule { }
