import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VendorDashboardRoutingModule } from './vendor-dashboard-routing.module';
import { VendorDashboardComponent } from './pages/vendor-dashboard/vendor-dashboard.component';
import { DashboardModule } from 'app/dashboard/dashboard.module';

@NgModule({
  imports: [
    CommonModule,
    VendorDashboardRoutingModule,
    DashboardModule
  ],
  declarations: [VendorDashboardComponent]
})
export class VendorDashboardModule { }
