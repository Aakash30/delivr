import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VendorDashboardComponent } from './pages/vendor-dashboard/vendor-dashboard.component';
import { CardsDashboardComponent } from 'app/dashboard/pages/cards-dashboard.component';
import { BusinessDashboardResolverService } from 'app/core/resolver/business-dashboard-resolver.service';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        component: CardsDashboardComponent,
        data: {
          title: 'dashboard'
        },
        resolve: {
          pageData: BusinessDashboardResolverService
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VendorDashboardRoutingModule { }
