import { Component, OnInit } from '@angular/core';
import { Chart } from 'chart.js';

@Component({
  selector: 'app-vendor-dashboard',
  templateUrl: './vendor-dashboard.component.html',
  styleUrls: ['./vendor-dashboard.component.scss']
})
export class VendorDashboardComponent implements OnInit {

  BarChart = [];
  pieChart = [];
  constructor() { }



  ngOnInit() {


    // Bar chart:
    this.BarChart = new Chart('barChart', {
      type: 'bar',
      data: {
        labels: ["Jan", "Feb", "Mar", "April", "May"],
        datasets: [{
          label: '# of Votes',
          data: [3, 7, 3, 5, 2],
          backgroundColor: [
            '#EAEDFF',
            '#EAEDFF',
            '#EAEDFF',
            '#EAEDFF',
            '#EAEDFF',
          ],
          borderColor: [
            '#EAEDFF',
            '#EAEDFF',
            '#EAEDFF',
            '#EAEDFF',
            '#EAEDFF',
          ],
          borderWidth: 1
        }]
      },
      options: {
        responsive: true,
        title: {
          text: "Bar Chart",
          display: true
        },
        scales: {
          yAxes: [{
            ticks: {
              display: false
            }
          }]
          // yAxes: [{
          //     angleLines: {
          //         display: false
          //     }
          // }]
        }
      }
    });

    // pie chart:
    this.pieChart = new Chart('pieChart', {
      type: 'doughnut',
      data: {
        labels: ["New Bikes", "On Rent", "On Lease", "Unassigned"],
        datasets: [{
          label: '# of Votes',
          data: [9, 7, 3, 5],
          backgroundColor: [
            'rgba(255, 99, 132, 0.2)',
            'rgba(54, 162, 235, 0.2)',
            'rgba(255, 206, 86, 0.2)',
            'rgba(75, 192, 192, 0.2)',
          ],
          borderColor: [
            'rgba(255,99,132,1)',
            'rgba(54, 162, 235, 1)',
            'rgba(255, 206, 86, 1)',
            'rgba(75, 192, 192, 1)',
          ],
          borderWidth: 1
        }]
      },
      options: {
        title: {
          text: "Pie Chart",
          display: true
        },
        scales: {
          yAxes: [{

            ticks: {
              beginAtZero: true
            }
          }]
        }
      }
    });

  }

}
