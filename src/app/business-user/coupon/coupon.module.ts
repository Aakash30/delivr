import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CouponRoutingModule } from './coupon-routing.module';
import { CouponListComponent } from './pages/coupon-list/coupon-list.component';
import { SharedModule } from 'app/shared/shared.module';
import { CouponFormComponent } from './pages/coupon-form/coupon-form.component';
import { CouponDetailsComponent } from './pages/coupon-details/coupon-details.component';


@NgModule({
  imports: [
    CommonModule,
    CouponRoutingModule,
    SharedModule
  ],
  declarations: [CouponListComponent, CouponFormComponent, CouponDetailsComponent]
})
export class CouponModule { }
