import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CouponListComponent } from './pages/coupon-list/coupon-list.component';
import { CouponFormComponent } from './pages/coupon-form/coupon-form.component';
import { CouponDetailsComponent } from './pages/coupon-details/coupon-details.component';
import { CouponResolverService } from 'app/core/resolver/coupon-resolver.service';
import { AuthGuardService as AuthGuard } from 'app/core/guards/auth-guard.service';
import { RolesGuardService as RoleGuard } from 'app/core/guards/roles-guard.service';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'coupon',
        component: CouponListComponent,
        data: {
          sectionTitle: 'Coupon',
        },
        resolve: {
          pageData: CouponResolverService
        }
      },
      {
        path: 'coupon/add',
        component: CouponFormComponent,
        data: {
          sectionTitle: 'Add Coupon',
          buttonName: 'Create Coupon',
          onlyForUser: ['vendor', 'sub_ordinate'],
          expectedRole: ['coupon-management']
        },
        resolve: {
          pageData: CouponResolverService
        },
        canActivate: [AuthGuard, RoleGuard]
      },
      {
        path: 'coupon/edit/:id',
        component: CouponFormComponent,
        data: {
          sectionTitle: 'Edit Coupon',
          buttonName: 'Update Coupon',
          onlyForUser: ['vendor', 'sub_ordinate'],
          expectedRole: ['coupon-management']
        },
        canActivate: [AuthGuard, RoleGuard],
        resolve: {
          pageData: CouponResolverService
        }
      },
      {
        path: 'coupon/details/:id',
        component: CouponDetailsComponent,
        data: {
          sectionTitle: 'Coupon Details',
          onlyForUser: ['vendor', 'sub_ordinate'],
          expectedRole: ['coupon-management']
        },
        canActivate: [AuthGuard, RoleGuard],
        resolve: {
          pageData: CouponResolverService
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CouponRoutingModule { }
