import { Component, OnInit, OnDestroy } from '@angular/core';
import { Location, DatePipe } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { validateAllFormFields } from 'app/shared/utils/custom-validators';
import { MessageService } from 'primeng/api';
import { UtilityService } from 'app/core/services/utility.service';
import { CouponManagementService } from 'app/core/services/coupon-management.service';
import { takeUntil, map } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { ErrorHandlerService } from 'app/core/services/error-handler.service';

interface Country {
  name: string;
  code: string;
}

interface City {
  name: string;
  code: string;
}

interface User {
  name: string;
  code: string;
}

@Component({
  selector: 'app-coupon-form',
  templateUrl: './coupon-form.component.html',
  styleUrls: ['./coupon-form.component.scss']
})
export class CouponFormComponent implements OnInit, OnDestroy {

  countries: Country[];
  selectedCountry: Country;
  cities: City[];
  selectedCity: City;
  users: User[];
  selectedType: User;

  // cars: SelectItem[];
  selectedCar: string;

  sectionTitle: String;
  buttonName: String;
  id: number;
  currentUrl: string;
  couponForm: FormGroup;
  isSubmitted: Boolean = false;

  countryListFromAPI: string[];
  cityListFromAPI: string[];
  userListFromAPI: string[];

  startDate: Date;
  endDate: Date;
  today = new Date();

  couponFormData: any;
  businessAreaList: any = [];
  fileCouponObject: any;
  couponImageObject: any[] = [];

  imageDisplayFlag: boolean;
  imageUrl: any[] = [];

  private _unsubscribe = new Subject<Boolean>();

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private location: Location,
    private messageService: MessageService,
    public utility: UtilityService,
    private datePipe: DatePipe,
    private couponManagementService: CouponManagementService,
    private errorHandling: ErrorHandlerService
  ) {
    this.sectionTitle = route.snapshot.data['sectionTitle'];
    this.buttonName = route.snapshot.data['buttonName'];
    this.id = utility.base64Decode(route.snapshot.paramMap.get('id'));

    this.currentUrl = router.url;
  }

  ngOnInit() {

    this.couponFormData = this.route.snapshot.data.pageData.data;
    this.businessAreaList = this.couponFormData.businessAreaList;

    if (this.businessAreaList.length > 0) {
      this.businessAreaList = this.utility.arrayOfObjectToConvertInDropdownFormat(this.businessAreaList, "address_branch_name", "branch_area_id");
    }

    this.couponForm = new FormGroup({
      business_branch_id: new FormControl(null, [Validators.required]),
      coupon_id: new FormControl(null),
      coupon_code: new FormControl(''),
      description: new FormControl('', [Validators.required]),
      expired_on: new FormControl('', [Validators.required]),
      start_date: new FormControl('', [Validators.required]),
      coupon_image: new FormControl('', [Validators.required])
    });
    this.couponForm.controls.coupon_code.patchValue(this.couponFormData.coupon_code);

    if (this.couponFormData.hasOwnProperty('couponData')) {
      this.couponForm.patchValue(this.couponFormData.couponData);


      this.couponFormData.coupon_code = this.couponFormData.couponData.coupon_code;
      this.couponImageObject.push(this.couponFormData.couponData.coupon_image);

      this.endDate = new Date(this.couponFormData.couponData.expired_on);
      this.couponFormData.couponData.expired_on = this.endDate;
      this.couponForm.controls.expired_on.patchValue(this.endDate);

      this.startDate = new Date(this.couponFormData.couponData.start_date);
      this.couponFormData.couponData.start_date = this.startDate;
      this.couponForm.controls.start_date.patchValue(this.startDate);
    }

  }

  refreshCode() {
    this.utility.loaderStart();
    this.couponManagementService.generateCode().pipe(map((success: any) => {
      if (success) {
        return success;
      }
    }), takeUntil(this._unsubscribe)).subscribe(
      (success: any) => {
        this.couponForm.controls.coupon_code.setValue(success.data.randomCode);
        this.utility.loaderStop();
      },
      error => {
        this.utility.loaderStop();
        this.errorHandling.routeAccordingToError(error);
      }
    )
  }

  uploadImage(event, key) {

    this.fileCouponObject = event.target.files[0];
    if (event.target.attributes.limit != undefined) {

      let limit = event.target.attributes.limit.value;
      if (Array.from(event.target.files).length > limit) {
        this.couponForm["controls"]['coupon_image'].setErrors({ "limit": true })
        return false;

      } else {
        this.couponForm.controls['coupon_image'].setErrors(null)
      }
    }

    var fileType = event.target.attributes.accept.value == ".jpg, .jpeg" ? /image\/png|image\/jpeg|image\/jpg/ : '';

    let maxsize = event.target.attributes.maxsize.value;

    let errorType = false;
    let errorSize = false;
    let fileArray = event.target.files;
    Array.from(fileArray).forEach(element => {
      if (!element["type"].match(fileType)) {
        errorType = true;
      }
      if (maxsize < element["size"]) {
        //allow 2 mb
        errorSize = true;
      }
    });

    if (errorType) {
      this.couponForm.controls['coupon_image'].setErrors({ "pattern": true });
      return false;

    } else {
      this.couponForm.controls['coupon_image'].setErrors(null);
    }

    if (errorSize) {
      this.couponForm.controls['coupon_image'].setErrors({ "size": true });
      return false;

    } else {
      this.couponForm.controls['coupon_image'].setErrors(null);
    }
    this.couponForm.controls['coupon_image'].setValue(event.target.files[0]);

    const reader = new FileReader();
    reader.onload = (event: any) => {
      this.couponImageObject.push(event.target.result);
      if (this.couponImageObject.length > 1) {
        this.couponImageObject.shift();
      }
    };
    reader.readAsDataURL(event.target.files[0]);
  }

  /**
   * submits coupon details
   */
  onSubmit() {
    this.isSubmitted = true;
    if (this.couponForm.valid) {
      this.utility.loaderStart();

      let data = this.couponForm.value;

      data.start_date = this.datePipe.transform(data.start_date, 'yyyy-MM-dd');
      data.expired_on = this.datePipe.transform(data.expired_on, 'yyyy-MM-dd');

      if (data.coupon_id == null) {
        delete data.coupon_id;
      }

      let couponData = {};

      couponData = data;

      this.couponManagementService.addUpdateCoupon(couponData, this.fileCouponObject).pipe(takeUntil(this._unsubscribe)).subscribe(
        (success: any) => {
          this.messageService.add({ severity: 'success', summary: "Success", detail: success.message });
          this.utility.loaderStop();
          this.router.navigate(['/business/coupon']);
        },
        error => {
          this.utility.loaderStop();
          this.errorHandling.routeAccordingToError(error);
        }
      )

      this.isSubmitted = false;
    }
    else {
      validateAllFormFields(this.couponForm);

      this.utility.scrollToError();
    }
  }

  resetForm() {
    this.location.back();
    this.isSubmitted = false;
  }

  get couponModule() {
    return this.couponForm.controls;
  }


  ngOnDestroy() {
    this._unsubscribe.next(true);
    this._unsubscribe.complete();
  }
}
