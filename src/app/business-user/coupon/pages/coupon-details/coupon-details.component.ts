import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-coupon-details',
  templateUrl: './coupon-details.component.html',
  styleUrls: ['./coupon-details.component.scss']
})
export class CouponDetailsComponent implements OnInit {

  sectionTitle: String;
  buttonName: String;
  public currentUrl: String;
  id: number;
  couponList: any[];

  couponFormData: any;
  businessAreaList: any;
  couponDetailsData: any;

  constructor(private route: ActivatedRoute, private router: Router) {
    this.sectionTitle = route.snapshot.data['sectionTitle'];
    this.buttonName = route.snapshot.data['buttonName']
    this.id = +(route.snapshot.paramMap.get('id'));
    this.currentUrl = router.url;

  }

  ngOnInit() {

    this.couponFormData = this.route.snapshot.data.pageData.data;
    this.couponDetailsData = this.couponFormData.couponData;

  }

}
