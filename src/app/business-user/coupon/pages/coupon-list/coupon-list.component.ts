import { Component, OnInit, OnDestroy } from '@angular/core';
import { trigger, transition, style, animate } from '@angular/animations';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { DatePipe } from '@angular/common';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { CouponManagementService } from 'app/core/services/coupon-management.service';
import { UtilityService } from 'app/core/services/utility.service';
import { ErrorHandlerService } from 'app/core/services/error-handler.service';

interface Country {
  name: string;
  code: string;
}

interface City {
  name: string;
  code: string;
}

interface User {
  name: string;
  code: string;
}

@Component({
  selector: 'app-coupon-list',
  templateUrl: './coupon-list.component.html',
  styleUrls: ['./coupon-list.component.scss'],
  animations: [
    trigger('openClose', [
      transition(':enter', [
        style({ transform: 'translateX(20%)', opacity: 0 }),
        animate('500ms', style({ transform: 'translateX(0)', opacity: 1 })),
      ]),
      transition(':leave', [
        style({ transform: 'translateX(0)', opacity: 1, display: 'none' }),
        animate('500ms', style({ transform: 'translateX(20%)', opacity: 0 })),
      ]),
    ])
  ],
})
export class CouponListComponent implements OnInit, OnDestroy {

  countries: Country[];
  selectedCountry: Country;
  cities: City[];
  selectedCity: City;
  users: User[];
  selectedType: User;
  coupons: any[];

  couponList: any;
  couponData: any = [];
  today = new Date();
  todayDate: any;
  buttonTitle: string;

  selectedValue: string = 'Active';
  status: string = "";
  offset: number;
  code: string = "";
  total: number = 0;
  dateValue1;
  dateValue2;
  endDateValue1;
  endDateValue2;

  rangeValues: number[] = [0, 100];

  private _unSubscribe = new Subject<Boolean>();

  constructor(
    private _route: ActivatedRoute,
    private DateP: DatePipe,
    private couponManagementService: CouponManagementService,
    public utility: UtilityService,
    private errorHandling: ErrorHandlerService
  ) {
  }

  isSubmitted: Boolean = false;
  couponListForm: FormGroup;
  // show filter function
  isVisible: boolean = false;

  countryListFromAPI: string[];
  cityListFromAPI: string[];
  userListFromAPI: string[];

  showFilter() {
    this.isVisible = !this.isVisible;
  }


  ngOnInit() {

    /**get inital data */
    const pageContent = this._route.snapshot.data.pageData.data;
    this.couponData = pageContent.couponList;
    this.todayDate = this.DateP.transform(this.today);

    this.total = pageContent.total;
    /**setup form */
    this.couponListForm = new FormGroup({
      start_range_from: new FormControl(''),
      start_range_to: new FormControl(''),
      end_range_from: new FormControl(''),
      end_range_to: new FormControl(''),
      expiring_on: new FormControl(null)
    });
  }


  searchListing() {
    this.utility.loaderStart();
    let data: any = {};

    /**prepare filter data */
    if (this.offset != 0) {
      data.offset = this.offset;
    }

    if (this.code.trim() != "") {
      data.code = this.code.trim();
    }

    // expiring on
    if (this.couponListForm.value.expiring_on != null) {
      data.expired_on = this.couponListForm.value.expiring_on.toString();
    }

    // start date range
    if (this.couponListForm.value.start_range_from != null && this.couponListForm.value.start_range_to != null) {
      data.start_range_from = this.couponListForm.value.start_range_from.toString();
      data.start_range_to = this.couponListForm.value.start_range_to.toString();
    }

    // end date range
    if (this.couponListForm.value.end_range_from != null && this.couponListForm.value.end_range_to != null) {
      data.end_range_from = this.couponListForm.value.end_range_from.toString();
      data.end_range_to = this.couponListForm.value.end_range_to.toString();
    }

    this.couponManagementService.getCoupons(data).pipe(takeUntil(this._unSubscribe)).subscribe(
      (success: any) => {
        this.couponData = success.data.couponList;
        this.isVisible = false;
        this.utility.loaderStop();
      },
      error => {
        this.errorHandling.routeAccordingToError(error);
        this.utility.loaderStop();
      }
    )
  }


  /**keyword search */
  searchText(event) {

    this.code = event.target.value;
    if (event.charCode == 13) {
      // on click of enter
      this.searchListing();
    }
  }

  /**clear keyword search */
  clearInput(event) {
    this.code = event;
    this.searchListing();
  }

  /**change slider changes on moving the slider range */
  changeSlider() {
    this.couponListForm.controls.discount_range.markAsDirty();
    this.couponListForm.controls.discount_range.markAsTouched();
  }

  /**submits the filter */
  onSubmit() {
    this.isSubmitted = true;

    this.isSubmitted = true;
    if (this.couponListForm.dirty && this.couponListForm.touched) {
      this.searchListing();
    }
    this.isSubmitted = false;
    this.isVisible = false;

  }

  /**
   * clear filter
   */
  clearFilter() {
    this.couponListForm.reset();
    this.isSubmitted = false;
    this.isVisible = false;
  }

  /**
   * paginator
   * @param event pagination event stores page and offset details
   */
  paginate(event) {
    if (this.offset != undefined) {
      this.offset = parseInt(event.first);
      this.searchListing();
    } else {
      this.offset = parseInt(event.first);

    }

  }

  ngOnDestroy() {
    this._unSubscribe.next(true);
    this._unSubscribe.complete();
  }
}
