import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RequestForModuleListComponent } from './pages/request-for-module-list/request-for-module-list.component';
import { RequestForModuleDetailsComponent } from './pages/request-for-module-details/request-for-module-details.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        redirectTo: 'list',
        pathMatch: 'full',
        data: {
          sectionTitle: 'Request for vehicle',
        }
      },
      {
        path: 'list',
        component: RequestForModuleListComponent,
        data: {
          sectionTitle: 'Request for vehicle',
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RequestForModuleRoutingModule { }
