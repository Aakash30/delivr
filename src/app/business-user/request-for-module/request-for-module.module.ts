import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RequestForModuleRoutingModule } from './request-for-module-routing.module';
import { RequestForModuleListComponent } from './pages/request-for-module-list/request-for-module-list.component';
import { SharedModule } from 'app/shared/shared.module';
import { RequestForModuleDetailsComponent } from './pages/request-for-module-details/request-for-module-details.component';

@NgModule({
  imports: [
    CommonModule,
    RequestForModuleRoutingModule,
    SharedModule
  ],
  declarations: [RequestForModuleListComponent, RequestForModuleDetailsComponent]
})
export class RequestForModuleModule { }
