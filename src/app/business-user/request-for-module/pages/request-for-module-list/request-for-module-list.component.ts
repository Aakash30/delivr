import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { trigger, transition, style, animate } from '@angular/animations';

interface Status {
  name: string;
  code: string;
}

@Component({
  selector: 'app-request-for-module-list',
  templateUrl: './request-for-module-list.component.html',
  styleUrls: ['./request-for-module-list.component.scss'],
  animations: [
    trigger('openClose', [
      transition(':enter', [
        style({ transform: 'translateX(20%)', opacity: 0 }),
        animate('500ms', style({ transform: 'translateX(0)', opacity: 1 })),
      ]),
      transition(':leave', [
        style({ transform: 'translateX(0)', opacity: 1, display: 'none' }),
        animate('500ms', style({ transform: 'translateX(20%)', opacity: 0 })),
      ]),
    ])
  ],
})
export class RequestForModuleListComponent implements OnInit {

  activeTab: string;
  items: MenuItem[];
  activeItem: MenuItem;
  requestList: any[];
  newList: any[];
  resolvedList: any[];
  isSubmitted: boolean = false;
  isVisible: boolean = false;

  requestModuleForm: FormGroup;



  constructor() { }

  ngOnInit() {
    this.activeTab = 'all';

    this.items = [
      {
        label: 'All Request'
      },
      {
        label: 'New'
      },
      {
        label: 'Resolved'
      },
    ];
    this.requestList = [
      {
        id: 1,
        businessName: 'Alfredo Corp',
        name: 'Alfredo Dawson',
        email: 'alfredo.dawson@gmail.com',
        contact: '(305) 851-0168',
        status: 'New',
      },
      {
        id: 2,
        businessName: 'Alfredo Corp',
        name: 'Alfredo Dawson',
        email: 'alfredo.dawson@gmail.com',
        contact: '(305) 851-0168',
        status: 'Ongoing',
      },
      {
        id: 3,
        businessName: 'Alfredo Corp',
        name: 'Alfredo Dawson',
        email: 'alfredo.dawson@gmail.com',
        contact: '(305) 851-0168',
        status: 'Resolved',
      }
    ];
    this.newList = [
      {
        id: 1,
        businessName: 'Alfredo Corp',
        name: 'Alfredo Dawson',
        email: 'alfredo.dawson@gmail.com',
        contact: '(305) 851-0168',
        status: 'New',
      },
      {
        id: 2,
        businessName: 'Alfredo Corp',
        name: 'Alfredo Dawson',
        email: 'alfredo.dawson@gmail.com',
        contact: '(305) 851-0168',
        status: 'Ongoing',
      },
      {
        id: 3,
        businessName: 'Alfredo Corp',
        name: 'Alfredo Dawson',
        email: 'alfredo.dawson@gmail.com',
        contact: '(305) 851-0168',
        status: 'Resolved',
      }
    ];
    this.resolvedList = [
      {
        id: 1,
        businessName: 'Alfredo Corp',
        name: 'Alfredo Dawson',
        email: 'alfredo.dawson@gmail.com',
        contact: '(305) 851-0168',
        status: 'New',
      },
      {
        id: 2,
        businessName: 'Alfredo Corp',
        name: 'Alfredo Dawson',
        email: 'alfredo.dawson@gmail.com',
        contact: '(305) 851-0168',
        status: 'Ongoing',
      },
      {
        id: 3,
        businessName: 'Alfredo Corp',
        name: 'Alfredo Dawson',
        email: 'alfredo.dawson@gmail.com',
        contact: '(305) 851-0168',
        status: 'Resolved',
      }
    ];
    this.activeItem = this.items[0];

    this.requestModuleForm = new FormGroup({
      brand: new FormControl(''),
      model: new FormControl(''),
      serviceRangeOne: new FormControl('', [Validators.pattern(/^[0-9]\d{0,3}$/)]),
      serviceRangeTwo: new FormControl('', [Validators.pattern(/^[0-9]\d{0,3}$/)]),
      distanceRangeOne: new FormControl('', [Validators.pattern(/^[0-9]\d{0,3}$/)]),
      distanceRangeTwo: new FormControl('', [Validators.pattern(/^[0-9]\d{0,3}$/)]),
      country: new FormControl(''),
      city: new FormControl(''),
      status: new FormControl(''),
      statusList: new FormControl(''),

    });


  }


  getItem(event, index) {
    event.preventDefault();
    if (index === 0) {
      this.activeTab = 'all';
    } else if (index === 1) {
      this.activeTab = 'new';
    } else if (index === 2) {
      this.activeTab = 'resolved';
    }
  }

  onSubmit() {
    this.isSubmitted = true;
    if (this.requestModuleForm.valid) {
      this.requestModuleForm.reset();
      this.isSubmitted = false;
    }
    else {
    }
  }

  // show filter function
  showFilter() {
    this.isVisible = !this.isVisible;
  }

  closeFilter() {
    this.requestModuleForm.reset();
    this.isSubmitted = false;
    this.isVisible = false;
  }


}
