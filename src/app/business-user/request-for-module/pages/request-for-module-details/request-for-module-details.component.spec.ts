import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestForModuleDetailsComponent } from './request-for-module-details.component';

describe('RequestForModuleDetailsComponent', () => {
  let component: RequestForModuleDetailsComponent;
  let fixture: ComponentFixture<RequestForModuleDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequestForModuleDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestForModuleDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
