import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VendorMaintenanceRoutingModule } from './vendor-maintenance-routing.module';
import { SharedModule } from 'app/shared/shared.module';
import { MaintenanceListComponent } from './pages/maintenance-list/maintenance-list.component';

@NgModule({
  imports: [
    CommonModule,
    VendorMaintenanceRoutingModule,
    SharedModule
  ],
  declarations: [MaintenanceListComponent]
})
export class VendorMaintenanceModule { }
