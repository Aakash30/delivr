import { Component, OnInit } from '@angular/core';
import { trigger, transition, style, animate } from '@angular/animations';
import { MenuItem } from 'primeng/api';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';

interface Brand {
  name: string;
  code: string;
}

interface Status {
  name: string;
  code: string;
}

@Component({
  selector: 'app-maintenance-list',
  templateUrl: './maintenance-list.component.html',
  styleUrls: ['./maintenance-list.component.scss'],
  animations: [
    trigger('openClose', [
      transition(':enter', [
        style({ transform: 'translateX(20%)', opacity: 0 }),
        animate('500ms', style({ transform: 'translateX(0)', opacity: 1 })),
      ]),
      transition(':leave', [
        style({ transform: 'translateX(0)', opacity: 1, display: 'none' }),
        animate('500ms', style({ transform: 'translateX(20%)', opacity: 0 })),
      ]),
    ])
  ],
})
export class MaintenanceListComponent implements OnInit {

  brands: Brand[];
  selectedBrand: Brand;
  status: Status[];
  selectedStatus: Status[];
  activeTab: string;
  items: MenuItem[];
  activeItem: MenuItem;
  newList: any[];
  ongoingList: any[];
  serviceList: any[];
  partList: any[];
  maintenanceForm: FormGroup;
  getItem(event, index) {
    event.preventDefault();
    if (index === 0) {
      this.activeTab = 'new';
    } else if (index === 1) {
      this.activeTab = 'ongoing';
    } else if (index === 2) {
      this.activeTab = 'service';
    } else if (index === 3) {
      this.activeTab = 'part';
    }
  }

  isSubmitted: Boolean = false;
  // show filter function
  isVisible: boolean = false;

  brandListFromAPI: string[];
  statusListFromAPI: string[];

  showFilter() {
    this.isVisible = !this.isVisible;

  }

  closeFilter() {
    this.isVisible = false;
  }
  constructor() { }

  ngOnInit() {
    this.activeTab = 'new';

    this.items = [
      {
        label: 'New Services'
      },
      {
        label: 'Ongoing Services'
      },
      {
        label: 'Service History'
      },
      {
        label: 'Part History'
      },
    ];
    this.newList = [
      {
        id: 1,
        brand: 'Hero',
        model: 'CD Delux',
        regNo: 'F 65022',
        engine: '82834924',
        lastService: 'May 20, 2019',
        serviceType: 'Oil change',
        distance: '3423',
        renewal: '4000',
        status: 'Pending'
      },
      {
        id: 2,
        brand: 'Hero',
        model: 'CD Delux',
        regNo: 'F 65022',
        engine: '82834924',
        lastService: 'May 20, 2019',
        serviceType: 'Oil change',
        distance: '3423',
        renewal: '4000',
        status: 'Ongoing'
      },
      {
        id: 3,
        brand: 'Hero',
        model: 'CD Delux',
        regNo: 'F 65022',
        engine: '82834924',
        lastService: 'May 20, 2019',
        serviceType: 'Oil change',
        distance: '3423',
        renewal: '4000',
        status: 'Completed'
      }
    ];
    this.ongoingList = [
      {
        id: 1,
        brand: 'Hero',
        model: 'CD Delux',
        regNo: 'F 65022',
        engine: '82834924',
        lastService: 'May 20, 2019',
        serviceType: 'Oil change',
        distance: '3423',
        renewal: '4000',
        status: 'Pending'
      },
      {
        id: 2,
        brand: 'Hero',
        model: 'CD Delux',
        regNo: 'F 65022',
        engine: '82834924',
        lastService: 'May 20, 2019',
        serviceType: 'Oil change',
        distance: '3423',
        renewal: '4000',
        status: 'Ongoing'
      },
      {
        id: 3,
        brand: 'Hero',
        model: 'CD Delux',
        regNo: 'F 65022',
        engine: '82834924',
        lastService: 'May 20, 2019',
        serviceType: 'Oil change',
        distance: '3423',
        renewal: '4000',
        status: 'Completed'
      }
    ];
    this.serviceList = [
      {
        id: 1,
        brand: 'Hero',
        model: 'CD Delux',
        regNo: 'F 65022',
        engine: '82834924',
        lastService: 'May 20, 2019',
        serviceType: 'Oil change',
        distance: '3423',
        renewal: '4000',
        status: 'Pending'
      },
      {
        id: 2,
        brand: 'Hero',
        model: 'CD Delux',
        regNo: 'F 65022',
        engine: '82834924',
        lastService: 'May 20, 2019',
        serviceType: 'Oil change',
        distance: '3423',
        renewal: '4000',
        status: 'Ongoing'
      },
      {
        id: 3,
        brand: 'Hero',
        model: 'CD Delux',
        regNo: 'F 65022',
        engine: '82834924',
        lastService: 'May 20, 2019',
        serviceType: 'Oil change',
        distance: '3423',
        renewal: '4000',
        status: 'Completed'
      }
    ];
    this.partList = [
      {
        id: 1,
        brand: 'Hero',
        model: 'CD Delux',
        regNo: 'F 65022',
        engine: '82834924',
        lastService: 'May 20, 2019',
        serviceType: 'Oil change',
        distance: '3423',
        renewal: '4000',
        status: 'Pending'
      },
      {
        id: 2,
        brand: 'Hero',
        model: 'CD Delux',
        regNo: 'F 65022',
        engine: '82834924',
        lastService: 'May 20, 2019',
        serviceType: 'Oil change',
        distance: '3423',
        renewal: '4000',
        status: 'Ongoing'
      },
      {
        id: 3,
        brand: 'Hero',
        model: 'CD Delux',
        regNo: 'F 65022',
        engine: '82834924',
        lastService: 'May 20, 2019',
        serviceType: 'Oil change',
        distance: '3423',
        renewal: '4000',
        status: 'Completed'
      }
    ];
    this.activeItem = this.items[0];

    this.maintenanceForm = new FormGroup({
      brand: new FormControl('', Validators.required),
      modal: new FormControl('', Validators.required),
      service: new FormControl('', Validators.required),
      status: new FormControl('', Validators.required),
    });

    this.brandListFromAPI = ['Audi', 'BMW', 'Fiat'];
    this.brands = this.arrayOfStringsToArrayOfObjects(this.brandListFromAPI);

    this.statusListFromAPI = ['Audi', 'BMW', 'Fiat'];
    this.status = this.arrayOfStringsToArrayOfObjects(this.statusListFromAPI);

    // brand dropdown list
    // this.brands = [
    //   { name: 'New York', code: 'NY' },
    //   { name: 'Rome', code: 'RM' },
    //   { name: 'London', code: 'LDN' },
    //   { name: 'Istanbul', code: 'IST' },
    //   { name: 'Paris', code: 'PRS' }
    // ];
    // State dropdown list
    // this.status = [
    //   { name: 'Pending', code: 'ACT' },
    //   { name: 'Ongoing', code: 'INA' },
    //   { name: 'Completed', code: 'INA' },
    // ];
  }

  arrayOfStringsToArrayOfObjects(arr: string[]) {
    const newArray = [];
    arr.forEach(element => {
      newArray.push({
        label: element,
        value: element
      });
    });
    return newArray;
  }

  arrayOfObjectToArrayOfStrings(obj: []) {
    const newArray = [];
    obj.forEach(element => {
      newArray.push(element['value']);
    });
    return newArray;
  }

  onSubmit() {
    this.isSubmitted = true;
    if (this.maintenanceForm.valid) {
      this.maintenanceForm.reset();
      this.isSubmitted = false;
    }
    else {
    }
  }

  resetForm() {

    this.maintenanceForm.reset();
    this.isSubmitted = false;
    // this.display = false;
  }

  // searchText(event) {
  //   this.keyword = event.target.value;
  //   console.log("keyword::", this.keyword);
  //   if (event.charCode == 13) {
  //     // on click of enter
  //     this.listing();
  //   }
  //   this.listing();
  // }

}
