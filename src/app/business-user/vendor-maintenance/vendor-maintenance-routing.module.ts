import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MaintenanceListComponent } from './pages/maintenance-list/maintenance-list.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        component: MaintenanceListComponent,
        data: {
          sectionTitle: 'Maintenance'
        }
      },
      {
        path: 'maintenance-list',
        component: MaintenanceListComponent,
        data: {
          sectionTitle: 'Maintenance',
          buttonName: 'Create'
        }
      },

    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VendorMaintenanceRoutingModule { }
