import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VendorVehicleManagementRoutingModule } from './vendor-vehicle-management-routing.module';
import { VehicleListComponent } from './pages/vehicle-list/vehicle-list.component';
import { SharedModule } from 'app/shared/shared.module';
import { LeasePlanListComponent } from './pages/lease-plan-list/lease-plan-list.component';
import { VehicleListDetailsComponent } from './pages/vehicle-list-details/vehicle-list-details.component';
import { LeasePlanDetailsComponent } from './pages/lease-plan-details/lease-plan-details.component';
import { VendorVehicleManagementLayoutComponent } from './vendor-vehicle-management-layout/vendor-vehicle-management-layout.component';

@NgModule({
  imports: [
    CommonModule,
    VendorVehicleManagementRoutingModule,
    SharedModule
  ],
  declarations: [VehicleListComponent, LeasePlanListComponent, VehicleListDetailsComponent, LeasePlanDetailsComponent, VendorVehicleManagementLayoutComponent]
})
export class VendorVehicleManagementModule { }
