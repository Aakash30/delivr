import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VehicleListComponent } from './pages/vehicle-list/vehicle-list.component';
import { LeasePlanListComponent } from './pages/lease-plan-list/lease-plan-list.component';
import { VehicleListDetailsComponent } from './pages/vehicle-list-details/vehicle-list-details.component';
import { LeasePlanDetailsComponent } from './pages/lease-plan-details/lease-plan-details.component';
import { VendorVehicleResolverService } from 'app/core/resolver/vendor-vehicle-resolver.service';
import { VendorVehicleManagementLayoutComponent } from './vendor-vehicle-management-layout/vendor-vehicle-management-layout.component';

const routes: Routes = [
  {
    path: "",
    component: VendorVehicleManagementLayoutComponent,
    children: [
      {
        path: "vehicle-lease-plan",
        component: LeasePlanListComponent,
        data: {
          sectionTitle: "Lease Plan",
          buttonName: "Create"
        },
        resolve: {
          pageData: VendorVehicleResolverService
        }
      },
      {
        path: "vehicle-list",
        component: VehicleListComponent,
        data: {
          sectionTitle: "Vehicle List",
          buttonName: "Create"
        },
        resolve: {
          pageData: VendorVehicleResolverService
        }
      },
      {
        path: "vehicle-list/details/:id",
        component: VehicleListDetailsComponent,
        data: {
          sectionTitle: "Details",
          buttonName: "Create"
        },
        resolve: {
          pageData: VendorVehicleResolverService
        }
      },
      {
        path: "vehicle-lease-plan/details/:id",
        component: LeasePlanDetailsComponent,
        data: {
          sectionTitle: "Details"
        },
        resolve: {
          pageData: VendorVehicleResolverService
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VendorVehicleManagementRoutingModule { }
