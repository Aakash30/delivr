import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VendorVehicleManagementLayoutComponent } from './vendor-vehicle-management-layout.component';

describe('VendorVehicleManagementLayoutComponent', () => {
  let component: VendorVehicleManagementLayoutComponent;
  let fixture: ComponentFixture<VendorVehicleManagementLayoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VendorVehicleManagementLayoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VendorVehicleManagementLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
