import { Component, OnInit } from '@angular/core';
import { UtilityService } from 'app/core/services/utility.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-vendor-vehicle-management-layout',
  templateUrl: './vendor-vehicle-management-layout.component.html',
  styleUrls: ['./vendor-vehicle-management-layout.component.scss']
})
export class VendorVehicleManagementLayoutComponent implements OnInit {

  constructor(private utilityService: UtilityService, private router: Router) { }

  ngOnInit() {
  }

}
