import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeasePlanListComponent } from './lease-plan-list.component';

describe('LeasePlanListComponent', () => {
  let component: LeasePlanListComponent;
  let fixture: ComponentFixture<LeasePlanListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeasePlanListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeasePlanListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
