import { Component, OnInit } from '@angular/core';
import { trigger, transition, style, animate } from '@angular/animations';
import { ActivatedRoute } from '@angular/router';
import { UtilityService } from 'app/core/services/utility.service';
import { VendorVehicleService } from 'app/core/services/vendor-vehicle.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';


@Component({
  selector: 'app-lease-plan-list',
  templateUrl: './lease-plan-list.component.html',
  styleUrls: ['./lease-plan-list.component.scss'],
  animations: [
    trigger('openClose', [
      transition(':enter', [
        style({ transform: 'translateX(20%)', opacity: 0 }),
        animate('500ms', style({ transform: 'translateX(0)', opacity: 1 })),
      ]),
      transition(':leave', [
        style({ transform: 'translateX(0)', opacity: 1, display: 'none' }),
        animate('500ms', style({ transform: 'translateX(20%)', opacity: 0 })),
      ]),
    ])
  ],
})
export class LeasePlanListComponent implements OnInit {

  leaseList: any[];
  today: Date;
  maxDate: Date;
  dateValue1;
  dateValue2;

  offset: number;
  keyword: string = "";
  total: number = 0;

  private _unsubscribe = new Subject<Boolean>();

  constructor(private route: ActivatedRoute, public utility: UtilityService, private _vehicleService: VendorVehicleService) { }

  // show filter function
  isVisible: boolean = false;

  showFilter() {
    this.isVisible = !this.isVisible;
  }

  closeFilter() {
    this.isVisible = false;
  }


  ngOnInit() {
    // get today date
    this.today = new Date();

    const pageContent = this.route.snapshot.data.pageData.data;
    this.leaseList = pageContent.leasePlanData;
    this.total = pageContent.total;
  }

  listing() {

    let data = {};

    if (this.keyword.trim() != "") {
      data["keyword"] = this.keyword.trim();
    }

    if (this.offset != 0) {
      data["offset"] = this.offset;
    }
    this._vehicleService.fetchLeaseBatch(data).pipe(takeUntil(this._unsubscribe)).subscribe((success: any) => {
      this.leaseList = success.data.leasePlanData;

    });
  }
  /** pagination */
  paginate(event) {
    if (this.offset != undefined) {
      this.offset = parseInt(event.first);
      this.listing();
    } else {
      this.offset = parseInt(event.first);

    }
  }

  /** APply key search */
  searchText(event) {
    this.keyword = event.target.value;
    if (event.charCode == 13) {
      // on click of enter
      this.listing();
    }
  }

  /** clear key search */
  clearInput(event) {
    this.keyword = event;
    this.listing();
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this._unsubscribe.next(true);
    this._unsubscribe.complete();
  }
}
