import { Component, OnInit } from '@angular/core';
import { trigger, transition, style, animate } from '@angular/animations';
import { ActivatedRoute } from '@angular/router';
import { VendorVehicleService } from 'app/core/services/vendor-vehicle.service';
import { ErrorHandlerService } from 'app/core/services/error-handler.service';
import { FormGroup, FormControl } from '@angular/forms';
import { UtilityService } from 'app/core/services/utility.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-vehicle-list',
  templateUrl: './vehicle-list.component.html',
  styleUrls: ['./vehicle-list.component.scss'],
  animations: [
    trigger('openClose', [
      transition(':enter', [
        style({ transform: 'translateX(20%)', opacity: 0 }),
        animate('500ms', style({ transform: 'translateX(0)', opacity: 1 })),
      ]),
      transition(':leave', [
        style({ transform: 'translateX(0)', opacity: 1, display: 'none' }),
        animate('500ms', style({ transform: 'translateX(20%)', opacity: 0 })),
      ]),
    ])
  ],
})
export class VehicleListComponent implements OnInit {

  brands: any[];
  modelFromAPI: any[];
  models: any[] = [];
  vehicleList: any[];
  total: number = 0;
  offset: number;

  vehicleFilterForm: FormGroup;
  keyword: string = "";

  // show filter function
  isVisible: boolean = false;

  private _unsubscribe = new Subject<Boolean>();

  showFilter() {
    this.isVisible = !this.isVisible;

  }

  closeFilter() {
    this.isVisible = false;
  }

  constructor(private route: ActivatedRoute, private vehicleService: VendorVehicleService, private errorHandling: ErrorHandlerService, private utility: UtilityService) { }

  ngOnInit() {

    this.vehicleFilterForm = new FormGroup({
      brand: new FormControl(null),
      model: new FormControl(null)
    });
    const pageContent = this.route.snapshot.data.pageData.data;
    this.vehicleList = pageContent.vehicleList;
    this.total = pageContent.count;

    // brand dropdown list
    this.brands = this.utility.arrayOfObjectToConvertInDropdownFormat(pageContent.brand_list, "brand_name", "brand_id");

    this.modelFromAPI = pageContent.model_list;
  }



  /** calls the server api to get the list */
  listing() {
    this.utility.loaderStart();
    let data = {};
    if (this.offset != 0) {
      data["offset"] = this.offset;
    }

    if (this.vehicleFilterForm.value.brand != null && this.vehicleFilterForm.value.brand != "") {
      data["brand"] = this.vehicleFilterForm.value.brand;
    }

    if (this.vehicleFilterForm.value.model != null && this.vehicleFilterForm.value.model != "") {
      data["model"] = this.vehicleFilterForm.value.model;
    }

    if (this.keyword.trim() != "") {
      data["keyword"] = this.keyword.trim();
    }

    this.vehicleService.fetchVehicleList(data).pipe(takeUntil(this._unsubscribe)).subscribe((success: any) => {
      this.vehicleList = success.data.vehicleList;
      this.total = success.data.count;
      this.utility.loaderStop();
    }, error => {
      this.errorHandling.routeAccordingToError(error);
      this.utility.loaderStop();
    });

  }

  /** Apply filter to the list */
  filter() {
    if (this.vehicleFilterForm.touched && this.vehicleFilterForm.dirty) {
      this.offset = 0;
      this.listing();
    }
    this.isVisible = false;
    this.vehicleFilterForm.reset();
  }

  /** Clear list filter */
  clearFilter() {
    if (this.vehicleFilterForm.dirty && this.vehicleFilterForm.touched) {
      this.vehicleFilterForm.reset();
      this.offset = 0;
      this.listing();
    } else {
      this.vehicleFilterForm.reset();
    }
    this.isVisible = false;
  }

  /** pagination */
  paginate(event) {
    if (this.offset != undefined) {
      this.offset = parseInt(event.first);
      this.listing();
    } else {
      this.offset = parseInt(event.first);
    }

  }

  /** APply key search */
  searchText(event) {
    this.keyword = event.target.value;
    if (event.charCode == 13) {
      this.offset = 0;
      // on click of enter
      this.listing();
    }
  }

  /** clear key search */
  clearInput(event) {
    this.keyword = event;
    this.offset = 0;
    this.listing();
  }

  filterModelOnBrandChange() {
    if (this.vehicleFilterForm.value.brand != null && this.vehicleFilterForm.value.brand != "") {
      let filteredModel = this.modelFromAPI.filter(element => {
        return element.brand_id == this.vehicleFilterForm.value.brand;
      });
      this.models = this.utility.arrayOfObjectToConvertInDropdownFormat(filteredModel, "model_name", "model_id");
    } else {
      this.models = [];
    }
    this.vehicleFilterForm.controls["model"].setValue(null);
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this._unsubscribe.next(true);
    this._unsubscribe.complete();
  }
}
