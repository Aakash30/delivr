import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UtilityService } from 'app/core/services/utility.service';
import { FormGroup, FormControl } from '@angular/forms';
import { trigger, transition, style, animate } from '@angular/animations';
import { VendorVehicleService } from 'app/core/services/vendor-vehicle.service';
import { ErrorHandlerService } from 'app/core/services/error-handler.service';
import { validateAllFormFields, noWhitespaceValidator } from 'app/shared/utils/custom-validators';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-lease-plan-details',
  templateUrl: './lease-plan-details.component.html',
  styleUrls: ['./lease-plan-details.component.scss'],
  animations: [
    trigger('openClose', [
      transition(':enter', [
        style({ transform: 'translateX(20%)', opacity: 0 }),
        animate('500ms', style({ transform: 'translateX(0)', opacity: 1 })),
      ]),
      transition(':leave', [
        style({ transform: 'translateX(0)', opacity: 1, display: 'none' }),
        animate('500ms', style({ transform: 'translateX(20%)', opacity: 0 })),
      ]),
    ])
  ],
})
export class LeasePlanDetailsComponent implements OnInit {

  leasePlanBatch: any;
  today: Date;
  maxDate: Date;

  constructor(private route: ActivatedRoute, private utility: UtilityService, private vehicleService: VendorVehicleService, private errorHandling: ErrorHandlerService) { }

  leasePlanList: any[];
  leaseList: any[];

  brandListFromAPI: any[];
  modelListFromAPI: any[];
  brands;
  models: any[] = [];
  leaseFilter: FormGroup;
  id;
  keyword: string = "";

  // show filter function
  isVisible: boolean = false;

  private _unsubscribe = new Subject<Boolean>();
  currency = "";

  showFilter() {
    this.isVisible = !this.isVisible;

  }

  closeFilter() {
    this.isVisible = false;
  }


  ngOnInit() {
    this.today = new Date();

    this.id = this.route.snapshot.params.id;

    const pageContent = this.route.snapshot.data.pageData.data;

    this.leaseList = pageContent.batchData;

    this.currency = this.leaseList[0] ? this.leaseList[0].vehicle.currency : "";
    this.leasePlanBatch = pageContent.batchDetails;
    this.brandListFromAPI = pageContent.brand_list;
    this.modelListFromAPI = pageContent.model_list;

    this.brands = this.utility.arrayOfObjectToConvertInDropdownFormat(this.brandListFromAPI, "brand_name", "brand_id");

    this.leaseFilter = new FormGroup({
      brand: new FormControl(null),
      model: new FormControl(null)
    });
  }


  /** filter models on the basis of brand */
  filterModelOnBrandChange() {
    if (this.leaseFilter.value.brand != null && this.leaseFilter.value.brand != "") {
      let filteredModel = this.modelListFromAPI.filter(element => {
        return element.brand_id == this.leaseFilter.value.brand
      });
      this.models = this.utility.arrayOfObjectToConvertInDropdownFormat(filteredModel, "model_name", "model_id");

    } else {
      this.models = [];
    }
    this.leaseFilter.controls["model"].setValue(null);
    this.listing();
  }

  listing() {
    this.utility.loaderStart();
    let data = {};

    if (this.keyword.trim() != "") {
      data["keyword"] = this.keyword;
    }

    if (this.leaseFilter.value.brand != null && this.leaseFilter.value.brand != "") {
      data["brand"] = this.leaseFilter.value.brand;
    }

    if (this.leaseFilter.value.model != null && this.leaseFilter.value.model != "") {
      data["model"] = this.leaseFilter.value.model;
    }

    this.vehicleService.fetchLeaseBatchDetails(this.utility.base64Decode(this.id), data).pipe(takeUntil(this._unsubscribe)).subscribe((success: any) => {
      this.leaseList = success.data.batchData;
      this.utility.loaderStop();
    }, error => {
      this.errorHandling.routeAccordingToError(error);
      this.utility.loaderStop();
    })
  }

  /** Apply filter to the list */
  filter() {
    if (this.leaseFilter.touched && this.leaseFilter.dirty) {
      this.listing();
    }
    this.isVisible = false;
  }

  /** Clear list filter */
  clearFilter() {
    if (this.leaseFilter.dirty && this.leaseFilter.touched) {
      this.leaseFilter.reset();
      this.listing();
    } else {
      this.leaseFilter.reset();
    }
    this.isVisible = false;
  }


  /** APply key search */
  searchText(event) {
    this.keyword = event.target.value;
    if (event.charCode == 13) {
      // on click of enter
      this.listing();
    }
  }


  /** clear key search */
  clearInput(event) {
    this.keyword = event;
    this.listing();
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this._unsubscribe.next(true);
    this._unsubscribe.complete();
  }
}
