import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeasePlanDetailsComponent } from './lease-plan-details.component';

describe('LeasePlanDetailsComponent', () => {
  let component: LeasePlanDetailsComponent;
  let fixture: ComponentFixture<LeasePlanDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeasePlanDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeasePlanDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
