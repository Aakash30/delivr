import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-vehicle-list-details',
  templateUrl: './vehicle-list-details.component.html',
  styleUrls: ['./vehicle-list-details.component.scss']
})
export class VehicleListDetailsComponent implements OnInit {

  activeTab: string;
  items: MenuItem[];
  activeItem: MenuItem;
  allocatedList: any[] = [];
  maintainList: any[];
  leaseList: any[] = [];
  sectionTitle: String;
  imageDisplayFlag: boolean = false;
  vehicles: any;
  imageUrl: any;
  today = new Date();

  getItem(event, index) {
    event.preventDefault();
    if (index === 0) {
      this.activeTab = 'allocated';
    } else if (index === 1) {
      this.activeTab = 'lease';
    }
    //  else if (index === 2) {
    //   this.activeTab = 'lease';
    // }
  }

  addDisplay: Boolean = false;

  modifyVehicle() {
    this.addDisplay = true;
  }
  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.activeTab = 'allocated';

    this.items = [
      {
        label: 'Allocated'
      },
      // {
      //   label: 'Maintenance'
      // },
      {
        label: 'Lease'
      },
    ];

    const pageContent = this.route.snapshot.data.pageData.data;

    this.vehicles = pageContent.vehicleDetails;

    if (pageContent.vehicleDetails.vehicle_mapping[0].Users != null) {
      this.allocatedList.push(pageContent.vehicleDetails.vehicle_mapping[0].Users);
    }
    if (pageContent.vehicleDetails.vehicle_mapping[0].batch != null) {
      this.leaseList.push(pageContent.vehicleDetails.vehicle_mapping[0].batch);
    }
    this.activeItem = this.items[0];
  }

  // image viewer function
  openImageDialog(type, imageObject, key) {

    if(imageObject){
      this.imageDisplayFlag = true;
    }
    
    switch (type) {

      case type = 'registration_card':
        this.imageUrl = imageObject;
        break;

      case type = 'insurance':
        this.imageUrl = imageObject;
        break;

      case type = 'vehicle-images':
        imageObject.forEach((item, index) => {
          if (index == key) {
            this.imageUrl = item.vehicle_image;
          }
        });
        break;
    }
  }

}
