import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VehicleListDetailsComponent } from './vehicle-list-details.component';

describe('VehicleListDetailsComponent', () => {
  let component: VehicleListDetailsComponent;
  let fixture: ComponentFixture<VehicleListDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VehicleListDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VehicleListDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
