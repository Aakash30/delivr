import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RequestForRepairListComponent } from './pages/request-for-repair-list/request-for-repair-list.component';
import { RequestForRepairDetailsComponent } from './pages/request-for-repair-details/request-for-repair-details.component';
import { RepairRequestBusinessResolverService } from 'app/core/resolver/repair-request-business-resolver.service';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        component: RequestForRepairListComponent,
        data: {
          sectionTitle: 'Request For Repair',
          buttonName: 'Create',
          onlyForUser: ['vendor', 'sub_ordinate'],
          expectedRole: ['request-for-repair']
        },
        resolve: {
          pageData: RepairRequestBusinessResolverService
        }
      },
      {
        path: 'details/:id',
        component: RequestForRepairDetailsComponent,
        data: {
          sectionTitle: 'Request For Repair',
          buttonName: 'Create',
          onlyForUser: ['vendor', 'sub_ordinate'],
          expectedRole: ['request-for-repair']
        },
        resolve: {
          pageData: RepairRequestBusinessResolverService
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RequestForRepairRoutingModule { }
