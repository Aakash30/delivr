import { Component, OnInit } from '@angular/core';
import { UtilityService } from 'app/core/services/utility.service';
import { ActivatedRoute } from '@angular/router';
import { Subject } from 'rxjs';
import { RepairRequestBusinessService } from 'app/core/services/repair-request-business.service';
import { FormGroup, FormControl } from '@angular/forms';
import { trigger, transition, style, animate } from '@angular/animations';
import { takeUntil } from 'rxjs/operators';
import { ErrorHandlerService } from 'app/core/services/error-handler.service';

interface Status {
  name: string;
  code: string;
}


@Component({
  selector: 'app-request-for-repair-list',
  templateUrl: './request-for-repair-list.component.html',
  styleUrls: ['./request-for-repair-list.component.scss'],
  animations: [
    trigger('openClose', [
      transition(':enter', [
        style({ transform: 'translateX(20%)', opacity: 0 }),
        animate('500ms', style({ transform: 'translateX(0)', opacity: 1 })),
      ]),
      transition(':leave', [
        style({ transform: 'translateX(0)', opacity: 1, display: 'none' }),
        animate('500ms', style({ transform: 'translateX(20%)', opacity: 0 })),
      ]),
    ])
  ],
})
export class RequestForRepairListComponent implements OnInit {


  requestList: any[];

  status: Status[];
  selectedRequestStatus: Status[];

  requestStatusList: string[];
  total: number = 0;
  offset: number = 0;
  keyword: string = "";
  dateValue2;
  dateValue1;


  // startDate = new Date();

  isSubmitted: Boolean = false;

  isVisible: boolean = false;

  requestRepairForm: FormGroup;

  brandList: string[];
  modelList: string[];
  modelListFromAPI: string[];
  statusListFromAPI: string[];

  isRequestSubmitted: boolean = false;

  private _unsubscribe = new Subject<Boolean>();

  constructor(
    public _utility: UtilityService,
    private _route: ActivatedRoute,
    private _repairRequestService: RepairRequestBusinessService,
    private _errorHandling: ErrorHandlerService
  ) { }

  ngOnInit() {

    const pageContent = this._route.snapshot.data.pageData.data;
    this.requestList = pageContent.requestForRepair;
    this.total = pageContent.total;

    this.requestRepairForm = new FormGroup({
      brand: new FormControl(null),
      model: new FormControl(null),
      status: new FormControl(null),
      startDate: new FormControl(''),
      endDate: new FormControl('')
    });

    let brandsFromApi = pageContent.vehicleBrandList;
    this.brandList = this._utility.arrayOfObjectToConvertInDropdownFormat(brandsFromApi, "brand_name", "brand_id");

    this.modelListFromAPI = pageContent.vehicleModelList;
    this.modelList = [];

    this.requestStatusList = ['New', 'Ongoing', 'Finished'];
    this.status = this._utility.arrayOfStringsToArrayOfObjects(this.requestStatusList);

  }

  showFilter() {
    this.isVisible = !this.isVisible;

  }
  /**
   * loads data list - usually used for filter and pagination
   */
  listing() {

    /**filter data array */
    let data = {};

    if (this.keyword.trim() != "") {
      data["keyword"] = this.keyword.trim();
    }

    if (this.offset != 0) {
      data["offset"] = this.offset;
    }

    if (this.requestRepairForm.value.brand != null) {
      data["brand"] = this.requestRepairForm.value.brand;
    }

    if (this.requestRepairForm.value.model != null) {
      data["model"] = this.requestRepairForm.value.model;
    }

    if (this.requestRepairForm.value.status != null) {
      data["status"] = this.requestRepairForm.value.status.toLowerCase();
    }

    this._repairRequestService.fetchRepairRequest(data).pipe(takeUntil(this._unsubscribe)).subscribe((success: any) => {
      this.requestList = success.data.requestForRepair;
      this.total = success.data.total;
    }, error => {
      this._errorHandling.routeAccordingToError(error);
    })
  }



  /** key searching */
  searchText(event) {
    if (event.charCode == 13) {

      this.keyword = event.target.value;

      this.listing();
    }
  }

  /** clears category input */
  clearInput(event) {
    this.keyword = event;
    this.listing();
  }

  /**
   * 
   * @param event get pagination event
   */
  paginate(event) {
    this.offset = event.first;
  }

  /**
   * submits filter form
   */
  onSubmit() {
    this.isSubmitted = true;
    if (this.requestRepairForm.valid) {

      this.isSubmitted = true;
      this.listing();
    }
    else {
      this.isSubmitted = false;
    }
    this.isVisible = false;
  }

  /**
   * clear filter form and reset filter
   */
  clearFilter() {
    this.requestRepairForm.reset();
    this.isSubmitted = false;
    this.isVisible = false;
    this.listing();
  }

  closeFilter() {
    this.isVisible = false;
  }

  onchangeBrand(selectedBrand) {
    this.requestRepairForm.controls.model.setValue(null);
    if (selectedBrand != null) {
      let modelArray = [];
      this.modelListFromAPI.filter((element: any) => {
        if (element.brand_id == selectedBrand) modelArray.push(element);
      });
      this.modelList = this._utility.arrayOfObjectToConvertInDropdownFormat(modelArray, "model_name", "model_id");
    } else {
      this.modelList = [];
    }
  }

  ngOnDestroy(): void {
    this._unsubscribe.next(true);
    this._unsubscribe.complete();
  }

}
