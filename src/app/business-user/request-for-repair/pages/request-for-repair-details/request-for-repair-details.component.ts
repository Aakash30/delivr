import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-request-for-repair-details',
  templateUrl: './request-for-repair-details.component.html',
  styleUrls: ['./request-for-repair-details.component.scss']
})
export class RequestForRepairDetailsComponent implements OnInit {

  pageContent: any;
  vehicleData: any;
  serviceList: any;

  constructor(
    private _activateRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    this.pageContent = this._activateRoute.snapshot.data.pageData.data;
    this.vehicleData = this.pageContent.requestDetails.vehicleData;
    this.serviceList = this.pageContent.requestDetails.requestedService;
  }

}
