import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VendorTrackDriverRoutingModule } from './vendor-track-driver-routing.module';
import { TrackDriverListComponent } from './pages/track-driver-list/track-driver-list.component';
import { SharedModule } from 'app/shared/shared.module';
import { TrackDriverDetailsComponent } from './pages/track-driver-details/track-driver-details.component';
import { AgmCoreModule } from "@agm/core";
import { AgmDirectionModule } from "agm-direction";

@NgModule({
  imports: [
    CommonModule,
    VendorTrackDriverRoutingModule,
    SharedModule,
    AgmCoreModule.forRoot({
      apiKey: "AIzaSyDHapbY7BfjFKhd2WJ4BHv6YDfWuPYhoL8"
    }),
    AgmDirectionModule
  ],
  declarations: [TrackDriverListComponent, TrackDriverDetailsComponent]
})
export class VendorTrackDriverModule { }
