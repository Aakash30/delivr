import { Component, OnInit, AfterViewInit } from '@angular/core';
import { RadioButton } from 'primeng/radiobutton';
import { ActivatedRoute } from '@angular/router';

interface cols {
  field: String;
  header: String;
}

@Component({
  selector: 'app-track-driver-list',
  templateUrl: './track-driver-list.component.html',
  styleUrls: ['./track-driver-list.component.scss']
})
export class TrackDriverListComponent implements OnInit, AfterViewInit {

  selectedValue: any;
  trackList: any[];
  showMap: boolean = false;
  cols: any[];
  selectedCar4: cols;
  order

  constructor(private route: ActivatedRoute) { }

  // orgin - Indore - center of the map when it is initialised
  public center_lat: Number = 22.719568;
  public center_lng: Number = 75.857727;

  public trip_details;


  ngOnInit() {
    this.trackList = this.route.snapshot.data.pageData.data.driverList;
  }

  markerOptions: any;
  origin: any;
  destination: any;
  renderOpts: any;

  ngAfterViewInit() {
    this.renderOpts = {
      suppressMarkers: true
    };

    this.markerOptions = {
      origin: {
        icon: 'assets/img/icons/delivr-bike marker.svg',
        opacity: 1,
        label: ''
      },

      destination: {
        icon: 'assets/img/icons/maps-and-flags.svg',
        opacity: 1,
        label: ''
      }

    }
    this.origin = {
      lat: 0,
      lng: 0
    }
    this.destination = {
      lat: 0,
      lng: 0
    }
  }
  selectCarWithButton() {

    if (this.selectedValue) {
      if (this.selectedValue.tracker[0]) {
        this.origin.lat = parseFloat(this.selectedValue.tracker[0].driver_latitude);
        this.origin.lng = parseFloat(this.selectedValue.tracker[0].driver_longitude);
        this.showMap = true;

      } else {
        this.showMap = false;
      }

      if (this.selectedValue.orders[0]) {
        this.destination.lat = parseFloat(this.selectedValue.orders[0].latitude);
        this.destination.lng = parseFloat(this.selectedValue.orders[0].longitude);

        this.order = this.selectedValue.orders[0];
        this.showMap = true;

        this.center_lat = parseFloat(this.selectedValue.orders[0].center_latitude);
        this.center_lng = parseFloat(this.selectedValue.orders[0].center_longitude);

      } else {
        this.showMap = false;
        this.order = null;
      }

    } else {
      this.showMap = false;
      this.order = null;
    }
  }

}
