import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrackDriverListComponent } from './track-driver-list.component';

describe('TrackDriverListComponent', () => {
  let component: TrackDriverListComponent;
  let fixture: ComponentFixture<TrackDriverListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrackDriverListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrackDriverListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
