import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrackDriverDetailsComponent } from './track-driver-details.component';

describe('TrackDriverDetailsComponent', () => {
  let component: TrackDriverDetailsComponent;
  let fixture: ComponentFixture<TrackDriverDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrackDriverDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrackDriverDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
