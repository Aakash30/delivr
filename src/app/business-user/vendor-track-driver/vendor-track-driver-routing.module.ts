import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TrackDriverListComponent } from './pages/track-driver-list/track-driver-list.component';
import { TrackDriverDetailsComponent } from './pages/track-driver-details/track-driver-details.component';
import { TrackDriverResolverService } from 'app/core/resolver/track-driver-resolver.service';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        component: TrackDriverListComponent,
        data: {
          sectionTitle: 'Track Driver'
        },
        resolve: {
          pageData: TrackDriverResolverService
        }
      },
      {
        path: 'details/:id',
        component: TrackDriverDetailsComponent,
        data: {
          sectionTitle: 'Details',
          buttonName: 'Create'
        }
      },
      {
        path: 'track-driver',
        component: TrackDriverListComponent,
        data: {
          sectionTitle: 'Track Driver',
          buttonName: 'Create'
        }
      },

    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VendorTrackDriverRoutingModule { }
