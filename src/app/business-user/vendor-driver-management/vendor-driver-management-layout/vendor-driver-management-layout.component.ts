import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UtilityService } from 'app/core/services/utility.service';

@Component({
  selector: 'app-vendor-driver-management-layout',
  templateUrl: './vendor-driver-management-layout.component.html',
  styleUrls: ['./vendor-driver-management-layout.component.scss']
})
export class VendorDriverManagementLayoutComponent implements OnInit {

  constructor(private utilityService: UtilityService, private router: Router
  ) {

  }

  ngOnInit() {

    let decidedUrl = "";
    if (this.router.url == "/business/driver") {
      if (this.utilityService.checkUserRoleWithRoute('driver-management', ['vendor', 'sub_ordinate'])) {
        decidedUrl = "/list";
      } else if (this.utilityService.checkUserRoleWithRoute('driver-feedback', ['vendor', 'sub_ordinate'])) {
        decidedUrl = "/feedback";
      }
      this.router.navigateByUrl(this.router.url + "" + decidedUrl);
    }


  }

}
