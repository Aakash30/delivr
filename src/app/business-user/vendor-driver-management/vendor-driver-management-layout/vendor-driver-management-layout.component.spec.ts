import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VendorDriverManagementLayoutComponent } from './vendor-driver-management-layout.component';

describe('VendorDriverManagementLayoutComponent', () => {
  let component: VendorDriverManagementLayoutComponent;
  let fixture: ComponentFixture<VendorDriverManagementLayoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VendorDriverManagementLayoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VendorDriverManagementLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
