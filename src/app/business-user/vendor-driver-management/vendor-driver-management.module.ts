import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LightboxModule } from 'ngx-lightbox';

import { VendorDriverManagementRoutingModule } from './vendor-driver-management-routing.module';
import { DriverListComponent } from './pages/driver-list/driver-list.component';
import { DriverFeedbackComponent } from './pages/driver-feedback/driver-feedback.component';
import { SharedModule } from 'app/shared/shared.module';
import { DriverFormComponent } from './pages/driver-form/driver-form.component';
import { DriverDetailsComponent } from './pages/driver-details/driver-details.component';
import { ImageViewerComponent } from './components/image-viewer/image-viewer.component';
import { DriverFeedbackDetailsComponent } from './pages/driver-feedback-details/driver-feedback-details.component';
import { VendorDriverManagementLayoutComponent } from './vendor-driver-management-layout/vendor-driver-management-layout.component';

@NgModule({
  imports: [
    CommonModule,
    VendorDriverManagementRoutingModule,
    SharedModule,
    LightboxModule
  ],
  bootstrap: [DriverListComponent],
  declarations: [DriverListComponent, DriverFeedbackComponent, DriverFormComponent, DriverDetailsComponent, ImageViewerComponent, DriverFeedbackDetailsComponent, VendorDriverManagementLayoutComponent]
})
export class VendorDriverManagementModule { }
