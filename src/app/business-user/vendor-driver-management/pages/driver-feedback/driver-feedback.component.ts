import { Component, OnInit } from '@angular/core';
import { trigger, transition, style, animate } from '@angular/animations';
import { ActivatedRoute } from '@angular/router';
import { DriverManagementService } from 'app/core/services/driver-management.service';
import { ErrorHandlerService } from 'app/core/services/error-handler.service';
import { UtilityService } from 'app/core/services/utility.service';
import { Subject, pipe } from 'rxjs';
import { takeUntil } from 'rxjs/operators';


@Component({
  selector: 'app-driver-feedback',
  templateUrl: './driver-feedback.component.html',
  styleUrls: ['./driver-feedback.component.scss'],
})
export class DriverFeedbackComponent implements OnInit {

  feedback: any[];
  total: number = 0;
  offset: number;
  keyword: string = "";
  // show filter function
  isVisible: boolean = false;

  private _unsubscribe = new Subject<Boolean>();

  constructor(private route: ActivatedRoute,
    private driverService: DriverManagementService,
    private errorHandling: ErrorHandlerService,
    public utility: UtilityService) { }

  ngOnInit() {

    const pageContent = this.route.snapshot.data.pageData.data;

    this.feedback = pageContent.driverList;
    this.total = pageContent.count;


  }

  fetchFeedback() {
    this.utility.loaderStart();
    let data: any = {};
    if (this.offset != 0) {
      data.offset = this.offset;
    }

    if (this.keyword.trim() != "") {
      data.keyword = this.keyword;
    }

    this.driverService.fetchDriverAverageRating(data).pipe(takeUntil(this._unsubscribe)).subscribe((success: any) => {
      this.feedback = success.data.driverList;
      this.total = success.data.count;
      this.utility.loaderStop();
    }, error => {
      this.utility.loaderStop();

      this.errorHandling.routeAccordingToError(error);
    })
  }

  paginate(event) {
    if (this.offset != undefined) {
      this.offset = parseInt(event.first);
      this.fetchFeedback();
    } else {
      this.offset = parseInt(event.first);
    }

  }

  searchText(event) {

    this.keyword = event.target.value;
    if (event.charCode == 13) {
      this.offset = 0;
      // on click of enter
      this.fetchFeedback();
    }
  }

  clearInput(event) {
    this.keyword = event;
    this.offset = 0;
    this.fetchFeedback();
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this._unsubscribe.next(true);
    this._unsubscribe.complete();
  }
}
