
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DriverManagementService } from 'app/core/services/driver-management.service';
import { NgxUiLoaderService } from "ngx-ui-loader";
import { MessageService } from "primeng/api";
import { HttpResponse } from "@angular/common/http";
import { UtilityService } from 'app/core/services/utility.service';
interface Status {
  name: string;
  code: string;
}

@Component({
  selector: 'app-driver-details',
  templateUrl: './driver-details.component.html',
  styleUrls: ['./driver-details.component.scss']
})
export class DriverDetailsComponent implements OnInit {

  status: Status[];
  selectedStatus: Status;
  sectionTitle: String;
  buttonName: String;
  id: number;
  imageUrl: any;

  openImageDisplay: boolean = false;

  public currentUrl: String;
  pageData: any;
  driver: {};
  vehicleData: {};

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    public utility: UtilityService
  ) {
    this.sectionTitle = route.snapshot.data["sectionTitle"];
    this.buttonName = route.snapshot.data["buttonName"];
    this.id = utility.base64Decode(route.snapshot.paramMap.get("id"));
    this.currentUrl = router.url;
  }

  async ngOnInit() {

    const pageData = this.route.snapshot.data.pageData.data;
    this.pageData = pageData;

    this.driver = pageData;
    this.driver["license_document"] = this.driver["license_document"] != "" ? JSON.parse(this.driver["license_document"]) : [];

    this.driver["passport_document"] = this.driver["passport_document"] != "" ? JSON.parse(this.driver["passport_document"]) : [];

    this.driver["tollgate_document"] = this.driver["tollgate_document"] != "" ? JSON.parse(this.driver["tollgate_document"]) : [];


    this.vehicleData = pageData.vehicles[0];
  }


  // open dialog function
  openImageDialog(type, imageObject, key) {

    this.openImageDisplay = true;
    switch (type) {
      case type = 'license':
        imageObject.forEach((item, index) => {
          if (key == index) {
            this.imageUrl = item;
          }
        });
        break;

      case type = 'passport':
        imageObject.forEach((item, index) => {
          if (key == index) {
            this.imageUrl = item;
          }
        });
        break;


      case type = 'registration_card':
        this.imageUrl = imageObject;
        break;

      case type = 'insurance':
        this.imageUrl = imageObject;
        break;

      case type = 'vehicle':
        imageObject.forEach((item, index) => {
          if (key == index) {
            this.imageUrl = item.vehicle_image;
          }
        });
        break;
    }
  }

  // close dialog function
  closeImageDialog() {
    this.openImageDisplay = false;
  }

  previousPage() {
    this.router.navigateByUrl('/business/driver-list');
  }
}
