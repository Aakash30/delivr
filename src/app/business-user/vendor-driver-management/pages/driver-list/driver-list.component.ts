//push this
import { Component, OnInit, ViewChild } from "@angular/core";
import { MenuItem, SelectItem, MessageService } from "primeng/api";
import {
  trigger,
  state,
  style,
  transition,
  animate,
  query,
  stagger,
  keyframes
} from "@angular/animations";
import { constructor } from "core-js/library/es6/object";
//import { DriverManagementService } from "app/services/driver.service";
import { DriverManagementService } from 'app/core/services/driver-management.service';
import { ActivatedRoute, Router } from "@angular/router";
import {
  FormGroup,
  FormControl,
  Validators,
  FormBuilder
} from "@angular/forms";
import { forEach } from "@angular/router/src/utils/collection";
import { UtilityService } from "app/core/services/utility.service";
import { ErrorHandlerService } from "app/core/services/error-handler.service";
import { publishLast, refCount, takeUntil } from "rxjs/operators";
import { Lightbox, LightboxConfig } from 'ngx-lightbox';
import { Subject } from "rxjs";

interface Brand {
  name: string;
  code: string;
}

interface Country {
  name: string;
  code: string;
}

interface City {
  name: string;
  code: string;
}

// require('../../../../../../node_modules')
@Component({
  selector: 'app-driver-list',
  templateUrl: './driver-list.component.html',
  styleUrls: ['./driver-list.component.scss'],
  animations: [
    trigger("openClose", [
      transition(":enter", [
        style({ transform: "translateX(20%)", opacity: 0 }),
        animate("450ms", style({ transform: "translateX(0)", opacity: 1 }))
      ]),
      transition(":leave", [
        style({ transform: "translateX(0)", opacity: 1, display: "none" }),
        animate("400ms", style({ transform: "translateX(20%)", opacity: 0 }))
      ])
    ])
  ]
})

export class DriverListComponent implements OnInit {

  public data: any = [];
  activeTab: string;
  items: MenuItem[];
  activeItem: MenuItem;
  driverList: any[];
  blockList: any[];
  availableList: any[];
  brands: Brand[];
  selectedBrand: Brand;
  countries: Country[] = [];
  selectedCountry: Country;
  cities: City[] = [];

  sectionTitle: String;
  isSubmitted: Boolean = false;
  posibleValue: string[] = ['active', 'block'];
  modelListFromAPI: any;
  modelList: any[];
  total: number;
  searchTextFlag: boolean = false;
  sharedLocationDisplay: boolean = false;
  searchValue: string = "";

  brandListFromAPI: string[];
  countryListFromAPI: string[];
  cityListFromAPI: string[];

  /** for filters */
  selectedCity: City;
  status: string = "";
  offset: number;
  keyword: string = "";

  public _album = [];

  getItem(event, index) {
    event.preventDefault();
    if (index === 0) {
      this.status = "";

    } else if (index === 1) {
      this.status = "active";

    } else if (index === 2) {
      this.status = "pending";

    } else if (index === 3) {
      this.status = "deactive";
    }
    this.offset = 0;
    this.listing();
  }

  // show filter function
  isVisible: boolean = false;

  showFilter() {
    this.isVisible = !this.isVisible;
  }

  closeFilter() {
    this.isVisible = false;
  }

  selectedValue: string = "Active";
  driverFilterForm: FormGroup;

  private _unsubscribe = new Subject<Boolean>();

  constructor(
    private route: ActivatedRoute,
    private driverManagementService: DriverManagementService,
    private messageService: MessageService,
    private router: Router,
    public utility: UtilityService,
    private errorHandling: ErrorHandlerService,
    private _lightbox: Lightbox,
    private _lightboxConfig: LightboxConfig,
  ) {
    this.sectionTitle = route.snapshot.data["sectionTitle"];
  }

  ngOnInit() {

    this.activeTab = "all";

    this.items = [
      {
        label: "All drivers"
      },
      {
        label: "Active"
      },
      {
        label: "Pending"
      },
      {
        label: "Block"
      }
    ];
    this.driverList = [];
    this.blockList = [];
    this.availableList = [];

    this.activeItem = this.items[0];

    const pageContent = this.route.snapshot.data.pageData.data;

    this.driverList = pageContent.driverList;

    this.total = pageContent.count;

    this.countryListFromAPI = pageContent.country;
    this.countries = this.utility.arrayOfObjectToConvertInDropdownFormat(this.countryListFromAPI, 'country', "country_id");

    this.cityListFromAPI = this.route.snapshot.data.pageData.data.city;

    this.driverFilterForm = new FormGroup({
      country: new FormControl(null),
      city: new FormControl(null),
    });

  }

  listing() {

    let data: any = {};
    this.utility.loaderStart();

    if (this.status != "") {
      data.status = this.status;
    }

    if (this.driverFilterForm.controls["country"].value != null && this.driverFilterForm.controls["country"].value != "") {
      data.country = this.driverFilterForm.controls["country"].value;
    }

    if (this.driverFilterForm.controls["city"].value != null && this.driverFilterForm.controls["city"].value != "") {
      data.city = this.driverFilterForm.controls["city"].value;
    }

    if (this.offset != 0) {
      data.offset = this.offset;
    }

    if (this.keyword.trim() != "") {
      data.keyword = this.keyword.trim();
    }

    this.driverManagementService.getDrivers(data)
      .subscribe(
        (success: any) => {
          this.driverList = [];
          this.driverList = success.data.driverList;

          this.total = success.data.count;

          if (this.status == "") {
            this.activeTab = "all";
          } else if (this.status == "active") {
            this.activeTab = "active";
          } else {
            this.activeTab == "block";
          }
          this.utility.loaderStop();
        },
        error => {
          this.utility.loaderStop();
          this.errorHandling.routeAccordingToError(error)
        }
      );
  }

  openLocation(rowDriver) {
    this._album = [];
    if (rowDriver.length > 0) {
      rowDriver.forEach(element => {
        this._album.push({ src: element.image, caption: element.address });
      });
      this._lightbox.open(this._album, 0, { wrapAround: true, showImageNumberLabel: true });
    } else {
      this.utility.showError("error", "Error", "Driver has not shared any location.");
    }

  }


  close(): void {
    this._album = [];
    this._lightbox.close();

  }

  /** on basis of country get cities */
  filterCountry(event) {

    if (this.driverFilterForm.controls["country"].value != "" && this.driverFilterForm.controls["country"].value != null) {
      let countryId = this.driverFilterForm.controls["country"].value;

      let cityData = this.cityListFromAPI.filter((cityData: any) => {
        return cityData.country_id == countryId;

      });
      this.cities = this.utility.arrayOfObjectToConvertInDropdownFormat(cityData, 'city', 'city_id');
    } else {
      this.cities = [];
    }
    this.driverFilterForm.controls["city"].setValue(null);
  }


  onSubmit() {
    this.isSubmitted = true;
    this.listing();
    this.isVisible = false;
  }

  /** reset form filter */
  clearFilter() {
    if (this.driverFilterForm.dirty && this.driverFilterForm.touched) {
      this.offset = 0;
      this.driverFilterForm.reset();
      this.listing();
    } else {
      this.driverFilterForm.reset();
    }
    this.isVisible = false;
  }

  actionOnDriver(status, id) {
    this.utility.loaderStart();
    this.driverManagementService.activeOrBlockDriver(id, status).subscribe((success: any) => {
      this.messageService.add({
        severity: 'success',
        summary: "Success",
        detail: success.message
      });

      this.utility.loaderStop();
      // setTimeout(() => {
      //   this.listing();
      // }, 200);
    }, error => {
      this.utility.loaderStop();

      this.errorHandling.routeAccordingToError(error);
    });
  }

  paginate(event) {
    if (this.offset != undefined) {
      this.offset = parseInt(event.first);
      this.listing();
    } else {
      this.offset = parseInt(event.first);
    }

  }

  searchText(event) {

    this.keyword = event.target.value;
    if (event.charCode == 13) {
      this.offset = 0;
      // on click of enter
      this.listing();
    }
  }



  openSharedDialog() {
    this.sharedLocationDisplay = true;
  }

  closeSharedDialog() {
    this.sharedLocationDisplay = false;
  }

  clearInput(event) {
    this.keyword = event;
    this.offset = 0;
    this.listing();
  }

  resendOTP(driver) {
    this.utility.loaderStart();
    let data = { user_id: driver.user_id, 'user_type': "driver", "mobile_number": driver.mobile_number, name: driver.driver };
    this.driverManagementService.resendOTP(data).pipe(takeUntil(this._unsubscribe)).subscribe((success: any) => {
      this.messageService.add({
        severity: 'success',
        summary: "Success",
        detail: "OTP sent successfully"
      });
      this.utility.loaderStop();
    }, error => {
      this.utility.loaderStop();

      this.errorHandling.routeAccordingToError(error);
    })
  }

  ngOnDestroy() {
    this._unsubscribe.next(true);
    this._unsubscribe.complete();
  }

}
