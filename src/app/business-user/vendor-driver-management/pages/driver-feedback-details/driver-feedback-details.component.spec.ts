import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DriverFeedbackDetailsComponent } from './driver-feedback-details.component';

describe('DriverFeedbackDetailsComponent', () => {
  let component: DriverFeedbackDetailsComponent;
  let fixture: ComponentFixture<DriverFeedbackDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DriverFeedbackDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DriverFeedbackDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
