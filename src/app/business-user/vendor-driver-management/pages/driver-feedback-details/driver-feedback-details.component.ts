import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DriverManagementService } from 'app/core/services/driver-management.service';
import { UtilityService } from 'app/core/services/utility.service';
import { ErrorHandlerService } from 'app/core/services/error-handler.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-driver-feedback-details',
  templateUrl: './driver-feedback-details.component.html',
  styleUrls: ['./driver-feedback-details.component.scss']
})
export class DriverFeedbackDetailsComponent implements OnInit {

  feedbackReview: any[];
  total: number = 0;
  offset: number;
  keyword: string = "";
  id;
  driverName: string = "";
  sectionTitle: string;


  // show filter function
  isVisible: boolean = false;

  showFilter() {
    this.isVisible = !this.isVisible;

  }

  closeFilter() {
    this.isVisible = false;
  }

  private _unsubscribe = new Subject<Boolean>();

  constructor(private route: ActivatedRoute, private driverService: DriverManagementService, private utility: UtilityService, private errorHandling: ErrorHandlerService) { }

  ngOnInit() {

    this.id = this.utility.base64Decode(this.route.snapshot.params.id);

    const pageContent = this.route.snapshot.data.pageData.data;

    this.feedbackReview = pageContent.orderReviewData;
    this.total = pageContent.count;
    this.driverName = pageContent.driverListData.driver;
    this.sectionTitle = pageContent.driverListData.driver + "'s " + this.route.snapshot.data['sectionTitle']
  }

  fetchFeedbackDetails() {
    this.utility.loaderStart();
    let data = {};
    if (this.offset != 0) {
      data["offset"] = this.offset;
    }

    if (this.keyword.trim() != "") {
      data["keyword"] = this.keyword;
    }
    this.driverService.fetchDriverReviews(this.id, data).pipe(takeUntil(this._unsubscribe)).subscribe((success: any) => {
      this.feedbackReview = success.data.orderReviewData;
      this.total = success.data.count;
      this.utility.loaderStop();

    }, error => {
      this.errorHandling.routeAccordingToError(error);
      this.utility.loaderStop();

    });
  }

  paginate(event) {
    if (this.offset != undefined) {
      this.offset = parseInt(event.first);
      this.fetchFeedbackDetails();
    } else {
      this.offset = parseInt(event.first);
    }

  }

  searchText(event) {

    this.keyword = event.target.value;
    if (event.charCode == 13) {
      this.offset = 0;
      // on click of enter
      this.fetchFeedbackDetails();
    }
  }


  clearInput(event) {
    this.keyword = event;
    this.offset = 0;
    this.fetchFeedbackDetails();
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this._unsubscribe.next(true);
    this._unsubscribe.complete();
  }

}
