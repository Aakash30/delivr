import { Component, OnInit, ɵConsole, ViewChild, HostListener } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import {
  validateAllFormFields, noWhitespaceValidator, blankSpaceInputNotValid
} from '../../../../shared/utils/custom-validators';
import { DriverManagementService } from 'app/core/services/driver-management.service';
import { FileUploadService } from 'app/core/services/file-upload.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { finalize, map, takeUntil } from 'rxjs/operators';
import { MessageService } from 'primeng/api';
import { element } from '@angular/core/src/render3';
import { UtilityService } from 'app/core/services/utility.service';
import { FileUpload } from 'primeng/fileupload';
import { ConfirmationService } from 'primeng/api';
import { ThrowStmt } from '@angular/compiler';
import { ErrorHandlerService } from 'app/core/services/error-handler.service';
import { Subject } from 'rxjs';
import { Dropdown } from 'primeng/dropdown';



interface CountryData {
  label: string;
  value: string;
}

interface City {
  label: string;
  value: string;
}


@Component({
  selector: 'app-driver-form',
  templateUrl: './driver-form.component.html',
  styleUrls: ['./driver-form.component.scss']
})


export class DriverFormComponent implements OnInit {
  countries: CountryData[];
  selectedCountry: CountryData;
  cities: City[];
  selectedCity: City;
  businessBranch: any;
  businessBranchFromAPI: [];
  public isSubmitted: Boolean = false;
  imageDisplayFlag: boolean = false;
  sectionTitle: string;
  buttonName: string;
  registrationNumber: string = '';
  vehicleList: any;
  driverForm: FormGroup;
  mapVehicle;
  selectedFile: any[] = [];
  driverId;
  countrycode;
  country;
  file: File;
  countryListFromAPI: string[];
  cityListFromAPI: string[];
  uploadedFiles: any[] = [];
  driverDetails;
  imageLicenseObject: any[] = [];
  imagePassportObject = [];
  imageTollgateObject = [];
  imageEmirateIdObject: any[] = [];
  imageObject = [];

  imageUrl: any;

  private _unsubscribe = new Subject<Boolean>();

  @ViewChild('branch') branch: Dropdown;
  @ViewChild('countryId') countryId: Dropdown;
  @ViewChild('cityId') cityId: Dropdown;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private location: Location,
    private driverManagementService: DriverManagementService,
    private loader: NgxUiLoaderService,
    private messageService: MessageService,
    private uploader: FileUploadService,
    public utility: UtilityService,
    private confirmationService: ConfirmationService,
    private errorHandling: ErrorHandlerService
  ) {
    this.sectionTitle = route.snapshot.data['sectionTitle'];
    this.buttonName = route.snapshot.data['buttonName'];
  }


  ngOnInit() {
    this.countrycode = '+';
    this.driverId = this.route.snapshot.paramMap.get("id");

    this.driverForm = new FormGroup({
      first_name: new FormControl('', [Validators.required, noWhitespaceValidator]),
      last_name: new FormControl('', [Validators.required, noWhitespaceValidator]),
      email_id: new FormControl('', [Validators.email]),
      mobile_number: new FormControl('', [
        Validators.required,
        // Validators.pattern(/^[0-9]\d{0,9}$/), 
        noWhitespaceValidator
      ]),
      country: new FormControl('', [Validators.required]),
      city: new FormControl('', [Validators.required]),
      business_branch_id: new FormControl('', [Validators.required]),
      address_line_1: new FormControl('', [Validators.required, noWhitespaceValidator]),
      address_line_2: new FormControl('', [Validators.required, noWhitespaceValidator]),
      license_document: new FormControl(''),
      passport_document: new FormControl(''),
      tollgate_document: new FormControl(''),
      emirates_id: new FormControl(''),
      register: new FormControl('', [Validators.required])
    });
    this.setFormValue();


  }

  setFormValue() {

    this.countryListFromAPI = this.route.snapshot.data.pageData.data.countryList;
    this.cityListFromAPI = this.route.snapshot.data.pageData.data.cityList;

    this.businessBranchFromAPI = this.route.snapshot.data.pageData.data.businessArea;
    this.driverDetails = this.route.snapshot.data.pageData.data.driver;

    if (this.countryListFromAPI.length > 0) {
      this.countries = this.utility.arrayOfObjectToConvertInDropdownFormat(this.countryListFromAPI, "country", "country_id", "status", "inActive");
    }

    if (this.cityListFromAPI.length > 0) {
      this.cities = [];
    }

    if (this.businessBranchFromAPI.length > 0) {
      this.businessBranch = this.utility.arrayOfObjectToConvertInDropdownFormat(this.businessBranchFromAPI, "address_branch_name", "branch_area_id", "status", "deactive");
    }

    if (this.driverDetails) {
      this.mapVehicle = this.driverDetails.vehicles[0].vehicle;
      this.mapVehicle.map_id = this.driverDetails.vehicles[0].map_id;

      let mobileNumber = this.driverDetails.mobile_number.split('-');
      this.countrycode = mobileNumber[0];

      this.filterOnCountry(this.driverDetails.country);

      this.driverDetails.mobile_number = mobileNumber[1];

      this.imageLicenseObject = this.driverDetails.license_document != "" ? JSON.parse(this.driverDetails.license_document) : [];
      this.imagePassportObject = this.driverDetails.passport_document != "" ? JSON.parse(this.driverDetails.passport_document) : [];
      this.imageTollgateObject = this.driverDetails.tollgate_document != "" ? JSON.parse(this.driverDetails.tollgate_document) : [];
      this.imageEmirateIdObject = this.driverDetails.emirates_id != "" ? JSON.parse(this.driverDetails.emirates_id) : [];

      this.registrationNumber = this.driverDetails.vehicles[0].vehicle.vehicle_number;

      this.driverDetails.register = this.driverDetails.vehicles[0].vehicle.vehicle_model.model_name;
      this.driverDetails.license_document = this.driverDetails.license_document != "" ? JSON.parse(this.driverDetails.license_document) : "";

      this.driverDetails.passport_document = this.driverDetails.passport_document != "" ? JSON.parse(this.driverDetails.passport_document) : "";

      this.driverDetails.tollgate_document = this.driverDetails.tollgate_document != "" ? JSON.parse(this.driverDetails.tollgate_document) : "";
      this.driverDetails.emirates_id = this.driverDetails.emirates_id != "" ? JSON.parse(this.driverDetails.emirates_id) : "";

      this.driverForm.patchValue(this.driverDetails)
    }
  }

  /** submits form and add driver in the system */
  onSubmit() {
    this.isSubmitted = true;

    if (this.driverForm.valid) {

      if (this.driverForm.dirty && this.driverForm.touched) {
        let data = Object.assign({}, this.driverForm.value);
        data["vehicles"] = {};
        data["vehicles"].map_id = this.mapVehicle.map_id;

        if (this.driverId) {
          data.user_id = this.utility.base64Decode(this.driverId);
        }
        data.mobile_number = this.countrycode + "-" + data.mobile_number;

        if (data.license_document != "") {
          data.license_document = JSON.stringify(this.driverForm.value.license_document);
        }

        if (data.passport_document != "") {
          data.passport_document = JSON.stringify(this.driverForm.value.passport_document);
        }

        if (data.tollgate_document != "") {
          data.tollgate_document = JSON.stringify(this.driverForm.value.tollgate_document);
        }

        if (data.emirates_id != "") {
          data.emirates_id = JSON.stringify(this.driverForm.value.emirates_id);
        }

        /**
         * call server to update data
         */
        this.driverManagementService.addDriver(data).pipe(takeUntil(this._unsubscribe)).subscribe(
          (success: any) => {

            this.messageService.add({
              severity: 'success',
              summary: 'Addition Success',
              detail: success.message
            });
            this.loader.stop();
            this.router.navigateByUrl("/business/driver-list");
          },
          error => {
            this.errorHandling.routeAccordingToError(error);
            this.loader.stop();
          }
        );

        this.isSubmitted = false;
      } else {
        this.router.navigateByUrl("/business/driver-list")
      }


    } else {
      validateAllFormFields(this.driverForm);
      this.utility.scrollToError();
    }
  }
  /**
   * changes on branch - filters country and city based on branch
   * @param $event 
   */
  filterBranch($event) {
    let branch_id = this.driverForm.value.business_branch_id;
    if (branch_id) {
      let selectedBranch = this.businessBranchFromAPI.filter((branch: any) => {
        return branch.branch_area_id == branch_id
      });
      this.filterOnCountry(selectedBranch[0]['country']);
      this.driverForm.controls.country.setValue(selectedBranch[0]['country']);
      this.driverForm.controls.city.setValue(selectedBranch[0]['city']);
    } else {
      this.countries = this.utility.arrayOfObjectToConvertInDropdownFormat(this.countryListFromAPI, "country", "country_id", "status", "inActive");
      this.filterOnCountry();
    }

  }

  filterOnCountry(country?) {
    let countryId = country ? country : this.driverForm.value.country;
    if (countryId != null) {
      let selectedCountry: any = this.countryListFromAPI.filter((countryData: any) => {
        return countryData.country_id == countryId;
      });
      let selectedCity = this.cityListFromAPI.filter((cityData: any) => {
        return cityData.country_id == countryId;

      });

      this.cities = this.utility.arrayOfObjectToConvertInDropdownFormat(selectedCity, "city", "city_id", "status", "inActive")
      this.countrycode = selectedCountry[0].country_code;
    } else {
      this.countrycode = '+';
      this.cities = [];
    }

  }

  removeFile(file: File, uploader: FileUpload, key) {
    const index = uploader.files.indexOf(file);
    uploader.remove(null, index);
    this.driverForm.controls[key].setValue('');
  }

  uploadImage(event, key) {
    if (event.target.attributes.limit != undefined) {

      let limit = event.target.attributes.limit.value;
      if (Array.from(event.target.files).length > limit) {

        this.driverForm["controls"][key].setErrors({ "limit": true })
        return false;
      } else {
        this.driverForm.controls[key].setErrors(null)
      }
    }

    var fileType = event.target.attributes.accept.value == "application/pdf" ? /application\/pdf/ : /image\/png|image\/jpeg|image\/jpg/;

    let maxsize = event.target.attributes.maxsize.value;

    let errorType = false;
    let errorSize = false;
    let fileArray = event.target.files;
    Array.from(fileArray).forEach(element => {
      if (!element["type"].match(fileType)) {
        errorType = true;
      }
      if (maxsize < element["size"]) {
        //allow 2 mb
        errorSize = true;
      }
    });

    if (errorType) {
      this.driverForm.controls[key].setErrors({ "pattern": true });
      return false;

    } else {
      this.driverForm.controls[key].setErrors(null);
    }

    if (errorSize) {
      this.driverForm.controls[key].setErrors({ "size": true });
      return false;

    } else {
      this.driverForm.controls[key].setErrors(null);
    }

    this.uploader.fileUploadImages(Array.from(event.target.files)).pipe(takeUntil(this._unsubscribe)).subscribe((success: any) => {
      if (key == 'license_document') {
        success.image.forEach(element => {
          this.imageLicenseObject.push(element.url);
          if (this.imageLicenseObject.length > 2) {

            this.imageLicenseObject.pop();
            this.imageLicenseObject.push(element.url);
          }

        });

        this.driverForm.controls[key].setValue(this.imageLicenseObject);

      } if (key == 'passport_document') {
        success.image.forEach(element => {
          this.imagePassportObject.push(element.url);
        });

        this.driverForm.controls[key].setValue(this.imagePassportObject);

      }
      if (key == 'tollgate_document') {
        success.image.forEach(element => {
          this.imageTollgateObject.push(element.url);
        });
        this.driverForm.controls[key].setValue(this.imageTollgateObject);


      }

      if (key == 'emirates_id') {
        success.image.forEach(element => {
          this.imageEmirateIdObject.push(element.url);
        });
        this.driverForm.controls[key].setValue(this.imageEmirateIdObject);
      }
      this.driverForm.controls[key].markAsDirty();
      this.driverForm.controls[key].markAsTouched();

      this.messageService.add({
        severity: "success",
        summary: "Files added",
        detail: ""
      });

    });

  }

  // delete function call
  delete(index, key, url) {

    if (key == 'license_document') {
      this.imageLicenseObject.splice(index, 1);

      if (this.imageLicenseObject.length != 0)
        this.driverForm.controls[key].setValue(this.imageLicenseObject);
      else
        this.driverForm.controls[key].setValue("");
    }
    if (key == 'passport_document') {
      this.imagePassportObject.splice(index, 1);

      if (this.imagePassportObject.length != 0)
        this.driverForm.controls[key].setValue(this.imagePassportObject);
      else
        this.driverForm.controls[key].setValue("");

    }
    if (key == 'tollgate_document') {
      this.imageTollgateObject.splice(index, 1);
      if (this.imageTollgateObject.length != 0)
        this.driverForm.controls[key].setValue(this.imageTollgateObject);
      else
        this.driverForm.controls[key].setValue("");
    }

    if (key == 'emirates_id') {
      this.imageEmirateIdObject.splice(index, 1);
      if (this.imageEmirateIdObject.length != 0)
        this.driverForm.controls[key].setValue(this.imageEmirateIdObject);
      else
        this.driverForm.controls[key].setValue("");
    }

    this.driverForm.controls[key].markAsDirty();
    this.driverForm.controls[key].markAsTouched();
  }


  onUpload(event) {
    for (let file of event.files) {
      this.uploadedFiles.push(file);
    }
    this.messageService.add({ severity: 'info', summary: 'File Uploaded', detail: '' });
  }


  searchVehicles(event) {
    if (this.driverForm.controls["register"].value.trim() != "") {
      this.driverManagementService.fetchAvailableVehicle(this.driverForm.controls["register"].value.trim()).pipe(takeUntil(this._unsubscribe)).subscribe(
        (success: any) => {
          this.vehicleList = success.data.vehicleList;
        });
    } else {
      this.vehicleList = [];
      this.registrationNumber = "";
    }

  }

  chooseVehicle(vehicle) {

    this.driverForm.controls["register"].setValue(vehicle.vehicle_model.model_name)
    this.mapVehicle = vehicle;
    this.registrationNumber = vehicle.vehicle_number;
    this.vehicleList = [];
  }

  resetForm() {

    if (this.driverForm.dirty && this.driverForm.touched) {

      this.confirmationService.confirm({
        message: 'You have some unsaved changes.',
        header: 'Confirmation',
        reject: () => {
          this.location.back();
        },
        rejectLabel: "Discard Changes",
        accept: () => {
          this.onSubmit();
        },
        acceptLabel: "Save & Close"
      });
    } else {
      this.location.back();
    }
  }

  get getDriverFormControl() {
    return this.driverForm.controls;
  }

  openImageDialog(type, imageObject, key) {

    this.imageDisplayFlag = true;

    switch (type) {
      case type = 'license_document':
        imageObject.forEach((item, index) => {
          if (key === index) {
            this.imageUrl = item;
          }
        })
        break;

      case type = 'passport_document':
        imageObject.forEach((item, index) => {
          if (key === index) {
            this.imageUrl = item;
          }
        })
        break;

      case type = 'insurance':
        this.imageUrl = imageObject;
        break;

      case type = 'registration_card':
        this.imageUrl = imageObject;
        break;

      case type = 'vehicle_image':
        imageObject.forEach((item, index) => {
          if (key == index) {
            this.imageUrl = item.vehicle_image;
          }
        });
        break;

      case type = 'emirates':
        imageObject.forEach((item, index) => {
          if (key == index) {
            this.imageUrl = item;
          }
        });
        break;
    }

  }

  closeImageDaialog() {
    this.imageDisplayFlag = false;
  }


  ngOnDestroy() {
    this._unsubscribe.next(true);
    this._unsubscribe.complete();
  }
}

