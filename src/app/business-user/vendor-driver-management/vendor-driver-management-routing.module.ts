import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DriverListComponent } from './pages/driver-list/driver-list.component';
import { DriverFeedbackComponent } from './pages/driver-feedback/driver-feedback.component';
import { DriverFormComponent } from './pages/driver-form/driver-form.component';
import { DriverDetailsComponent } from './pages/driver-details/driver-details.component';
import { DriverManagementResolverService } from 'app/core/resolver/driver-management-resolver.service';
import { DriverFeedbackDetailsComponent } from './pages/driver-feedback-details/driver-feedback-details.component';
import { VendorDriverManagementLayoutComponent } from './vendor-driver-management-layout/vendor-driver-management-layout.component';
import { AuthGuardService as AuthGuard } from 'app/core/guards/auth-guard.service';
import { RolesGuardService as RoleGuard } from 'app/core/guards/roles-guard.service';

const routes: Routes = [
  {
    path: '',
    component: VendorDriverManagementLayoutComponent,
    children: [
      {
        path: 'driver-list',
        component: DriverListComponent,
        canActivate: [AuthGuard, RoleGuard],
        data: {
          sectionTitle: 'Driver List',
          buttonName: 'Add Driver',
          onlyForUser: ['vendor', 'sub_ordinate'],
          expectedRole: ['driver-management']
        },
        resolve: {
          pageData: DriverManagementResolverService
        }
      },
      {
        path: 'driver-list/add',
        component: DriverFormComponent,
        canActivate: [AuthGuard, RoleGuard],
        data: {
          sectionTitle: 'Add Driver',
          buttonName: 'Add Driver',
          onlyForUser: ['vendor', 'sub_ordinate'],
          expectedRole: ['driver-management']
        },
        resolve: {
          pageData: DriverManagementResolverService
        }
      },
      {
        path: 'driver-list/edit/:id',
        component: DriverFormComponent,
        canActivate: [AuthGuard, RoleGuard],
        data: {
          sectionTitle: 'Edit Driver',
          buttonName: 'Save',
          onlyForUser: ['vendor', 'sub_ordinate'],
          expectedRole: ['driver-management']
        },
        resolve: {
          pageData: DriverManagementResolverService
        }
      },
      {
        path: 'driver-list/details/:id',
        component: DriverDetailsComponent,
        canActivate: [AuthGuard, RoleGuard],
        data: {
          sectionTitle: 'Driver Details',
          buttonName: 'Add Order',
          onlyForUser: ['vendor', 'sub_ordinate'],
          expectedRole: ['driver-management']
        },
        resolve: {
          pageData: DriverManagementResolverService
        }
      },
      {
        path: 'driver-feedback',
        component: DriverFeedbackComponent,
        canActivate: [AuthGuard, RoleGuard],
        data: {
          sectionTitle: 'Driver Feedback',
          onlyForUser: ['vendor', 'sub_ordinate'],
          expectedRole: ['driver-feedback']
        }, resolve: {
          pageData: DriverManagementResolverService
        }
      },
      {
        path: 'driver-feedback/details/:id',
        component: DriverFeedbackDetailsComponent,
        canActivate: [AuthGuard, RoleGuard],
        data: {
          sectionTitle: 'Feedback',
          onlyForUser: ['vendor', 'sub_ordinate'],
          expectedRole: ['driver-feedback']
        }, resolve: {
          pageData: DriverManagementResolverService
        }
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VendorDriverManagementRoutingModule { }
