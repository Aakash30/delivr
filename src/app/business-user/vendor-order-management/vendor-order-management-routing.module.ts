import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OrderManagementListComponent } from './pages/order-management-list/order-management-list.component';
import { OrderManagementFormComponent } from './pages/order-management-form/order-management-form.component';
import { OrderManagementDetailsComponent } from './pages/order-management-details/order-management-details.component';
import { OrdersResolverService } from 'app/core/resolver/orders-resolver.service';
import { AssignDriverComponent } from './pages/assign-driver/assign-driver.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        component: OrderManagementListComponent,
        data: {
          sectionTitle: 'Order Management'
        },
        resolve: {
          pageData: OrdersResolverService
        }
      },
      {
        path: 'details/:id',
        component: OrderManagementDetailsComponent,
        data: {
          sectionTitle: 'Details',
          buttonName: 'Create'
        },
        resolve: {
          pageData: OrdersResolverService
        }
      },
      {
        path: 'edit/:id',
        component: OrderManagementFormComponent,
        data: {
          sectionTitle: 'Edit Order',
          buttonName: 'Save'
        },
        resolve: {
          pageData: OrdersResolverService
        }
      },
      {
        path: 'add',
        component: OrderManagementFormComponent,
        data: {
          sectionTitle: 'Create Order',
          buttonName: 'Create Order'
        },
        resolve: {
          pageData: OrdersResolverService
        }
      },
      {
        path: 'assign-driver/:id',
        component: AssignDriverComponent,
        // data: {
        //   sectionTitle: 'Create Order',
        //   buttonName: 'Create Order'
        // },
        resolve: {
          pageData: OrdersResolverService
        }
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VendorOrderManagementRoutingModule { }
