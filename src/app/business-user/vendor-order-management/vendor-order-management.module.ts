import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VendorOrderManagementRoutingModule } from './vendor-order-management-routing.module';
import { OrderManagementListComponent } from './pages/order-management-list/order-management-list.component';
import { SharedModule } from 'app/shared/shared.module';
import { OrderManagementDetailsComponent } from './pages/order-management-details/order-management-details.component';
import { OrderManagementFormComponent } from './pages/order-management-form/order-management-form.component';
import { AgmCoreModule, GoogleMapsAPIWrapper } from '@agm/core';
import { AssignDriverComponent } from './pages/assign-driver/assign-driver.component';
import { AgmJsMarkerClustererModule } from "@agm/js-marker-clusterer";

@NgModule({
  imports: [
    CommonModule,
    VendorOrderManagementRoutingModule,
    SharedModule,
    AgmCoreModule.forRoot({
      apiKey: "AIzaSyDHapbY7BfjFKhd2WJ4BHv6YDfWuPYhoL8", //AIzaSyD5g_QO7b1pBfgl9OtX6vchzZwX1Vmxo2Y
      libraries: ['places']
    }),
    AgmJsMarkerClustererModule
  ],
  declarations: [OrderManagementListComponent, OrderManagementDetailsComponent, OrderManagementFormComponent, AssignDriverComponent],
  providers: [
    GoogleMapsAPIWrapper
  ]
})
export class VendorOrderManagementModule { }
