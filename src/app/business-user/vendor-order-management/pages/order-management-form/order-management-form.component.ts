import {
  Component,
  OnInit,
  NgZone,
  ElementRef,
  ViewChild,
} from "@angular/core";
import { Location } from "@angular/common";
import { ActivatedRoute, Router } from "@angular/router";
import {
  FormGroup,
  FormControl,
  Validators,
  AbstractControl,
  FormBuilder,
  FormArray,
} from "@angular/forms";
import {
  validateAllFormFields,
  noWhitespaceValidator,
} from "app/shared/utils/custom-validators";
import { UtilityService } from "app/core/services/utility.service";
import { MapsAPILoader, MouseEvent } from "@agm/core";
import { MessageService, ConfirmationService } from "primeng/api";
import { OrderManagementService } from "app/core/services/order-management.service";
import { NgxUiLoaderService } from "ngx-ui-loader";
import { CustomValidators } from "ng5-validation";
import { ErrorHandlerService } from "app/core/services/error-handler.service";

declare var google: any;

interface Channel {
  name: string;
  code: string;
}

interface Country {
  name: string;
  code: string;
}

interface City {
  name: string;
  code: string;
}

interface Payment {
  name: string;
  code: string;
}

interface Branch {
  name: string;
  code: string;
}
interface Marker {
  lat: number;
  lng: number;
  label?: string;
  draggable: boolean;
}
@Component({
  selector: "app-order-management-form",
  templateUrl: "./order-management-form.component.html",
  styleUrls: ["./order-management-form.component.scss"],
})
export class OrderManagementFormComponent implements OnInit {
  formBuilder: FormBuilder = new FormBuilder();

  order_date: Date;
  minDate: Date;
  channels: Channel[];
  selectedChannel: Channel;
  payment: Payment[];
  selectedPayment: Payment;
  countries: Country[];
  selectedCountry: Country;
  cities: City[] = [];
  selectedCity: City;
  businessBranchList: Branch[];
  selectedBranch: Branch;
  maxDate: Date;
  sectionTitle: String;
  buttonName: String;
  today: Date;
  countrycode: string = "+";
  currency = "";
  isSubmitted: Boolean = false;
  inputTotal: number;
  orderData: any;
  order_id;

  orderManagementForm;

  channelListFromAPI: string[];
  countryListFromAPI: string[];
  stateListFromAPI: string[];
  cityListFromAPI: string[];
  paymentListFromAPI: string[];
  businesBranchListFromAPI: string[];

  latitude: number = 0;
  longitude: number = 0;
  zoom: number = 12;
  paceChange;
  OrderItem: FormArray;

  @ViewChild("address") searchElementRef: ElementRef;

  private geoCoder;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private location: Location,
    public utility: UtilityService,
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone,
    private messageService: MessageService,
    private orderService: OrderManagementService,
    private loader: NgxUiLoaderService,
    private confirmationService: ConfirmationService,
    private errorHandling: ErrorHandlerService
  ) {
    this.sectionTitle = route.snapshot.data["sectionTitle"];
    this.buttonName = route.snapshot.data["buttonName"];
    if (route.snapshot.paramMap.get("id")) {
      this.order_id = utility.base64Decode(route.snapshot.paramMap.get("id"));
    }
  }

  initItemsForm() {
    // initialize our address
    return this.formBuilder.group({
      item_id: new FormControl(null),
      item_name: new FormControl("", Validators.required),
      quantity: new FormControl("", [
        Validators.required,
        Validators.pattern(/^[0-9]\d{0,5}$/),
      ]),
      price: new FormControl("", [Validators.required]),
    });
  }

  ngOnInit() {
    this.today = new Date();

    const pageData = this.route.snapshot.data.pageData.data;

    this.orderData = pageData.orders;
    this.countryListFromAPI = pageData.country;

    this.countries = this.utility.arrayOfObjectToConvertInDropdownFormat(
      this.countryListFromAPI,
      "country",
      "country_id",
      "status",
      "inActive"
    );

    this.cityListFromAPI = pageData.city;

    this.channelListFromAPI = pageData.channelList;
    this.channels = this.utility.arrayOfStringsToArrayOfObjects(
      this.channelListFromAPI
    );

    this.paymentListFromAPI = pageData.paymentMethods;
    this.payment = this.utility.arrayOfStringsToArrayOfObjects(
      this.paymentListFromAPI
    );

    this.businesBranchListFromAPI = pageData.businessBranchList;
    this.businessBranchList = this.utility.arrayOfObjectToConvertInDropdownFormat(
      this.businesBranchListFromAPI,
      "address_branch_name",
      "branch_area_id",
      "status",
      "deactive"
    );

    this.orderManagementForm = this.formBuilder.group({
      order_date: new FormControl("", Validators.required),
      channel: new FormControl("", Validators.required),
      first_name: new FormControl("", Validators.required),
      last_name: new FormControl("", Validators.required),
      email: new FormControl("", Validators.required),
      mobile_number: new FormControl("", [
        Validators.required,
        noWhitespaceValidator,
      ]),
      country: new FormControl("", Validators.required),
      city: new FormControl("", Validators.required),
      address_1: new FormControl("", Validators.required),
      address_2: new FormControl("", Validators.required),
      postal_code: new FormControl("", [
        Validators.required,
        noWhitespaceValidator,
        // Validators.pattern(/^[0-9]\d{5}$/)
      ]),
      payment_method: new FormControl("", Validators.required),
      subtotal: new FormControl("0.00", [Validators.required]),
      business_branch_id: new FormControl("", Validators.required),
      OrderItem: this.formBuilder.array([]),
    });

    /** if form is in edit mode */
    if (this.orderData) {
      let mobileNumber = this.orderData.mobile_number.split("-");
      this.countrycode = mobileNumber[0];
      this.orderData.mobile_number = mobileNumber[1];
      this.order_date = new Date(this.orderData.order_date);
      this.orderData.order_date = this.order_date;

      this.orderData.OrderItem.forEach((order) => {
        this.OrderItem = this.orderManagementForm.get("OrderItem") as FormArray;
        this.OrderItem.push(this.initItemsForm());
      });

      this.orderManagementForm.patchValue(this.orderData);

      // this.changeCountry();
      let selectedCountry: any = this.countryListFromAPI.filter(
        (countryData: any) => {
          return countryData.country_id == this.orderData.country;
        }
      );

      let alphaCode = selectedCountry[0].alpha_code;
      this.currency = selectedCountry[0].currency;
      this.reloadMap(alphaCode);
      this.latitude = parseFloat(this.orderData.latitude);
      this.longitude = parseFloat(this.orderData.longitude);
    } else {
      this.OrderItem = this.orderManagementForm.get("OrderItem") as FormArray;
      this.OrderItem.push(this.initItemsForm());
      this.reloadMap();
    }
  }

  /** filters on changing branch */
  changeBranch() {
    let branch_id = this.orderManagementForm.value.business_branch_id;

    if (branch_id != null) {
      let selectedBranch = this.businesBranchListFromAPI.filter(
        (branch: any) => {
          return branch.branch_area_id == branch_id;
        }
      );

      this.orderManagementForm.controls.country.setValue(
        selectedBranch[0]["country"]
      );
      this.orderManagementForm.controls.city.setValue(
        selectedBranch[0]["city"]
      );
    } else {
      this.orderManagementForm.controls.country.setValue(null);
      this.orderManagementForm.controls.city.setValue(null);
    }
    this.changeCountry();
  }

  /** filters on changing country */

  changeCountry() {
    let countryId = this.orderManagementForm.value.country;

    this.orderManagementForm.controls.address_2.setValue("");

    if (countryId != null) {
      let selectedCountry: any = this.countryListFromAPI.filter(
        (countryData: any) => {
          return countryData.country_id == countryId;
        }
      );
      let selectedCity = this.cityListFromAPI.filter((cityData: any) => {
        return cityData.country_id == countryId;
      });
      this.cities = this.utility.arrayOfObjectToConvertInDropdownFormat(
        selectedCity,
        "city",
        "city_id",
        "status",
        "inActive"
      );
      this.countrycode = selectedCountry[0].country_code;
      let alphaCode = selectedCountry[0].alpha_code;
      this.currency = selectedCountry[0].currency;
      this.reloadMap(alphaCode);
    } else {
      this.cities = [];
      this.reloadMap();
    }
  }

  reloadMap(alphaCode?) {
    if (alphaCode) {
      this.mapsAPILoader.load().then(() => {
        this.geoCoder = new google.maps.Geocoder();
        const autocomplete = new google.maps.places.Autocomplete(
          this.searchElementRef.nativeElement,
          {
            // types: ["address"],
            componentRestrictions: { country: alphaCode },
          }
        );
        this.paceChange = autocomplete.addListener("place_changed", () => {
          this.ngZone.run(() => {
            //get the place result
            let place: google.maps.places.PlaceResult = autocomplete.getPlace();

            //verify result
            if (place.geometry === undefined || place.geometry === null) {
              return;
            }

            //set latitude, longitude and zoom
            this.latitude = place.geometry.location.lat();
            this.longitude = place.geometry.location.lng();
            this.orderManagementForm.controls.address_2.setValue(
              place.formatted_address
            );
            this.zoom = 12;
          });
        });
      });
    } else {
      this.mapsAPILoader.load().then(() => {
        this.geoCoder = new google.maps.Geocoder();
        const autocomplete = new google.maps.places.Autocomplete(
          this.searchElementRef.nativeElement
          //,
          // {
          //types: ["address"],
          // }
        );
        this.paceChange = autocomplete.addListener("place_changed", () => {
          this.ngZone.run(() => {
            //get the place result
            let place: google.maps.places.PlaceResult = autocomplete.getPlace();

            //verify result
            if (place.geometry === undefined || place.geometry === null) {
              return;
            }

            //set latitude, longitude and zoom
            this.latitude = place.geometry.location.lat();
            this.longitude = place.geometry.location.lng();
            this.zoom = 12;
          });
        });
      });
    }
  }

  /** Cards Add / Remove */
  addMoreItems() {
    let control = <FormArray>this.orderManagementForm.controls.OrderItem;
    control.push(this.initItemsForm());
  }

  removeItems(i: number) {
    // remove address from the list
    const control = <FormArray>this.orderManagementForm.controls["OrderItem"];
    control.removeAt(i);
  }

  calculatePrice() {
    const control = this.orderManagementForm.controls["OrderItem"].controls;

    let total = 0;
    control.forEach((priceController, i) => {
      if (
        priceController.controls.quantity.value != "" &&
        priceController.controls.price.value != ""
      )
        total +=
          parseFloat(priceController.controls.quantity.value) *
          parseFloat(priceController.controls.price.value);
    });
    this.orderManagementForm.controls.subtotal.setValue(total.toFixed(2));
  }

  onSubmit() {
    this.isSubmitted = true;
    if (this.orderManagementForm.valid) {
      this.loader.start();

      if (this.orderManagementForm.dirty && this.orderManagementForm.touched) {
        let data = this.orderManagementForm.value;
        data.mobile_number = this.countrycode + "-" + data.mobile_number;
        data.latitude = this.latitude;
        data.longitude = this.longitude;
        if (this.order_id) {
          data.order_id = this.order_id;
        }

        this.orderService.addUpdateOrder(data).subscribe(
          (success: any) => {
            this.messageService.add({
              severity: "success",
              summary: success.message,
              detail: "",
            });
            this.loader.stop();
            if (this.order_id) {
              this.router.navigateByUrl("/business/order-management");
            } else {
              this.location.back();
            }
          },
          (error) => {
            this.errorHandling.routeAccordingToError(error);
            this.loader.stop();
          }
        );
        this.isSubmitted = false;
      } else {
        this.loader.stop();
        this.location.back();
      }
    } else {
      validateAllFormFields(this.orderManagementForm);
      this.utility.scrollToError();
    }
  }

  resetForm() {
    if (this.orderManagementForm.dirty && this.orderManagementForm.touched) {
      this.confirmationService.confirm({
        message: "You have some unsaved changes.",
        header: "Confirmation",
        accept: () => {
          this.onSubmit();
        },
        reject: () => {
          this.location.back();
        },
        rejectLabel: "Discard Changes",
        acceptLabel: "Save & Close",
      });
    } else {
      this.location.back();
    }
  }
  // form convient getter
  get orderForm() {
    return this.orderManagementForm.controls;
  }

  markerDragEnd($event: MouseEvent) {
    this.latitude = $event.coords.lat;
    this.longitude = $event.coords.lng;
    this.getAddress(this.latitude, this.longitude);
  }

  getAddress(latitude, longitude) {
    this.geoCoder.geocode(
      { location: { lat: latitude, lng: longitude } },
      (results, status) => {
        if (status === "OK") {
          if (results[0]) {
            this.zoom = 12;
            this.orderManagementForm.controls.address_2.setValue(
              results[0].formatted_address
            );
          } else {
            window.alert("No results found");
          }
        } else {
          window.alert("Geocoder failed due to: " + status);
        }
      }
    );
  }
}
