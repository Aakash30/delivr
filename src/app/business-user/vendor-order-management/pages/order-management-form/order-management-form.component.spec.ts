import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderManagementFormComponent } from './order-management-form.component';

describe('OrderManagementFormComponent', () => {
  let component: OrderManagementFormComponent;
  let fixture: ComponentFixture<OrderManagementFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderManagementFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderManagementFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
