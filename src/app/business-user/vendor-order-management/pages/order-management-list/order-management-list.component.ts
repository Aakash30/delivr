import { Component, OnInit, OnDestroy } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { trigger, transition, style, animate } from '@angular/animations';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { UtilityService } from 'app/core/services/utility.service';
import { OrderManagementService } from 'app/core/services/order-management.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { DashboardFilterRedirectService } from 'app/core/services/dashboard-filter-redirect.service';
import { ErrorHandlerService } from 'app/core/services/error-handler.service';

interface Status {
  name: string;
  code: string;
}

interface Channel {
  name: string;
  code: string;
}

@Component({
  selector: 'app-order-management-list',
  templateUrl: './order-management-list.component.html',
  styleUrls: ['./order-management-list.component.scss'],
  animations: [
    trigger('openClose', [
      transition(':enter', [
        style({ transform: 'translateX(20%)', opacity: 0 }),
        animate('500ms', style({ transform: 'translateX(0)', opacity: 1 })),
      ]),
      transition(':leave', [
        style({ transform: 'translateX(0)', opacity: 1, display: 'none' }),
        animate('500ms', style({ transform: 'translateX(20%)', opacity: 0 })),
      ]),
    ])
  ],
})

export class OrderManagementListComponent implements OnInit, OnDestroy {

  items: MenuItem[];
  activeItem: MenuItem;
  ordersList: any[];
  selectedStatus: Status;
  channels: Channel[];
  selectedChannel: Channel;
  newOrderList: any[];
  deliverOrderList: any[];
  sectionTitle: String;
  orderFilterForm: FormGroup;
  dateValue1;
  dateValue2;

  status: String = "";
  offset: number;
  total: number = 0;
  keyword: String = "";

  channelListFromAPI: string[];

  private _unsubscribe = new Subject<Boolean>();

  getItem(event, index) {
    event.preventDefault();
    if (index === 0) {

      this.status = "";
    } else if (index === 1) {
      this.status = "new";
    } else if (index === 2) {
      this.status = "assigned";

    } else if (index === 3) {
      this.status = "accepted";
    } else if (index === 4) {
      this.status = "ongoing";
    } else if (index === 5) {
      this.status = "delivered";
    }
    this.offset = 0;
    this.filterOrders();
  }

  isSubmitted: Boolean = false;
  isVisible: boolean = false;

  showFilter() {
    this.isVisible = !this.isVisible;
  }

  constructor(private route: ActivatedRoute, private utility: UtilityService, private orderService: OrderManagementService,
    private _vehicleFetch: DashboardFilterRedirectService, private _errorHandling: ErrorHandlerService) { }

  ngOnInit() {

    this.items = [
      {
        label: 'All Orders'
      },
      {
        label: 'New Orders'
      },
      {
        label: 'Assigned'
      },
      {
        label: 'Accepted'
      },
      {
        label: 'On Going'
      },
      {
        label: 'Delivered Orders'
      },
    ];
    this.activeItem = this.items[0];
    let filter = this._vehicleFetch.filterFor.subscribe((res) => {
      if (res) {
        switch (res) {
          case "new":
            this.status = "new";
            this.activeItem = this.items[1];
            break;
          case "delivered":
            this.status = "delivered";
            this.activeItem = this.items[5];
            break;
          default:
            this.status = "";
            this.activeItem = this.items[0];
        }
      } else {
      }
    });

    filter.unsubscribe();
    this._vehicleFetch.setFilter("");
    this.ordersList = this.route.snapshot.data.pageData.data.orderList;
    this.channelListFromAPI = this.route.snapshot.data.pageData.data.channelList;
    this.total = this.route.snapshot.data.pageData.data.total;



    this.orderFilterForm = new FormGroup({
      channel: new FormControl(null),
      orderRangeOne: new FormControl(''),
      orderRangeTwo: new FormControl(''),
    });

    this.channels = this.utility.arrayOfStringsToArrayOfObjects(this.channelListFromAPI);

  }

  filterOrders() {

    let data: any = {};

    if (this.status != "") {
      data.status = this.status;
    }

    if (this.offset != 0) {
      data.offset = this.offset;
    }

    if (this.dateValue1) {
      data.start_date = this.dateValue1.toISOString();
    }

    if (this.dateValue2) {
      data.end_date = this.dateValue2.toISOString();
    }

    if (this.keyword.trim() != "") {
      data.keyword = this.keyword;
    }

    if (this.orderFilterForm.value.channel) {
      data.channel = this.orderFilterForm.value.channel;
    }

    if (this.keyword.trim() != "") {
      data.keyword = this.keyword.trim();
    }

    this.orderService.getOrders(data).pipe(takeUntil(this._unsubscribe)).subscribe(
      (success: any) => {
        this.ordersList = success.data.orderList; ''
        this.total = success.data.total;
      },
      error => {
        this._errorHandling.routeAccordingToError(error);
      });
  }

  resetForm() {
    this.orderFilterForm.reset();
    this.isSubmitted = false;
    this.isVisible = false;
  }

  clearFilter() {

    if (this.orderFilterForm.dirty && this.orderFilterForm.touched) {
      this.orderFilterForm.reset();
      this.filterOrders();
    } else {
      this.orderFilterForm.reset();
    }

    this.isVisible = false;
  }

  onSubmit() {
    this.isSubmitted = true;
    this.filterOrders();
    this.isVisible = false;
    this.isSubmitted = false;
  }

  closeFilter() {
    this.isSubmitted = false;
    this.isVisible = false;
  }

  chooseDate(event) {
  }

  paginate(event) {
    if (this.offset != undefined) {
      this.offset = event.first;
      this.filterOrders();
    }
    this.offset = event.first;
  }


  searchText(event) {
    this.keyword = event.target.value;
    let key = event.keyCode || event.charCode;
    if (event.charCode == 13 || event.key == 'Backspace') {
      // on click of enter
      this.filterOrders();
    }
    // this.filterOrders();
  }

  clearInput(event) {
    this.keyword = event;
    this.filterOrders();
  }

  ngOnDestroy() {
    this._unsubscribe.next(true);
    this._unsubscribe.complete();
  }

}
