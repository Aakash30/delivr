import { Component, OnInit, PipeTransform, Pipe } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from "@angular/common";
import { OrderManagementService } from 'app/core/services/order-management.service';
import { UtilityService } from 'app/core/services/utility.service';

interface Status {
  name: string;
  code: string;
}

@Component({
  selector: 'app-order-management-details',
  templateUrl: './order-management-details.component.html',
  styleUrls: ['./order-management-details.component.scss']
})
export class OrderManagementDetailsComponent implements OnInit {


  checked: boolean;
  drivers: any[];
  driversUI: any[] = [];
  sectionTitle: String;
  buttonName: String;
  id;
  public currentUrl: String;
  status: Status[];
  selectedStatus: Status[];
  addDisplay: Boolean = false;
  orderData: any;
  data: string;
  rating: any = [];


  assignDriver() {
    this.addDisplay = true;
  }
  
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private _location: Location,
    private orderService: OrderManagementService,
    public utility: UtilityService
  ) {
    this.sectionTitle = route.snapshot.data['sectionTitle'];
    this.buttonName = route.snapshot.data['buttonName']
    this.id = (route.snapshot.paramMap.get('id'));
    this.currentUrl = router.url;
  }

  ngOnInit() {

    // Status dropdown list
    this.status = [
      { name: 'New', code: ' P' },
      { name: 'Delivered', code: 'OG' },
    ];

    const pageData = this.route.snapshot.data.pageData.data;
    this.orderData = pageData.orderDetail;
    this.driversUI.push(this.orderData.driver);
  }

  // back to previous page
  previousPage() {
    this.router.navigateByUrl('/business/order-management');
  }

  getRating() {
    for (let i = 0; i <= this.orderData.rating; i++) {
      return "hello";
    }
  }
}
