import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderManagementDetailsComponent } from './order-management-details.component';

describe('OrderManagementDetailsComponent', () => {
  let component: OrderManagementDetailsComponent;
  let fixture: ComponentFixture<OrderManagementDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderManagementDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderManagementDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
