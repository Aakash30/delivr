import { Component, OnInit } from '@angular/core';
import { Location } from "@angular/common";
import { ActivatedRoute, Router } from '@angular/router';
import { UtilityService } from 'app/core/services/utility.service';
import { OrderManagementService } from 'app/core/services/order-management.service';
import { MessageService } from 'primeng/api';
import { ErrorHandlerService } from 'app/core/services/error-handler.service';

interface cols {
  field: String;
  header: String;
}


@Component({
  selector: 'app-assign-driver',
  templateUrl: './assign-driver.component.html',
  styleUrls: ['./assign-driver.component.scss']
})
export class AssignDriverComponent implements OnInit {

  sectionTitle: any;
  buttonName: any;
  id: any;
  currentUrl: any;

  selectedValue: string;
  driverList: any[];
  inputRadio: boolean = false;
  cols: any[];
  selectedCar4: cols;
  pageData: any;
  encodeId: any;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private utility: UtilityService,
    private orderService: OrderManagementService,
    private messageService: MessageService,
    private errorHandling: ErrorHandlerService,
    private _location: Location
  ) {
    this.sectionTitle = route.snapshot.data['sectionTitle'];
    this.buttonName = route.snapshot.data['buttonName']
    this.id = utility.base64Decode(route.snapshot.paramMap.get('id'));
    this.encodeId = this.route.snapshot.paramMap.get('id');
    this.currentUrl = router.url;
  }

  ngOnInit() {

    this.pageData = this.route.snapshot.data.pageData.data;
    this.driverList = this.pageData.driverList;
    this.refreshButton();


  }

  selectCarWithButton(car: cols) {
    this.selectedCar4 = car;
  }


  renderOpts = {
    suppressMarkers: true
  };

  clicked(e) {
  }

  onClick(event) {
    this.inputRadio = true;
  }

  onMouseOver(infoWindow, gm) {

    if (gm.lastOpen != null) {
      gm.lastOpen.close();
    }

    gm.lastOpen = infoWindow;

    infoWindow.open();
  }

  checkIsAssigned(index) {
    if (this.driverList[index].orders[0]) {
      if (this.driverList[index].orders[0].orderlist.length) {
        let assignedId = this.driverList[index].orders[0].orderlist.filter((element) => {
          return element == this.id
        });
        if (assignedId.length > 0) {
          return true;
        } else {
          return false;
        }
      } else {
        return false
      }
    } else {
      return false
    }
  }

  // orgin - Indore - center of the map when it is initialised
  public center_lat: Number = 22.719568;
  public center_lng: Number = 75.857727;

  public trip_details;

  public origin: any;
  public destination: any;

  assignOrder(driver_id) {

    let data = { "driver_id": driver_id, "order_id": this.id }
    this.orderService.assignOrderToDriver(data).subscribe((success: any) => {
      this.messageService.add({
        severity: 'success',
        summary: success.message,
        detail: ''
      });
      this.refreshButton();

    }, error => {
      this.errorHandling.routeAccordingToError(error);
    })
  }

  // back to previous page
  previousPage() {
    this.router.navigateByUrl(`/business/order-management/details/${this.encodeId}`);
  }

  refreshButton() {
    this.orderService.fetchDriversForOrder(this.id).subscribe(
      (success: any) => {
        this.driverList = success.data.driverList;
      },
      error => {
        this.errorHandling.routeAccordingToError(error)
      }
    );
  }


}
