import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VendorRequestForVehicleRoutingModule } from './vendor-request-for-vehicle-routing.module';
import { RequestForVehicleListComponent } from './pages/request-for-vehicle-list/request-for-vehicle-list.component';
import { SharedModule } from 'app/shared/shared.module';


@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    VendorRequestForVehicleRoutingModule
  ],
  declarations: [RequestForVehicleListComponent]
})
export class VendorRequestForVehicleModule { }
