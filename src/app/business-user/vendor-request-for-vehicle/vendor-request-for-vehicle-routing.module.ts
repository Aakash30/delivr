import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RequestForVehicleListComponent } from './pages/request-for-vehicle-list/request-for-vehicle-list.component';
import { VehicleRequestResolverService } from 'app/core/resolver/vehicle-request-resolver.service';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        component: RequestForVehicleListComponent,
        data: {
          sectionTitle: 'Request for Vehicle'
        },
        resolve: {
          pageData: VehicleRequestResolverService
        }
      },
      {
        path: 'maintenance-list',
        component: RequestForVehicleListComponent,
        data: {
          sectionTitle: 'Request for vehicle',
          buttonName: 'New'
        }
      },

    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VendorRequestForVehicleRoutingModule { }
