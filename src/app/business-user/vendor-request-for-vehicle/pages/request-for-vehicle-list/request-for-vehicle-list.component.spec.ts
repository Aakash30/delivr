import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestForVehicleListComponent } from './request-for-vehicle-list.component';

describe('RequestForVehicleListComponent', () => {
  let component: RequestForVehicleListComponent;
  let fixture: ComponentFixture<RequestForVehicleListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequestForVehicleListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestForVehicleListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
