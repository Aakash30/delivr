import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { VehicleRequestService } from 'app/core/services/vehicle-request.service';
import { map, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { MessageService } from 'primeng/api';
import { ErrorHandlerService } from 'app/core/services/error-handler.service';
import { validateAllFormFields } from 'app/shared/utils/custom-validators';


@Component({
  selector: 'app-request-for-vehicle-list',
  templateUrl: './request-for-vehicle-list.component.html',
  styleUrls: ['./request-for-vehicle-list.component.scss']
})
export class RequestForVehicleListComponent implements OnInit {

  vehicleList: any[];
  requestDialogFlag: boolean = false;

  requestDialogForm: FormGroup;
  total = 0;
  offset: number;
  keyword = "";
  isRequestSubmitted: boolean = false;
  today = new Date();

  private _unsubscribe = new Subject<boolean>();

  // show filter function
  constructor(
    private route: ActivatedRoute,
    private vehicleRequestService: VehicleRequestService,
    private messageService: MessageService,
    private errorHandling: ErrorHandlerService
  ) { }

  ngOnInit() {
    const pageContent = this.route.snapshot.data.pageData.data;
    this.vehicleList = pageContent.vehicleRequestList;
    this.total = pageContent.total

    this.requestDialogForm = new FormGroup({
      requested_vehicle_count: new FormControl('', [Validators.required])
    });

  }

  listRequests() {

    let data: any = {};

    if (this.offset != 0) {
      data.offset = this.offset;
    }

    if (this.keyword.trim() != "") {
      data.keyword = this.keyword;
    }

    this.vehicleRequestService.getMyRequestsList(data).pipe(
      map((success: any) => {
        if (success) {
          return success.data;
        }
      }),
      takeUntil(this._unsubscribe)

    ).subscribe((success: any) => {
      this.vehicleList = success.vehicleRequestList;

      this.total = success.total
    }, error => {
      this.errorHandling.routeAccordingToError(error);
    })
  }

  resetForm() {
    this.requestDialogForm.reset();
    this.requestDialogFlag = false;
  }

  openRequestDialog() {
    this.requestDialogFlag = true;
  }

  closeRequestDialog() {
    this.requestDialogFlag = false;
  }

  get requestDialogModule() {
    return this.requestDialogForm.controls;
  }

  onRequestSubmit() {
    this.isRequestSubmitted = true;
    if (this.requestDialogForm.valid) {
      let data = this.requestDialogForm.value;
      this.vehicleRequestService.raiseNewRequest(data).pipe(
        map((success: any) => {
          if (success) {
            return success;
          }
        }),
        takeUntil(this._unsubscribe)
      ).subscribe((success: any) => {
        this.messageService.add({
          severity: 'success',
          summary: 'Request Submitted',
          detail: success.message
        });
        this.requestDialogForm.reset();
        this.listRequests();
        this.closeRequestDialog();
      }, error => {
        this.messageService.add({
          severity: 'error',
          summary: 'action failed',
          detail: error.message
        })
        this.errorHandling.routeAccordingToError(error);
      });
    }
    else {
      validateAllFormFields(this.requestDialogForm);
    }
  }

  paginate(event) {
    if (this.offset != undefined) {
      this.offset = parseInt(event.first);
      this.listRequests();
    } else {
      this.offset = parseInt(event.first);
    }
  }

  searchText(event) {

    this.keyword = event.target.value;
    if (event.charCode == 13) {
      // on click of enter
      this.listRequests();
    }
  }

  clearInput(event) {
    this.keyword = event;
    this.listRequests();
  }

  ngOnDestroy() {
    this._unsubscribe.next(true);
    this._unsubscribe.complete();
  }
}
