import { Component, OnInit, HostListener } from '@angular/core';
declare var $: any;

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  @HostListener('window:scroll', ['$event'])
  onWindowScroll(e) {
    if (window.pageYOffset > 50) {
      let element = document.getElementById('navbar');
      element.classList.add('sticky');
      $('#navbar .logo-image img').attr('src', 'assets/project-images/delivr-new-logo.png');
    } else {
      let element = document.getElementById('navbar');
      element.classList.remove('sticky');
      $('#navbar .logo-image img').attr('src', 'assets/project-images/delivr-landing-logo.svg');
    }
  }
}
