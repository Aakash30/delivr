import { Component, OnInit, HostListener, ViewChild, ElementRef, NgZone } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UtilityService } from 'app/core/services/utility.service';
import { Router } from '@angular/router';
import { MapsAPILoader } from '@agm/core';
import { LandingService } from 'app/core/services/landing.service';
import { validateAllFormFields } from 'app/shared/utils/custom-validators';

declare var $: any;

@Component({
  selector: 'app-partners',
  templateUrl: './partners.component.html',
  styleUrls: ['./partners.component.scss']
})
export class PartnersComponent implements OnInit {

  @ViewChild('signupForm') signupForm: ElementRef;
  @ViewChild('search') searchElement: ElementRef;


  lat: number;
  lng: number;
  userList: any = [];
  retryData: any;
  address: string;
  fullAddress: any = [];
  id: any;
  userRole: string;
  isSubmitted: boolean = false;

  partnerForm: FormGroup;
  city: string;
  currentYear: any;

  slides = [
    {
      name: "Fyodor Dyuzhenkov",
      city: "Puneaaaaa",
      img: "./assets/project-images/carousel-user-2.svg",
      description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."
    },
    {
      name: "Fyodor Dyuzhenkov",
      city: "Pune1111",
      img: "./assets/project-images/carousel-user-1.svg",
      description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."
    },
    {
      name: "Fyodor Dyuzhenkov",
      city: "Pune22222",
      img: "./assets/project-images/carousel-user-2.svg",
      description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."
    },
    {
      name: "Fyodor Dyuzhenkov",
      city: "Pune3333",
      img: "./assets/project-images/carousel-user-1.svg",
      description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."
    }
  ];
  slideConfig = {
    "slidesToShow": 3, "centerPadding": '10px', "centerMode": true,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 1,
          centerMode: false
        }
      }
    ]
  };


  constructor(
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone,
    private landingService: LandingService,
    private utilityService: UtilityService,
    private router: Router
  ) { }

  ngOnInit() {

    this.partnerForm = new FormGroup({
      name: new FormControl('', [Validators.required]),
      city: new FormControl('', [Validators.required]),
      number: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required]),
    });

    this.currentYear = new Date().getFullYear();
  }

  @HostListener('window:scroll', ['$event'])
  onWindowScroll($event) {
    if (window.pageYOffset > 50) {
      let element = document.getElementById('navbar');
      element.classList.add('sticky');
      $('#navbar .logo-image img').attr('src', 'assets/project-images/delivr-new-logo.png');
    } else {
      let element = document.getElementById('navbar');
      element.classList.remove('sticky');
      $('#navbar .logo-image img').attr('src', 'assets/project-images/delivr-landing-logo.svg');
    }
  }

  onChangeLocation(event) {
    this.userList = [];
  }

  loadMap() {
    var add = '';
    this.mapsAPILoader.load().then(
      () => {
        this.address;
        const autocomplete = new google.maps.places.Autocomplete(this.searchElement.nativeElement, {
          types: ['geocode']
        });
        autocomplete.setComponentRestrictions(
          { 'country': ['IN'] });

        autocomplete.addListener('place_changed', () => {
          this.ngZone.run(() => {
            const place: google.maps.places.PlaceResult = autocomplete.getPlace();
            if (place.geometry === undefined || place.geometry === null) {

              return;
            }
            this.lat = place.geometry.location.lat();
            this.lng = place.geometry.location.lng();
            this.fullAddress = place.address_components;
            this.address = place.formatted_address;

            this.partnerForm.controls['city'].setValue(this.fullAddress);
          });
        });
      }
    );
  }

  onSubmit() {
    this.isSubmitted = true;
    if (this.partnerForm.valid) {
      let data = {
        name: this.partnerForm.controls.name.value,
        city: this.partnerForm.controls.city.value,
        number: this.partnerForm.controls.number.value,
        email: this.partnerForm.controls.email.value,
        formType: 'rider'
      }
      // console.log('data', data); return

      this.utilityService.loaderStart();
      this.landingService.addContactRequest(data).subscribe(
        (success: any) => {
          this.utilityService.showSuccess('success', success.body.message);
          this.utilityService.loaderStop();

        },
        error => {
          // this.utilityService.errorMessage(error);
          this.utilityService.loaderStop();
        }
      )
    } else {
      validateAllFormFields(this.partnerForm);
    }
  }

  // scroll to particular section
  scrollToSection(el: any) {
    el.scrollIntoView({
      behavior: 'smooth',
      block: 'center',
      inline: 'start'
    });

  }


}
