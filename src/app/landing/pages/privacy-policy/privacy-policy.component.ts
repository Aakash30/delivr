import { Component, OnInit, HostListener } from '@angular/core';

declare var $: any;
@Component({
  selector: 'app-privacy-policy',
  templateUrl: './privacy-policy.component.html',
  styleUrls: ['./privacy-policy.component.scss']
})
export class PrivacyPolicyComponent implements OnInit {

  currentYear: any;
  constructor() { }

  ngOnInit() {
    this.currentYear = new Date().getFullYear();
  }

  @HostListener('window:scroll', ['$event'])
  onWindowScroll($event) {
    if (window.pageYOffset > 50) {
      let element = document.getElementById('navbar');
      element.classList.add('sticky');
      $('#navbar .logo-image img').attr('src', 'assets/project-images/delivr-new-logo.png');
    } else {
      let element = document.getElementById('navbar');
      element.classList.remove('sticky');
      $('#navbar .logo-image img').attr('src', 'assets/project-images/delivr-landing-logo.svg');
    }
  }

}
