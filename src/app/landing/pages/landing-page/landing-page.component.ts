import { Component, OnInit, HostListener } from '@angular/core';
import { Router } from '@angular/router';
declare var $: any;

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.scss']
})
export class LandingPageComponent implements OnInit {

  slides = [
    {
      name: "King Riders",
      img: "./assets/project-images/testimonials-1.png",
      description: "Amazing workshop & Super Technicians! Our Riders’ time is most valuable and critical factor in delivery services. This is where Powerlease Team as our key partners adds value in keeping the fleet ready for business.  Thank you Powerlease for supporting us always with your “Excellent Customer Service” and “Fleet Management Expertise”."
    },
    {
      name: "Decent Food Supply Services L.L.C",
      img: "./assets/project-images/testimonials-2.png",
      description: "Trained staff, very reliable. Highly Recommended. “Really prove their name very decent staff“ Great service 👍"
    },
    {
      name: "Delivery",
      img: "./assets/project-images/testimonials-3.png",
      description: "Absolutely Splendid services offered by the team. We are very impressed with their professionalism and courtesy. Thank you, we are with you for life. Keep up the great work!"
    },
    {
      name: "MM Delivery",
      img: "./assets/project-images/testimonials-4.png",
      description: "Accept this testimonial as a small token of our appreciation for your fantastic service. Powerlease has wonderful and friendly staff, with a high standard of work ethic. They always go that extra mile to make our life easier."
    },
    {
      name: "Knight Riders",
      img: "./assets/project-images/testimonials-5.png",
      description: "My company has been dealing with Power lease since their inception. I have nothing but accolades for Mona, Deepak and their team. Always prompt, courteous, professional and competitive.” They believe in teamwork and sharing the leads of their clients looking for delivery services. Mr. Deepak is creating a niche marketplace for him and partners like us with unique solution and excellent customer service."
    },
    {
      name: "Foober Delivery Services",
      img: "./assets/project-images/testimonials-6.png",
      description: "We frequently work with Powerlease to rent bikes and they have been wonderful each time. Friendly staff and brand-new bikes at a very competitive rate, a great experience. Not many businesses go the extra mile for their customers. Their attention to detail and willingness to go beyond the rental is quite visible. Looking forward to working with their team in the future!"
    }
  ];
  slideConfig = {
    "slidesToShow": 3, "centerPadding": '10px', "centerMode": true,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 2,
          centerMode: false,
          slidesToScroll: 2,
        }
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          centerMode: false
        }
      }
    ]
  };

  currentYear: any;

  constructor(
    private router: Router
  ) { }

  ngOnInit() {
    this.currentYear = new Date().getFullYear();
    // console.log('current year::', this.currentYear);
  }

  @HostListener('window:scroll', ['$event'])
  onWindowScroll($event) {

    if (window.pageYOffset > 50) {

      let element = document.getElementById('navbar');
      element.classList.add('sticky');
      $('#navbar .logo-image img').attr('src', 'assets/project-images/delivr-new-logo.png');

    } else {
      let element = document.getElementById('navbar');
      element.classList.remove('sticky');
      $('#navbar .logo-image img').attr('src', 'assets/project-images/delivr-landing-logo.svg');

    }

  }

  callPage() {
    this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
    this.router.navigate(['']);
    }); 
  }


}
