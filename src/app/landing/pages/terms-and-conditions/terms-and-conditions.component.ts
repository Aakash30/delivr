import { Component, OnInit, HostListener, Renderer2, ElementRef, Renderer } from '@angular/core';

@Component({
  selector: 'app-terms-and-conditions',
  templateUrl: './terms-and-conditions.component.html',
  styleUrls: ['./terms-and-conditions.component.scss']
})
export class TermsAndConditionsComponent implements OnInit {

  elementClicked = 1;
  descTabClicked: number;
  disabled: boolean = false;
  toggleMenuFlag: boolean = false;
  descTabFlag: boolean = false;
  currentYear: any;

  constructor(
    private renderer2: Renderer2,
    private renderer: Renderer,
    private elRef: ElementRef
  ) { }

  ngOnInit() {
    this.currentYear = new Date().getFullYear();
  }

  onClick(e) {
    // console.log('li clicked::', e.target.value);
    this.elementClicked = e.target.value;
    if (this.elementClicked === 4) {
      this.toggleMenuFlag = !this.toggleMenuFlag;
    }
  }

  descTabClick(e) {

    // console.log('target value', e.target.value);
    // console.log('desc tab flag', this.descTabFlag);

    this.descTabClicked = e.target.value;

    if (this.descTabClicked === e.target.value) {
      this.descTabFlag = !this.descTabFlag;
    }
    else {
      this.descTabFlag = false;
    }
  }

  // @HostListener('window:scroll', ['$event'])
  // onWindowScroll(e) {
  //   if (window.pageYOffset > 50) {
  //     let element = document.getElementById('navbar');
  //     element.classList.add('sticky');
  //     $('#navbar .logo-image img').attr('src', 'assets/project-images/delivr-landing-logo-scroll.svg');
  //   } else {
  //     let element = document.getElementById('navbar');
  //     element.classList.remove('sticky');
  //     $('#navbar .logo-image img').attr('src', 'assets/project-images/delivr-landing-logo.svg');
  //   }
  // }


  hidedropdown() {

    $('.dropdown-menu').slideToggle();
  }





}
