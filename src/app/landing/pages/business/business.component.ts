import { Component, OnInit, HostListener, ViewChild, ElementRef, NgZone, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MapsAPILoader, LatLngBounds } from '@agm/core';
import { LandingService } from 'app/core/services/landing.service';
import { UtilityService } from 'app/core/services/utility.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { AuthService } from 'app/core/services/auth.service';
import { validateAllFormFields } from 'app/shared/utils/custom-validators';

declare var $: any;


@Component({
  selector: 'app-business',
  templateUrl: './business.component.html',
  styleUrls: ['./business.component.scss']
})
export class BusinessComponent implements OnInit, OnDestroy {

  @ViewChild('signupForm') signupForm: ElementRef;
  @ViewChild('search') searchElement: ElementRef;

  lat: number;
  lng: number;
  userList: any = [];
  retryData: any;
  address: string;
  email: string;
  fullAddress: any = [];
  id: any;
  city: any;
  userRole: string;
  isSubmitted: boolean = false;
  currentYear: any;

  private _unsubscribe = new Subject<Boolean>();

  businessForm: FormGroup;

  slides = [
    {
      name: "King Riders",
      img: "./assets/project-images/testimonials-1.png",
      description: "Amazing workshop & Super Technicians! Our Riders’ time is most valuable and critical factor in delivery services. This is where Powerlease Team as our key partners adds value in keeping the fleet ready for business.  Thank you Powerlease for supporting us always with your “Excellent Customer Service” and “Fleet Management Expertise”."
    },
    {
      name: "Decent Food Supply Services L.L.C",
      img: "./assets/project-images/testimonials-2.png",
      description: "Trained staff, very reliable. Highly Recommended. “Really prove their name very decent staff“ Great service 👍"
    },
    {
      name: "Delivery",
      img: "./assets/project-images/testimonials-3.png",
      description: "Absolutely Splendid services offered by the team. We are very impressed with their professionalism and courtesy. Thank you, we are with you for life. Keep up the great work!"
    },
    {
      name: "MM Delivery",
      img: "./assets/project-images/testimonials-4.png",
      description: "Accept this testimonial as a small token of our appreciation for your fantastic service. Powerlease has wonderful and friendly staff, with a high standard of work ethic. They always go that extra mile to make our life easier."
    },
    {
      name: "Knight Riders",
      img: "./assets/project-images/testimonials-5.png",
      description: "My company has been dealing with Power lease since their inception. I have nothing but accolades for Mona, Deepak and their team. Always prompt, courteous, professional and competitive.” They believe in teamwork and sharing the leads of their clients looking for delivery services. Mr. Deepak is creating a niche marketplace for him and partners like us with unique solution and excellent customer service."
    },
    {
      name: "Foober Delivery Services",
      img: "./assets/project-images/testimonials-6.png",
      description: "We frequently work with Powerlease to rent bikes and they have been wonderful each time. Friendly staff and brand-new bikes at a very competitive rate, a great experience. Not many businesses go the extra mile for their customers. Their attention to detail and willingness to go beyond the rental is quite visible. Looking forward to working with their team in the future!"
    }
  ];
  slideConfig = {
    "slidesToShow": 3, "centerPadding": '10px', "centerMode": true,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 1,
          centerMode: false
        }
      }
    ]
  };

  testimonialSlides = [
    {
      // poster: "./assets/project-images/testi-v.png",
      src: "./assets/videoclick/Video.MOV"
    }
    // {
    //   poster: "./assets/project-images/testi-v.png",
    //   src: "./assets/videoclick/testimonial-1.mp4"
    // },
    // {
    //   poster: "./assets/project-images/testi-v.png",
    //   src: "./assets/videoclick/testimonial-1.mp4"
    // },

  ];
  testimonialSlideConfig = { "slidesToShow": 2, "slidesToScroll": 2, dots: true };

  control = false;
  ap = "true";
  constructor(
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone,
    private landingService: LandingService,
    private utilityService: UtilityService,
    private router: Router,
    private authService: AuthService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.businessForm = new FormGroup({
      name: new FormControl('', [Validators.required]),
      city: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required]),
      number: new FormControl('', [Validators.required])
    });

    this.currentYear = new Date().getFullYear();
  }


  @HostListener('window:scroll', ['$event'])
  onWindowScroll($event) {
    if (window.pageYOffset > 50) {
      let element = document.getElementById('navbar');
      element.classList.add('sticky');
      // $('#navbar .logo-image img').attr('src', 'assets/project-images/delivr-landing-logo-scroll.svg');
      $('#navbar .logo-image img').attr('src', 'assets/project-images/delivr-new-logo.png');
    } else {
      let element = document.getElementById('navbar');
      element.classList.remove('sticky');
      $('#navbar .logo-image img').attr('src', 'assets/project-images/delivr-landing-logo.svg');
    }
  }


  showHideControls() {
    this.control = true;
    var a = <HTMLInputElement>document.getElementById('video');
    a.setAttribute('controls', 'true');
    $('video').attr('autoplay', 'true');
    let audioPlayer = <HTMLVideoElement>document.getElementById('video');
    audioPlayer.play();
    let tabElements = <HTMLVideoElement>document.getElementById('playbtn');
    tabElements.classList.remove('show');
    tabElements.classList.add('hide');


  }

  showHideControls2() {
    this.control = true;
    var a = <HTMLInputElement>document.getElementById('video2');
    a.setAttribute('controls', 'true');
    $('video2').attr('autoplay', 'true');
    let audioPlayer = <HTMLVideoElement>document.getElementById('video2');
    audioPlayer.play();
    let tabElements = <HTMLVideoElement>document.getElementById('playbtn2');
    tabElements.classList.remove('show');
    tabElements.classList.add('hide');


  }

  onChangeLocation(event) {
    this.userList = [];
  }

  loadMap() {
    var add = '';
    this.mapsAPILoader.load().then(
      () => {
        this.address;
        const autocomplete = new google.maps.places.Autocomplete(this.searchElement.nativeElement, {
          types: ['geocode']
        });
        autocomplete.setComponentRestrictions(
          { 'country': ['IN'] });

        autocomplete.addListener('place_changed', () => {
          this.ngZone.run(() => {
            const place: google.maps.places.PlaceResult = autocomplete.getPlace();
            if (place.geometry === undefined || place.geometry === null) {
              return;
            }
            this.lat = place.geometry.location.lat();
            this.lng = place.geometry.location.lng();
            this.fullAddress = place.address_components;
            this.address = place.formatted_address;
            this.businessForm.controls['city'].setValue(this.fullAddress[0].long_name);
          });
        });
      }
    );
  }

  onSubmit() {
    this.isSubmitted = true;
    if (this.businessForm.valid) {
      // let formType;
      // user role
      // if (this.router.url == '/delivr-for-business') {
      //   formType = 'business';
      // } else if (this.router.url == '/delivr-for-partner') {
      //   formType = 'partner';
      // }
      let data = {
        name: this.businessForm.controls.name.value,
        city: this.businessForm.controls.city.value,
        number: this.businessForm.controls.number.value,
        email: this.businessForm.controls.email.value,
        formType: 'business'
      }


      this.utilityService.loaderStart();
      this.landingService.addContactRequest(data).pipe(takeUntil(this._unsubscribe)).subscribe(
        (success: any) => {
          this.utilityService.showSuccess('success', success.body.message);
          // this.utilityService.showSuccess(success.message);
          this.utilityService.loaderStop();

        },
        error => {
          // this.utilityService.errorMessage(error);
          this.utilityService.loaderStop();
        }
      )
    } else {
      validateAllFormFields(this.businessForm);
    }
  }

  // scroll to particular section
  scrollToSection(el: any) {
    el.scrollIntoView({
      behavior: 'smooth',
      block: 'center',
      inline: 'start'
    });
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this._unsubscribe.next(true);
    this._unsubscribe.complete();
  }


}
