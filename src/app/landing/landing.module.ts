import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LandingRoutingModule } from './landing-routing.module';
import { LandingPageComponent } from './pages/landing-page/landing-page.component';
import { SharedModule } from 'app/shared/shared.module';
import { PartnersComponent } from './pages/partners/partners.component';
import { BusinessComponent } from './pages/business/business.component';
import { AboutComponent } from './pages/about/about.component';
import { TermsAndConditionsComponent } from './pages/terms-and-conditions/terms-and-conditions.component';
import { AgmCoreModule } from '@agm/core';
import { LandingPageLayoutComponent } from './landing-page-layout/landing-page-layout.component';
import { ComingSoonComponent } from './pages/coming-soon/coming-soon.component';
import { PrivacyPolicyComponent } from './pages/privacy-policy/privacy-policy.component';
// import { AgmCoreModule } from '@agm/core';

@NgModule({
  imports: [
    CommonModule,
    LandingRoutingModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDHapbY7BfjFKhd2WJ4BHv6YDfWuPYhoL8',
      libraries: ['places']
    }),
    SharedModule
  ],
  declarations: [LandingPageComponent, PartnersComponent, BusinessComponent, AboutComponent, TermsAndConditionsComponent, LandingPageLayoutComponent, ComingSoonComponent, PrivacyPolicyComponent],
  exports: [BusinessComponent]
})
export class LandingModule { }
