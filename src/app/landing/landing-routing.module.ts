import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LandingPageComponent } from './pages/landing-page/landing-page.component';
import { PartnersComponent } from './pages/partners/partners.component';
import { BusinessComponent } from './pages/business/business.component';
import { AboutComponent } from './pages/about/about.component';
import { TermsAndConditionsComponent } from './pages/terms-and-conditions/terms-and-conditions.component';
import { LandingPageLayoutComponent } from './landing-page-layout/landing-page-layout.component';
import { ComingSoonComponent } from './pages/coming-soon/coming-soon.component';
import { ErrorPageComponent } from 'app/error-page/error-page.component';
import { PrivacyPolicyComponent } from './pages/privacy-policy/privacy-policy.component';

const routes: Routes = [
  {
    path: '',
    component: LandingPageLayoutComponent,
    children: [
      {
        path: 'delivr-for-riders',
        component: PartnersComponent,
      },
      {
        path: 'delivr-for-business',
        component: BusinessComponent
      },
      {
        path: 'about',
        component: AboutComponent,
      },
      {
        path: 'terms-and-conditions',
        component: TermsAndConditionsComponent,
      },
      {
        path: 'privacy-policy',
        component: PrivacyPolicyComponent,
      },
      {
        path: 'coming-soon',
        component: ComingSoonComponent,
      },
      {
        path: '',
        component: LandingPageComponent,
        data: {
          sectionTitle: 'Maintenance',
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LandingRoutingModule { }
