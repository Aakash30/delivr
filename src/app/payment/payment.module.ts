import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from 'app/shared/shared.module';
import { PaymentRoutingModule } from './payment-routing.module';
import { PaymentDetailsComponent } from './pages/payment-details/payment-details.component';

@NgModule({
  imports: [
    CommonModule,
    PaymentRoutingModule,
    SharedModule
  ],
  declarations: [PaymentDetailsComponent]
})
export class PaymentModule { }
