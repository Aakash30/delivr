import { Component, OnInit, Input, OnDestroy } from "@angular/core";
import {
  FormGroup,
  FormControl,
  Validators,
} from "@angular/forms";
import {
  validateAllFormFields,
} from "../../../shared/utils/custom-validators";
import { ActivatedRoute } from "@angular/router";
import { trigger, style, transition, animate } from "@angular/animations";
import { PaymentService } from "../../../core/services/payment.service";
import { MessageService } from "primeng/api";
import { UtilityService } from "app/core/services/utility.service";
import { Subject } from "rxjs/internal/Subject";
import { takeUntil } from "rxjs/operators";
import { ErrorHandlerService } from "app/core/services/error-handler.service";

@Component({
  selector: "app-payment-details",
  templateUrl: "./payment-details.component.html",
  animations: [
    trigger("openClose", [
      transition(":enter", [
        style({ transform: "translateX(20%)", opacity: 0 }),
        animate("500ms", style({ transform: "translateX(0)", opacity: 1 }))
      ]),
      transition(":leave", [
        style({ transform: "translateX(0)", opacity: 1, display: "none" }),
        animate("500ms", style({ transform: "translateX(20%)", opacity: 0 }))
      ])
    ])
  ],
  styleUrls: ["./payment-details.component.scss"]
})
export class PaymentDetailsComponent implements OnInit, OnDestroy {
  table: {
    total_price: number;
    paid_price: number;
  };

  addDisplay: Boolean = false;
  isSubmitted: boolean = false;
  paymentForm: FormGroup;
  offset: number;
  maxPrice = 1000;
  isVisible: boolean;
  value: any;
  remaining: number = 0;
  currentPayment = {
    payment_id: 0,
    business_name: "",
    id: null,
    batch_id: null,
    last_installment: false
  };

  private _unsubscribe = new Subject<Boolean>();

  constructor(
    private route: ActivatedRoute,
    private paymentService: PaymentService,
    private messageService: MessageService,
    public _utility: UtilityService,
    private errorHandling: ErrorHandlerService
  ) { }

  ngOnInit() {

    const pageData = this.route.snapshot.data.pageData.data;
    this.table = pageData.paymentList;

    this.paymentForm = new FormGroup({
      amount: new FormControl("", [Validators.required]),
      date: new FormControl("", [Validators.required]),
      id: new FormControl(null)
    });
  }

  updater(e, paymentDetails, batchData, index) {
    this.currentPayment = {
      payment_id: paymentDetails.payment_id,
      business_name: batchData.business_name,
      id: paymentDetails.id,
      batch_id: batchData.id,
      last_installment: (batchData.installment_counts == index + 1)
    };
    this.remaining = (batchData.total_price) - (batchData.paid_price);
    this.addDisplay = true;
  }

  listing() {

    this.paymentService.fetchPayments()
      .pipe(takeUntil(this._unsubscribe))
      .subscribe(
        (success: any) => {
          this.table = success.data.paymentList;
        },
        error => {
          this.errorHandling.routeAccordingToError(error);
        }
      )
  }

  resetForm() {
    this.addDisplay = false;
    this.paymentForm.reset();
    this.isSubmitted = false;
  }

  searchText(e) {
    this.paymentService.fetchPayments()
      .pipe(takeUntil(this._unsubscribe))
      .subscribe(
        (res: any) => {
          this.messageService.add({
            severity: "success",
            summary: res.message,
            detail: "payments fetched"
          });
        },
        err => {
          this.errorHandling.routeAccordingToError(err);
        }
      );
  }


  onSubmit() {

    this.isSubmitted = true;
    let check = true;
    if (this.currentPayment.last_installment && this.remaining - this.paymentForm.value.amount != 0) {
      check = confirm("You have remaining payments after submitting. Do you want to continue? ")
    }

    if (this.paymentForm.valid && check) {

      let data = {
        amount: this.paymentForm.value.amount,
        date: this.paymentForm.value.date.toString(),
        paymentId: this.currentPayment.payment_id,
        id: this.currentPayment.id,
        batch_id: this.currentPayment.batch_id
      };


      this.paymentService.updatePayment(data)
        .pipe(takeUntil(this._unsubscribe))
        .subscribe(
          (res: any) => {
            this.messageService.add({
              severity: "success",
              summary: "payment updated",
              detail: res.message
            });
            this.addDisplay = false;
            this.listing();
          },
          err => {
            this.errorHandling.routeAccordingToError(err);
          }
        );
    } else {
      validateAllFormFields(this.paymentForm);
    }
  }

  addPayment(slug) {
    this.addDisplay = true;
  }


  paginate(event) {
    if (this.offset != undefined) {
      this.offset = parseInt(event.first);
      this.listing();
    } else {
      this.offset = parseInt(event.first);
    }
  }


  checkLimit(event, formController) {
    let value = event.target.value;
    let remainingAmount = this.remaining;
    if (remainingAmount - value < 0) {

      var newValue = value.substring(0, value.length - 1);
      formController.setValue(newValue)
    }
  }

  get paymentModule() {
    return this.paymentForm.controls;
  }

  ngOnDestroy() {
    this._unsubscribe.next(true);
    this._unsubscribe.complete();
  }
}
