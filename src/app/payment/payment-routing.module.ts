import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PaymentDetailsComponent } from './pages/payment-details/payment-details.component';
import { PaymentResolverService } from 'app/core/resolver/payment-resolver.service';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        component: PaymentDetailsComponent,
        data: {
          breadcrumb: 'Payment',
          sectionTitle: 'Payment',
          onlyForUser: ['super_admin'],
          expectedRole: ['payments']
        },
        resolve: {
          pageData: PaymentResolverService
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PaymentRoutingModule { }
