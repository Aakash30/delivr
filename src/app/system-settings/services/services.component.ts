import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { UtilityService } from 'app/core/services/utility.service';
import { AdminServiceSettingsService } from 'app/core/services/admin-service-settings.service';
import { takeUntil, map } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { ErrorHandlerService } from 'app/core/services/error-handler.service';

@Component({
  selector: 'app-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.scss'],
  animations: [
    trigger('rowExpansionTrigger', [
      state('void', style({
        transform: 'translateX(-10%)',
        opacity: 0
      })),
      state('active', style({
        transform: 'translateX(0)',
        opacity: 1
      })),
      transition('* <=> *', animate('400ms cubic-bezier(0.86, 0, 0.07, 1)'))
    ])
  ]
})
export class ServicesComponent implements OnInit, OnDestroy {

  keyword: string = "";
  serviceList;
  total: number = 0;
  offset: number = 0;

  private _unsubscribe = new Subject<boolean>();

  constructor(private route: ActivatedRoute, public utility: UtilityService, private serviceSetting: AdminServiceSettingsService, private errorHandling: ErrorHandlerService) { }

  ngOnInit() {
    const pageContent = this.route.snapshot.data.pageData.data;
    this.serviceList = pageContent.serviceData;
    this.total = pageContent.total;

  }

  filterServices() {
    let data: any = {};

    if (this.keyword.trim() != "") {
      data.keyword = this.keyword.trim();
    }

    if (this.offset != 0) {
      data.offset = this.offset;
    }

    this.serviceSetting.fetchServices(data).pipe(
      map((success: any) => {
        if (success) {
          return success.data;
        }
      }
      ),
      takeUntil(this._unsubscribe)
    ).subscribe((success: any) => {
      this.serviceList = success.serviceData;
      this.total = success.total;
    }, error => {
      this.errorHandling.routeAccordingToError(error);
    })
  }

  /** key searching */
  searchText(event) {
    if (event.charCode == 13) {

      this.keyword = event.target.value;
      this.filterServices();
    }

  }
  /** clears category input */
  clearInput(event) {
    this.keyword = event;
    this.filterServices();
  }
  paginate(event) {
    this.offset = parseInt(event.first);
    this.filterServices();
  }

  ngOnDestroy() {
    this._unsubscribe.next(true);
    this._unsubscribe.complete();
  }
}
