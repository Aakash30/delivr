import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { noWhitespaceValidator, blankSpaceInputNotValid, validateAllFormFields } from 'app/shared/utils/custom-validators';
import { AdminServiceSettingsService } from 'app/core/services/admin-service-settings.service';
import { ErrorHandlerService } from 'app/core/services/error-handler.service';
import { ActivatedRoute } from '@angular/router';
import { MessageService } from 'primeng/api';
import { UtilityService } from 'app/core/services/utility.service';
import { Subject } from 'rxjs/internal/Subject';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-service-category',
  templateUrl: './service-category.component.html',
  styleUrls: ['./service-category.component.scss']
})
export class ServiceCategoryComponent implements OnInit, OnDestroy {

  displayForm: boolean = false;
  formButton: String = "Add";
  formHeading: String = "Add Service Category";
  serviceCategoryForm: FormGroup;
  isSubmitted: boolean = false;
  serviceCategoryList: [];
  total: number = 0;
  offset: number = 0;
  keyword: string = "";
  possibleValue = ['on', 'off'];

  private _unsubscribe = new Subject<Boolean>();


  constructor(
    private serviceSetting: AdminServiceSettingsService,
    private errorHandling: ErrorHandlerService,
    private route: ActivatedRoute,
    private messageService: MessageService,
    private _utility: UtilityService
  ) { }

  ngOnInit() {

    this.serviceCategoryForm = new FormGroup({
      service_category_id: new FormControl(null),
      service_category_name: new FormControl('', [Validators.required, noWhitespaceValidator, blankSpaceInputNotValid])
    });

    const pageContent = this.route.snapshot.data.pageData.data;

    this.serviceCategoryList = pageContent.serviceCategoryList;
    this.total = pageContent.count;


  }

  get serviceForm() {
    return this.serviceCategoryForm.controls;
  }

  filterServiceCategory() {

    let data: any = {};

    if (this.keyword.trim() != "") {
      data.keyword = this.keyword;
    }
    if (this.offset != 0) {
      data.offset = this.offset;
    }

    this.serviceSetting.fetchServiceCategory(data)
      .pipe(takeUntil(this._unsubscribe))
      .subscribe((success: any) => {
        this.serviceCategoryList = success.data.serviceCategoryList;
        this.total = success.data.count;
      })
  }

  paginate(event) {
    if (this.offset != undefined) {
      this.offset = parseInt(event.first);
      this.filterServiceCategory();
    } else {
      this.offset = parseInt(event.first);
    }

  }

  changeStatus(event, id) {
    let data = {
      status: event ? "on" : "off",
      service_category_id: id
    };

    this.serviceSetting.updateServiceCategoryStatus(data)
      .pipe(takeUntil(this._unsubscribe))
      .subscribe((success: any) => {
        this.messageService.add({
          severity: "success",
          summary: "Success",
          detail: success.message
        });
        this.filterServiceCategory();
      }, error => {
        this.errorHandling.routeAccordingToError(error);
      });
  }
  /**
   * opens form for adding or editing form
   * @param index defines row index in case of edit form
   */
  addEditServiceCategory(index?) {
    if (index != undefined) {
      this.formButton = "Update";
      this.formHeading = "Add Service Category";
      let editData = this.serviceCategoryList[index];
      this.serviceCategoryForm.patchValue(editData);
    } else {
      this.formButton = "Add";
      this.formHeading = "Add Service Category";
    }
    this.displayForm = true;
  }

  /** add update service category */
  onSubmit() {
    this.isSubmitted = true;
    if (this.serviceCategoryForm.valid) {

      if (this.serviceCategoryForm.dirty && this.serviceCategoryForm.touched) {

        let data = this.serviceCategoryForm.value;
        this.serviceSetting.addUpdateServiceCategory(data)
          .pipe(takeUntil(this._unsubscribe))
          .subscribe((success: any) => {
            this.resetForm();
            this.messageService.add({
              severity: "success",
              summary: success.message,
              detail: ""
            });
            this.filterServiceCategory();
          }, error => {
            this.errorHandling.routeAccordingToError(error);
          })

      } else {
        this.resetForm();

        this.messageService.add({
          severity: "success",
          summary: "Service category has been updated successfully.",
          detail: ""
        });
      }
    } else {
      validateAllFormFields(this.serviceCategoryForm);
    }
  }

  resetForm() {
    this.serviceCategoryForm.reset();
    this.displayForm = false;
  }

  /** key searching */
  searchText(event) {
    if (event.charCode == 13) {

      this.keyword = event.target.value;
      this.filterServiceCategory();
    }

  }
  /** clears category input */
  clearInput(event) {
    this.keyword = event;
    this.filterServiceCategory();
  }

  ngOnDestroy() {
    this._unsubscribe.next(true);
    this._unsubscribe.complete();
  }

}
