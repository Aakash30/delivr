import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ServiceCategoryComponent } from './service-category/service-category.component';
import { AdminServiceSettingResolverService } from 'app/core/resolver/admin-service-setting-resolver.service';
import { ServicesComponent } from './services/services.component';
import { ServiceAddUpdateFormComponent } from './service-add-update-form/service-add-update-form.component';
import { BrandComponent } from './brand/brand.component';
import { CountriesComponent } from 'app/country/pages/countries/countries.component';
import { BrandDetailsComponent } from './brand-details/brand-details.component';
import { AuthGuardService as AuthGuard } from './../core/guards/auth-guard.service';
import { RolesGuardService as RoleGuard } from './../core/guards/roles-guard.service';
import { CountryResolverService } from 'app/core/resolver/country-resolver.service';
import { BrandModelSettingResolverService } from 'app/core/resolver/brand-model-setting-resolver.service';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'country',
        component: CountriesComponent,
        loadChildren: './../country/country.module#CountryModule',
        canActivate: [AuthGuard, RoleGuard],
        data: {
          sectionTitle: 'Countries',
          onlyForUser: ['super_admin'],
          expectedRole: ['location-setting']
        },
        resolve: {
          pageData: CountryResolverService,
        }
      },
      {
        path: 'brand',
        component: BrandComponent,
        canActivate: [AuthGuard, RoleGuard],
        data: {
          sectionTitle: 'Brands',
          onlyForUser: ['super_admin'],
          expectedRole: ['brand-model-setting']
        },
        resolve: {
          pageData: BrandModelSettingResolverService
        }
      },
      {
        path: 'brand/brand-details/:id',
        component: BrandDetailsComponent,
        canActivate: [AuthGuard, RoleGuard],
        data: {
          sectionTitle: 'Brands',
          onlyForUser: ['super_admin'],
          expectedRole: ['brand-model-setting']
        },
        resolve: {
          pageData: BrandModelSettingResolverService
        }
      },
      {
        path: 'service-category',
        component: ServiceCategoryComponent,
        canActivate: [AuthGuard, RoleGuard],
        data: {
          sectionTitle: 'Service Category',
          onlyForUser: ['super_admin'],
          expectedRole: ['service-category-setting']
        },
        resolve: {
          pageData: AdminServiceSettingResolverService
        }
      },
      {
        path: 'service-list',
        component: ServicesComponent,
        canActivate: [AuthGuard, RoleGuard],
        data: {
          sectionTitle: 'Services',
          onlyForUser: ['super_admin'],
          expectedRole: ['service-setting']
        },
        resolve: {
          pageData: AdminServiceSettingResolverService
        }
      },
      {
        path: 'service-list/add-service',
        component: ServiceAddUpdateFormComponent,
        canActivate: [AuthGuard, RoleGuard],
        data: {
          sectionTitle: 'Add Service',
          buttonName: 'Add Service',
          onlyForUser: ['super_admin'],
          expectedRole: ['service-setting']
        },
        resolve: {
          pageData: AdminServiceSettingResolverService
        }
      },
      {
        path: 'service-list/edit-service/:id',
        component: ServiceAddUpdateFormComponent,
        canActivate: [AuthGuard, RoleGuard],
        data: {
          sectionTitle: 'Edit Service',
          buttonName: 'Update Service',
          onlyForUser: ['super_admin'],
          expectedRole: ['service-setting']
        },
        resolve: {
          pageData: AdminServiceSettingResolverService
        }
      }
    ],
  }
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SystemSettingsRoutingModule { }
