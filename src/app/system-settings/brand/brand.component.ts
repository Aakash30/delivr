import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { validateAllFormFields } from 'app/shared/utils/custom-validators';
import { ActivatedRoute } from '@angular/router';
import { BrandModelSettingService } from 'app/core/services/brand-model-setting.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { ErrorHandlerService } from 'app/core/services/error-handler.service';
import { UtilityService } from 'app/core/services/utility.service';
import { MessageService } from 'primeng/api';

interface Brand {
  name: string
  code: string
}

@Component({
  selector: 'app-brand',
  templateUrl: './brand.component.html',
  styleUrls: ['./brand.component.scss']
})
export class BrandComponent implements OnInit {

  brands: any[];

  isSubmitted: boolean;

  brandForm: FormGroup;

  modalDisplayFlag: boolean = false;
  total: number = 0;
  offset: number;

  keyword: string = "";
  modalTitle: string = "";
  modalButton: string = "";

  private _unsubscribe = new Subject<Boolean>();

  constructor(private _route: ActivatedRoute, private brandModelService: BrandModelSettingService,
    private errorHandler: ErrorHandlerService, public utility: UtilityService,
    private messageService: MessageService) { }

  ngOnInit() {

    const pageContent = this._route.snapshot.data.pageData.data;

    this.brands = pageContent.fetchBrands;
    this.total = pageContent.count;


    this.brandForm = new FormGroup({
      brand_id: new FormControl(null),
      brand_name: new FormControl('', [Validators.required])
    })
  }

  /**
   * opens up form for add/edit brand
   * @param brandData defines the selected brand specially used in case of editing data
   */
  showModal(brandData?) {

    if (brandData) {
      this.modalTitle = "Edit Brand";
      this.modalButton = "Update Brand";
      let formData = {
        brand_id: brandData.brand_id,
        brand_name: brandData.brand_name
      };

      this.brandForm.patchValue(formData);
    } else {
      this.modalTitle = "Add Brand";
      this.modalButton = "Add Brand";
    }

    this.modalDisplayFlag = true;
  }

  /**
   * loads list of brands. Specially used for filters and pagination
   */
  listing() {
    let data = {};

    if (this.keyword.trim() != "") {
      data["keyword"] = this.keyword.trim();
    }

    if (this.offset != 0) {
      data["offset"] = this.offset;
    }

    this.brandModelService.fetchBrandList(data).pipe(takeUntil(this._unsubscribe)).subscribe((success: any) => {

      this.brands = success.data.fetchBrands;
      this.total = success.data.count;

    }, error => {
      this.errorHandler.routeAccordingToError(error);
    });
  }
  /**
   * Submits the add brand form. This is used to add / update brand name
   */
  onSubmit() {
    this.isSubmitted = true;
    if (this.brandForm.valid) {
      let dataToSend = this.brandForm.value;

      this.brandModelService.addUpdateBrandData(dataToSend).pipe(takeUntil(this._unsubscribe)).subscribe((success: any) => {

        this.messageService.add({
          severity: "success",
          summary: "Success",
          detail: success.message
        });
        this.resetForm();
        this.listing();
      }, error => {
        this.errorHandler.routeAccordingToError(error);
      });
    }
    else {
      validateAllFormFields(this.brandForm);
    }
  }

  resetForm() {
    this.modalDisplayFlag = false;
    this.brandForm.reset();
  }

  /** key searching */
  searchText(event) {
    if (event.charCode == 13) {
      this.keyword = event.target.value;
      this.listing();
    }
  }

  /** clears category input */
  clearInput(event) {
    this.keyword = event;
    this.listing();
  }

  /**pagination */
  paginate(event) {
    if (this.offset != undefined) {
      this.offset = parseInt(event.first);
      this.listing();
    } else {
      this.offset = parseInt(event.first);
    }
  }

  get brandModule() {
    return this.brandForm.controls;
  }

  ngOnDestroy(): void {
    this._unsubscribe.next(true);
    this._unsubscribe.complete();

  }

}
