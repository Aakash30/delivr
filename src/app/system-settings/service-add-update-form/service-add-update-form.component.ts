import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder, FormArray } from '@angular/forms';
import { noWhitespaceValidator, validateAllFormFields } from 'app/shared/utils/custom-validators';
import { UtilityService } from 'app/core/services/utility.service';
import { ActivatedRoute, Router } from '@angular/router';
import { empty, Subject } from 'rxjs';
import { AdminServiceSettingsService } from 'app/core/services/admin-service-settings.service';
import { ErrorHandlerService } from 'app/core/services/error-handler.service';
import { MessageService, ConfirmationService } from 'primeng/api';
import { map, takeUntil } from 'rxjs/operators';

interface Category {
  name: string;
  code: string;
  status: string;
}

interface Country {
  name: string;
  code: string;
  status: string;
}


interface Brands {
  name: string;
  code: string;
}

interface Model {
  name: string;
  code: string;
  brand_id: number;
}

@Component({
  selector: 'app-service-add-update-form',
  templateUrl: './service-add-update-form.component.html',
  styleUrls: ['./service-add-update-form.component.scss']
})
export class ServiceAddUpdateFormComponent implements OnInit, OnDestroy {

  formBuilder: FormBuilder = new FormBuilder();

  categories: Category[];
  selectedCategory: Category;

  countries: Country[];
  selectedCountry: Country;

  brands: Brands[];
  selectedBranch: Brands;
  showFormFlag: boolean = true;

  models: Model[];
  selectedModel: Model;

  arrayModel: any = [];

  sectionTitle: string;
  buttonName: string;

  isSubmitted: boolean = false;
  countryBranchFlag: boolean = true;
  serviceUpdateForm: FormGroup;
  selectedValue: string = 'Paid';

  public servicePricing: FormArray;
  public serviceListForm: FormGroup;
  serviceData;

  private _unsubscribe = new Subject<boolean>();

  constructor(
    public _utitlity: UtilityService,
    private route: ActivatedRoute,
    private router: Router,
    private serviceSetting: AdminServiceSettingsService,
    private errorHandling: ErrorHandlerService,
    private messageService: MessageService,
    private _confirmationService: ConfirmationService
  ) {
    this.sectionTitle = route.snapshot.data['sectionTitle'];
    this.buttonName = route.snapshot.data['buttonName'];
  }

  ngOnInit() {

    const pageContent = this.route.snapshot.data.pageData.data;

    //An array of categories
    this.categories = this._utitlity.arrayOfObjectToConvertInDropdownFormat(pageContent.serviceCategory, "service_category_name", "service_category_id", "status", "off");

    //An array of countries
    this.countries = this._utitlity.arrayOfObjectToConvertInDropdownFormat(pageContent.country, "country", "country_id", "status", "inActive");

    //An array of branches
    this.brands = this._utitlity.arrayOfObjectToConvertInDropdownFormat(pageContent.vehicleBrand, "brand_name", "brand_id");

    //An array of models
    this.models = pageContent.vehicleModel;


    this.serviceListForm = this.formBuilder.group({
      list_id: null,
      service_category: new FormControl(null, [Validators.required]),
      service_name: new FormControl('', [Validators.required, noWhitespaceValidator]),
      type: new FormControl("paid"),
      servicePricing: this.formBuilder.array([])
    });

    if (pageContent.serviceData) {
      this.serviceData = pageContent.serviceData;
      // let category = this.filterFromPredefinedList(this.categories, "service_category_id", this.serviceData.service_category)
      // console.log(category);
      // this.serviceData.service_category = category[0];
      let index = 0;
      this.serviceData.servicePricing.forEach(service => {
        this.servicePricing = this.serviceListForm.get(
          'servicePricing'
        ) as FormArray;
        this.servicePricing.push(this.initItemsForm());
        this.arrayModel[index] = (this._utitlity.arrayOfObjectToConvertInDropdownFormat(this.models, "model_name", "model_id"));
        index++;
      });
      this.serviceListForm.patchValue(this.serviceData);
    } else {
      this.addMoreItems();
    }
  }

  initItemsForm() {
    // initialize our address
    return this.formBuilder.group({
      id: null,
      country: new FormControl(null, [Validators.required]),
      brand: new FormControl(null, [Validators.required]),
      model: new FormControl(null, [Validators.required]),
      engine_capacity: new FormControl('', [Validators.required]),
      price: new FormControl('', [Validators.required]),

    });

  }


  addMoreItems() {
    /** Cards Add / Remove */
    const control = <FormArray>this.serviceListForm.controls['servicePricing'];
    control.push(this.initItemsForm());
    this.arrayModel.push(this._utitlity.arrayOfObjectToConvertInDropdownFormat(this.models, "model_name", "model_id"));
  }

  removeItems(i: number) {
    // remove address from the list
    let control = <FormArray>this.serviceListForm.controls['servicePricing'];
    control.removeAt(i);
    this.arrayModel.splice(i, 1);

  }

  // submit function called
  onSubmit() {
    this.isSubmitted = true;

    if (this.serviceListForm.valid) {

      if (this.serviceListForm.dirty && this.serviceListForm.touched) {
        let data = Object.assign({}, this.serviceListForm.value);

        if (data.type == "paid") {
          if (data.servicePricing.length > 0) {

            let servicePrice = JSON.parse(JSON.stringify(data.servicePricing));

            for (let i = 0; i < data.servicePricing.length; i++) {
              if (servicePrice[i].id == null) {
                delete servicePrice[i].id;
              }

            }

            data.servicePricing = servicePrice;

            this.serviceSetting.addUpdateServiceData(data).pipe(
              map((success: any) => {
                if (success) {
                  return success;
                }
              }),
              takeUntil(this._unsubscribe)
            ).subscribe((success: any) => {

              this.messageService.add({
                severity: "success",
                summary: "Action completed",
                detail: success.message
              });
              this.router.navigateByUrl('/admin/service-list');
            }, error => {
              this.errorHandling.routeAccordingToError(error);
            })
          } else {
            this.messageService.add({
              severity: "error",
              summary: "Error",
              detail: "Add atleast one pricing"
            });
          }
        }
      } else {
        this.messageService.add({
          severity: "success",
          summary: "Action completed",
          detail: "Service has been updated successfully."
        });
        this.router.navigateByUrl('/admin/service-list');
      }
    } else {
      validateAllFormFields(this.serviceListForm);

    }

  }

  onChange(event) {

    this.countryBranchFlag = true;
    // const control = <FormArray>this.serviceListForm.controls['servicePricing'];
    // control.push(this.initItemsForm());
    if (this.serviceListForm.controls.type.value == "free") {
      this.showFormFlag = false;
      let formArrayLength = this.serviceListForm.controls['servicePricing']['controls'].length;
      for (formArrayLength; formArrayLength > 0; formArrayLength--) {
        this.removeItems(formArrayLength - 1)
      }


    } else if (this.serviceListForm.controls.type.value == "paid") {
      this.showFormFlag = true;
      this.addMoreItems();
    }
  }

  resetForm() {

    if (this.serviceListForm.dirty && this.serviceListForm.touched) {

      this._confirmationService.confirm({
        message: 'You have some unsaved changes.',
        header: 'Confirmation',
        reject: () => {
          this.router.navigateByUrl('/admin/service-list');
        },
        rejectLabel: "Discard Changes",
        accept: () => {
          this.onSubmit();
        },
        acceptLabel: "Save & Close"
      });
    } else {
      this.router.navigateByUrl('/admin/service-list');
    }
  }


  disableOptionClick(isDisabledOption) {
    if (isDisabledOption) {
      event.stopPropagation();
    }
  }

  onChangeBrand(event, index) {
    //modelsFromServer
    let brand_selected = event.value;
    let filteredModel = this.models.filter((element) => {
      return element.brand_id == brand_selected;
    });
    this.arrayModel[index] = this._utitlity.arrayOfObjectToConvertInDropdownFormat(filteredModel, "model_name", "model_id");
  }

  ngOnDestroy() {
    this._unsubscribe.next(true);
    this._unsubscribe.complete();
  }
}
