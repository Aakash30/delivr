import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServiceAddUpdateFormComponent } from './service-add-update-form.component';

describe('ServiceAddUpdateFormComponent', () => {
  let component: ServiceAddUpdateFormComponent;
  let fixture: ComponentFixture<ServiceAddUpdateFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ServiceAddUpdateFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServiceAddUpdateFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
