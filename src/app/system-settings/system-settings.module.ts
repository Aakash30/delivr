import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SystemSettingsRoutingModule } from './system-settings-routing.module';
import { ServiceCategoryComponent } from './service-category/service-category.component';

import { SharedModule } from 'app/shared/shared.module';
import { ServicesComponent } from './services/services.component';
import { ServiceAddUpdateFormComponent } from './service-add-update-form/service-add-update-form.component';
import { BrandComponent } from './brand/brand.component';
import { CountryModule } from 'app/country/country.module';
import { BrandDetailsComponent } from './brand-details/brand-details.component';



@NgModule({
  imports: [
    CommonModule,
    SystemSettingsRoutingModule,
    SharedModule,
    CountryModule,

  ],
  declarations: [ServiceCategoryComponent, ServicesComponent, ServiceAddUpdateFormComponent, BrandComponent, BrandDetailsComponent]
})
export class SystemSettingsModule { }
