import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { validateAllFormFields } from 'app/shared/utils/custom-validators';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { UtilityService } from 'app/core/services/utility.service';
import { BrandModelSettingService } from 'app/core/services/brand-model-setting.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ErrorHandlerService } from 'app/core/services/error-handler.service';
import { MessageService } from 'primeng/api';


@Component({
  selector: 'app-brand-details',
  templateUrl: './brand-details.component.html',
  styleUrls: ['./brand-details.component.scss']
})
export class BrandDetailsComponent implements OnInit {


  modalDisplayFlag: boolean;
  brandDetails: any[];
  isSubmitted: boolean;
  modelForm: FormGroup;
  total: number = 0;
  offset: number = 0;
  keyword: string = "";
  sectionTitle: string = "";
  modalTitle: string = "";
  modalButton: string = "";

  brand;
  private _unsubscribe = new Subject<Boolean>();

  constructor(
    private _route: ActivatedRoute, private _location: Location, public utilityService: UtilityService, private _brandModelService: BrandModelSettingService, private errorHandler: ErrorHandlerService, private messageService: MessageService
  ) { }

  ngOnInit() {

    const pageContent = this._route.snapshot.data.pageData.data;

    this.brandDetails = pageContent.modelList;
    this.total = pageContent.count;
    this.sectionTitle = pageContent.brandName;
    this.brand = this._route.snapshot.params.id;


    this.modelForm = new FormGroup({
      model_id: new FormControl(null),
      model_name: new FormControl('', [Validators.required]),
      brand_id: new FormControl(this.utilityService.base64Decode(this.brand))
    });

  }

  /**
   * opens up form for add/edit model
   * @param modelData defines the selected model specially used in case of editing data
   */
  showDialog(modelData?) {
    if (modelData) {
      this.modalTitle = "Edit Model";
      this.modalButton = "Update Model";
      let formData = {
        model_id: modelData.model_id,
        model_name: modelData.model_name,
        brand_id: this.utilityService.base64Decode(this.brand)
      };

      this.modelForm.patchValue(formData);

    } else {
      this.modalTitle = "Add Model";
      this.modalButton = "Add Model";
    }

    this.modalDisplayFlag = true;
  }

  /**
   * list down all the model list based on selected brand
   */
  listing() {

    let data = {};

    if (this.keyword.trim() != "") {
      data["keyword"] = this.keyword.trim();
    }

    if (this.offset != 0) {
      data["offset"] = this.offset;
    }

    this._brandModelService.getBrandModelDetails(this.brand, data).pipe(takeUntil(this._unsubscribe)).subscribe((success: any) => {
      this.brandDetails = success.data.modelList;
      this.total = success.data.count;
    }, error => {
      this.errorHandler.routeAccordingToError(error);
    });
  }

  /** key searching */
  searchText(event) {
    if (event.charCode == 13) {

      this.keyword = event.target.value;

      this.listing();
    }

  }
  /** clears category input */
  clearInput(event) {
    this.keyword = event;
    this.listing();
  }

  /**pagination */
  paginate(event) {
    this.offset = event.first;
    this.listing();
  }

  /**
   * Submits add form for adding / updating model name
   */
  onSubmit() {
    this.isSubmitted = true;
    if (this.modelForm.valid) {
      let data = this.modelForm.value;
      this._brandModelService.addUpdateModelData(data).pipe(takeUntil(this._unsubscribe)).subscribe((success: any) => {
        this.messageService.add({
          severity: "success",
          summary: "Success",
          detail: success.message
        });
        this.resetForm();
        this.listing();
      }, error => {
        this.errorHandler.routeAccordingToError(error);
      })
    }
    else {
      validateAllFormFields(this.modelForm);
    }
  }

  resetForm() {
    this.modalDisplayFlag = false;
    this.modelForm.reset();
  }

  previousPage() {
    this._location.back();
  }

  get modelModule() {
    return this.modelForm.controls;
  }

}
