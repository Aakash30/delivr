import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RequestForModuleListComponent } from 'app/business-user/request-for-module/pages/request-for-module-list/request-for-module-list.component';
import { RequestForNewVehicleListingComponent } from './pages/request-for-new-vehicle-listing/request-for-new-vehicle-listing.component';
import { VehicleManagementResolverService } from 'app/core/resolver/vehicle-management-resolver.service';
import { NewVehicleResolverService } from 'app/core/resolver/new-vehicle-resolver.service';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        component: RequestForNewVehicleListingComponent,
        data: {
          sectionTitle: 'Request for New Vehicle',
        },
        resolve: {
          pageData: NewVehicleResolverService
        }
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RequestForNewVehicleRoutingModule { }
