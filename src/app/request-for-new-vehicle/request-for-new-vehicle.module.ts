import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RequestForNewVehicleRoutingModule } from './request-for-new-vehicle-routing.module';
import { RequestForNewVehicleListingComponent } from './pages/request-for-new-vehicle-listing/request-for-new-vehicle-listing.component';
import { SharedModule } from 'app/shared/shared.module';


@NgModule({
  imports: [
    CommonModule,
    RequestForNewVehicleRoutingModule,
    SharedModule
  ],
  declarations: [RequestForNewVehicleListingComponent]
})
export class RequestForNewVehicleModule { }
