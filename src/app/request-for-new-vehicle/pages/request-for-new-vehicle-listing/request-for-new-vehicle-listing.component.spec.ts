import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestForNewVehicleListingComponent } from './request-for-new-vehicle-listing.component';

describe('RequestForNewVehicleListingComponent', () => {
  let component: RequestForNewVehicleListingComponent;
  let fixture: ComponentFixture<RequestForNewVehicleListingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequestForNewVehicleListingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestForNewVehicleListingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
