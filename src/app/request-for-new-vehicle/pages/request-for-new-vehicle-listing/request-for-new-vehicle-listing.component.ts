import { Component, OnInit, OnDestroy } from "@angular/core";
import { MenuItem, MessageService } from "primeng/api";
import { FormGroup, FormControl } from "@angular/forms";
import { trigger, transition, style, animate } from "@angular/animations";
import { ActivatedRoute } from "@angular/router";
import { VehicleService } from "app/core/services/vehicle.service";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { UtilityService } from "app/core/services/utility.service";
import { NewVehicleService } from "app/core/services/new-vehicle.service";
import { ErrorHandlerService } from "app/core/services/error-handler.service";

interface Status {
  value: string;
  label: string;
}

@Component({
  selector: "app-request-for-new-vehicle-listing",
  templateUrl: "./request-for-new-vehicle-listing.component.html",
  styleUrls: ["./request-for-new-vehicle-listing.component.scss"],
  animations: [
    trigger("openClose", [
      transition(":enter", [
        style({ transform: "translateX(20%)", opacity: 0 }),
        animate("500ms", style({ transform: "translateX(0)", opacity: 1 })),
      ]),
      transition(":leave", [
        style({ transform: "translateX(0)", opacity: 1, display: "none" }),
        animate("500ms", style({ transform: "translateX(20%)", opacity: 0 })),
      ]),
    ]),
  ],
})
export class RequestForNewVehicleListingComponent implements OnInit, OnDestroy {
  status: Status[];
  selectedStatus: Status[];

  statusList: string = "";
  offset: number;
  keyword: string = "";
  today = new Date();

  requestVehicleData: any;
  total: string;
  dateValue1;
  dateValue2;
  requestedRaised;

  requestStatus: Status[];
  selectedRequestStatus: any = [];

  vehicleList: any;

  isSubmitted: Boolean = false;
  requestRepairForm: FormGroup;
  // show filter function
  isVisible: boolean = false;

  showFilter() {
    this.isVisible = !this.isVisible;
  }

  serviceListFromAPI: string[];
  statusListFromAPI: string[];

  private _unsbscribe = new Subject<Boolean>();

  constructor(
    private _route: ActivatedRoute,
    private _vehicleService: NewVehicleService,
    private _utility: UtilityService,
    private _messageService: MessageService,
    private _errorHandling: ErrorHandlerService
  ) {}

  ngOnInit() {
    this.vehicleList = this._route.snapshot.data.pageData.data;
    this.requestVehicleData = this.vehicleList.vehicleRequestList;

    this.total = this.vehicleList.total;

    this.requestRepairForm = new FormGroup({
      requested_raised_on: new FormControl(null),
      status: new FormControl(null),
    });

    this.statusListFromAPI = ["Pending", "Finished", "Rejected"];
    this.status = this._utility.arrayOfStringsToArrayOfObjects(
      this.statusListFromAPI
    );

    this.requestStatus = [
      { value: "pending", label: "Pending" },
      { value: "finished", label: "Finished" },
      { value: "rejected", label: "Rejected" },
      { value: "custom", label: "Custom" },
    ];

    if (this.requestVehicleData.length > 0) {
      this.requestVehicleData.forEach((item, index) => {
        this.selectedRequestStatus.push(item.status);
      });
    }
  }

  paginate(event) {
    if (this.offset != undefined) {
      this.offset = parseInt(event.first);
      this.listing();
    } else {
      this.offset = parseInt(event.first);
    }
  }

  listing() {
    this._utility.loaderStart();
    let data: any = {};

    if (this.statusList != "") {
      data.status = this.status;
    }

    if (this.offset != 0) {
      data.offset = this.offset;
    }

    if (this.keyword.trim() != "") {
      data.keyword = this.keyword.trim();
    }

    if (
      this.requestRepairForm.controls.requested_raised_on.value ||
      (this.requestRepairForm.controls.requested_raised_on.value != "" &&
        this.requestRepairForm.controls.requested_raised_on.value != null)
    ) {
      data[
        "requested_raised_on"
      ] = this.requestRepairForm.controls.requested_raised_on.value.toISOString();
    }

    if (this.requestRepairForm.controls.status.value != null) {
      data["status"] = this.requestRepairForm.controls.status.value;
    }

    this._vehicleService
      .getFetchVehicleRequests(data)
      .pipe(takeUntil(this._unsbscribe))
      .subscribe(
        (success: any) => {
          this.requestVehicleData = [];
          this.requestVehicleData = success.data.vehicleRequestList;
          this.requestVehicleData.forEach((item, index) => {
            this.selectedRequestStatus.push(item.status);
          });
          this.total = success.data.total;
          this._utility.loaderStop();
        },
        (error) => {
          this._errorHandling.routeAccordingToError(error);
          this._utility.loaderStop();
        }
      );
  }

  disableOptionClick(isDisabledOption) {
    if (isDisabledOption) {
      event.stopPropagation();
    }
  }

  onSubmit() {
    this.isSubmitted = true;
    if (this.requestRepairForm.dirty && this.requestRepairForm.touched) {
      this.listing();

      this.isSubmitted = false;
      this.isVisible = false;
    } else {
      this.isVisible = false;
    }
  }

  onChange(event, requestId, status) {
    this._utility.loaderStart();
    let data = {
      status: event.value,
    };
    this._vehicleService
      .performActionOnRequest(requestId, data)
      .pipe(takeUntil(this._unsbscribe))
      .subscribe(
        (success: any) => {
          this.listing();
          this._messageService.add({
            severity: "success",
            summary: "Status Changed",
            detail: `${success.message}`,
          });

          this._utility.loaderStop();
        },
        (error) => {
          this._errorHandling.routeAccordingToError(error);
          this._utility.loaderStop();
        }
      );
  }

  clearFilter() {
    this.requestRepairForm.reset();
    this.listing();
    this.isSubmitted = false;
    this.isVisible = false;
  }

  /** key searching */
  searchText(event) {
    if (event.charCode == 13) {
      this.keyword = event.target.value;
      this.offset = 0;
      this.listing();
    }
  }

  /** clears category input */
  clearInput(event) {
    this.keyword = event;
    this.listing();
  }

  ngOnDestroy() {
    this._unsbscribe.next(true);
    this._unsbscribe.complete();
  }
}
