import { Component, OnInit } from '@angular/core';

interface Status {
  name: string;
  code: string;
}

@Component({
  selector: 'app-request-for-module-details',
  templateUrl: './request-for-module-details.component.html',
  styleUrls: ['./request-for-module-details.component.scss']
})
export class RequestForModuleDetailsComponent implements OnInit {

  addDisplay: Boolean = false;
  checked: boolean;

  status: Status[];
  selectedStatus: Status[];

  constructor() { }

  ngOnInit() {

    // Status dropdown list
    this.status = [
      { name: 'Pending', code: ' P' },
      { name: 'Outgoing', code: 'OG' },
      { name: 'Finished', code: 'FIN' },
    ];
  }

  // show dialog
  showDialog() {
    this.addDisplay = true;
  }

  // close dialog
  resetForm() {
    this.addDisplay = false;
  }



}
