import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { trigger, transition, style, animate } from '@angular/animations';
import { ActivatedRoute } from '@angular/router';
import { RequestModuleService } from 'app/core/services/request-module.service';
import { Subject } from 'rxjs';
import { takeUntil, map } from 'rxjs/operators';
import { parse } from 'querystring';

@Component({
  selector: 'app-request-for-module-list',
  templateUrl: './request-for-module-list.component.html',
  styleUrls: ['./request-for-module-list.component.scss'],
  animations: [
    trigger('openClose', [
      transition(':enter', [
        style({ transform: 'translateX(20%)', opacity: 0 }),
        animate('500ms', style({ transform: 'translateX(0)', opacity: 1 })),
      ]),
      transition(':leave', [
        style({ transform: 'translateX(0)', opacity: 1, display: 'none' }),
        animate('500ms', style({ transform: 'translateX(20%)', opacity: 0 })),
      ]),
    ])
  ],
})
export class RequestForModuleListComponent implements OnInit {

  requestList: any[];
  isSubmitted: boolean = false;
  isVisible: boolean = false;
  keyword: string = "";
  total: number = 0;
  offset: number;

  private _unsubscribe = new Subject<Boolean>();

  constructor(private route: ActivatedRoute, private requestModuleService: RequestModuleService) { }

  ngOnInit() {
    const pageContent = this.route.snapshot.data.pageData.data;
    this.requestList = pageContent.listData;
    this.total = pageContent.total;

  }


  listing() {
    let data = {};

    if (this.keyword.trim() != "") {
      data["keyword"] = this.keyword.trim();
    }

    if (this.offset != 0) {
      data["offset"] = this.offset;
    }
    this.requestModuleService.getFetchContactRequestsForAdmin(data).pipe(map((success: any) => {
      if (success) {
        return success.data;
      }
    }), takeUntil(this._unsubscribe)).subscribe((success: any) => {
      this.requestList = success.listData;
      this.total = success.total;

    })
  }

  paginate(event) {
    if (this.offset != undefined) {
      this.offset = parseInt(event.first);
      this.listing();
    } else {
      this.offset = parseInt(event.first);
    }
  }

  searchText(event) {

    this.keyword = event.target.value;
    if (event.charCode == 13) {
      // on click of enter
      this.listing();
    }
  }

  clearInput(event) {
    this.keyword = event;
    this.listing();
  }
  ngOnDestroy() {
    this._unsubscribe.next(true);
    this._unsubscribe.complete();
  }

}
