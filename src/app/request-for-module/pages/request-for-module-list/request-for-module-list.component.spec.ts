import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestForModuleListComponent } from './request-for-module-list.component';

describe('RequestForModuleListComponent', () => {
  let component: RequestForModuleListComponent;
  let fixture: ComponentFixture<RequestForModuleListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequestForModuleListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestForModuleListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
