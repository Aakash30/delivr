import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RequestForModuleListComponent } from './pages/request-for-module-list/request-for-module-list.component';
import { RequestForModuleDetailsComponent } from './pages/request-for-module-details/request-for-module-details.component';
import { RequestModuleResolverService } from 'app/core/resolver/request-module-resolver.service';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        component: RequestForModuleListComponent,
        data: {
          sectionTitle: 'Request for Module',
        },
        resolve: {
          pageData: RequestModuleResolverService
        },
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RequestForModuleRoutingModule { }
