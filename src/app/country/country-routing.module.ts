import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CountriesComponent } from './pages/countries/countries.component';
import { CountryDetailsComponent } from './pages/country-details/country-details.component';
import { CityDetailsComponent } from './pages/city-details/city-details.component';
import { AuthGuardService as AuthGuard } from './../core/guards/auth-guard.service';
import { RolesGuardService as RoleGuard } from './../core/guards/roles-guard.service';
import { CountryResolverService } from 'app/core/resolver/country-resolver.service';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'country/details/:id',
        component: CountryDetailsComponent,
        canActivate: [AuthGuard, RoleGuard],
        data: {
          onlyForUser: ['super_admin'],
          expectedRole: ['location-setting']
        },
        resolve: {
          pageData: CountryResolverService
        }
      },
      {
        path: 'country/city-details/:id',
        component: CityDetailsComponent,
        canActivate: [AuthGuard, RoleGuard],
        data: {
          onlyForUser: ['super_admin'],
          expectedRole: ['location-setting']
        },
        resolve: {
          pageData: CountryResolverService
        }
      }

    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CountryRoutingModule { }
