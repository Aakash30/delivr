import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CountryRoutingModule } from './country-routing.module';
import { CountriesComponent } from './pages/countries/countries.component';
import { SharedModule } from 'app/shared/shared.module';
import { CountryDetailsComponent } from './pages/country-details/country-details.component';
import { CityDetailsComponent } from './pages/city-details/city-details.component';
@NgModule({
  imports: [
    CommonModule,
    CountryRoutingModule,
    SharedModule
  ],

  declarations: [
    CountriesComponent, CountryDetailsComponent, CityDetailsComponent
  ]
})
export class CountryModule { }
