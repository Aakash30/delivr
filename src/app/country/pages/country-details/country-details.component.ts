import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { Location } from '@angular/common';
import {
  validateAllFormFields,
  noWhitespaceValidator,
  blankSpaceInputNotValid
} from "../../../shared/utils/custom-validators";
import { CountryService } from "app/core/services/country-city.service";
import { NgxUiLoaderService } from "ngx-ui-loader";
import { MessageService } from "primeng/api";
import { ActivatedRoute, Router } from "@angular/router";
import { UtilityService } from "app/core/services/utility.service";
import { ErrorHandlerService } from "app/core/services/error-handler.service";

@Component({
  selector: "app-country-details",
  templateUrl: "./country-details.component.html",
  styleUrls: ["./country-details.component.scss"]
})
export class CountryDetailsComponent implements OnInit {
  cars: any[];
  id: number;
  list: any;
  display: Boolean = false;
  isSubmitted: Boolean = false;
  posibleValue: string[] = ['active', 'inActive'];
  public error;
  keyword: string = "";
  total: number = 0;
  offset: number = 0;
  sectionTitle: string = "";

  showDialog() {
    this.display = true;
  }

  check_login() {
    this.error = "";
  }

  editCountryForm = new FormGroup({
    cityName: new FormControl("", [
      Validators.required,
      Validators.pattern(/^[a-z A-Z ]*$/),
      noWhitespaceValidator,
      blankSpaceInputNotValid
    ])
  });

  resetForm() {
    this.isSubmitted = false;
    this.display = false;
    this.editCountryForm.reset();
  }

  onSubmit() {
    this.isSubmitted = true;
    if (this.editCountryForm.valid) {
      this.cs.addCity(this.id, this.editCountryForm.value).subscribe(
        (success: any) => {
          this.messageService.add({
            severity: "success",
            summary: "City has been added",
            detail: ""
          });
          this.listCity();
        },
        error => {
          // console.log("city addition error " + JSON.stringify(error));

          this.errorHandle.routeAccordingToError(error);
        }
      );

      //this.editCountryForm.reset();
      this.display = false;
    } else {
      validateAllFormFields(this.editCountryForm);
    }
  }
  constructor(
    private cs: CountryService,
    private _location: Location,
    private loader: NgxUiLoaderService,
    private messageService: MessageService,
    private route: ActivatedRoute,
    private router: Router,
    private utility: UtilityService,
    private errorHandle: ErrorHandlerService
  ) {
    this.id = utility.base64Decode(route.snapshot.paramMap.get("id"));
    this.sectionTitle = this.route.snapshot.data.pageData.data.country;

  }

  ngOnInit() {

    this.list = this.route.snapshot.data.pageData.data.cityList;
    this.total = this.route.snapshot.data.pageData.data.count;

  }

  previousPage() {
    this.router.navigate(['/admin/country']);
  }

  listCity() {
    this.loader.start();

    let data = {};
    if (this.keyword.trim() != "") {
      data["keyword"] = this.keyword;
    }

    if (this.offset != 0) {
      data["offset"] = this.offset;
    }
    this.cs.fetchCountryDetails(this.id, data).subscribe(
      (success: any) => {

        this.list = success.data.cityList;
        this.total = success.data.count;

        this.loader.stop();
      },
      error => {
        this.messageService.add({
          severity: "error",
          summary: "fetching failed",
          detail: `${error}`
        });
        this.errorHandle.routeAccordingToError(error);
        this.loader.stop();
      }
    );
  }
  changeStatus(status, id) {

    this.cs.changeCityStatus(id, status).subscribe((success: any) => {
      this.messageService.add({
        severity: 'success',
        summary: success.message,
        detail: 'status changed'
      });
      this.listCity();
    }, error => {
      this.errorHandle.routeAccordingToError(error);
    });
  }

  searchText(event) {
    this.keyword = event.target.value;
    if (event.charCode == 13) {
      // on click of enter
      this.listCity();
    }

  }

  paginate(event) {
    this.offset = parseInt(event.first);

    this.listCity();

  }

  get cityForm() {
    return this.editCountryForm.controls;
  }
}
