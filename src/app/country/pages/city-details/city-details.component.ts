import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { validateAllFormFields, noWhitespaceValidator, blankSpaceInputNotValid } from '../../../shared/utils/custom-validators';
import { CountryService } from 'app/core/services/country-city.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { MessageService } from 'primeng/api';
import { ActivatedRoute, Router } from '@angular/router';
import { ErrorHandlerService } from 'app/core/services/error-handler.service';
import { UtilityService } from 'app/core/services/utility.service';

@Component({
  selector: 'app-city-details',
  templateUrl: './city-details.component.html',
  styleUrls: ['./city-details.component.scss']
})
export class CityDetailsComponent implements OnInit {
  display: Boolean = false;
  isSubmitted: Boolean = false;
  public error;
  cityDetailsForm: FormGroup;
  submitData: any;
  id: any;
  list: any;
  posibleValue: string[] = ['active', 'inActive'];

  showDialog(name) {
    this.cityDetailsForm.controls['cityName'].setValue(name);
    this.display = true;
  }
  constructor(
    private cs: CountryService,
    private loader: NgxUiLoaderService,
    private messageService: MessageService,
    private route: ActivatedRoute,
    private _router: Router,
    private _utility: UtilityService,
    private errorHandling: ErrorHandlerService) {

    this.id = route.snapshot.paramMap.get("id");
  }

  ngOnInit() {
    this.loader.start();
    this.cityDetailsForm = new FormGroup({
      cityName: new FormControl('', [Validators.required, Validators.pattern(/^[a-z A-Z]*$/), noWhitespaceValidator, blankSpaceInputNotValid]),
    });

    this.list = this.route.snapshot.data.pageData.data.cityData;

    let c = 0;
    this.list.businessBranch.forEach(branch => {
      if (branch.business.vehicleMapping[0]) c += parseInt(branch.business.vehicleMapping[0].count);
    });
    this.list.c = c;

    this.loader.stop();
  }

  check_input() {
    this.error = '';
  }

  resetForm() {

    this.cityDetailsForm.reset();
    this.isSubmitted = false;
    this.display = false;
  }
  editCity(name, id, country) {
    this.cityDetailsForm.controls['cityName'].setValue(name);
    this.submitData = {
      "country_id": country,
      "city_id": id
    }
    this.showDialog(name);
  }

  onSubmit() {
    this.isSubmitted = true;
    if (this.cityDetailsForm.valid) {
      this.submitData.city = this.cityDetailsForm.value.cityName;
      this.cs.updateCity(this.submitData).subscribe((success: any) => {
        this.messageService.add({
          severity: 'success',
          summary: success.message,
          detail: 'City updated'
        })
        window.location.reload();
      }, error => {
        this.errorHandling.routeAccordingToError(error);
      });

      this.cityDetailsForm.reset();
      this.isSubmitted = false;
      this.display = false;
      this.error = '';
    }
    else {
      validateAllFormFields(this.cityDetailsForm);
    }
  }
  changeStatus(status, id) {

    this.cs.changeCityStatus(id, status).subscribe((success: any) => {
      this.messageService.add({
        severity: 'success',
        summary: success.message,
        detail: 'status changed'
      })
    }, error => {
      this.errorHandling.routeAccordingToError(error);
    });
  }

  previousPage() {
    this._router.navigate(['/admin/country/details', this._utility.base64Encode(this.list.Country.country_id)]);
  }

  get cityDetail() {
    return this.cityDetailsForm.controls;
  }
}
