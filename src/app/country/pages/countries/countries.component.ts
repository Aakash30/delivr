import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { validateAllFormFields, noWhitespaceValidator, blankSpaceInputNotValid } from '../../../shared/utils/custom-validators';
import { CountryService } from "app/core/services/country-city.service";
import { FileUploadService } from "app/core/services/file-upload.service";
import { ActivatedRoute, Router } from "@angular/router";
import { NgxUiLoaderService } from "ngx-ui-loader";
import { MessageService } from "primeng/api";
import { UtilityService } from 'app/core/services/utility.service';
import { jsondata } from '../../../../countries';
import { ErrorHandlerService } from 'app/core/services/error-handler.service';

@Component({
  selector: 'app-countries',
  templateUrl: './countries.component.html',
  styleUrls: ['./countries.component.scss']
})
export class CountriesComponent implements OnInit {
  addDisplay: Boolean = false;
  editDisplay: Boolean = false;
  isSubmitted: Boolean = false;
  isFormSubmitted: Boolean = false;
  countrylist: any = jsondata;
  jsonlist: any = jsondata;
  countryjson: any;
  id: number;
  checked: boolean;
  searchTextFlag: boolean = false;
  searchValue: string = "";
  public error;
  addCountryForm: FormGroup;
  editCountryForm: FormGroup;

  vehicles: any[];
  selectedValue: string = 'Active';
  posibleValue: string[] = ['active', 'inActive'];
  countries: any[] = [{ cities: [''], businessBranch: [''] }];
  keyword: string = "";
  offset: number = 0;
  total: number = 0;

  editable: {
    id: number;
    name: string;
    currency: string;
    code: string;
  };
  addCountry() {
    this.addDisplay = true;
  }

  // editCountry(id, name, currency, code) {
  //   this.editCountryForm.controls['countryName'].setValue(name);
  //   this.editCountryForm.controls['id'].setValue(id);
  //   this.editCountryForm.controls['currency'].setValue(currency);
  //   this.editCountryForm.controls['country_code'].setValue(parseInt(code));
  //   this.editDisplay = true;


  //   // this.editable = {
  //   //   id: id,
  //   //   name: name,
  //   //   currency: currency,
  //   //   code: code
  //   // }

  // }

  check_login() {
    this.error = '';
  }

  constructor(
    private cs: CountryService,
    private loader: NgxUiLoaderService,
    private messageService: MessageService,
    private route: ActivatedRoute,
    private utility: UtilityService,
    private errorHandling: ErrorHandlerService) {
    this.id = +route.snapshot.paramMap.get("id");
  }

  resetForm() {
    this.addCountryForm.reset();
    this.isSubmitted = false;
    this.addDisplay = false;

  }

  ngOnInit() {
    this.countryjson = this.utility.countryJSONtoDropdown(this.jsonlist, "name");

    this.editable = {
      id: undefined,
      name: '',
      currency: '',
      code: ''
    };

    this.total = this.route.snapshot.data.pageData.data.count;

    this.addCountryForm = new FormGroup({
      country: new FormControl('', [Validators.required]),
      currency: new FormControl(''),
      country_code: new FormControl(''),
      alpha_code: new FormControl('')
    });

  }

  listing() {

    let data = {};

    if (this.keyword.trim() != "") {
      data["keyword"] = this.keyword;
    }

    if (this.offset != 0) {
      data["offset"] = this.offset;
    }

    this.utility.loaderStart();
    this.cs.fetchCountry(data).subscribe(
      (success: any) => {
        this.countries = success.data.countryList;
        this.total = success.data.count;

        this.utility.loaderStop();
      },
      error => {

        this.messageService.add({
          severity: "error",
          summary: "fetching failed",
          detail: `${error}`
        });
        this.utility.loaderStop();
      }
    );
  }

  updateform(values) {
    this.addCountryForm.controls['currency'].setValue(values.currencies[0].code);
    this.addCountryForm.controls['country_code'].setValue('+' + values.callingCodes[0]);
    this.addCountryForm.controls['alpha_code'].setValue(values.alpha2Code);

  }

  vehicleCount(branches) {
    let count = 0;

    branches.forEach(branch => {
      if (branch.business) {
        count += branch.business.vehicleMapping[0] ? parseInt(branch.business.vehicleMapping[0].count) : 0;
      }
    });
    return count;
  }

  clearInput(event) {
    this.keyword = event;
    this.listing();
  }

  length(arr) {
    return arr.length;
  }

  onSubmit() {
    this.isSubmitted = true;
    if (this.addCountryForm.valid) {
      this.addCountryForm.controls['country'].setValue(this.addCountryForm.value.country.name)

      this.cs.addCountry(this.addCountryForm.value).subscribe(
        (success: any) => {
          this.messageService.add({
            severity: "success",
            summary: "Country Added",
            detail: ""
          });

          this.resetForm();
          this.listing();
          this.loader.stop();
        },
        error => {
          this.errorHandling.routeAccordingToError(error)
          this.loader.stop();
        }
      );
    }
    else {
      validateAllFormFields(this.addCountryForm);
    }
  }

  numberOnly(event): boolean {

    const charCode = event.which ? event.which : event.keyCode;
    if (charCode > 31 && charCode !== 43 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  changeStatus(status, id) {

    this.cs.changeStatus(id, status).subscribe((success: any) => {
      this.messageService.add({
        severity: 'success',
        summary: "Status updated",
        detail: success.message
      })
    }, error => {
      // this.errorHandling.routeAccordingToError(error);
    });
  }

  onEditSubmit() {
    this.isFormSubmitted = true;
    if (this.editCountryForm.valid) {
      // console.log('edit form value: ' + JSON.stringify(this.editCountryForm.value));

      let data = {
        "country_id": this.editCountryForm.value.id,
        "country": this.editCountryForm.value.countryName,
        "currency": this.editCountryForm.value.currency,
        "country_code": '+' + this.editCountryForm.value.country_code
      }

      this.cs.updateCountry(data).subscribe(
        (success: any) => {

          this.messageService.add({
            severity: "success",
            summary: "Country Updated",
            detail: ""
          });
          //this.router.navigateByUrl(`${this.role}/dashboard`);
          this.loader.stop();
          this.resetForm();
        },
        error => {
          this.messageService.add({
            severity: "error",
            summary: "Country Updation failed",
            detail: `${error}`
          });
          this.loader.stop();
        }
      );

      this.editDisplay = false;
      this.isFormSubmitted = false;
    }
    else {
      validateAllFormFields(this.editCountryForm);
    }
  }

  get countryForm() {
    return this.addCountryForm.controls;
  }

  searchText(event) {
    this.keyword = event.target.value;
    if (event.charCode == 13) {
      // on click of enter
      this.listing();
    }

  }

  paginate(event) {
    this.offset = parseInt(event.first);
    //if (this.offset != 1) {
    this.listing();
    //}

  }

  get editForm() {
    return this.editCountryForm.controls;
  }

}
