import { NgModule } from '@angular/core';
import { RouterModule, Routes, PreloadAllModules } from '@angular/router';
import { Full_ROUTES } from './core/routes/full-layout.routes';
import { LANDING_ROUTES } from './core/routes/landing-layout.routes';
import { LandingLayoutComponent } from './layouts/landing/landing-layout.component';
import { HomeGuardService as HomeGuard } from "./core/guards/home-guard.service";
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';

const appRoutes: Routes = [
  //{ path: '', loadChildren: () => import('./auth/auth.module').then(m => m.AuthModule) },
  {
    path: "",
    component: LandingLayoutComponent,
    canActivate: [HomeGuard],
    data: { title: "landing" },
    children: LANDING_ROUTES
  },
  {
    path: "",
    data: { title: "full Views" },
    children: Full_ROUTES
  },
  {
    path: '**',
    component: PageNotFoundComponent
  }
];

@NgModule({

  imports: [RouterModule.forRoot(appRoutes, { scrollPositionRestoration: 'enabled'})],
  exports: [RouterModule]
})

export class AppRoutingModule {
}
