import { Component, AfterViewChecked, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from 'app/core/services/auth.service';
import { MessageService } from 'primeng/api';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { SharedDataService } from 'app/core/services/shared.data.service';
import { WebsocketService } from 'app/core/services/websocket.service';
import { takeUntil } from 'rxjs/operators';
import jwt_decode from "jwt-decode"
import { NotificationsService } from 'app/core/services/notifications.service';
@Component({
  selector: "app-navbar",
  templateUrl: "./navbar.component.html",
  styleUrls: ["./navbar.component.scss"]
})
export class NavbarComponent implements AfterViewChecked {
  toggleClass = "ft-maximize";
  placement = "bottom-right";
  public isCollapsed = true;
  userType: any;
  role: string;
  routerUrl: any;
  userRole: string;
  totalNotification: boolean = false;
  notificationList: any = [];
  loginData: any;

  constructor(
    private route: Router,
    private authService: AuthService,
    private messageService: MessageService,
    private loader: NgxUiLoaderService,
    private sharedDataService: SharedDataService,
    private activatedRoute: ActivatedRoute,
    private webSocketService: WebsocketService,
    private notificationService: NotificationsService
  ) {
    this.routerUrl = this.activatedRoute.snapshot.url;
  }

  ngOnInit() {
    this.webSocketService.notificationData.subscribe(res => {
      if (res.notificationDot) {
        this.totalNotification = res.notificationDot;
      }
      if (res.notification && res.notification.length > 0) {
        this.notificationList = res.notification;
      }
    });

    if (localStorage.getItem("token")) {
      const token = localStorage.getItem("token").replace("Bearer ", "");

      var decoded = jwt_decode(token);

      this.userRole = JSON.parse(decoded.roles);
      var user_type = decoded.type;
    }

    if (user_type === "super_admin") {
      this.userRole = "admin";
    } else if (user_type === "vendor") {
      this.userRole = "business";
    }
  }

  ngAfterViewChecked() {
    // setTimeout(() => {
    //     var wrapperDiv = document.getElementsByClassName("wrapper")[0];
    //     var dir = wrapperDiv.getAttribute("dir");
    //     if (dir === 'rtl') {
    //         this.placement = 'bottom-left';
    //     }
    //     else if (dir === 'ltr') {
    //         this.placement = 'bottom-right';
    //     }
    // }, 3000);
  }

  profilePage() {
    this.route.navigateByUrl(`/${this.userRole}/profile`);
  }

  ToggleClass() {
    if (this.toggleClass === "ft-maximize") {
      this.toggleClass = "ft-minimize";
    } else this.toggleClass = "ft-maximize";
  }

  logout() {
    // call api
    this.loader.start();
    this.authService.logout().subscribe(
      (success: any) => {
        console.log(success);
        this.messageService.add({
          severity: "success",
          summary: "Logout Successful",
          detail: "Login Page"
        });
        this.route.navigateByUrl(`/${this.userRole}/login`);
        localStorage.removeItem("token");
        this.webSocketService.disconnect();
        this.loader.stop();
      },
      error => {
        console.log(error);
        this.messageService.add({
          severity: "error",
          summary: "Logout Failed",
          detail: `${error}`
        });
        this.loader.stop();
      }
    );
  }

  showNotification() {
    this.route.navigateByUrl(`/${this.userRole}/notification`);
  }

  markAsRead() {
    this.notificationService.updateNotification().subscribe(res => {
      if (res["success"] == true) {
        this.totalNotification = false;
      }
    })
  }
}
