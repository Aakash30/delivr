import { Component, OnInit, Input } from '@angular/core';
import { MenuItem } from 'primeng/components/common/api';
@Component({
  selector: 'vendor-tabs-table-wrapper',
  template: `
    <!-- tabs wrapper -->
    <div class="tabs-wrapper">
      <p-tabMenu [model]="items" [activeItem]="activeItem" styleClass="prime-tabs-wrapper">
        <ng-template pTemplate="item" let-item let-i="index">
          <div (click)="getItem($event, i, item.label)">
            <div class="ui-menuitem-icon" [ngClass]="item.icon" *ngIf="item.icon" style="font-size: 2em"></div>
            <div class="ui-menuitem-text">
              {{item.label}}
            </div>
          </div>
        </ng-template>
      </p-tabMenu>
    </div>

    <!--All Vendors-->
    <div class="table-wrapper vehicle-wrapper" *ngIf="activeTab === 'all'">
      <p-table [value]="vendorList" styleClass="country-list-wrapper" [responsive]="true" [paginator]="true" [rows]="10"
        sortField="id" [sortOrder]="-1">
        <ng-template pTemplate="header">
          <tr>
            <th [pSortableColumn]="'serial'">Serial #
              <p-sortIcon [field]="'serial'"></p-sortIcon>
            </th>
            <th>Business Type</th>
            <th>Business Name</th>
            <th>Email Id</th>
            <th>Contact No.</th>
            <th>Location</th>
            <th>Status</th>
            <th>Action</th>
          </tr>
        </ng-template>
        <ng-template pTemplate="body" let-vendor>
          <tr>
            <td><span class="ui-column-title">Serial</span>{{vendor.serial}}</td>
            <td><span class="ui-column-title">Business Type</span>{{vendor.vendorType}}</td>
            <td><span class="ui-column-title">Business Name</span>{{vendor.vendorName}}</td>
            <td><span class="ui-column-title">Email Id</span>{{vendor.emailid}}</td>
            <td><span class="ui-column-title">Contact No.</span>{{vendor.contact}}</td>
            <td><span class="ui-column-title">Location</span>{{vendor.location}}</td>
            <td>
              <span class="ui-column-title">Status</span>
              {{vendor.status === "true" ? "Inactive" : "Active"}}
            </td>
            <td>
              <span class="ui-column-title">Action</span>
              <a class="success p-0" [routerLink]="['../business-list/business-details', vendor.id]">
                <i class="fa fa-eye font-medium-1 "></i>
                View
              </a>
            </td>
          </tr>
        </ng-template>
      </p-table>
    </div>

    <!--Active-->
    <div class="table-wrapper vehicle-wrapper" *ngIf="activeTab === 'active'">
      <p-table [value]="activeList" styleClass="country-list-wrapper" [responsive]="true" [paginator]="true" [rows]="10"
        sortField="id" [sortOrder]="-1">
        <ng-template pTemplate="header">
          <tr>
          <th [pSortableColumn]="'serial'">Serial #
          <p-sortIcon [field]="'serial'"></p-sortIcon>
        </th>
        <th>Business Type</th>
        <th>Business Name</th>
        <th>Email Id</th>
        <th>Contact No.</th>
        <th>Location</th>
        <th>Status</th>
        <th>Action</th>
          </tr>
        </ng-template>
        <ng-template pTemplate="body" let-active>
          <tr>
            <td><span class="ui-column-title">Serial</span>{{active.serial}}</td>
            <td><span class="ui-column-title">Business Type</span>{{active.vendorType}}</td>
            <td><span class="ui-column-title">Business Name</span>{{active.vendorName}}</td>
            <td><span class="ui-column-title">Email Id</span>{{active.emailid}}</td>
            <td><span class="ui-column-title">Contact No.</span>{{active.contact}}</td>
            <td><span class="ui-column-title">Location</span>{{active.location}}</td>
            <td>
              <span class="ui-column-title">Status</span>
              {{active.status === "true" ? "Inactive" : "Active"}}
            </td>
            <td>
              <span class="ui-column-title">Action</span>
              <a class="success p-0" [routerLink]="['../business-list/business-details', active.id]">
                <i class="fa fa-eye font-medium-1 "></i>
                View
              </a>
            </td>
          </tr>
        </ng-template>
      </p-table>
    </div>
    <!--Rejected-->
    <div class="table-wrapper vehicle-wrapper" *ngIf="activeTab === 'rejected'">
      <p-table [value]="rejectedList" styleClass="country-list-wrapper" [responsive]="true" [paginator]="true" [rows]="2"
        sortField="id" [sortOrder]="-1">
        <ng-template pTemplate="header">
          <tr>

          <th [pSortableColumn]="'serial'">Serial #
          <p-sortIcon [field]="'serial'"></p-sortIcon>
        </th>
        <th>Business Type</th>
        <th>Business Name</th>
        <th>Email Id</th>
        <th>Contact No.</th>
        <th>Location</th>
        <th>Status</th>
        <th>Action</th>
          </tr>
        </ng-template>
        <ng-template pTemplate="body" let-rejected>
          <tr>
            <td><span class="ui-column-title">Serial</span>{{rejected.serial}}</td>
            <td><span class="ui-column-title">Business Type</span>{{rejected.vendorType}}</td>
            <td><span class="ui-column-title">Business Name</span>{{rejected.vendorName}}</td>
            <td><span class="ui-column-title">Email Id</span>{{rejected.emailid}}</td>
            <td><span class="ui-column-title">Contact No.</span>{{rejected.contact}}</td>
            <td><span class="ui-column-title">Location</span>{{rejected.location}}</td>
            <td>
              <span class="ui-column-title">Status</span>
              {{rejected.status === "true" ? "Inactive" : "Active"}}
            </td>
            <td>
              <span class="ui-column-title">Action</span>
              <a class="success p-0" [routerLink]="['../business-list/business-details', rejected.id]">
                <i class="fa fa-eye font-medium-1 "></i>
                View
              </a>
            </td>
          </tr>
        </ng-template>
      </p-table>
    </div>
    `,
  styles: [
    `.table-wrapper {
            margin-top:30px;
        }
         ::ng-deep .country-list-wrapper .ui-table-thead tr {
            border-top: 2px solid #D9E0EA;
            border-bottom: 2px solid #D9E0EA;
          }
           ::ng-deep .country-list-wrapper .ui-table-thead tr th {
            background: #F5F7FA !important;
            border: none !important;
            color: #4A4A4A !important;
          }
           ::ng-deep .country-list-wrapper .ui-table-tbody tr {
            border-top: 2px solid #D9E0EA;
            border-bottom: 2px solid #D9E0EA;
          }
          ::ng-deep .country-list-wrapper .ui-table-tbody tr:nth-child(odd) td {
            background: #E7EDF6 !important;
            border: none !important;
          }
         ::ng-deep .country-list-wrapper .ui-table-tbody tr td {
            background: #F5F7FA !important;
            border: none !important;
            color: #8F8F8F;
            font-weight: 500;
          }

          ::ng-deep .table-wrapper .ui-table .ui-sortable-column.ui-state-highlight .ui-sortable-column-icon {
            color: #848484;
          }

          ::ng-deep .table-wrapper .ui-paginator .ui-paginator-pages .ui-paginator-page.ui-state-active {
            background-color: #E9BA00;
            color: #ffffff;
        }
        /* generic tabs styling */

    ::ng-deep .prime-tabs-wrapper .ui-tabmenuitem .ui-menuitem-link .ui-menuitem-text {
        color: #8F8F8F;
        font-size: 16px;
    }

::ng-deep .prime-tabs-wrapper .ui-tabmenuitem {
  border: 1px solid #f5f7fa !important;
  background-color: #f5f7fa !important;
  border-bottom: 0px solid #D9E0EA !important;
}

::ng-deep .prime-tabs-wrapper .ui-tabmenuitem.ui-state-active {
  background-color: #f5f7fa !important;
  border: 2px solid #D9E0EA !important;
  border-bottom: 0px solid !important;
}

::ng-deep .prime-tabs-wrapper .ui-tabmenuitem.ui-state-active .ui-menuitem-link .ui-menuitem-icon {
  color: #3B4693 !important;
  font-weight: 500 !important;
  font-size:16px;
}

::ng-deep .prime-tabs-wrapper .ui-tabmenuitem.ui-state-active .ui-menuitem-link .ui-menuitem-text {
  color: #3B4693 !important;
  font-weight: 500 !important;
  font-size:16px;
}

::ng-deep .vehicle-wrapper {
  margin-top:0 !important;
}


::ng-deep .vehicle-wrapper .country-list-wrapper .ui-table-thead tr {
  border-top: 0px solid #D9E0EA !important;
}

 ::ng-deep .vehicle-wrapper .ui-sortable-column.ui-state-highlight .ui-sortable-column-icon {
  color: #8F8F8F !important;
 }

 ::ng-deep .vehicle-wrapper .ui-sortable-column:not(.ui-state-highlight):hover .ui-sortable-column-icon {
  color: #8F8F8F !important;
 }

 ::ng-deep .vehicle-wrapper .ui-paginator .ui-paginator-pages .ui-paginator-page.ui-state-active {
  background-color: #3B4693;
  color: #ffffff;
  border-radius: 4px;
 }

 ::ng-deep .vehicle-wrapper .ui-paginator {
  background-color: #f5f7fa;
   border:none;
    margin-top: 24px;
    text-align: right;
 }
 `
  ]
})
export class VendorTabsTableWrapperComponent implements OnInit {
  activeTab: string;
  items: MenuItem[];
  activeItem: MenuItem;
  vendorList: any[];
  activeList: any[];
  rejectedList: any[];
  sectionTitle: String;
  constructor() { }
  getItem(event, index) {
    event.preventDefault();
    if (index === 0) {
      this.activeTab = 'all';
    } else if (index === 1) {
      this.activeTab = 'active';
    } else if (index === 2) {
      this.activeTab = 'rejected';
    }
  }
  ngOnInit() {

    this.activeTab = 'all';

    this.items = [
      {
        label: 'All Vendors'
      },
      {
        label: 'Active'
      },
      {
        label: 'Rejected'
      },
    ];
    this.vendorList = [
      {
        id: 1,
        serial: '1024',
        vendorType: 'ecommerce',
        vendorName: 'bjorn',
        emailid: 'bjorn.mcmahon@g...',
        contact: '9876543210',
        location: 'Hatta',
        status: true,
      },
      {
        id: 2,
        serial: '1025',
        vendorType: 'ecommerce',
        vendorName: 'bjorn',
        emailid: 'bjorn.mcmahon@g...',
        contact: '9876543210',
        location: 'Hatta',
        status: true,
      },
      {
        id: 3,
        serial: '1026',
        vendorType: 'ecommerce',
        vendorName: 'bjorn',
        emailid: 'bjorn.mcmahon@g...',
        contact: '9876543210',
        location: 'Hatta',
        status: true,
      }
    ];
    this.activeList = [
      {
        id: 1,
        serial: '1026',
        vendorType: 'ecommerce',
        vendorName: 'bjorn',
        emailid: 'bjorn.mcmahon@g...',
        contact: '9876543210',
        location: 'Hatta',
        status: true,
      },
      {
        id: 2,
        serial: '1026',
        vendorType: 'ecommerce',
        vendorName: 'bjorn',
        emailid: 'bjorn.mcmahon@g...',
        contact: '9876543210',
        location: 'Hatta',
        status: true,
      },
      {
        id: 3,
        serial: '1026',
        vendorType: 'ecommerce',
        vendorName: 'bjorn',
        emailid: 'bjorn.mcmahon@g...',
        contact: '9876543210',
        location: 'Hatta',
        status: true,
      }
    ];
    this.rejectedList = [
      {
        id: 1,
        serial: '1026',
        vendorType: 'ecommerce',
        vendorName: 'bjorn',
        emailid: 'bjorn.mcmahon@g...',
        contact: '9876543210',
        location: 'Hatta',
        status: true,
      },
      {
        id: 2,
        serial: '1026',
        vendorType: 'ecommerce',
        vendorName: 'bjorn',
        emailid: 'bjorn.mcmahon@g...',
        contact: '9876543210',
        location: 'Hatta',
        status: true,
      },
      {
        id: 3,
        serial: '1026',
        vendorType: 'ecommerce',
        vendorName: 'bjorn',
        emailid: 'bjorn.mcmahon@g...',
        contact: '9876543210',
        location: 'Hatta',
        status: true,
      }
    ];
    this.activeItem = this.items[0];
  }
}