import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ConfirmationService } from 'primeng/api';

@Component({
  selector: 'app-active-switch',
  template: `
    <h3 class="country-card-text">
        <p-inputSwitch (onChange)="handleChange($event)" [(ngModel)]="checked" styleClass="prime-switch-override">
        </p-inputSwitch>
        {{val}}
    </h3>
  `,
  styles: [`
  .country-card-text {
    font-size: 14px;
    color: #8f8f8f;
    font-weight: 500;
    margin-bottom: 0;
    padding-bottom:12px;
    text-transform: capitalize;
  }
  `]
})
export class ActiveSwitchComponent implements OnInit {
  // checked: string = 'active';
  @Input() checked: boolean;
  @Input() possibleValue: string[];

  @Output() change: EventEmitter<boolean> = new EventEmitter<boolean>();

  val: string = 'Available';
  constructor(private confirmationService: ConfirmationService) { }
  ngOnInit() {

    if (this.possibleValue)
      this.val = this.checked ? this.possibleValue[0] : this.possibleValue[1];
  }
  handleChange(e) {
    this.confirmationService.confirm({
      message: 'Are you sure that you want to proceed?',
      header: 'Confirmation',
      icon: 'pi pi-exclamation-triangle',
      reject: () => {
        // this.val = this.val;
        this.checked = !this.checked;
      },
      accept: () => {
        if (this.checked) {
          this.val = (!this.possibleValue[0]) ? 'Available' : this.possibleValue[0];
        } else if (!this.checked) {
          this.val = (!this.possibleValue[1]) ? 'Unavailable' : this.possibleValue[1];
        }

        this.change.emit(this.checked);
      }
    });

  }


}
