import { Component, OnInit, Input } from '@angular/core';
import { MenuItem } from 'primeng/components/common/api';
@Component({
  selector: 'vehicle-tabs-table-wrapper',
  template: `
    <!-- tabs wrapper -->
    <div class="tabs-wrapper">
      <p-tabMenu [model]="items" [activeItem]="activeItem" styleClass="prime-tabs-wrapper">
        <ng-template pTemplate="item" let-item let-i="index">
          <div (click)="getItem($event, i, item.label)">
            <div class="ui-menuitem-icon" [ngClass]="item.icon" *ngIf="item.icon" style="font-size: 2em"></div>
            <div class="ui-menuitem-text">
              {{item.label}}
            </div>
          </div>
        </ng-template>
      </p-tabMenu>
    </div>
  
    <!--All Vehicles-->
    <div class="table-wrapper vehicle-wrapper" *ngIf="activeTab === 'all'">
      <p-table [value]="vehicleList" styleClass="country-list-wrapper" [responsive]="true" [paginator]="true" [rows]="10"
        sortField="id" [sortOrder]="-1">
        <ng-template pTemplate="header">
          <tr>
            <th [pSortableColumn]="'id'">Id
              <p-sortIcon [field]="'id'"></p-sortIcon>
            </th>
            <th>Brand</th>
            <th>Model</th>
            <th>Location</th>
            <th>Price</th>
            <th>Status</th>
            <th>Action</th>
          </tr>
        </ng-template>
        <ng-template pTemplate="body" let-vehicle>
          <tr [ngClass] = "(isChecked == true)? 'unchecked': 'checked'">
            <td><span class="ui-column-title">Id</span>{{vehicle.id}}</td>
            <td><span class="ui-column-title">Brand</span>{{vehicle.brand}}</td>
            <td><span class="ui-column-title">Model</span>{{vehicle.model}}</td>
            <td><span class="ui-column-title">Location</span>{{vehicle.location}}</td>
            <td><span class="ui-column-title">Price</span>{{vehicle.price}}</td>
            <td>
              <span class="ui-column-title">Status</span>
              <app-active-switch></app-active-switch>
            </td>
            <td>
              <span class="ui-column-title">Action</span>
              <a class="success p-0 action-link" [routerLink]="['../vehicle-details', vehicle.id]">
                <i class="fa fa-eye font-medium-1 "></i>
                View
              </a>
            </td>
          </tr>
        </ng-template>
      </p-table>
    </div>
  
    <!--On Lease-->
    <div class="table-wrapper vehicle-wrapper" *ngIf="activeTab === 'lease'">
      <p-table [value]="leaseList" styleClass="country-list-wrapper" [responsive]="true" [paginator]="true" [rows]="10"
        sortField="id" [sortOrder]="-1">
        <ng-template pTemplate="header">
          <tr>
            <th [pSortableColumn]="'id'">Id
              <p-sortIcon [field]="'id'"></p-sortIcon>
            </th>
            <th>Brand</th>
            <th>Model</th>
            <th>Location</th>
            <th>Price</th>
            <th>Status</th>
            <th>Action</th>
          </tr>
        </ng-template>
        <ng-template pTemplate="body" let-lease>
          <tr>
            <td><span class="ui-column-title">Id</span>{{lease.id}}</td>
            <td><span class="ui-column-title">Brand</span>{{lease.brand}}</td>
            <td><span class="ui-column-title">Model</span>{{lease.model}}</td>
            <td><span class="ui-column-title">Location</span>{{lease.location}}</td>
            <td><span class="ui-column-title">Price</span>{{lease.price}}</td>
            <td>
              <span class="ui-column-title">Status</span>
              {{lease.status === "true" ? "Inactive" : "Active"}}
            </td>
            <td>
              <span class="ui-column-title">Action</span>
              <a class="success p-0" [routerLink]="['../vehicle-details', lease.id]">
                <i class="fa fa-eye font-medium-1 "></i>
                View
              </a>
            </td>
          </tr>
        </ng-template>
      </p-table>
    </div>
    <!--Available-->
    <div class="table-wrapper vehicle-wrapper" *ngIf="activeTab === 'available'">
      <p-table [value]="availableList" styleClass="country-list-wrapper" [responsive]="true" [paginator]="true" [rows]="2"
        sortField="id" [sortOrder]="-1">
        <ng-template pTemplate="header">
          <tr>
  
            <th [pSortableColumn]="'id'">Id
              <p-sortIcon [field]="'id'"></p-sortIcon>
            </th>
            <th>Brand</th>
            <th>Model</th>
            <th>Location</th>
            <th>Price</th>
            <th>Status</th>
            <th>Action</th>
          </tr>
        </ng-template>
        <ng-template pTemplate="body" let-available>
          <tr>
            <td><span class="ui-column-title">Id</span>{{available.id}}</td>
            <td><span class="ui-column-title">Brand</span>{{available.brand}}</td>
            <td><span class="ui-column-title">Model</span>{{available.model}}</td>
            <td><span class="ui-column-title">Location</span>{{available.location}}</td>
            <td><span class="ui-column-title">Price</span>{{available.price}}</td>
            <td>
              <span class="ui-column-title">Status</span>
              {{available.status === "true" ? "Inactive" : "Active"}}
            </td>
            <td>
              <span class="ui-column-title">Action</span>
              <a class="success p-0" [routerLink]="['../vehicle-details', available.id]">
                <i class="fa fa-eye font-medium-1 "></i>
                View
              </a>
            </td>
          </tr>
        </ng-template>
      </p-table>
    </div>
    `,
  styles: [
    `.table-wrapper {
            margin-top:30px;
        }
         ::ng-deep .country-list-wrapper .ui-table-thead tr {
            border-top: 2px solid #D9E0EA;
            border-bottom: 2px solid #D9E0EA;
          }
           ::ng-deep .country-list-wrapper .ui-table-thead tr th {
            background: #F5F7FA !important;
            border: none !important;
            color: #4A4A4A !important;
          }
           ::ng-deep .country-list-wrapper .ui-table-tbody tr {
            border-top: 2px solid #D9E0EA;
            border-bottom: 2px solid #D9E0EA;
          }
          ::ng-deep .country-list-wrapper .ui-table-tbody tr:nth-child(odd) td {
            background: #E7EDF6 !important;
            border: none !important;
          }
         ::ng-deep .country-list-wrapper .ui-table-tbody tr td {
            background: #F5F7FA !important;
            border: none !important;
            color: #8F8F8F;
            font-weight: 500;
          }
        
          ::ng-deep .table-wrapper .ui-table .ui-sortable-column.ui-state-highlight .ui-sortable-column-icon {
            color: #848484;
          }
        
          ::ng-deep .table-wrapper .ui-paginator .ui-paginator-pages .ui-paginator-page.ui-state-active {
            background-color: #E9BA00;
            color: #ffffff;
        }
        /* generic tabs styling */

    ::ng-deep .prime-tabs-wrapper .ui-tabmenuitem .ui-menuitem-link .ui-menuitem-text {
        color: #8F8F8F;
        font-size: 16px;
    }

::ng-deep .prime-tabs-wrapper .ui-tabmenuitem {
  border: 1px solid #f5f7fa !important;
  background-color: #f5f7fa !important;
  border-bottom: 0px solid #D9E0EA !important;
}

::ng-deep .prime-tabs-wrapper .ui-tabmenuitem.ui-state-active {
  background-color: #f5f7fa !important;
  border: 2px solid #D9E0EA !important;
  border-bottom: 0px solid !important;
}

::ng-deep .prime-tabs-wrapper .ui-tabmenuitem.ui-state-active .ui-menuitem-link .ui-menuitem-icon {
  color: #3B4693 !important;
  font-weight: 500 !important;
  font-size:16px;
}

::ng-deep .prime-tabs-wrapper .ui-tabmenuitem.ui-state-active .ui-menuitem-link .ui-menuitem-text {
  color: #3B4693 !important;
  font-weight: 500 !important;
  font-size:16px;
}

::ng-deep .vehicle-wrapper {
  margin-top:0 !important;
}


::ng-deep .vehicle-wrapper .country-list-wrapper .ui-table-thead tr {
  border-top: 0px solid #D9E0EA !important;
}

 ::ng-deep .vehicle-wrapper .ui-sortable-column.ui-state-highlight .ui-sortable-column-icon {
  color: #8F8F8F !important;
 }

 ::ng-deep .vehicle-wrapper .ui-sortable-column:not(.ui-state-highlight):hover .ui-sortable-column-icon {
  color: #8F8F8F !important;
 }

 ::ng-deep .vehicle-wrapper .ui-paginator .ui-paginator-pages .ui-paginator-page.ui-state-active {
  background-color: #3B4693;
  color: #ffffff;
  border-radius: 4px;
 }

 ::ng-deep .vehicle-wrapper .ui-paginator {
  background-color: #f5f7fa;
   border:none;
    margin-top: 24px;
    text-align: right;
 }
 `
  ]
})
export class VehicleTabsTableWrapperComponent implements OnInit {
  activeTab: string;
  items: MenuItem[];
  activeItem: MenuItem;
  vehicleList: any[];
  leaseList: any[];
  availableList: any[];
  sectionTitle: String;
  constructor() { }
  getItem(event, index) {
    event.preventDefault();
    if (index === 0) {
      this.activeTab = 'all';
    } else if (index === 1) {
      this.activeTab = 'lease';
    } else if (index === 2) {
      this.activeTab = 'available';
    }
  }
  ngOnInit() {

    this.activeTab = 'all';

    this.items = [
      {
        label: 'All Vehicles'
      },
      {
        label: 'On Lease'
      },
      {
        label: 'Available'
      },
    ];
    this.vehicleList = [
      {
        id: 1,
        brand: '1234',
        model: '2014',
        location: 'UAE',
        price: '2000',
        status: true,
      },
      {
        id: 2,
        brand: '1234',
        model: '2014',
        location: 'UAE',
        price: '2000',
        status: true,
      },
      {
        id: 3,
        brand: '1234',
        model: '2014',
        location: 'UAE',
        price: '2000',
        status: true,
      }
    ];
    this.leaseList = [
      {
        id: 1,
        brand: '1234',
        model: '2014',
        location: 'dubai',
        price: '2000',
        status: true,
      },
      {
        id: 2,
        brand: '1234',
        model: '2014',
        location: 'dubai',
        price: '2000',
        status: true,
      },
      {
        id: 3,
        brand: '1234',
        model: '2014',
        location: 'dubai',
        price: '2000',
        status: true,
      }
    ];
    this.availableList = [
      {
        id: 4,
        brand: '1234',
        model: '2014',
        location: 'dubai',
        price: '2000',
        status: true,
      },
      {
        id: 2,
        brand: '1234',
        model: '2014',
        location: 'dubai',
        price: '2000',
        status: true,
      },
      {
        id: 3,
        brand: '1234',
        model: '2014',
        location: 'dubai',
        price: '2000',
        status: true,
      }
    ];
    this.activeItem = this.items[0];
  }
}