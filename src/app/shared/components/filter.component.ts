import { Component, OnInit } from '@angular/core';
import { trigger, transition, style, animate } from '@angular/animations';

interface Brand {
    name: string;
    code: string;
}

interface Country {
    name: string;
    code: string;
}

interface City {
    name: string;
    code: string;
}

@Component({
    selector: 'app-filter-wrapper',
    template: `
  <div *ngIf="isVisible" class="filter-wrapper" [@openClose]>
    <div class="filter-flex-wrapper">
      <div>
        <h3 class="filter-title-text">Filter</h3>
      </div>
      <div (click)="closeFilter()">
        <i class="fa fa-times filter-close-icon"></i>
      </div>
    </div>
    <div>
      <div>
        <p class="filter-text">Brand</p>
        <p-dropdown [options]="brands" [(ngModel)]="selectedBrand" placeholder="Select brand name" optionLabel="name"
          [showClear]="true" styleClass="prime-dropdown-wrapper"></p-dropdown>
      </div>
      <div class="padding-top-24">
        <p class="filter-text">Model</p>
        <div class="filter-search-input">
          <input type="text" placeholder="Enter model name" />
        </div>
      </div>
      <div class="padding-top-24">
        <p class="filter-text">Country</p>
        <p-dropdown [options]="countries" [(ngModel)]="selectedCountry" placeholder="Select country" optionLabel="name"
          [showClear]="true" styleClass="prime-dropdown-wrapper"></p-dropdown>
      </div>
      <div class="padding-top-24">
        <p class="filter-text">City</p>
        <p-dropdown [options]="cities" [(ngModel)]="selectedCity" placeholder="Select city" optionLabel="name"
          [showClear]="true" styleClass="prime-dropdown-wrapper"></p-dropdown>
      </div>
      <div class="padding-top-24">
        <p class="filter-text">Status</p>
        <div class="prime-radio-wrapper">
          <p-radioButton name="group1" value="Active" label="Active" [(ngModel)]="selectedValue" inputId="opt1">
          </p-radioButton>
          <p-radioButton name="group1" value="Inactive" label="Inactive" [(ngModel)]="selectedValue" inputId="opt1"
            styleClass="inactive-radio">
          </p-radioButton>
        </div>
      </div>
      <div class="padding-top-24">
        <p class="filter-text">Price</p>
        <div>
          <input type="text" class="filter-small-input">
          and
          <input type="text" class="filter-small-input">
        </div>
      </div>
      <div class="full-button-wrapper margin-top-100">
        <a>Apply Filters</a>
      </div>

    </div>
  </div>
  `,
    styles: [
        ` 
 .filter-wrapper {
  position: fixed;
    /* bottom: -23%; */
    top:20%;
    right: 0;
    min-height: 731px;
    z-index: 1;
    border-radius: 5px;
    background-color: white;
    padding: 20px 15px;
    box-shadow: 2px 2px 9px rgba(0, 0, 0, 0.28);
 }

 .filter-wrapper .filter-flex-wrapper {
   display: flex;
   justify-content: space-between;
 }

 .filter-wrapper .filter-title-text {
  font-size: 25px;
  color: #4A4A4A;
  font-weight: 600;
  margin-bottom: 0;
  padding-bottom: 32px;
 }

 /* modal search input */
 .filter-search-input input[type=text], .form-search-input input[type=text]  {
  width: 100%;
  border: 1px solid #d8d8d8;
  padding: 5px 10px;
  color: #8f8f8f;
  border-radius: 3px;
}

.filter-text, .form-text {
  font-size: 14px;
  font-weight: 500;
  color: #4A4A4A;
  margin-bottom: 0;
}

.filter-close-icon {
  color: #8F8F8F;
  font-size: 18px;
  cursor: pointer;
}

.filter-small-input {
    width: 7em;
    border: 1px solid #d8d8d8;
    padding: 5px 10px;
    color: #8f8f8f;
    border-radius: 3px;
  }
  
  .full-button-wrapper {
    background: #e9ba00;
      padding: 10px 15px;
      min-width: 100%;
      text-align: center;
      border-radius: 3px;
      color: #fff;
      font-weight: 400;
      cursor: pointer;
  }
  
  .margin-top-100 {
    margin-top:100px;
  }
  
  .padding-top-24 {
    padding-top:24px;
  }
  
  
    `,
    ],
    animations: [
        trigger('openClose', [
            transition(':enter', [
                style({ transform: 'translateX(20%)', opacity: 0 }),
                animate('500ms', style({ transform: 'translateX(0)', opacity: 1 })),
            ]),
            transition(':leave', [
                style({ transform: 'translateX(0)', opacity: 1, display: 'none' }),
                animate('500ms', style({ transform: 'translateX(20%)', opacity: 0 })),
            ]),
        ])
    ],
})
export class FilterComponent implements OnInit {
    brands: Brand[];
    selectedBrand: Brand;
    countries: Country[];
    selectedCountry: Country;
    cities: City[];
    selectedCity: City;

    // show filter function
    isVisible: boolean = false;

    showFilter() {
        this.isVisible = !this.isVisible;

    }
    constructor() { }

    ngOnInit() {
        // brand dropdown list
        this.brands = [
            { name: 'New York', code: 'NY' },
            { name: 'Rome', code: 'RM' },
            { name: 'London', code: 'LDN' },
            { name: 'Istanbul', code: 'IST' },
            { name: 'Paris', code: 'PRS' }
        ];
        // Country dropdown list
        this.countries = [
            { name: 'New York', code: 'NY' },
            { name: 'Rome', code: 'RM' },
            { name: 'London', code: 'LDN' },
            { name: 'Istanbul', code: 'IST' },
            { name: 'Paris', code: 'PRS' }
        ];
        // City dropdown list
        this.cities = [
            { name: 'New York', code: 'NY' },
            { name: 'Rome', code: 'RM' },
            { name: 'London', code: 'LDN' },
            { name: 'Istanbul', code: 'IST' },
            { name: 'Paris', code: 'PRS' }
        ];
    }

}
