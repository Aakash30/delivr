import { Component, OnInit, EventEmitter, Output, Input, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-search',
  template: `
   <div class="search-input">
  <input type="text" [placeholder]="placeHolderText" value="" #searchInput (keypress)="searchText($event)" [(ngModel)]="searchValue" (keydown.backspace)="searchText($event)"   (blur)= "trimText(searchInput.value)">
    <ng-container *ngIf = "searchInput.value.length == 0">
      <div class="search-icon">
        <i class="card-icon fa fa-search"></i>
      </div>
    </ng-container>
    <ng-container *ngIf = "searchInput.value.length > 0">
      <div class="search-icon" (click) = "clearInput()">
        <i class="card-icon fa fa-close"></i>
      </div>
    </ng-container>
</div> 
  `,
  styles: [
    ` .search-input {
      position: relative;
      display: inline-block;
      input[type=text] {
          width: 16em;
          border: 1px solid #d8d8d8;
          padding: 8px 10px;
          border-radius: 3px;
        }
        .search-icon {
          position: absolute;
          top: 8px;
          right: 10px;
        }
        .search-icon svg {
          width: 20px;
          height: 20px;
        }
    }
    `,
  ],
})
export class SearchComponent implements OnInit {

  searchValue: string = '';
  clearString: string = '';
  searchTextFlag: boolean = false;
  @Output() public keyEvent = new EventEmitter();
  @Output() public clearEvent = new EventEmitter();
  @Output() public placeholderEvent = new EventEmitter();
  @Input() public placeHolderText: string = "Search";

  constructor() { }

  ngOnInit() {
  }

  searchText(event) {
    this.keyEvent.emit(event);
  }

  trimText(value) {
    this.searchValue = value.trim();
  }

  clearInput() {
    this.searchValue = null;
    this.clearEvent.emit(this.clearString);
  }
}
