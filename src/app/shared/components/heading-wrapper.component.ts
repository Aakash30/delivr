import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-heading-wrapper',
  template: `
    <div class="heading-wrapper">
        <h3>{{sectionTitle}}</h3>
    </div>
  `,
  styles: [`
  .heading-wrapper h3 {
    text-transform: uppercase;
    font-size: 28px;
    font-weight: 600;
    color: #4a4a4a;
  }
  `]
})
export class HeadingWrapperComponent implements OnInit {
  buttonTitle: string;
  @Input() sectionTitle: String;
  constructor(private route: ActivatedRoute, private router: Router) {
    this.sectionTitle = route.snapshot.data['sectionTitle'];
  }
  ngOnInit() {
  }

}
