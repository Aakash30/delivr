import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-button-wrapper',
  template: `
    <div class="button-wrapper">
      <a>{{buttonTitle}}</a>
    </div>
  `,
  styles: [`
  .button-wrapper {
    background: #e9ba00;
    padding: 10px 15px;
    min-width: 11em;
    text-align: center;
    border-radius: 3px;
    color: #fff;
    font-weight: 400;
    cursor: pointer;
  }
  `]
})
export class ButtonWrapperComponent implements OnInit {
  @Input() buttonTitle: string;
  constructor(
    private _route: ActivatedRoute
  ) {
    this.buttonTitle = this._route.snapshot.data['sectionTitle'];
  }

  ngOnInit() {
  }

}
