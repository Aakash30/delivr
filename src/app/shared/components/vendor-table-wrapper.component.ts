import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-vendor-table-wrapper',
  template: `
  <div class="table-wrapper">
    <p-table [value]="vendors" styleClass="country-list-wrapper" [responsive]="true" [paginator]="true" [rows]="10"
    sortField="city" [sortOrder]="1">
      <ng-template pTemplate="header">
        <tr>
         <th [pSortableColumn]="'serial'">Serial#
            <p-sortIcon [field]="'serial'"></p-sortIcon>
          </th>
          <th>Business Type</th>
          <th>Status</th>
          <th>Action</th>
        </tr>
      </ng-template>
      <ng-template pTemplate="body" let-vendor>
        <tr>
          <td><span class="ui-column-title">Serial</span>{{vendor.serial}}</td>
          <td><span class="ui-column-title">Business Type</span>{{vendor.vendorType}}</td>
          <td>
            <span class="ui-column-title">Status</span>
            {{vendor.status === "true" ? "Rejected" : "Active"}}
          </td>
          <td>
            <span class="ui-column-title">Action</span>
            <a class="success p-0" [routerLink]="['../business-type-details', vendor.id]">
              <i class="fa fa-eye font-medium-1 "></i>
              View
            </a>
          </td>
        </tr>
      </ng-template>
    </p-table>
  </div>
  `,
  styles: [`
  .table-wrapper {
    margin-top:30px;
}
 ::ng-deep .country-list-wrapper .ui-table-thead tr {
    border-top: 2px solid #D9E0EA;
    border-bottom: 2px solid #D9E0EA;
  }
   ::ng-deep .country-list-wrapper .ui-table-thead tr th {
    background: #F5F7FA !important;
    border: none !important;
    color: #4A4A4A !important;
  }
   ::ng-deep .country-list-wrapper .ui-table-tbody tr {
    border-top: 2px solid #D9E0EA;
    border-bottom: 2px solid #D9E0EA;
  }
  ::ng-deep .country-list-wrapper .ui-table-tbody tr:nth-child(odd) td {
    background: #E7EDF6 !important;
    border: none !important;
  }
 ::ng-deep .country-list-wrapper .ui-table-tbody tr td {
    background: #F5F7FA !important;
    border: none !important;
    color: #8F8F8F;
    font-weight: 500;
  }

  ::ng-deep .table-wrapper .ui-table .ui-sortable-column.ui-state-highlight .ui-sortable-column-icon {
    color: #848484;
  }

  ::ng-deep .table-wrapper .ui-paginator .ui-paginator-pages .ui-paginator-page.ui-state-active {
    background-color: #3B4693;
    color: #ffffff;
    border-radius: 4px;
   }
  
   ::ng-deep .table-wrapper .ui-paginator {
    background-color: #f5f7fa;
     border:none;
      margin-top: 24px;
      text-align: right;
   }

  `]
})
export class VendorTableWrapperComponent implements OnInit {
  vendors: any[];

  constructor() { }

  ngOnInit() {
    this.vendors = [
      {
        id: 1,
        serial: '1234',
        vendorType: 'ecommerce',
        status: false
      },
      {
        id: 2,
        serial: '1236',
        vendorType: 'ecommerce',
        status: true
      },
      {
        id: 3,
        serial: '1238',
        vendorType: 'ecommerce',
        status: true
      }
    ]
  }

}
