import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BusinessService } from "app/core/services/business.service";
import { NgxUiLoaderService } from "ngx-ui-loader";
import { MessageService } from "primeng/api";
import { Location } from "@angular/common";
import { CalendarModule } from 'primeng/calendar';
import { load } from '@angular/core/src/render3';
import { UtilityService } from 'app/core/services/utility.service';
import * as moment from 'moment';
import { BranchService } from 'app/core/services/branch.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ErrorHandlerService } from 'app/core/services/error-handler.service';

@Component({
  selector: 'app-assign-bike',
  templateUrl: './assign-bike.component.html',
  styleUrls: ['./assign-bike.component.scss']
})
export class AssignBikeComponent implements OnInit, OnDestroy {

  bikeList: any[];
  sectionTitle: string;
  buttonName: string;
  id: number;
  currentUrl: any;
  startDate: Date;
  minDate: Date;
  endDate: Date;
  maxDate: Date;
  installment_type: number;
  installments: any[];
  total: number = 0;
  paymentPayable = "";
  duration: string;
  keyword: string = "";
  batchId;

  perDayOrMonth = "Per day";
  flagForBusiness: boolean = true;

  now = moment();
  private _unsubscribe = new Subject<Boolean>();

  selectedValue: [{
    vehicle_id: number;
    price: string;
    vehicle_number: string;
    engine_number: string;
    brand_name: string;
    model_name: string;
    currency: string;
    lease_price: string
    isBatchVehicle: boolean
  }];

  rowData: any = [];
  constructor(private route: ActivatedRoute, private router: Router, private businessService: BusinessService,
    private _branchService: BranchService,
    private loader: NgxUiLoaderService,
    private messageService: MessageService,
    public utilities: UtilityService,
    private location: Location,
    private errorHandling: ErrorHandlerService) {
    this.sectionTitle = route.snapshot.data['sectionTitle'];
    this.buttonName = route.snapshot.data['buttonName']
    this.id = utilities.base64Decode(route.snapshot.paramMap.get('id'));

    this.batchId = route.snapshot.paramMap.get('batch');
    this.currentUrl = router.url;

    this.installments = [
      { label: 'Monthly (default)', value: 1 },
      { label: 'Quartelry', value: 2 },
      { label: 'Half-Yearly', value: 3 }
    ];
  }

  ngOnInit() {

    const pageContent = this.route.snapshot.data.pageData.data;
    if (pageContent.currentBranch) {
      this.flagForBusiness = false;
      this.sectionTitle = this.sectionTitle + " to " + pageContent.currentBranch.address_branch_name;
    } else {
      this.installment_type = 1;
      this.sectionTitle = this.sectionTitle + " to " + pageContent.currentBusiness.business_name;
    }
    let selectedArray;
    if (this.batchId) {
      this.bikeList = pageContent.vehicleList.map((element) => {
        if (element.isBatchVehicle) {
          if (selectedArray) {
            selectedArray.push(element);
          } else {
            selectedArray = [element];
          }
        }
        return element;
      });
      this.selectedValue = selectedArray;

    } else {
      this.bikeList = pageContent.vehicleList;

    }


  }

  resetform() {
    this.location.back();
  }

  setDuration(selectionType) {

    if (selectionType == 'startDate') {
      let selectedDate = this.startDate;
      let newDate = moment(selectedDate).add(1, "day");
      this.minDate = new Date(newDate.toDate());
    } else {
      let selectedDate = this.endDate;
      let newDate = moment(selectedDate).subtract(1, "day");
      this.maxDate = new Date(newDate.toDate());
    }
    if (this.startDate != null && this.endDate != null) {
      this.duration = this.utilities.dateDifference(this.startDate.toString(), this.endDate.toString());
      var end = moment(this.endDate, "YYYY-MM-DD");
      var start = moment(this.startDate, "YYYY-MM-DD");
      let days = end.diff(start, 'days');
      this.total = 0;
      this.perDayOrMonth = "Per day";
      let checkYear = this.dateDiff(this.startDate, this.endDate).years;
      if (checkYear > 0) {
        this.perDayOrMonth = "Per month";
        this.total = 0;
        if (this.selectedValue.length > 0) {
          this.selectedValue.forEach((dataVehicle) => {
            this.total += parseFloat(dataVehicle.lease_price);
          })
        }
        let calculateMonth = 12;
        if (this.dateDiff(this.startDate, this.endDate).months != 0) {
          calculateMonth = (calculateMonth * (checkYear)) + this.dateDiff(this.startDate, this.endDate).months;
        } else {
          calculateMonth = calculateMonth * checkYear;
        }
        this.paymentPayable = ((this.total) * calculateMonth).toFixed(2);
      } else {

        if (this.selectedValue.length > 0) {
          this.selectedValue.forEach((dataVehicle) => {
            this.total += parseFloat(dataVehicle.lease_price) / 30;
          })
        }
        this.paymentPayable = ((this.total) * days).toFixed(2);
      }
    }

  }

  searchText(event) {
    this.keyword = event.target.value;
    if (event.charCode == 13) {
      // on click of enter
      this.getVehicles();
    }

  }

  getVehicles() {
    let data: any = {};

    this.loader.start();
    if (this.keyword.trim() != "") {

      data["keyword"] = this.keyword.trim();
    }

    if (this.flagForBusiness) {
      if (this.batchId) {
        this.businessService.fetchbatchvehicles(this.route.snapshot.paramMap.get('id'), this.batchId, data).pipe(takeUntil(this._unsubscribe)).subscribe(
          (success: any) => {
            this.bikeList = success.data.vehicleList;
            this.loader.stop();
          },
          error => {
            this.errorHandling.routeAccordingToError(error);
            this.loader.stop();
          }
        );
      } else {
        this.businessService.fetchvehicles(this.route.snapshot.paramMap.get('id'), data).pipe(takeUntil(this._unsubscribe)).subscribe(
          (success: any) => {
            this.bikeList = success.data.vehicleList;

            this.loader.stop();
          },
          error => {
            this.errorHandling.routeAccordingToError(error);
            this.loader.stop();
          }
        );
      }

    } else {
      this._branchService.fetchvehiclesForAssignBike(data).pipe(takeUntil(this._unsubscribe)).subscribe(
        (success: any) => {
          this.bikeList = success.data.vehicle;

          this.loader.stop();
        },
        error => {
          this.errorHandling.routeAccordingToError(error);
          this.loader.stop();
        }
      );
    }

  }

  clearInput(event) {
    this.keyword = event;
    this.getVehicles();
  }


  onRowSelect(event) {
    // debugger;

    // this.total += parseFloat(event.data.price) / 30;

    // if (this.startDate != null && this.endDate != null) {
    //   var end = moment(this.endDate, "YYYY-MM-DD");
    //   var start = moment(this.startDate, "YYYY-MM-DD");
    //   let days = end.diff(start, 'days');
    //   this.paymentPayable = ((this.total) * days).toFixed(2);
    // }
  }

  onRowsSelectionChange(event) {
    this.total = 0;


    this.perDayOrMonth = "Per day";
    if (this.startDate != null && this.endDate != null) {
      var end = moment(this.endDate, "YYYY-MM-DD");
      var start = moment(this.startDate, "YYYY-MM-DD");
      let days = end.diff(start, 'days');

      let checkYear = this.dateDiff(this.startDate, this.endDate).years;
      if (checkYear > 0) {
        this.perDayOrMonth = "Per month";
        this.total = 0;
        if (event.length > 0) {
          event.forEach((dataVehicle) => {
            this.total += parseFloat(dataVehicle.lease_price);
          })
        }
        let calculateMonth = 12;
        if (this.dateDiff(this.startDate, this.endDate).months != 0) {
          calculateMonth = (calculateMonth * (checkYear)) + this.dateDiff(this.startDate, this.endDate).months;
        } else {
          calculateMonth = calculateMonth * checkYear;
        }
        this.paymentPayable = ((this.total) * calculateMonth).toFixed(2);
      } else {

        if (event.length > 0) {
          event.forEach((dataVehicle) => {
            this.total += parseFloat(dataVehicle.lease_price) / 30;
          })
        }

        this.paymentPayable = ((this.total) * days).toFixed(2);
      }

    }
  }

  onRowUnselect(event) {
    // this.total -= parseFloat(event.data.price) / 30;

    // if (this.startDate != null && this.endDate != null) {
    //   var end = moment(this.endDate, "YYYY-MM-DD");
    //   var start = moment(this.startDate, "YYYY-MM-DD");
    //   let days = end.diff(start, 'days');
    //   this.paymentPayable = ((this.total) * days).toFixed(2);
    // }
  }

  dateDiff(end, start) {
    var a = moment(start, "YYYY-MM-DD");
    var b = moment(end, "YYYY-MM-DD");

    var years = a.diff(b, 'year');
    b.add(years, 'years');

    var months = a.diff(b, 'months');
    b.add(months, 'months');

    var days = a.diff(b, 'days');

    return { years: years, months: months, days: days };
  }

  onClick(event) {
    let vehicles = [];

    if (this.selectedValue) {

      //this.loader.start();
      if (this.flagForBusiness) {

        if (!this.startDate || !this.endDate) {
          this.messageService.add({
            severity: "error",
            summary: 'Dates not selected',
            detail: 'Please choose dates'
          });
          return false;
        }

        var end = moment(this.endDate, "YYYY-MM-DD");
        var start = moment(this.startDate, "YYYY-MM-DD");

        let extraMonths, paymentOffset;

        let installments = Math.ceil(end.diff(start, 'months', true));
        let extraDays = this.dateDiff(start, end).days;

        if (this.installment_type == 1) {
          installments = Math.ceil(end.diff(start, 'months', true));
          paymentOffset = extraDays != 0 ? extraDays + " days" : "";
        }
        if (this.installment_type == 2) {
          installments = Math.ceil(end.diff(start, 'months', true) / 3);
          extraMonths = Math.floor(this.dateDiff(start, end).months % 3);
          paymentOffset = (extraMonths != 0 ? extraMonths + " months " : "") + (extraDays != 0 ? extraDays + " days" : "");
        }
        if (this.installment_type == 3) {
          installments = Math.ceil(end.diff(start, 'months', true) / 6);
          extraMonths = Math.floor(this.dateDiff(start, end).months % 6);
          paymentOffset = (extraMonths != 0 ? extraMonths + " months " : "") + (extraDays != 0 ? extraDays + " days" : "");
        }

        this.selectedValue.forEach(element => {

          vehicles.push(element.vehicle_id.toString())

        });

        let requestObject = {};
        if (this.batchId) {
          requestObject = {
            "startDate": this.startDate,
            "endDate": this.endDate,
            "installment_type": this.installment_type,
            "installment_count": installments,
            "totalPrice": this.paymentPayable,
            "payment_offset": paymentOffset,
            "vehicleList": vehicles,
            "batch_id": this.batchId
          }

          this.renewBatch(requestObject);
        } else {
          requestObject = {
            "startDate": this.startDate,
            "endDate": this.endDate,
            "installment_type": this.installment_type,
            "installment_count": installments,
            "totalPrice": this.paymentPayable,
            "payment_offset": paymentOffset,
            "vehicleList": vehicles
          }

          this.assignToBusiness(requestObject);
        }


      } else {

        this.selectedValue.forEach(element => {
          vehicles.push(element["map_id"]);

        });
        let requestObject = {
          "map_id": vehicles,
          "business_area_id": this.id
        }
        this.assignToBranch(requestObject);
        this.loader.stop();
      }

    }
    else {
      this.messageService.add({
        severity: "error",
        summary: "Error",
        detail: 'Please choose at least 1 vehicle'
      });
    }
  }

  /**
   * function is called to renew batch
   * @param requestObject - new batch option
   */
  renewBatch(requestObject) {
    this.businessService.renewBatch(this.utilities.base64Encode(this.id), requestObject).pipe(takeUntil(this._unsubscribe)).subscribe(
      (success: any) => {

        this.loader.stop();

        this.messageService.add({
          severity: "success",
          summary: "vehicle(s) assigned",
          detail: success.message
        });

        this.location.back();
      },
      error => {

        this.errorHandling.routeAccordingToError(error);
        this.loader.stop();
      }
    );
  }

  assignToBusiness(requestObject) {
    //------------------------------------------------Call API------------------------------------------------//
    this.businessService.assignbike(this.id, requestObject).pipe(takeUntil(this._unsubscribe)).subscribe(
      (success: any) => {

        this.loader.stop();

        this.messageService.add({
          severity: "success",
          summary: "vehicle(s) assigned",
          detail: success.message
        });

        this.location.back();
      },
      error => {
        this.errorHandling.routeAccordingToError(error);
        this.loader.stop();
      }
    );
  }

  assignToBranch(requestObject) {
    //------------------------------------------------Call API------------------------------------------------//
    this._branchService.assignBikesToBranch(requestObject).pipe(takeUntil(this._unsubscribe)).subscribe((success: any) => {
      this.loader.stop();

      this.messageService.add({
        severity: "success",
        summary: "Success",
        detail: success.message
      });

      this.location.back();
    },
      error => {
        this.errorHandling.routeAccordingToError(error);
        this.loader.stop();
      })
  }

  ngOnDestroy() {
    this._unsubscribe.next(true);
    this._unsubscribe.complete();
  }
}
