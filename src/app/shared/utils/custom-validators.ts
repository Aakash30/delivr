import {
  FormControl,
  FormGroup,
  FormArray,
  AbstractControl,
} from "@angular/forms";

export function noWhitespaceValidator(control: FormControl) {
  const isWhitespace = (control.value || "").trim().length === 0;
  const isValid = !isWhitespace;
  return isValid ? null : { whitespace: true };
}

export function passwordMatchValidator(control: FormGroup) {
  const password = control.get("password");
  const confirmPassword = control.get("confirmPassword");
  return password && confirmPassword && password.value == confirmPassword.value
    ? true
    : false;
}

// export function priceCompareValidator(control: FormControl) {
//   let price = control.root['controls.price;
//   let lease_price = control.get("lease_price");
//   console.log(price.value);
//   console.log(lease_price.value);
//   console.log(
//     price && lease_price && parseInt(price.value) < parseInt(lease_price.value)
//       ? { pricevalid: true }
//       : null
//   );
//   if (
//     price.value != "" &&
//     lease_price.value != "" &&
//     parseInt(price.value) < parseInt(lease_price.value)
//   ) {
//     return { pricevalid: true };
//   } else {
//     return null;
//   }
// }

export function priceCompareValidor(control: FormGroup) {
  let price = control.get("price");
  let lease_price = control.get("lease_price");
  console.log(price.value);
  console.log(lease_price.value);
  console.log(
    price && lease_price && parseInt(price.value) < parseInt(lease_price.value)
      ? { pricevalid: true }
      : null
  );
  if (
    price.value != "" &&
    lease_price.value != "" &&
    parseInt(price.value) < parseInt(lease_price.value)
  ) {
    lease_price.setErrors({ pricevalid: true });
    return { pricevalid: true };
  } else {
    return null;
  }
}

export function openLinkInNewTab(link: string) {
  let url = "";
  if (!/^http[s]?:\/\//.test(link)) {
    url += "http://";
  }
  url += link;
  window.open(url, "_blank");
}

export function validateAllFormFields(formGroup: FormGroup | FormArray) {
  Object.keys(formGroup.controls).forEach((field) => {
    const control = formGroup.get(field);
    if (control instanceof FormControl) {
      control.markAsTouched({ onlySelf: true });
    } else if (control instanceof FormGroup || control instanceof FormArray) {
      validateAllFormFields(control);
    }
  });
}

export function blankSpaceInputNotValid(control: AbstractControl) {
  if (control && control.value && !control.value.replace(/\s/g, "").length) {
    control.setValue("");
  }
  return null;
}

export function priceValidator(control: AbstractControl) {
  if (control && control.value && !control.value.replace(/\s/g, "").length) {
    control.setValue("");
  }
  return null;
}

export function numeric(control: AbstractControl) {
  let val = control.value;

  if (val === null || val === "") return null;

  if (!val.toString().match(/^[0-9]+(\.?[0-9]+)?$/))
    return { invalidNumber: true };

  return null;
}
