import { RouteInfo } from './sidebar.metadata';
import { RolesGuardService } from 'app/core/guards/roles-guard.service';

//Sidebar menu Routes and data
export const ROUTES: RouteInfo[] = [
    {
        path: '/admin/dashboard', title: 'Dashboard', icon: 'ft-home', class: '', badge: '', user_type: 'admin', role: 'dashboard', badgeClass: 'badge badge-pill badge-danger float-right mr-1 mt-1', isExternalLink: false, submenu: [
        ]
    },
    // {
    //     path: '/admin/country/dashboard', title: 'Country', icon: 'ft-map-pin', class: '', badge: '', user_type: 'admin', role: 'location-setting', badgeClass: 'badge badge-pill badge-danger float-right mr-1 mt-1', isExternalLink: false, submenu: [
    //     ]
    // },
    {
        path: '/admin/', title: 'Vehicle', icon: 'ft-life-buoy', class: 'has-sub', badge: '', user_type: 'admin', role: '', badgeClass: 'badge badge-pill badge-danger float-right mr-1 mt-1', isExternalLink: false,
        submenu: [
            {
                path: '/admin/vehicles-list', title: 'Vehicle List', icon: '', class: '', badge: '', user_type: 'admin', role: 'vehicle-management', badgeClass: 'badge badge-pill badge-danger float-right mr-1 mt-1', isExternalLink: false, submenu: [
                ]
            },
            {
                path: '/admin/vehicles-inventory', title: 'Inventory', icon: '', class: '', badge: '', user_type: 'admin', role: 'vehicle-inventory', badgeClass: 'badge badge-pill badge-danger float-right mr-1 mt-1', isExternalLink: false, submenu: [
                ]
            }
        ]
    },
    {
        path: '/admin/', title: 'Business', icon: 'ft-user', class: 'has-sub', badge: '', user_type: 'admin', role: '', badgeClass: 'badge badge-pill badge-danger float-right mr-1 mt-1', isExternalLink: false,
        submenu: [
            {
                path: '/admin/business-type', title: 'Business Type', icon: '', class: '', badge: '', user_type: 'admin', role: 'business-category', badgeClass: 'badge badge-pill badge-danger float-right mr-1 mt-1', isExternalLink: false, submenu: [
                ]
            },
            {
                path: '/admin/business-list', title: 'Business list', icon: '', class: '', badge: '', user_type: 'admin', role: 'business-management', badgeClass: 'badge badge-pill badge-danger float-right mr-1 mt-1', isExternalLink: false, submenu: [
                ]
            },
        ]
    },
    {
        path: '/admin/', title: 'System Settings', icon: 'ft-settings', class: 'has-sub', badge: '', user_type: 'admin', role: '', badgeClass: 'badge badge-pill badge-danger float-right mr-1 mt-1', isExternalLink: false,
        submenu: [
            {
                path: '/admin/country', title: 'Country', icon: '', class: '', badge: '', user_type: 'admin', role: 'location-setting', badgeClass: 'badge badge-pill badge-danger float-right mr-1 mt-1', isExternalLink: false, submenu: [
                ]
            },
            {
                path: '/admin/brand', title: 'Vehicle Brand', icon: '', class: '', badge: '', user_type: 'admin', role: 'service-setting', badgeClass: 'badge badge-pill badge-danger float-right mr-1 mt-1', isExternalLink: false, submenu: [
                ]
            },
            {
                path: '/admin/service-category', title: 'Service Category', icon: '', class: '', badge: '', user_type: 'admin', role: 'service-setting', badgeClass: 'badge badge-pill badge-danger float-right mr-1 mt-1', isExternalLink: false, submenu: [
                ]
            },
            {
                path: '/admin/service-list', title: 'Services', icon: '', class: '', badge: '', user_type: 'admin', role: 'service-setting', badgeClass: 'badge badge-pill badge-danger float-right mr-1 mt-1', isExternalLink: false, submenu: [
                ]
            },
        ]
    },
    {
        path: '/admin/maintenance/list', title: 'Maintenance', icon: 'ft-sliders', class: '', badge: '', user_type: 'admin', role: 'maintenance', badgeClass: 'badge badge-pill badge-danger float-right mr-1 mt-1', isExternalLink: false, submenu: [
        ]
    },
    {
        path: '/admin/payment', title: 'Payment', icon: 'ft-credit-card', class: '', badge: '', user_type: 'admin', role: 'payments', badgeClass: 'badge badge-pill badge-danger float-right mr-1 mt-1', isExternalLink: false, submenu: [
        ]
    },
    {
        path: '/admin/coupon', title: 'Coupon', icon: 'ft-server', class: '', badge: '', user_type: 'admin', role: 'view-coupons', badgeClass: 'badge badge-pill badge-danger float-right mr-1 mt-1', isExternalLink: false, submenu: [
        ]
    },
    {
        path: '/admin/request-for-repair', title: 'Request For Repair', icon: 'ft-sliders', class: '', badge: '', user_type: 'admin', role: 'repair-request-management', badgeClass: 'badge badge-pill badge-danger float-right mr-1 mt-1', isExternalLink: false, submenu: [
        ]
    },
    {
        path: '/admin/request-for-new-vehicle', title: 'Request For New Vehicle', icon: 'ft-sliders', class: '', badge: '', user_type: 'admin', role: 'vehicle-request-management', badgeClass: 'badge badge-pill badge-danger float-right mr-1 mt-1', isExternalLink: false, submenu: [
        ]
    },
    {
        path: '/admin/request-for-module', title: 'Request For Module', icon: 'ft-box', class: '', badge: '', user_type: 'admin', role: 'module-request-management', badgeClass: 'badge badge-pill badge-danger float-right mr-1 mt-1', isExternalLink: false, submenu: [
        ]
    },
    {
        path: '/admin/report/list', title: 'Report', icon: 'ft-alert-octagon', class: '', badge: '', user_type: 'admin', role: 'reports-management', badgeClass: 'badge badge-pill badge-danger float-right mr-1 mt-1', isExternalLink: false, submenu: [
        ]
    },
    {
        path: '/business/dashboard', title: 'Dashboard', icon: 'ft-home', class: '', badge: '', user_type: 'business', role: 'dashboard', badgeClass: 'badge badge-pill badge-danger float-right mr-1 mt-1', isExternalLink: false, submenu: [
        ]
    },
    {
        path: '/business/order-management', title: 'Order Management', icon: 'ft-package', class: '', badge: '', user_type: 'business', role: 'order-management', badgeClass: 'badge badge-pill badge-danger float-right mr-1 mt-1', isExternalLink: false, submenu: [
        ]
    },
    {
        path: '/business/', title: 'Driver Management', icon: 'ft-life-buoy', class: 'has-sub', badge: '', user_type: 'business', role: '', badgeClass: 'badge badge-pill badge-danger float-right mr-1 mt-1', isExternalLink: false, submenu: [
            {
                path: '/business/driver-list', title: 'Driver List', icon: '', class: '', badge: '', user_type: 'business', role: 'driver-management', badgeClass: 'badge badge-pill badge-danger float-right mr-1 mt-1', isExternalLink: false, submenu: [
                ]
            },
            {
                path: '/business/driver-feedback', title: 'Driver Feedback', icon: '', class: '', badge: '', user_type: 'business', role: 'driver-feedback', badgeClass: 'badge badge-pill badge-danger float-right mr-1 mt-1', isExternalLink: false, submenu: [
                ]
            },
        ]
    },
    {
        path: '/business/', title: 'Vehicle Management', icon: 'ft-compass', class: 'has-sub', badge: '', user_type: 'business', role: '', badgeClass: 'badge badge-pill badge-danger float-right mr-1 mt-1', isExternalLink: false, submenu: [
            {
                path: '/business/vehicle-list', title: 'Vehicle List', icon: '', class: '', badge: '', user_type: 'business', role: 'vehicle-details', badgeClass: 'badge badge-pill badge-danger float-right mr-1 mt-1', isExternalLink: false, submenu: [
                ]
            },
            {
                path: '/business/vehicle-lease-plan', title: 'Lease Plan', icon: '', class: '', badge: '', user_type: 'business', role: 'vehicle-lease-plan', badgeClass: 'badge badge-pill badge-danger float-right mr-1 mt-1', isExternalLink: false, submenu: [
                ]
            },
        ]
    },
    {
        path: '/business/branch', title: 'Branch', icon: 'ft-codepen', class: '', badge: '', user_type: 'business', role: 'branch-management', badgeClass: 'badge badge-pill badge-danger float-right mr-1 mt-1', isExternalLink: false, submenu: [
        ]
    },
    {
        path: '/business/track-driver', title: 'Track Driver', icon: 'ft-map', class: '', badge: '', user_type: 'business', role: 'track-your-driver', badgeClass: 'badge badge-pill badge-danger float-right mr-1 mt-1', isExternalLink: false, submenu: [
        ]
    },
    {
        path: '/business/maintenance', title: 'Maintenance', icon: 'ft-settings', class: '', badge: '', user_type: 'business', role: 'maintenance', badgeClass: 'badge badge-pill badge-danger float-right mr-1 mt-1', isExternalLink: false, submenu: [
        ]
    },
    {
        path: '/business/request-for-vehicle', title: 'Request for New Vehicles', icon: 'ft-refresh-ccw', class: '', badge: '', user_type: 'business', role: 'raise-vehicle-request', badgeClass: 'badge badge-pill badge-danger float-right mr-1 mt-1', isExternalLink: false, submenu: [
        ]
    },
    {
        path: '/business/tickets', title: 'Tickets', icon: 'ft-server', class: '', badge: '', user_type: 'business', role: 'ticket-management', badgeClass: 'badge badge-pill badge-danger float-right mr-1 mt-1', isExternalLink: false, submenu: [
        ]
    },
    {
        path: '/business/accounts', title: 'Accounts', icon: 'ft-credit-card', class: '', badge: '', user_type: 'business', role: 'accounts-management', badgeClass: 'badge badge-pill badge-danger float-right mr-1 mt-1', isExternalLink: false, submenu: [
        ]
    },
    {
        path: '/business/coupon', title: 'Coupon', icon: 'ft-server', class: '', badge: '', user_type: 'business', role: 'coupon-management', badgeClass: 'badge badge-pill badge-danger float-right mr-1 mt-1', isExternalLink: false, submenu: [
        ]
    },
    {
        path: '/business/admin-list', title: 'Admin', icon: 'ft-user', class: '', badge: '', user_type: 'business', role: 'user-management', badgeClass: 'badge badge-pill badge-danger float-right mr-1 mt-1', isExternalLink: false, submenu: [
        ]
    },
    {
        path: '/business/request-for-repair', title: 'Request For Repair', icon: 'ft-sliders', class: '', badge: '', user_type: 'business', role: 'request-for-repair', badgeClass: 'badge badge-pill badge-danger float-right mr-1 mt-1', isExternalLink: false, submenu: [
        ]
    },
    // {
    //     path: '/business/notification/list', title: 'Notification', icon: 'ft-user', class: '', badge: '', user_type: 'business', role: 'notification', badgeClass: 'badge badge-pill badge-danger float-right mr-1 mt-1', isExternalLink: false, submenu: [
    //     ]
    // },
];
