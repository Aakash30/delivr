import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { ROUTES } from './sidebar-routes.config';
import { RouteInfo } from "./sidebar.metadata";
import { Router, ActivatedRoute } from "@angular/router";
import { TranslateService } from '@ngx-translate/core';
import { ThrowStmt } from '@angular/compiler';
import * as jwt_decode from 'jwt-decode';
import { isArray } from 'util';
import { of } from 'rxjs';

declare var $: any;

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  providers: []
})

export class SidebarComponent implements OnInit {

  public menuItems: any[] = [];
  public role: string;
  public acessRightName: string;
  userType: string;
  userRole: string;

  constructor(
    private router: Router,
    public translate: TranslateService,
    private _route: ActivatedRoute,
    private _location: Location
  ) {
  }

  ngOnInit() {
    $.getScript('./assets/js/app-sidebar.js');

    let token = localStorage.getItem("token").replace("Bearer ", "");

    var decoded = jwt_decode(token);

    var user_roles = JSON.parse(decoded.roles);
    var user_type = decoded.type;
    this.userType = user_type != 'super_admin' ? 'business' : 'admin';
    let subMenu = [];
    ROUTES.forEach(menuItem => {
      subMenu = [];
      if (menuItem.submenu.length > 0) {

        menuItem.submenu.forEach(subMenuItem => {
          if (menuItem.user_type == this.userType && user_roles.indexOf(subMenuItem.role) !== -1) {
            subMenu.push(subMenuItem);
          }
        })
        if (subMenu.length > 0) {

          menuItem.submenu = subMenu;
          this.menuItems.push(menuItem);
        }
        // if (isArray(menuItem.role)) {
        //     for (let i = 0; i < menuItem.role.length; i++) {
        //         if (menuItem.user_type == this.userType && user_roles.indexOf(menuItem.role[i]) !== -1) {
        //             this.menuItems.push(menuItem);
        //             break;
        //         }
        //     }
        // } else {
        //     if (menuItem.user_type == this.userType && user_roles.indexOf(menuItem.role) !== -1) {

        //         this.menuItems.push(menuItem);
        //     }
        // }

      } else {

        if (menuItem.user_type == this.userType && user_roles.indexOf(menuItem.role) !== -1) {
          this.menuItems.push(menuItem);
        }
      }


    });
  }

  //NGX Wizard - skip url change
  ngxWizardFunction(path: string) {
    if (path.indexOf('forms/ngx') !== -1)
      this.router.navigate(['forms/ngx/wizard'], { skipLocationChange: false });
  }


  // redirect to login page
  routeToLogin() {
    // this._location.reload();
    if (localStorage.getItem('token')) {
      if (this.userType === "admin") {
        this.router.navigateByUrl("/admin/dashboard");
      }
      if (this.userType === "business") {
        this.router.navigateByUrl("/business/dashboard");
      }
    } else {
      this.router.navigateByUrl(`${this.userRole}/login`);
    }
  }
}
