// Sidebar route metadata
export interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
    badge: string;
    user_type: string;
    role: any;
    badgeClass: string;
    isExternalLink: boolean;
    submenu: RouteInfo[];
}
