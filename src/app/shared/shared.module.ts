import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { FooterComponent } from './footer/footer.component';
import { NavbarComponent } from './navbar/navbar.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { ToggleFullscreenDirective } from './directives/toggle-fullscreen.directive';
import { InputSwitchModule } from 'primeng/inputswitch';
import { TableModule, EditableRow, EditableColumn } from 'primeng/table';
import { DialogModule } from 'primeng/dialog';
import { TabMenuModule } from 'primeng/tabmenu';
import { ButtonWrapperComponent } from './components/button-wrapper.component';
import { SearchComponent } from './components/search.component';
import { ActiveSwitchComponent } from './components/active-swtich.component';
import { TableWrapperComponent } from './components/table-wrapper.component';
import { VehicleTabsTableWrapperComponent } from './components/vehicle-tabs-table-wrapper.component';
import { VendorTabsTableWrapperComponent } from './components/vendor-tabs-table-wrapper.component';
import { FilterComponent } from './components/filter.component';
import { VendorTableWrapperComponent } from './components/vendor-table-wrapper.component';
import { HeadingWrapperComponent } from './components/heading-wrapper.component';
import { DropdownModule } from 'primeng/dropdown';
import { RadioButtonModule } from 'primeng/radiobutton';
import { FileUploadModule } from 'primeng/fileupload';
import { CheckboxModule } from 'primeng/checkbox';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { CalendarModule } from 'primeng/calendar';
import { BreadcrumbComponent } from './components/breadcrumb/breadcrumb.component';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ConfirmationService, MessageService } from 'primeng/api';
import { ToastModule } from 'primeng/toast';
import { UtilityService } from 'app/core/services/utility.service';
import { NgxUiLoaderModule } from 'ngx-ui-loader';
import { AuthService } from 'app/core/services/auth.service';
import { ErrorHandlerService } from 'app/core/services/error-handler.service';
import { BaseService } from 'app/core/services/base.service';
import { AssignBikeComponent } from './components/assign-bike/assign-bike.component';
import { PaginatorModule } from 'primeng/paginator';
import { VirtualScrollerModule } from 'primeng/virtualscroller';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { KeyFilterModule } from 'primeng/keyfilter';
import { DataViewModule } from 'primeng/dataview';
import { PanelModule } from 'primeng/panel';
import { SlickCarouselModule } from 'ngx-slick-carousel';
import { ToastrModule } from 'ngx-toastr';
import { SliderModule } from 'primeng/slider';
import { LightboxModule } from 'primeng/lightbox';

@NgModule({
    exports: [
        FormsModule,
        ReactiveFormsModule,
        CommonModule,
        FooterComponent,
        NavbarComponent,
        SidebarComponent,
        ToggleFullscreenDirective,
        NgbModule,
        TranslateModule,
        InputSwitchModule,
        TableModule,
        DialogModule,
        TabMenuModule,
        ButtonWrapperComponent,
        SearchComponent,
        VendorTableWrapperComponent,
        ActiveSwitchComponent,
        TableWrapperComponent,
        HeadingWrapperComponent,
        DropdownModule,
        RadioButtonModule,
        FileUploadModule,
        VehicleTabsTableWrapperComponent,
        FilterComponent,
        VendorTabsTableWrapperComponent,
        CheckboxModule,
        InputTextareaModule,
        CalendarModule,
        BreadcrumbComponent,
        ConfirmDialogModule,
        NgxUiLoaderModule,
        ToastModule,
        AssignBikeComponent,
        PaginatorModule,
        VirtualScrollerModule,
        AutoCompleteModule,
        DataViewModule,
        PanelModule,
        EditableRow,
        EditableColumn,
        KeyFilterModule,
        SlickCarouselModule,
        SliderModule,
        LightboxModule

    ],
    imports: [
        FormsModule,
        RouterModule,
        CommonModule,
        NgbModule,
        TranslateModule,
        InputSwitchModule,
        TableModule,
        DialogModule,
        TabMenuModule,
        DropdownModule,
        RadioButtonModule,
        FileUploadModule,
        CheckboxModule,
        InputTextareaModule,
        CalendarModule,
        ConfirmDialogModule,
        NgxUiLoaderModule,
        ToastModule,
        PaginatorModule,
        VirtualScrollerModule,
        AutoCompleteModule,
        DataViewModule,
        PanelModule,
        KeyFilterModule,
        SlickCarouselModule,
        SliderModule,
        LightboxModule,
        ToastrModule.forRoot()

    ],
    declarations: [
        FooterComponent,
        NavbarComponent,
        SidebarComponent,
        VendorTableWrapperComponent,
        AssignBikeComponent,
        ToggleFullscreenDirective,
        ButtonWrapperComponent,
        SearchComponent,
        ActiveSwitchComponent,
        TableWrapperComponent,
        HeadingWrapperComponent,
        VehicleTabsTableWrapperComponent,
        FilterComponent,
        VendorTabsTableWrapperComponent,
        BreadcrumbComponent,
    ]
})
export class SharedModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: SharedModule,
            providers: [
                ConfirmationService,
                UtilityService,
                MessageService,
                AuthService,
                ErrorHandlerService,
                BaseService,
                DatePipe
            ]
        };
    }
}
