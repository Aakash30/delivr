// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyC4N2N3fseCZ44buzH2W-TCAU_siPIzPvo",
    authDomain: "delivr-d6cdc.firebaseapp.com",
    databaseURL: "https://delivr-d6cdc.firebaseio.com/",
    projectId: "delivr-d6cdc",
    storageBucket: "delivr-d6cdc.appspot.com",
    messagingSenderId: "177114801075",
    appId: "1:177114801075:web:c08b38e2701acb42daa1c6"
  }
};
